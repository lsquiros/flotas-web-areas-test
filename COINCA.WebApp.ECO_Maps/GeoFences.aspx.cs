﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using AspMap;
using System.Globalization;
using ECO_Maps.Business.Intrack;
using AspMap.Web;

namespace ECO_Maps
{
    public partial class GeoFences : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();
        static string _lLicense = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_lLicense == null)
                _lLicense = System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"];

            AspMap.License.SetLicenseKey(_lLicense);
            string geoFence = Request.QueryString["GeoFence"];
            string countryCode = Request.QueryString["CountryCode"];
            string latDirection = Request.QueryString["latDirection"];
            string lonDirection = Request.QueryString["lonDirection"];
            string layer = Request.QueryString["Map"];
            customerId.Value = Request.QueryString["CustomerId"];
            Session["CountryCode"] = countryCode;
            if (geoFence == null) { geoFence = string.Empty; }
            if (countryCode == null) { countryCode = string.Empty; }
            if (latDirection == null) { latDirection = string.Empty; }
            if (lonDirection == null) { lonDirection = string.Empty; }
            if (layer == null) { layerMap.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"]; } else { layerMap.Value = layer; }
            LoadLabels();

            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                polygonToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                panTool.Visible = true;
                centerTool.Visible = true;
                polygonTool.Visible = true;
                pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                //GoogleMap.Visible = false;
                //CoincaMap.Visible = true;
                //GoogleMap.Visible = false;
                //ZoomInTool.Visible = false;
                //zoomOutTool.Visible = false;
                panTool.Visible = false;
                centerTool.Visible = false;
                polygonTool.Visible = false;
                //CoincaMap.Visible = true;
                //ZoomInToolGoogle.Visible = true;
                //zoomOutToolGoogle.Visible = true;
                panToolGoogle.Visible = true;
                centerToolGoogle.Visible = true;
                polygonToolGoogle.Visible = true;
                pnlSatellite.Visible = true;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                polygonToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                panTool.Visible = true;
                centerTool.Visible = true;
                polygonTool.Visible = true;
                pnlSatellite.Visible = false;
            }

            if (countryCode != string.Empty)
            {
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);

                if (Page.IsPostBack == false)
                {
                    if (geoFence == string.Empty)
                    {
                        if (layerMap.Value == "Coinca")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapCoinca.MapShapes.Clear();
                        }
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.MapShapes.Clear();
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapCoinca.MapShapes.Clear();
                        }
                    }

                    LoadProvincias();
                    LoadCantones();
                    if (geoFence.Contains("POLYGON"))
                    {
                        mapCoinca.Markers.Clear();
                        Polygon polygon = _mapsFunctions.GetPolygonFromString(geoFence, layerMap.Value);
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.MapShapes.Clear();
                            MapShape mapShape = mapCoinca.MapShapes.Add(polygon);
                            mapShape.Symbol.Size = 2;
                            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(polygon.Centroid.X, layerMap.Value), _mapsFunctions.GetY(polygon.Centroid.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 9);
                        }
                        else if (layerMap.Value == "GoogleRoad")
                        {

                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.MapShapes.Clear();
                            MapShape mapShape = mapGoogle.MapShapes.Add(polygon);
                            mapShape.Symbol.Size = 2;
                            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                            mapGoogle.CenterAndZoom(new AspMap.Point(polygon.Centroid.X, polygon.Centroid.Y), mapGoogle.ZoomLevels.Count - 9);
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            mapCoinca.MapShapes.Clear();
                            MapShape mapShape = mapCoinca.MapShapes.Add(polygon);
                            mapShape.Symbol.Size = 2;
                            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                            //mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(polygon.Centroid.X, layerMap.Value), _mapsFunctions.GetY(polygon.Centroid.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 9);
                            mapCoinca.CenterAndZoom(new AspMap.Point(polygon.Centroid.X, polygon.Centroid.Y), mapCoinca.ZoomLevels.Count - 6);
                        }
                    }
                    MapsBusiness businessMap = new MapsBusiness();
                    ECO_Maps.Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                    if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null && !geoFence.Contains("POLYGON"))
                    {
                        AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                        }
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                    }
                    if (latDirection != string.Empty && lonDirection != string.Empty && !geoFence.Contains("POLYGON"))
                    {
                        AspMap.Point punto = new AspMap.Point(Convert.ToDouble(lonDirection), Convert.ToDouble(latDirection));
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                        }
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapGoogle.ZoomLevels.Count - 2);
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            mapCoinca.Markers.Clear();
                            MarkerSymbol symbol = new MarkerSymbol("../Content/Images/Marker_1.png", 32, 32);
                            Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), symbol, "Búsqueda");
                            mapCoinca.Markers.Add(marker);

                            
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                        }
                    }
                }
            }

        }

        protected void LoadLabels()
        {
            Models.Maps info = _mapsFunctions.GetMapInfo(Convert.ToString(Session["CountryCode"]).Trim());
            if (info != null)
            {
                lblLevel1.Text = info.Level1 + ":";
                lblLevel2.Text = info.Level2 + ":";
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            if (directionsTable.Rows.Count == 0 && pointsSearchTable.Rows.Count == 0) { directionsTable.Visible = false; pnlResultButtons.Visible = false; }
            ScriptManager.RegisterStartupScript(this, GetType(), "fncMenuRefresh", "fncMenuRefresh();", true);
        }

        protected void map_PolygonToolCoinca(object sender, AspMap.Web.PolygonToolEventArgs e)
        {
            mapCoinca.MapShapes.Clear();
            MapShape mapShape = mapCoinca.MapShapes.Add(e.Polygon);
            mapShape.Symbol.Size = 2;
            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
            mapShape.Symbol.FillStyle = FillStyle.Invisible;
            ScriptManager.RegisterStartupScript(this, GetType(), "LoadGeoFenceValue", "LoadGeoFenceValue(\"" + _mapsFunctions.GetStringFromPolygon(e.Polygon, layerMap.Value) + "\");", true);
        }

        protected void map_PolygonToolGoogle(object sender, AspMap.Web.PolygonToolEventArgs e)
        {
            mapGoogle.MapShapes.Clear();
            MapShape mapShape = mapGoogle.MapShapes.Add(e.Polygon);
            mapShape.Symbol.Size = 2;
            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
            mapShape.Symbol.FillStyle = FillStyle.Invisible;
            ScriptManager.RegisterStartupScript(this, GetType(), "LoadGeoFenceValue", "LoadGeoFenceValue(\"" + _mapsFunctions.GetStringFromPolygon(e.Polygon, layerMap.Value) + "\");", true);
        }

        protected void provinciaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCantones();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string countryCode = Session["CountryCode"].ToString();
            List<Models.SearchResult> listPlaces = new List<Models.SearchResult>();
            if (layerMap.Value == "Coinca")
            {
                listPlaces = _mapsFunctions.Search(mapCoinca, countryCode, provinciaList.SelectedItem.Text.Trim(), cantonesList.SelectedItem.Text.Trim(), string.Empty, txtSearch.Text.Trim());
            }
            else //if (layerMap.Value == "GoogleRoad")
            {
                listPlaces = _mapsFunctions.Search(mapGoogle, countryCode, provinciaList.SelectedItem.Text.Trim(), cantonesList.SelectedItem.Text.Trim(), string.Empty, txtSearch.Text.Trim());
            }

            SearchPointsBusiness business = new SearchPointsBusiness();
            List<Models.SearchResult> listPoints = business.RetrievePoints(Convert.ToInt16(customerId.Value), txtSearch.Text.Trim()).ToList();
            Session["listPoints"] = listPoints;
            foreach (var x in listPoints) 
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }

            Session["listPlaces"] = listPlaces;
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            if (listPlaces.Count > 0)
            {
                directionsTable.Visible = true;
                pnlResultButtons.Visible = true;
                btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            }
            else
            {
                directionsTable.Visible = false;
                pnlResultButtons.Visible = false;
            }
        }

        TableCell Td(string text)
        {
            TableCell cell = new TableCell();
            cell.Text = text;
            if (text != string.Empty)
                cell.BorderWidth = Unit.Pixel(1);
            return cell;
        }

        void Tr(string text, string latitud, string longitud, string type)
        {
            TableRow row = new TableRow();
            row.Attributes.Add("onclick", "fncSeleccionaItem(" + latitud + "," + longitud + ")");
            row.Cells.Add(Td(text));
            if (type == "direction")
            {
                directionsTable.Rows.Add(row);
            }
            else 
            {
                pointsSearchTable.Rows.Add(row);
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            directionsTable.Visible = false;
            txtSearch.Text = string.Empty;
            cantonesList.Text = string.Empty;
            provinciaList.Text = string.Empty;
        }

        protected void LoadProvincias()
        {
            string countryCode = Session["CountryCode"].ToString();
            provinciaList.DataSource = _mapsFunctions.GetProvinciasList(countryCode);
            provinciaList.DataBind();
        }

        protected void LoadCantones()
        {
            string countryCode = Session["CountryCode"].ToString();
            cantonesList.DataSource = _mapsFunctions.GetCantonList(countryCode, provinciaList.SelectedItem.Text.Trim());
            cantonesList.DataBind();
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void btnDirectionsSearch_Click(object sender, EventArgs e)
        {
            List<Models.SearchResult> listPlaces = (List<Models.SearchResult>)Session["listPlaces"];
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            List<Models.SearchResult> listPoints = (List<Models.SearchResult>)Session["listPoints"];
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }
            pointsSearchTable.Visible = false;
            directionsTable.Visible = true;
            btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            btnPointsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("428BCA", NumberStyles.HexNumber)); //"#132C59";
        }

        protected void btnPointsSearch_Click(object sender, EventArgs e)
        {
            List<Models.SearchResult> listPlaces = (List<Models.SearchResult>)Session["listPlaces"];
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            List<Models.SearchResult> listPoints = (List<Models.SearchResult>)Session["listPoints"];
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }
            directionsTable.Visible = false;
            pointsSearchTable.Visible = true;
            btnPointsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("428BCA", NumberStyles.HexNumber)); //"#132C59";
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            if (layerMap.Value != "GoogleRoad")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleRoad');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleSatellite');", true);
            }
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt16(width.Value) >= 0 && Convert.ToInt16(height.Value) > 0)
            {
                pnlGoogle.Width = Convert.ToInt16(width.Value);
                pnlGoogle.Height = Convert.ToInt16(height.Value);
                pnlCoinca.Width = Convert.ToInt16(width.Value);
                pnlCoinca.Height = Convert.ToInt16(height.Value);
                mapGoogle.Width = Convert.ToInt16(width.Value);
                mapGoogle.Height = Convert.ToInt16(height.Value);
                mapCoinca.Width = Convert.ToInt16(width.Value);
                mapCoinca.Height = Convert.ToInt16(height.Value);
                ContentSearchId.Style.Add("width", width.Value + "px");                
            }
        }

    }
}