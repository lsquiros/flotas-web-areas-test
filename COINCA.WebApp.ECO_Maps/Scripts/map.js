$(document).ready(function () {
    fncMenuRefresh();
});

function fncMenuRefresh() {
    $('#mapMenu').children('img').each(function () {
        var style = $(this).attr('style').toLowerCase();
        $(this).removeClass("btnselected");
        if (style.indexOf("inset") > 0) {
            $(this).addClass("btnselected");
        }
    });
}

function fncSeleccionaItem(latiud, longitud, typeS) {

    //alert(latiud + '/' + longitud);

    var puntoLat = false;
    var puntoLon = false;
    var url = window.location.href;
    var urlSplit = url.split("?");
    var pageLoaded = true;
    url = '';
    url = url + urlSplit[0];
    if (urlSplit.length > 1) {
        var urlSplit2 = urlSplit[1].split("&");
        for (i = 0; i < urlSplit2.length; i++) {
            var urlSplit3 = urlSplit2[i].split("=");
            if (urlSplit3[0] == 'latDirection') {
                urlSplit3[1] = latiud;
                puntoLat = true;
            }
            else if (urlSplit3[0] == 'lonDirection') {
                urlSplit3[1] = longitud;
                puntoLon = true;
            }
            if (urlSplit3[0] == 'PageLoaded') {
                pageLoaded = false;
            }
            if (i == 0) {
                url = url + '?';
            }
            else {
                url = url + '&';
            }

            if (urlSplit3[0] == 'ExternalSearch') {
                url = url + urlSplit3[0] + '=false';
            }          
            else {
                url = url + urlSplit3[0] + '=' + urlSplit3[1];
            }
        }
    }
    if (puntoLat == false) {
        if (urlSplit.length > 1) {
            url = url + '&latDirection=' + latiud;
        }
        else {
            url = url + '?latDirection=' + latiud;
        }
    }
    if (puntoLon == false) {
        url = url + '&lonDirection=' + longitud;
    }
    if (pageLoaded == true) {
        url = url + '&PageLoaded=true';
	}

    if (typeS == 1)
    {
        url = url + '&CenterPointImage=1';
    }
    else
    {
        url = url + '&CenterPointImage=0';
    }

    window.location.href = url;
}

function fncSeleccionaItemCallTaker(latiud, longitud) {
    var puntoLat = false;
    var puntoLon = false;
    var url = window.location.href;
    var urlSplit = url.split("?");
    var pageLoaded = true;
    url = '';
    url = url + urlSplit[0];
    if (urlSplit.length > 1) {
        var urlSplit2 = urlSplit[1].split("&");
        for (i = 0; i < urlSplit2.length; i++) {
            var urlSplit3 = urlSplit2[i].split("=");
            if (urlSplit3[0] == 'latDirection') {
                urlSplit3[1] = latiud;
                puntoLat = true;
            }
            else if (urlSplit3[0] == 'lonDirection') {
                urlSplit3[1] = longitud;
                puntoLon = true;
            }
            if (urlSplit3[0] == 'PageLoaded') {
                pageLoaded = false
            }
            if (i == 0) {
                url = url + '?';
            }
            else {
                url = url + '&';
            }
            url = url + urlSplit3[0] + '=' + urlSplit3[1];
        }
    }
    if (puntoLat == false) {
        if (urlSplit.length > 1) {
            url = url + '&latDirection=' + latiud;
        }
        else {
            url = url + '?latDirection=' + latiud;
        }
    }
    if (puntoLon == false) {
        url = url + '&lonDirection=' + longitud;
    }
    if (pageLoaded == true) {
        url = url + '&PageLoaded=false';
    }
    window.location.href = url;
}

function changeMap(map) {
    var mapLoaded = false;
    var url = window.location.href;
    var urlSplit = url.split("?");
    var pageLoaded = true;
    url = '';
    url = url + urlSplit[0];
    if (urlSplit.length > 1) {
        var urlSplit2 = urlSplit[1].split("&");
        for (i = 0; i < urlSplit2.length; i++) {
            var urlSplit3 = urlSplit2[i].split("=");

            if (urlSplit3[0] == 'Map') {
                urlSplit3[1] = map;
                mapLoaded = true;
            }
            if (urlSplit3[0] == 'PageLoaded') {
                pageLoaded = false;
            }
            if (i == 0) {
                url = url + '?';
            }
            else {
                url = url + '&';
            }
            url = url + urlSplit3[0] + '=' + urlSplit3[1];
        }
    }
    if (mapLoaded == false) {
        if (urlSplit.length > 1) {
            url = url + '&Map=' + map;
        }
        else {
            url = url + '?Map=' + map
        }
    }
    if (pageLoaded == true) {
        url = url + '&PageLoaded=false';
    }
    window.location.href = url;
}

function changeMapCallCenter(map) {
    var mapLoaded = false;
    var url = window.location.href;
    var urlSplit = url.split("?");
    var pageLoaded = true;
    url = '';
    url = url + urlSplit[0];
    if (urlSplit.length > 1) {
        var urlSplit2 = urlSplit[1].split("&");
        for (i = 0; i < urlSplit2.length; i++) {
            var urlSplit3 = urlSplit2[i].split("=");

            if (urlSplit3[0] == 'Map') {
                urlSplit3[1] = map;
                mapLoaded = true;
            }
            if (urlSplit3[0] == 'PageLoaded') {
                pageLoaded = false;
            }
            if (i == 0) {
                url = url + '?';
            }
            else {
                url = url + '&';
            }
            url = url + urlSplit3[0] + '=' + urlSplit3[1];
        }
    }
    if (mapLoaded == false) {
        if (urlSplit.length > 1) {
            url = url + '&Map=' + map;
        }
        else {
            url = url + '?Map=' + map
        }
    }
    if (pageLoaded == true) {
        url = url + '&PageLoaded=false';
    }
    window.location.href = url;
}


