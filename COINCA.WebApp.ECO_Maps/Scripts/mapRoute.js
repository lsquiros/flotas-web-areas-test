function btnEditPointsClicked() {
    var btnEditPoints = $('#<%= btnEditPoints.ClientID %>');
    if (btnEditPoints != null) {
        btnEditPoints.click();
    }
}

function btnEditSegmentsClicked() {
    var btnEditSegments = $('#<%= btnEditSegments.ClientID %>');
    if (btnEditSegments != null) {
        btnEditSegments.click();
    }
}

function btnCalculatedSegmentsClicked() {
    var btnCalculatedSegments = $('#<%= btnCalculatedSegments.ClientID %>');
    if (btnCalculatedSegments != null) {
        btnCalculatedSegments.click();
    }
}
