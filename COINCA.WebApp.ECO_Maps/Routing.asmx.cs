﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using ECO_Maps.Utilities;
using ECO_Maps.Business.Intrack;
using ECO_Maps.Business.Routes;

namespace COINCA.WebApp.ECO_Maps
{
    /// <summary>
    /// Summary description for Routing
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Routing : System.Web.Services.WebService
    {

        [WebMethod]
        public double RoutingByStreet(string InitialCountry, string EndCountry, decimal InitialLatitudePoint, decimal InitialLongitudePoint, decimal EndLatitudePoint, decimal EndLongitudePoint)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            Maps mapsFunctions = new Maps();
            return Math.Round(mapsFunctions.GetDistance(InitialCountry, new AspMap.Point(Convert.ToDouble(InitialLongitudePoint), Convert.ToDouble(InitialLatitudePoint)), new AspMap.Point(Convert.ToDouble(EndLongitudePoint), Convert.ToDouble(EndLatitudePoint))), 2);
        }

        /// <summary>
        /// Service, get the lat and lon and returns the location
        /// </summary>
        /// <param name="LocationList"></param>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        [WebMethod]
        public string[] GetLocationsByLatLon(List<string> LocationList, string CountryCode)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            var bus = new GetLocationsBusiness();
            return bus.GetAddress(LocationList, CountryCode);
        }

        /// <summary>
        /// Service, Will add the route from a file
        /// </summary>
        /// <param name="PointList"></param>
        /// <param name="CountryCode"></param>
        /// <param name="routeId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool SaveRoutesFromImportData(string PointList, string CountryCode, int routeId, int UserId)
        {
            Maps MapUtilities = new Maps();
            
            var bus = new RoutesSaveFromService();
            if(bus.AddRoutesFromService(PointList, CountryCode, routeId, UserId))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get Provincias from Layer Maps
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        [WebMethod]
        public List<string> GetProvinciasFromMaps(string CountryCode)
        {
            try
            {
                Maps MapsFunctions = new Maps();

                return MapsFunctions.GetProvinciasList(CountryCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Cantons from Layer Maps
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <param name="Provincia"></param>
        /// <returns></returns>
        [WebMethod]
        public List<string> GetCantonesFromMaps(string CountryCode, string Provincia)
        {
            try
            {
                Maps MapsFunctions = new Maps();

                return MapsFunctions.GetCantonList(CountryCode, Provincia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
