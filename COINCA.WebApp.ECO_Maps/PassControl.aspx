﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="PassControl.aspx.cs" Inherits="ECO_Maps.PassControl1" EnableEventValidation="false" %>

<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <%: Styles.Render("~/bundles/css") %>
    </head>
    <body>
        <form id="PassControlForm" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                </Scripts>
            </asp:ScriptManager>
            <div id="body">
                <section class="content-wrapper main-content clear-fix">

                    <style>
                        /* GSOLANO: Corregir ubicación de los resultados de búsqueda cuando se filtra desde ECOsystem */
                        #divSearchResult {
                            margin-right: calc(53vh - 30px) !important;
                            margin-left: 0px !important;
                        }
                    </style>

                    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="loader" CssClass="loaderPassControl" runat="server">
                                <center>
                        <img src="Content/Images/loader.GIF" class="image-loader" />
                    </center>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
                            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                            <asp:HiddenField ID="distance" runat="server" Value="" />
                            <asp:HiddenField ID="Latitude" runat="server" Value="" />
                            <asp:HiddenField ID="Longitude" runat="server" Value="" />
                            <asp:HiddenField ID="customerId" runat="server" Value="false" />
                            <asp:HiddenField ID="width" runat="server" Value="0" />
                            <asp:HiddenField ID="height" runat="server" Value="0" />
                            <asp:HiddenField ID="PCReportData" runat="server" Value="" />

                            <asp:Panel ID="pnlMapPassControl" runat="server" CssClass="">
                                <div class="GeoFences-Content">
                                    <%--<div id="divSearchResultBtn">
                                            <asp:Panel id="pnlResultButtons" runat="server" Visible="false">
                                                <asp:LinkButton ID="btnDirectionsSearch" runat="server" CssClass="btnSearchResult" Width="163" Height="30" OnClick="btnDirectionsSearch_Click" >Direcciones</asp:LinkButton>
                                                <asp:LinkButton ID="btnPointsSearch" runat="server" CssClass="btnSearchResult" Width="163" Height="30" OnClick="btnPointsSearch_Click">Puntos</asp:LinkButton>
                                            </asp:Panel>
                                        </div>--%>
                                    <div id="divSearchResult">
                                        <asp:Panel ID="Panel10" runat="server">
                                            <asp:Table Visible="false" ID="directionsTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                            <asp:Table Visible="false" ID="pointsSearchTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                        </asp:Panel>
                                    </div>
                                    <div class="div100">
                                        <div class="ContentMap mapCss">
                                            <asp:Panel ID="pnlCoinca" runat="server" CssClass="mapMenu-controls" Visible="true">
                                                <aspmap:Map ID="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnCircleTool="map_CircleToolCoinca"></aspmap:Map>
                                                <div class="ColorListText" style="width: 240px;">
                                                    © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>
                                                </div>

                                            </asp:Panel>
                                            <asp:Panel ID="pnlGoogle" runat="server" CssClass="mapMenu-controls" Visible="false">
                                                <aspmap:Map ID="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnCircleTool="map_CircleToolGoogle"></aspmap:Map>
                                                <asp:Panel ID="pnlSatellite" runat="server" CssClass="pnlSatellite">
                                                    <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged" />
                                                </asp:Panel>
                                            </asp:Panel>
                                            <style> /*Estilo para los botones de zoom automatico*/
                                                .imageZoom{
                                                    margin-left: -7px;
                                                    margin-top: -2px;
                                                    height: 53px;
                                                    width: 53px;
                                                    border: 1px outset #ccc !important;
                                                }
                                            </style>
                                            <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                                                <div class="LeftMenu">
                                                    <center>
                                                        <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                                            <%--<asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('GoogleRoad')" ></asp:ImageButton>--%>
                                                            <%--<aspmap:MapToolButton id="ZoomInTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapCoinca" ToolTip="Zoom In" ></aspmap:MapToolButton>--%>
                                                            <%--<aspmap:MapToolButton id="zoomOutTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapCoinca" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                            <aspmap:MapToolButton id="panTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapCoinca" MapTool="Pan" ></aspmap:MapToolButton>
                                                            <%--<aspmap:MapToolButton id="centerTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapCoinca" MapTool="Center" ></aspmap:MapToolButton>--%>
                        	                                <aspmap:MapToolButton id="polygonTool" Visible="true" class="mapMenu" runat="server" ToolTip="Polygon Tool" ImageUrl="~/TOOLS/Custom/Circle.png" Map="mapCoinca" MapTool="Circle" ></aspmap:MapToolButton>
                                                            <aspmap:MapToolButton ID="distanceTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapCoinca" MapTool="Distance" ToolTip="Measure Distance" />
                                                            <button onclick="zoomIn()" type="button" class="mapMenu" style="margin-top: 4px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomIn.png" /></button>
                                                            <button onclick="zoomOut()" type="button" class="mapMenu" style="margin-top: 5px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomOut.png" /></button>

                                                          <%--  <asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                                            <aspmap:MapToolButton id="ZoomInToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapGoogle" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                            <aspmap:MapToolButton id="zoomOutToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapGoogle" MapTool="ZoomOut" ></aspmap:MapToolButton>
                                                            <aspmap:MapToolButton id="panToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapGoogle" MapTool="Pan" ></aspmap:MapToolButton>
                                                            <aspmap:MapToolButton id="centerToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapGoogle" MapTool="Center" ></aspmap:MapToolButton>
                        	                                <aspmap:MapToolButton id="polygonToolGoogle" Visible="false" class="mapMenu" runat="server" ToolTip="Polygon Tool" ImageUrl="~/TOOLS/Custom/Circle.png" Map="mapGoogle" MapTool="Circle" ></aspmap:MapToolButton>
                                                            <aspmap:MapToolButton ID="distanceToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapGoogle" MapTool="Distance" ToolTip="Measure Distance" />--%>
                                                        </div>                                                
                                                    </center>
                                                </div>
                                           </asp:Panel>
                                       </div>
                                    </div>
                            </asp:Panel>
                            <div class="mapMenu-controls">
                                <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                        <i class="glyphicon glyphicon-chevron-left"></i>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                </asp:LinkButton>
                            </div>

                            <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                            <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                            <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                            
                            <div class="ContentFooterPassControl1" style="display: none;">
                                <div class="BtnFooter t-right">
                                    <asp:LinkButton ID="btnOk" runat="server" Text="Buscar" class="btn btn-primary" title="Buscar" OnClick="btnOk_Click">
                                        <i class="glyphicon glyphicon-ok"></i>
                                    </asp:LinkButton>
                                </div>
                                <div class="FilterFooter">
                                    <div class="LabelSearch">
                                        <strong>
                                            <asp:Label ID="lblTo" class="control-label" runat="server" Text="Hasta: "></asp:Label>
                                        </strong>
                                    </div>
                                    <div class="ControlSearch">
                                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="txtDate form-control datepicker"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="FilterFooterBig">
                                    <div class="LabelSearchBig">
                                        <strong>
                                            <asp:Label ID="lblFrom" class="control-label" runat="server" Text="Rango de Fechas Desde: "></asp:Label>
                                        </strong>
                                    </div>
                                    <div class="ControlSearchBig">
                                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="txtDate form-control datepicker"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                                              
                            <asp:Panel ID="pnlResult" runat="server" Visible="false">
                                <div class="PassControl-result">
                                    <div class="table-white">
                                        <asp:GridView ID="GridResult" runat="server" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="InDateTime" HeaderText="Fecha Ingreso" ControlStyle-CssClass="PassControlTableColumn"></asp:BoundField>
                                                <asp:BoundField DataField="OutDateTime" HeaderText="Fecha Salida" ControlStyle-CssClass="PassControlTableColumn"></asp:BoundField>
                                                <asp:BoundField DataField="PlateId" HeaderText="Placa" ControlStyle-CssClass="PassControlTableColumn"></asp:BoundField>
                                                <asp:BoundField DataField="SubUnitName" HeaderText="Centro de Costos" ControlStyle-CssClass="PassControlTableColumn"></asp:BoundField>
                                                <asp:BoundField DataField="DriverName" HeaderText="Conductor" ControlStyle-CssClass="PassControlTableColumn"></asp:BoundField>
                                                <asp:BoundField DataField="Dallas" HeaderText="Dallas" ControlStyle-CssClass="PassControlTableColumn"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle BackColor="#3AC0F2" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                    

                                    <div class="dropdown">
                                        <button class="btn btn-success" type="button" id="dropdownMenu1" title="Exportar">
                                            <i class="mdi mdi-share"></i>&nbsp;&nbsp;Exportar Excel
                                        </button>
                                    </div>

                                    <div class="ContentFooter-back">
                                        <div class="BtnFooter t-right">
                                            <asp:LinkButton ID="btnBack" runat="server" Text="Atrás" class="btn btn-primary" title="Buscar" OnClick="btnBack_Click">
                                                    <i class="glyphicon glyphicon-arrow-left"></i>
                                            </asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                            </asp:Panel>

                            <div style="visibility: hidden; height: 0px;">
                                <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
                            </div>
                            <div id="ContentSearchId" class="ContentSearcPassControl col-sm-offset-1" runat="server">
                                <!--style="display:none;"> -->
                                <div class="col-sm-3">
                                    <!-- FilterSearch-->
                                    <div class="LabelSearch">
                                        <strong>
                                            <asp:Label ID="lblLevel1" class="control-label" runat="server" Text=""></asp:Label>
                                        </strong>
                                    </div>
                                    <div class="ControlSearch">
                                        <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="provinciaList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="provinciaList_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <!-- FilterSearch2 -->
                                    <div class="LabelSearch">
                                        <strong>
                                            <asp:Label ID="lblLevel2" class="control-label" runat="server" Text=""></asp:Label>
                                        </strong>
                                    </div>
                                    <div class="ControlSearch">
                                        <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="cantonesList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cantonList_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <!-- BtnSearch -->
                                    <div class="input-group">
                                        <asp:TextBox ID="txtSearch" runat="server" name="key" type="text" class="form-control" placeholder="Buscar..."></asp:TextBox>
                                        <span class="input-group-btn">
                                            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancelar" class="btn btn-default" OnClick="btnCancel_Click" title="Cancelar">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="BtnSearchPassControl" runat="server" Text="Buscar" class="btn btn-primary" OnClick="btnSearch_Click" title="Buscar">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </asp:LinkButton>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%: Scripts.Render("~/bundles/jquery") %>
                    <link rel="stylesheet" href="css/datepicker3.css" />
                    <script src="Scripts/jquery.ba-resize.min.js"></script>
                    <script src="Scripts/bootstrap-datepicker.js"></script>
                    <script>
                        $(function () {
                                        if ($('#<% =width.ClientID %>').val() == '0' && $('#<% =height.ClientID %>').val() == '0') {
                                resize();
                            }

                            //GSOLANO: corrige el espacio del margen al quitar los filtros del mapa, este espacio se opaca con el color de fondo.
                            //$("body").css("background-color", "#EEEEEE");
                        });

                        $(window).resize(function () {
                            resize();
                        });

                        function fncLoadDate() {
                            $('.datepicker').datepicker({
                                language: 'es',
                                minDate: new Date(),
                                format: 'dd/mm/yyyy',
                                autoclose: true,
                                todayHighlight: true
                            })
                        }

                        function resize() {
                            var width = $(window).width() - 5;
                            var height = $(window).height() - 40;
                            $('#<% =width.ClientID %>').attr('value', width);
                            $('#<% =height.ClientID %>').attr('value', height);
                            $("#ContentSearch").width(width);
                            $('#<%=btnResize.ClientID %>').click();
                        }

                        // Envía la información del reporte en un JSON
                        function returnResult(serialize) {
                            SetDefaultFilters();
                            parent.postMessage(serialize, "*");
                        }

                        // Envía los puntos dibujados en el mapa
                        function returnPointsResult() {
                            //---> [Points]Distance|Latitude|Longitude
                            var result = "[Points]" + $("#distance").val() + "|" +
                                         $("#Latitude").val() + "|" +
                                         $("#Longitude").val();
                            parent.postMessage(result, "*");
                        }

                        function SetDefaultFilters() {
                            var url = window.location.href;
                            var urlSplit = url.split("?");
                            url = '';
                            url = url + urlSplit[0];
                            if (urlSplit.length > 1) {
                                var urlSplit2 = urlSplit[1].split("&");
                                for (i = 0; i < urlSplit2.length; i++) {
                                    var urlSplit3 = urlSplit2[i].split("=");
                                    if (i == 0) {
                                        url = url + '?';
                                    }
                                    else {
                                        url = url + '&';
                                    }

                                    if (urlSplit3[0] == 'ExternalSearch') {
                                        url = url + urlSplit3[0] + '=false';
                                    }
                                    else if (urlSplit3[0] == 'ExternalReport') {
                                        url = url + urlSplit3[0] + '=false';
                                    }
                                    else {
                                        url = url + urlSplit3[0] + '=' + urlSplit3[1];
                                    }
                                }
                            }
                            window.location.href = url;
                        }

                        var map = AspMap.getMap('<%=mapCoinca.ClientID%>');

                        function zoomIn() {
                            map.zoomIn();
                        }

                        function zoomOut() {
                            map.zoomOut();
                        }
                    </script>
                </section>
            </div>
        </form>
    </body>
</html>
