﻿using System;
using ECO_Maps.Utilities;

namespace ECO_Maps.Models
{
    public class ReportLast
    {
        public decimal? UnitID { get; set; }
        public int? Device { get; set; }
        public DateTime GPSDateTime { get; set; }
        public DateTime RTCDateTime { get; set; }
        public DateTime RepDateTime { get; set; }
        public DateTime SvrDateTime { get; set; }
        public DateTime GPSDateTimeClient { get { return GPSDateTime.ToClientTime(); } }
        public DateTime RTCDateTimeClient { get { return GPSDateTime.ToClientTime(); } }
        public DateTime RepDateTimeClient { get { return GPSDateTime.ToClientTime(); } }
        public DateTime SvrDateTimeClient { get { return GPSDateTime.ToClientTime(); } }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Heading { get; set; }
        public int ReportID { get; set; }
        public double Odometer { get; set; }
        public double HDOP { get; set; }
        public byte InputStatus { get; set; }
        public byte OutputStatus { get; set; }
        public double VSSSpeed { get; set; }
        public double AnalogInput { get; set; }
        public string DriverID { get; set; }
        public double? Temperature1 { get; set; }
        public double? Temperature2 { get; set; }
        public int? MCC { get; set; }
        public int? MNC { get; set; }
        public int? LAC { get; set; }
        public int? CellID { get; set; }
        public int? RXLevel { get; set; }
        public int? GSMQuality { get; set; }
        public int? GSMStatus { get; set; }
        public int? Satellites { get; set; }
        public bool? RealTime { get; set; }
        public double? MainVolt { get; set; }
        public double? BatVolt { get; set; }
        public double? TotalDistance { get; set; }
        public int? Speeding { get; set; }
        public int Status { get; set; }
        public DateTime ActualDate { get; set; }
        public double Distance { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Direction { get; set; }
        public int? VehicleId { get; set; }
        public bool? IsFinish { get; set; }
        public string GPSId { get; set; }
        public string Temperature { get; set; }
        public string TemperatureStr { get { return HasCooler ? Temperature == "No Disponible" ? Temperature : string.Format("{0}°", Temperature) : Temperature; } }
        public bool HasCooler { get; set; }
        public int? ReportId { get; set; }
        public string SensorsXML { get; set; }
        public string ReportIdImage { get; set; }
        public string ReportIdDescription { get; set; }
        public int LevelZoom { get; set; }
    }
}
