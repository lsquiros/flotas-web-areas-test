﻿namespace ECO_Maps.Models
{
    public class Maps
    {
        public int MapId { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }
        public double? CenterLat { get; set; }
        public double? CenterLon { get; set; }
        public int? CenterZoom { get; set; }
        public string Directory { get; set; }
        public string Password { get; set; }
        public string Level1 { get; set; }
        public string Level2 { get; set; }
        public string Level3 { get; set; }
    }
}