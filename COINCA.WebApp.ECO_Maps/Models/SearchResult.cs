﻿

namespace ECO_Maps.Models
{
    public class SearchResult
    {
        public string description { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
    }
}