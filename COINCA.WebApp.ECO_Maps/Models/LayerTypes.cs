﻿namespace ECO_Maps.Models
{
    public class LayerTypes
    {
        public int LayerTypeId { get; set; }
        public int LayerType { get; set; }
        public int LayerId { get; set; }
        public bool LabelShow { get; set; }
        public string Label { get; set; }
        public bool BorderShow { get; set; }
        public int BorderColor { get; set; }
        public int BorderWidth { get; set; }
        public bool FillUse { get; set; }
        public int FillColor { get; set; }
        public bool ZoomUse { get; set; }
        public int ZoomMin { get; set; }
        public int ZoomMax { get; set; }
        public int StyleAspMap { get; set; }
        public int Priority { get; set; }
        public bool Visible { get; set; }
        public int? ImageSymbol { get; set; }
        public bool Active { get; set; }
    }
}
