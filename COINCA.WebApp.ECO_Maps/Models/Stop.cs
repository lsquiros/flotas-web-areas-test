﻿

namespace ECO_Maps.Models
{
    public class Stop
    {
        public int StopId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}

