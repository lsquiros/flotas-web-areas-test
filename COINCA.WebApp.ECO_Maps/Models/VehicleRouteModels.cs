﻿using System;
using System.Collections.Generic;

namespace ECO_Maps.Models
{
    public class VehicleRouteBase
    {
        public VehicleRouteBase()
        {
            Points = new List<RoutesPoints>();
            Segments = new List<RoutesSegments>();
        }

        public List<RoutesPoints> Points { get; set; }
        public List<RoutesSegments> Segments { get; set; }

        public class RoutesPoints
        {
            public int PointId { get; set; }
            public int RouteId { get; set; }
            public int PointReference { get; set; }
            public int Order { get; set; }
            public string Name { get; set; }
            public TimeSpan Time { get; set; }
            public int? TimeInt { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public int StopTime { get; set; }
        }
        public class RoutesSegments
        {
            public int SegmentId { get; set; }
            public int RouteId { get; set; }
            public int StartReference { get; set; }
            public int EndReference { get; set; }
            public string Poliyline { get; set; }
            public int? Time { get; set; }
            public double Distance { get; set; }
        }

    }
}


