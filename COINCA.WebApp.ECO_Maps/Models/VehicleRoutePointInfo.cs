﻿using System;

namespace ECO_Maps.Models
{
    public class VehicleRoutePointInfo
    {
        public int PointId { get; set; }
        public DateTime Date { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Time { get; set; }
        public double Distance { get; set; }
    }
}


