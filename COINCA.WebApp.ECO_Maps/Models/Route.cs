﻿using System.Collections.Generic;

namespace ECO_Maps.Models
{
    public class RoutesSegmentsBase
    {
        public RoutesSegmentsBase() 
        {
            Points = new List<RoutesPoints>();
            Segments = new List<RoutesSegments>();
        }

        public List<RoutesPoints> Points { get; set; }
        public List<RoutesSegments> Segments { get; set; }

        public class RoutesPoints
        {
            public int PointId { get; set; }
            public int RouteId { get; set; }
            public int PointReference { get; set; }
            public int Order { get; set; }
            public string Name { get; set; }
            public string Time { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public int StopTime { get; set; }
            public string Description { get; set; }
            public string Address { get; set; } 
            public int IsCommerce { get; set; }

            public string CommerceName { get; set; }
        }
        public class RoutesSegments 
        {
            public int SegmentId { get; set; }
            public int RouteId { get; set; }
            public int StartReference { get; set; }
            public int EndReference { get; set; }
            public string Poliyline { get; set; }
            public int Time { get; set; }
            public double Distance { get; set; }
        }

        public class RouteReportsJourney
        {
            public string Nombre { get; set; } 
            public string Valor1 { get; set; }
            public string Valor2 { get; set; }
            public string Valor3 { get; set; }
            public string Valor4 { get; set; }
            public string Color { get; set; }
            public string Fecha { get; set; }
        }

    }
}