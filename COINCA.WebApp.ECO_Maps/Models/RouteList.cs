﻿namespace ECO_Maps.Models
{
    public class PointsList
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int StopTime { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public int? flag { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public bool NewPoint { get; set; }
        public int PointReference { get; set; }
        public bool Deleted { get; set; }
        public int Iscommerce { get; set; }

        public string CommerceName { get; set; }

		public int RouteId { get; set; }
    }
}