﻿using System;
using ECO_Maps.Utilities;

namespace ECO_Maps.Models
{
    public class PassControlVehicle
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public int IntrackReference { get; set; }
        public string UnitID { get; set; }
        public DateTime InDateTime { get; set; }
        public DateTime InDateTimeClient { get { return InDateTime.ToClientTime(); } }
        public DateTime OutDateTime { get; set; }
        public DateTime OutDateTimeClient { get { return OutDateTime.ToClientTime(); } }
        public string PlateId { get; set; }
        public string VehicleName { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public int DriverId { get; set; }
        public string EncryptedDriverName { get; set; }
        public string DriverName { get; set; }
        public string Dallas { get; set; }
        public string EncryptedDallas { get; set; }
        public string EventOnOff { get; set; }
        public string Dir { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int ParentID { get; set; }
    }
}


