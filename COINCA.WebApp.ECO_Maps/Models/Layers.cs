﻿namespace ECO_Maps.Models
{
    public class Layers
    {
        public int LayerId { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public int LayerTypeId { get; set; }
        public int LayerSubtypeId { get; set; }
        public bool LabelShow { get; set; }
        public string Label { get; set; }
        public bool BorderUse { get; set; }
        public int BorderColor { get; set; }
        public int BorderWidth { get; set; }
        public bool FillUse { get; set; }
        public int FillColor { get; set; }
        public bool ZoomUse { get; set; }
        public int ZoomMin { get; set; }
        public int ZoomMax { get; set; }
        public int AspMapStyle { get; set; }
        public int Priority { get; set; }
        public bool Visible { get; set; }
        public bool Active { get; set; }
    }
}