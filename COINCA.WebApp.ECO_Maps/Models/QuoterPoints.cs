﻿namespace COINCA.WebApp.ECO_Maps.Models
{
    public class QuoterPoints
    {
        public int IdPoint { get; set; }
        public int Order { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}