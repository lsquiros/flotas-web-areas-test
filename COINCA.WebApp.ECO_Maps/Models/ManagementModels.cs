﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECO_Maps.Utilities;

namespace COINCA.WebApp.ECO_Maps.Models
{
    public class Commerces
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string Address { get; set; }

        public int? Type { get; set; }

        public string Code { get; set; }
    }

    public class Agents
    {
        public int? UserId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptName); } }
        
        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string Address { get; set; }

        public string CommerceName { get; set; }

        public string EventName { get; set; }

        public string Lapse { get; set; }

        public bool IsFinish { get; set; }

        public string ReconstructingId { get; set; }

        public int battery_percentage { get; set; }

        public DateTime GPSEventDate { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }
    }
}