﻿using System;

namespace ECO_Maps.Models
{
    public class MobileReportLast
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Type { get; set; }
        public int Heading { get; set; }

        public DateTime? GPSDateTime { get; set; }
        public double? Odometer { get; set; }
        public byte? InputStatus { get; set; }
        public byte? OutputStatus { get; set; }
        public double? VSSSpeed { get; set; }
		public double? MainVolt { get; set; }
        public double? BatVolt { get; set; }
    }
}