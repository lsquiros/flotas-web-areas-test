﻿
namespace ECO_Maps.Models
{
    public class SegmentList
    {
        public int id { get; set; }
        public int startPoint { get; set; }
        public int endPoint { get; set; }
        public AspMap.Polyline route { get; set; }
        public int time { get; set; }
        public double distance { get; set; }
    }
}