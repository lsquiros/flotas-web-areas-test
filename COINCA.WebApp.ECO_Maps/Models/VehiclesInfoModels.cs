﻿

namespace ECO_Maps.Models
{
    public class VehiclesInfo
    {
        public decimal UnitId { get; set; }
        public string PlateId { get; set; }
        public string Name { get; set; }
        public int VehicleId { get; set; }
        public int IntrackReference { get; set; }
    }
}