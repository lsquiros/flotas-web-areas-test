﻿namespace ECO_Maps.Models
{
    public class LayerMaps
    {
        public int MapId { get; set; }
        public int LayerId { get; set; }
        public string Description { get; set; }
    }
}