﻿namespace ECO_Maps.Models
{
    public class ImagesSymbols
    {
        public int ImageSymbolId { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Active { get; set; }
    }
}
