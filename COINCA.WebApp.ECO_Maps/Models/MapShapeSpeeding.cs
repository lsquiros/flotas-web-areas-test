﻿namespace ECO_Maps.Models
{
    public class MapShapeSpeeding
    {
        public AspMap.Polyline Polyline { get; set; }
        public int Speeding { get; set; }
    }
}