﻿/************************************************************************************************************
*  File    : CustomerThresholdsModels.cs
*  Summary : CustomerThresholds Models
*  Author  : Danilo Hidalgo
*  Date    : 22/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

namespace ECO_Maps.Models
{
    public class CustomerThresholds
    {
        public int? RedLower { get; set; }
        public int? RedHigher { get; set; }
        public int? YellowLower { get; set; }
        public int? YellowHigher { get; set; }
        public int? GreenLower { get; set; }
        public int? GreenHigher { get; set; }
    }
}
