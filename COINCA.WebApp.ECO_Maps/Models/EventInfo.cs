﻿namespace COINCA.WebApp.ECO_Maps.Models
{
    public class EventInfo
    {
        public double EventLatitude { get; set; }
        public double EventLongitude { get; set; }
        public int UnidId { get; set; }
        public double VehicleLatitude { get; set; }
        public double VehicleLongitude { get; set; }
        public int VehicleIntrack { get; set; }
        public string Image { get; set; }
        public string AliasVehicle { get; set; }
        public int AVL { get; set; }
        public double Status { get; set; }
        public double Speed { get; set; }
    }
}