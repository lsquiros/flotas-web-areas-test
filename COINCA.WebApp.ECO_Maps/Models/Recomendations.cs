﻿using System;

namespace ECO_Maps.Models
{
    public class Recomendations
    {
        public int UnidId { get; set; }
        public string UnitName { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int SubTypeId { get; set; }
        public string SubTypeName { get; set; }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string Driver { get; set; }
        public int VehicleId { get; set; }
        public DateTime GPSDateTime { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Distance { get; set; }
        public string Zone { get; set; }
        public string VehiclePlate { get; set; }
        public string AliasVehicle { get; set; }
        public string Image { get; set; }
        public string Assigned { get; set; }
        public bool IndQualitas { get; set; }
        public int AVL { get; set; }
        public double Status { get; set; }
        public double Speed { get; set; }
    }
}