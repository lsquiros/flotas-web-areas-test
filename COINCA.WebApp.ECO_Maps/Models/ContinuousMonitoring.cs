﻿using System;

namespace COINCA.WebApp.ECO_Maps.Models
{
    public class ContinuousMonitoring
    {
        public int VehicleId { get; set; }
        public string PlateId { get; set; }
        public string Name { get; set; }
        public DateTime GPSDateTime { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Heading { get; set; }
        public int Status { get; set; }
        public string StatusStr { get; set; }
        public string ImageURL { get; set; }
        public double VSSSpeed { get; set; }
        public DateTime ActualDate { get; set; }
        public bool IsMoving { get; set; }
        public double Distance { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Direction { get; set; }
        public string Temperature { get; set; }
        public string TemperatureStr { get { return HasCooler ? Temperature == "No Disponible" ? Temperature : string.Format("{0}°", Temperature) : Temperature; } }
        public bool HasCooler { get; set; }
        public int? ReportId { get; set; }
        public string SensorsXML { get; set; }
        public string ReportIdImage { get; set; }
        public string ReportIdDescription { get; set; }
    }
}