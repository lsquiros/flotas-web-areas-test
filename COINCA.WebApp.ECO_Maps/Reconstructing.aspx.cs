﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using AspMap;
using AspMap.Web;
using System.Globalization;
using ECO_Maps.Business;
using ECO_Maps.Utilities;

namespace ECO_Maps
{

    public partial class Reconstructing1 : System.Web.UI.Page
    {
        private static string _lLicense;
        private static int _refreshInterval;
        private readonly Maps _mapsFunctions = new Maps();
        private static Maps _mapsFunctionBus = new ECO_Maps.Utilities.Maps();
        private string countryCode = string.Empty;

        ///*
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_lLicense == null)
                _lLicense = System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"];

            if (_refreshInterval == 0)
                _refreshInterval = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["RefreshInterval"]);

            try
            {
                License.SetLicenseKey(_lLicense);
                string Params = Request.QueryString["Params"];
                string layer = Request.QueryString["Map"];
                var countryCode = string.Empty;
                layerMap.Value = layer ?? System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"];
                //if (CountryCode == null) { CountryCode = string.Empty; }
                if (Params != string.Empty)
                {
                    var p = Params.Split('|');
                    Session["CountryCode"] = countryCode = p[0];
                    Session["VehicleId"] = Convert.ToInt32(p[3]);
                    Session["InactiveTime"] = Convert.ToInt16(p[2]);
                    Session["GPSId"] = p[6];

                    if (Session["Playing"] != Session["VehicleId"] && p[4] != "0" && p[5] != "0")
                    {
                        Session["StartDate"] = p[4];
                        Session["EndDate"] = p[5];
                    }
                    if (p[4] == "0" && p[5] == "0")
                    {
                        Session["StartDate"] = null;
                        Session["EndDate"] = null;
                    }
                    Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[1]));

                    if (p.Length > 7)
                        Session["Zoom"] = Convert.ToInt16(p[p.Length - 1]);
                    else
                        Session["Zoom"] = 0;

                }
                if (countryCode != string.Empty)
                {
                    _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);
                    if (pageLoaded.Value == "false")
                    {
                        pageLoaded.Value = "true";
                        ClearRoute();
                    }
                    Models.Maps map = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                    if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
                    {

                        pnlCoinca.Visible = true;
                        //pnlCoinca.Visible = false;
                        //GoogleMap.Visible = false;
                        //CoincaMap.Visible = true;
                        //mapCoinca.EnableAnimation = false;
                        mapCoinca.EnableAnimation = true;
                        mapCoinca.AnimationInterval = 1000;
                        //pnlSatellite.Visible = true;
                        if (map != null && Session["VehicleDetail"] == null)
                        {
                            if (Convert.ToInt32(Session["ZOOM"]) == 0)
                                mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), Convert.ToInt32(map.CenterZoom + 3));
                            else
                            {
                                Models.ReportLast vehicleRoute = GetRouteSession().FirstOrDefault();
                                if (vehicleRoute != null)
                                    mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), vehicleRoute.LevelZoom);
                                else
                                    mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), Convert.ToInt32(map.CenterZoom + 3));
                            }
                            trackVehiclesCoincaOnLoad();
                        }

                    }
                    //    else
                    //    {
                    //    //pnlGoogle.Visible = true;
                    //    pnlCoinca.Visible = false;
                    //   // GoogleMap.Visible = false;
                    //    CoincaMap.Visible = true;
                    //    mapCoinca.EnableAnimation = false;
                    //   // mapGoogle.EnableAnimation = true;
                    //   // mapGoogle.AnimationInterval = 1000;
                    //    //pnlSatellite.Visible = true;
                    //    if (map != null)
                    //    {
                    //        mapGoogle.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), map.CenterZoom.Value + 3);
                    //        trackVehiclesGoogleOnLoad();
                    //    }
                    //}
                }
                if (Page.IsPostBack == false)
                {
                    Session["VehicleDetail"] = null; //if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                }

                var playing = Session["Playing"] == null ? string.Empty : Session["Playing"].ToString();


                if (playing != Session["GPSId"].ToString())
                {
                    Session["Playing"] = Session["GPSId"];
                    LoadRoute();
                }

                if (playing == "LR")
                {
                    ReloadRoute();
                }
            }
            catch (Exception ex)
            {
                if (Session["CountryCode"] != null)
                {
                    Models.Maps map = MapsBusiness.RetrieveMaps(Session["CountryCode"].ToString()).FirstOrDefault();
                    mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), Convert.ToInt32(map.CenterZoom + 3));
                }
            }
        }
       
        protected void ReloadRoute()
        {
            Models.Maps map = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
            LoadRoute();

            List<Models.ReportLast> lstVehicleRoute = GetRouteSession();

            if (Convert.ToInt32(Session["ZOOM"]) == 0)
                mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), Convert.ToInt32(map.CenterZoom + 3));
            else
            {
                if (lstVehicleRoute != null)
                    mapCoinca.CenterAndZoom(GetCenterPoint(lstVehicleRoute), lstVehicleRoute.FirstOrDefault().LevelZoom);
                else
                    mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), Convert.ToInt32(map.CenterZoom + 3));
            }
            AddInitFinishPoints(lstVehicleRoute);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "fncLoadDate", "fncLoadDate();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "updateDivColor", "updateDivColor();", true);
        }
        
        void returnDetail(int count, string data)
        {
            //var data = Session["VehicleDetail"] ?? string.Empty;
            if (count == 0 && Session["VehicleDetail"] == null)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + data + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + Session["VehicleDetail"] + "');", true);       
            }          
        }

        protected void mapGoogle_RefreshAnimationLayer(object sender, RefreshAnimationLayerArgs e)
        {
            //if (Session["ReconstructingPaused"] == null) return;
            //if ((bool)Session["ReconstructingPaused"] == false)
            //{
            //    MoveCount(_refreshInterval);
            //}
            //e.NeedRefreshMap = TrackVehiclesGoogle();
            //returnDetail(GetCountSession(), null);
        }

        protected void mapCoinca_RefreshAnimationLayer(object sender, RefreshAnimationLayerArgs e)
        {

            if (Session["ReconstructingPaused"] == null) return;
            if ((bool)Session["ReconstructingPaused"] == false)
            {
                MoveCount(_refreshInterval);
            }
            e.NeedRefreshMap = TrackVehiclesCoinca();
            returnDetail(GetCountSession(), null);
        }

        private bool TrackVehiclesCoinca()
        {
            int count = GetCountSession();
            List<Models.ReportLast> reports = GetRouteSession();
            if (count >= 0 && count < reports.Count && reports.Count > 0)
            {
                mapCoinca.AnimationLayer.Clear();
                int inactive = 0;
                if (!string.IsNullOrEmpty(Session["InactiveTime"].ToString())) { inactive = Convert.ToInt32(Session["InactiveTime"]); }

                Point location = new Point(_mapsFunctions.GetX(reports[count].Longitude, layerMap.Value), _mapsFunctions.GetY(reports[count].Latitude, layerMap.Value));
                GeoEvent geoEvent = new GeoEvent
                {
                    ImageUrl = _mapsFunctions.getCarImage(reports[count].InputStatus, reports[count].GPSDateTime, DateTime.UtcNow, 0, reports[count].VSSSpeed, false),
                    Location = location,
                    ImageWidth = 50,
                    ImageHeight = 50,
                    Rotation = reports[count].Heading
                };
                mapCoinca.AnimationLayer.Add(geoEvent);

                if(Convert.ToInt32(Session["ZOOM"]) == 0)
                    mapCoinca.CenterAndZoom(location, Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                else
                {
                    Models.ReportLast vehicleRoute = GetRouteSession().FirstOrDefault();
                    if (vehicleRoute != null)
                        mapCoinca.CenterAndZoom(GetCenterPoint(reports), vehicleRoute.LevelZoom);
                        //mapCoinca.CenterAndZoom(location, vehicleRoute.LevelZoom);
                    else
                        mapCoinca.CenterAndZoom(location, Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                }

                List<string> list = new List<string>();
                reports[count].VSSSpeed = Math.Round(reports[count].VSSSpeed, 0);
                DateTime timeZone = reports[count].GPSDateTime;
                reports[count].Date = timeZone.ToString("dd/MM/yyyy");
                reports[count].Time = timeZone.ToString("HH:mm:ss");
                reports[count].Status = _mapsFunctions.getCarStatusId(reports[count].InputStatus, reports[count].ActualDate, reports[count].GPSDateTime, inactive, reports[count].VSSSpeed);
                reports[count].VehicleId = Convert.ToInt32(Session["VehicleId"]);
                list.Add(reports[count].Latitude + "|" + reports[count].Longitude);
                reports[count].IsFinish = count == reports.Count - 1;
                reports[count].GPSId = Session["GPSId"] == null ? "" : Session["GPSId"].ToString();
                reports[count].SensorsXML = string.IsNullOrEmpty(reports[count].SensorsXML) ? reports[count].SensorsXML : reports[count].SensorsXML.Replace("\"", "'");
                var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                Session["VehicleDetail"] = jSearializer.Serialize(reports[count]);

                geoEvent.ImageWidth = 45;
                geoEvent.ImageHeight = 45;
                geoEvent.Rotation = reports[count].Heading;
                if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
                {
                    mapCoinca.AnimationLayer.Add(geoEvent);
                }
                return true;
            }
            return false;
        }

        void trackVehiclesCoincaOnLoad()
        {
            Models.ReportLast report = _mapsFunctionBus.GetVehicleReportLast(Session["VehicleId"].ToString()).FirstOrDefault();
            mapCoinca.AnimationLayer.Clear();
            int inactive = 0;
            if (!string.IsNullOrEmpty(Session["InactiveTime"].ToString())) { inactive = Convert.ToInt32(Session["InactiveTime"]); }

            if(report != null)
            { 
                Point location = new Point(_mapsFunctions.GetX(report.Longitude, layerMap.Value), _mapsFunctions.GetY(report.Latitude, layerMap.Value));
                GeoEvent geoEvent = new GeoEvent
                {
                    ImageUrl = _mapsFunctions.getCarImage(report.InputStatus, report.GPSDateTime, DateTime.UtcNow, 24, report.VSSSpeed, false),
                    Location = location,
                    ImageWidth = 50,
                    ImageHeight = 50,
                    Rotation = report.Heading
                };
                mapCoinca.AnimationLayer.Add(geoEvent);

                if (Convert.ToInt32(Session["ZOOM"]) == 0)
                    mapCoinca.CenterAndZoom(location, Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5)); 
                else
                {
                    Models.ReportLast vehicleRoute = GetRouteSession().FirstOrDefault();
                    if(vehicleRoute != null)
                        //mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(report[GetItemMedio(report.Count)].Longitude, layerMap.Value), _mapsFunctions.GetY(report[GetItemMedio(report.Count)].Latitude, layerMap.Value)), vehicleRoute.LevelZoom);
                        mapCoinca.CenterAndZoom(location, vehicleRoute.LevelZoom);
                    else
                        mapCoinca.CenterAndZoom(location, Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                }

                List<string> list = new List<string>();
                report.VSSSpeed = Math.Round(report.VSSSpeed, 0);
                DateTime timeZone = report.GPSDateTime;
                report.Date = timeZone.ToString("dd/MM/yyyy");
                report.Time = timeZone.ToString("HH:mm:ss");
                report.Status = _mapsFunctions.getCarStatusId(report.InputStatus, report.ActualDate, report.GPSDateTime, inactive, report.VSSSpeed);
                report.VehicleId = Convert.ToInt32(Session["VehicleId"]);
                report.IsFinish = GetRouteSession().Count == 0;
                report.GPSId = Session["GPSId"] == null ? "" : Session["GPSId"].ToString();            
                list.Add(report.Latitude + "|" + report.Longitude);
                var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                geoEvent.ImageWidth = 45;
                geoEvent.ImageHeight = 45;
                geoEvent.Rotation = report.Heading;
          
                mapCoinca.AnimationLayer.Add(geoEvent);
        
                returnDetail(0, jSearializer.Serialize(report));
                //var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                //ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + jSearializer.Serialize(report) + "');", true);
            }
        }

        //void trackVehiclesGoogleOnLoad()
        //{
        //    Models.ReportLast report = _mapsFunctionBus.GetVehicleReportLast(Session["VehicleId"].ToString()).FirstOrDefault();
        //    mapGoogle.AnimationLayer.Clear();
        //    int inactive = 0;
        //    if (!string.IsNullOrEmpty(Session["InactiveTime"].ToString())) { inactive = Convert.ToInt32(Session["InactiveTime"]); }

        //    Point location = new Point(_mapsFunctions.GetX(report.Longitude, layerMap.Value), _mapsFunctions.GetY(report.Latitude, layerMap.Value));
        //    GeoEvent geoEvent = new GeoEvent
        //    {
        //        ImageUrl = _mapsFunctions.getCarImage(report.InputStatus, report.GPSDateTime, DateTime.UtcNow, 0, report.VSSSpeed, false),
        //        Location = location,
        //        ImageWidth = 50,
        //        ImageHeight = 50,
        //        Rotation = report.Heading
        //    };
        //    mapGoogle.AnimationLayer.Add(geoEvent);
        //    mapGoogle.CenterAt(location);

        //    List<string> list = new List<string>();
        //    report.VSSSpeed = Math.Round(report.VSSSpeed, 0);
        //    DateTime timeZone = report.GPSDateTime;
        //    report.Date = timeZone.ToString("dd/MM/yyyy");
        //    report.Time = timeZone.ToString("HH:mm:ss");
        //    report.Status = _mapsFunctions.getCarStatusId(report.InputStatus, report.ActualDate, report.GPSDateTime, inactive, report.VSSSpeed);
        //    report.VehicleId = Convert.ToInt32(Session["VehicleId"]);
        //    report.IsFinish = ((List<Models.ReportLast>)GetRouteSession()).Count == 0;
        //    report.GPSId = Session["GPSId"] == null ? "" : Session["GPSId"].ToString();
        //    list.Add(report.Latitude + "|" + report.Longitude);           
        //    var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
          
        //    geoEvent.ImageWidth = 45;
        //    geoEvent.ImageHeight = 45;
        //    geoEvent.Rotation = report.Heading;
        //    if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
        //    {
        //        mapCoinca.AnimationLayer.Add(geoEvent);
        //    }
        //    else
        //    {
        //        mapGoogle.AnimationLayer.Add(geoEvent);
        //    }

        //    returnDetail(0, jSearializer.Serialize(report));
        //        //var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + jSearializer.Serialize(report) + "');", true); 
        //}

        //bool TrackVehiclesGoogle()
        //{
        //    int count = GetCountSession();
        //    List<Models.ReportLast> reports = GetRouteSession();
        //    if (count >= 0 && count < reports.Count && reports.Count > 0)
        //    {
        //        mapGoogle.AnimationLayer.Clear();
        //        int inactive = 0;
        //        if (!string.IsNullOrEmpty(Session["InactiveTime"].ToString())) { inactive = Convert.ToInt32(Session["InactiveTime"]); }

        //        Point location = new Point(_mapsFunctions.GetX(reports[count].Longitude, layerMap.Value), _mapsFunctions.GetY(reports[count].Latitude, layerMap.Value));
        //        GeoEvent geoEvent = new GeoEvent
        //        {
        //            ImageUrl = _mapsFunctions.getCarImage(reports[count].InputStatus, reports[count].GPSDateTime, DateTime.UtcNow, 0, reports[count].VSSSpeed, false),
        //            Location = location,
        //            ImageWidth = 50,
        //            ImageHeight = 50,
        //            Rotation = reports[count].Heading
        //        };
        //        mapGoogle.AnimationLayer.Add(geoEvent);
        //        mapGoogle.CenterAt(location);

        //        List<string> list = new List<string>();
        //        reports[count].VSSSpeed = Math.Round(reports[count].VSSSpeed, 0);
        //        DateTime timeZone = reports[count].GPSDateTime;
        //        reports[count].Date = timeZone.ToString("dd/MM/yyyy");
        //        reports[count].Time = timeZone.ToString("HH:mm:ss");
        //        reports[count].Status = _mapsFunctions.getCarStatusId(reports[count].InputStatus, reports[count].ActualDate, reports[count].GPSDateTime, inactive, reports[count].VSSSpeed);
        //        reports[count].VehicleId = Convert.ToInt32(Session["VehicleId"]);
        //        list.Add(reports[count].Latitude + "|" + reports[count].Longitude);
        //        reports[count].IsFinish = count == reports.Count-1;
        //        reports[count].GPSId = Session["GPSId"] == null ? "" : Session["GPSId"].ToString();
        //        var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        Session["VehicleDetail"] = jSearializer.Serialize(reports[count]);

        //        geoEvent.ImageWidth = 45;
        //        geoEvent.ImageHeight = 45;
        //        geoEvent.Rotation = reports[count].Heading;
        //        if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
        //        {
        //            mapCoinca.AnimationLayer.Add(geoEvent);
        //        }
        //        else
        //        {
        //            mapGoogle.AnimationLayer.Add(geoEvent);
        //        }

        //        return true;
        //    }
        //    return false;
        //}
       

        protected void btnPause_Click(object sender, EventArgs e)
        {
            Session["ReconstructingPaused"] = true;
            //btnPause.Visible = false;
            //btnPlay.Visible = true;
            ScriptManager.RegisterStartupScript(this, GetType(), "fncEnableButtons", "fncEnableButtons();", true);
            if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
            {
                mapCoinca.EnableAnimation = false;
            }
            //else
            //{
            //    //AspMap.Web.RefreshAnimationLayerArgs x = new RefreshAnimationLayerArgs();
            //    mapGoogle.EnableAnimation = false;
            //    //mapGoogle.AnimationInterval = 0;
            //    //mapGoogle_RefreshAnimationLayer(sender, x);
            //}
        }

        protected void btnPlay_Click(object sender, EventArgs e)
        {
            if (txtReset.Text == "RS") SetCountSession(0);

            Session["ReconstructingPaused"] = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "fncDisableButtons", "fncDisableButtons();", true);
            RefreshAnimationLayerArgs x = new RefreshAnimationLayerArgs();
            if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
            {
                mapCoinca.EnableAnimation = true;
                mapCoinca.AnimationInterval = 1000;
                mapCoinca_RefreshAnimationLayer(sender, x);
            }
            //else
            //{
            //    mapGoogle.EnableAnimation = true;
            //    mapGoogle.AnimationInterval = 1000;
            //    mapGoogle_RefreshAnimationLayer(sender, x);
            //}
        }
        
        protected void btnOk_Click(object sender, EventArgs e)
        {
            Session["StartDate"] = txtStartDate.Text;
            Session["EndDate"] = txtEndDate.Text;
            //Session["Playing"] = null;
            refrehing.Value = "true";
            LoadRoute();
            RefreshAnimationLayerArgs x = new RefreshAnimationLayerArgs();
            if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
            {
                mapCoinca.EnableAnimation = true;
                mapCoinca.AnimationInterval = 1000;
                mapCoinca_RefreshAnimationLayer(sender, x);
            }
            //else
            //{
            //    mapGoogle.EnableAnimation = true;
            //    mapGoogle.AnimationInterval = 1000;
            //    mapGoogle_RefreshAnimationLayer(sender, x);
            //}
            ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "refresh();", true);
        }
      
        protected void ClearControls()
        {
            //txtStartDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //txtEndDate.Text = txtStartDate.Text;
            //TxtStartTime.Text = DateTime.Today.ToString("hh:mm tt");
            //TxtEndTime.Text = TxtStartTime.Text;     
            ClearRoute();
            //ScriptManager.RegisterStartupScript(this, GetType(), "fncDisableButtons", "fncDisableButtons();", true);
            //ScriptManager.RegisterStartupScript(this, GetType(), "fncDisablePlayButtoms", "fncDisablePlayButtoms();", true);
        }

        protected void LoadRoute()
        {            
            ClearRoute(); 
            mapCoinca.RemoveAllLayers();
            mapCoinca.AnimationLayer.Clear();
            // mapGoogle.AnimationLayer.Clear();

            if (Session["StartDate"] != null && Session["EndDate"] != null)
            {
                DateTime startDate = DateTime.Parse(Session["StartDate"].ToString(), CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.Parse(Session["EndDate"].ToString(), CultureInfo.InvariantCulture);

                List<Models.ReportLast> vehicleRoute = new List<Models.ReportLast>();
                if (Convert.ToInt32(Session["ZOOM"]) == 0)//Zoom inactivo
                    vehicleRoute = _mapsFunctions.GetVehicleReportsRange(Convert.ToInt32(Session["VehicleId"]), startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"), false);
                else//Zoom activo
                    vehicleRoute = _mapsFunctions.GetVehicleReportsRange(Convert.ToInt32(Session["VehicleId"]), startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"), true);

                if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
                {
                    MapShape mapShapeGreen = mapCoinca.MapShapes.Add(_mapsFunctions.GetVehicleReportsPolyline(vehicleRoute, layerMap.Value), "route");
                    mapShapeGreen.Symbol.LineColor = System.Drawing.Color.DarkGreen;
                    mapShapeGreen.Symbol.Size = 3;
                    mapShapeGreen.Symbol.FillStyle = FillStyle.Invisible;
                    SetCountSession(0);
                    SetRouteSession(vehicleRoute);

                    if (vehicleRoute.Count > 0)
                    {
                        if (Convert.ToInt32(Session["ZOOM"]) == 0)
                            mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(vehicleRoute[0].Longitude, layerMap.Value), _mapsFunctions.GetY(vehicleRoute[0].Latitude, layerMap.Value)), Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                        else
                            mapCoinca.CenterAndZoom(GetCenterPoint(vehicleRoute), vehicleRoute[0].LevelZoom);
                    }

                    List<Models.MapShapeSpeeding> mapShapeList = _mapsFunctions.GetVehicleReportsSpeedingPolyline(vehicleRoute, layerMap.Value);
                    foreach (var polilyne in mapShapeList)
                    {
                        MapShape mapShape = mapCoinca.MapShapes.Add(polilyne.Polyline);
                        if (polilyne.Speeding != 0)
                        {
                            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
                            mapShape.Symbol.Size = 3;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                            SetCountSession(0);
                            SetRouteSession(vehicleRoute);
                        }
                        if (vehicleRoute.Count > 0)
                        {
                            if (Convert.ToInt32(Session["ZOOM"]) == 0)
                            {
                                mapCoinca.CenterAndZoom(new Point(_mapsFunctions.GetX(vehicleRoute[0].Longitude, layerMap.Value), _mapsFunctions.GetY(vehicleRoute[0].Latitude, layerMap.Value)), Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                            }
                            else
                            {
                                mapCoinca.CenterAndZoom(GetCenterPoint(vehicleRoute), vehicleRoute[0].LevelZoom);
                                AddInitFinishPoints(vehicleRoute);
                                break;
                            }

                        }
                    }

                    if (vehicleRoute.FirstOrDefault().HasCooler)
                    {
                        SetTemperatureMarkets(vehicleRoute);
                    }
                }
                if (Convert.ToInt32(Session["ZOOM"]) == 0)//Zoom inactivo
                {
                    refrehing.Value = "true";
                    Session["ReconstructingPaused"] = false;
                }
                else//Zoom activo
                {
                    refrehing.Value = "false";
                    Session["ReconstructingPaused"] = false;
                }
            }
        }

        protected void SetTemperatureMarkets(List<Models.ReportLast> route)
        {
            foreach (var item in route.Where(x => x.ReportIdImage != null).ToList())
            {
                Point location = new Point(_mapsFunctions.GetX(item.Longitude + 0.001, layerMap.Value), _mapsFunctions.GetY(item.Latitude, layerMap.Value));
                var symbol = new MarkerSymbol(item.ReportIdImage, 20, 40); 
                Marker geoEvent = new Marker(location, symbol, "", "");                
                mapCoinca.Markers.Add(geoEvent);
            }
        }

        protected void ClearRoute()
        {
            Session["ReconstructingRoute"] = null;
            Session["ReconstructingCount"] = 0;
            Session["ReconstructingRefresh"] = false;
            Session["ReconstructingPaused"] = true;
            Session["timevents"] = null;
            mapCoinca.MapShapes.Clear();
            mapCoinca.Markers.Clear();
            //mapGoogle.MapShapes.Clear();
            //mapGoogle.Markers.Clear();
        }

        protected void MappingTimeline(List<Models.ReportLast> reportList)
        {
            var  timelineList= string.Empty;
            var initTime = reportList[0].GPSDateTime;
            for (var i = 0; i < reportList.Count ; i++ )
            {
                
                if (reportList[i].GPSDateTime.Hour > initTime.Hour)
                {                  
                    initTime = reportList[i].GPSDateTime;
                    timelineList = timelineList + "<li><a data-date = \""+initTime.Date.ToString("MM/dd/yyyyThh:mm") +"\">" +
                                   initTime.Date.ToString("hh tt") + " </a></li>";
                }      
              
            }
            Session["timevents"] = timelineList;
        }

        protected List<Models.ReportLast> GetRouteSession()
        {
            var result = new List<Models.ReportLast>();
            if (Session["ReconstructingRoute"] != null)
            {
                result = (List<Models.ReportLast>)Session["ReconstructingRoute"];
            }
            return result;
        }

        protected void SetRouteSession(List<Models.ReportLast> route)
        {
            Session["ReconstructingRoute"] = route;
        }

        protected int GetCountSession()
        {
            var result = 0;
            if (Session["ReconstructingCount"] != null)
            {
                result = Convert.ToInt16(Session["ReconstructingCount"]);
            }
            return result;
        }

        protected void SetCountSession(int count)
        {
            Session["ReconstructingCount"] = count;
        }

        public void MoveCount(int steps)
        {
            int count = GetCountSession();
            if (count + steps >= 0)
            {
                if (count + steps <= GetRouteSession().Count - 1)
                {
                    Session["ReconstructingCount"] = count + steps;
                }
                else
                {
                    Session["ReconstructingCount"] = GetRouteSession().Count - 1;
                }
            }
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            //btnHideMapMenu.Visible = false;
            //btnShowMapMenu.Visible = true;
            //mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            //btnShowMapMenu.Visible = false;
            //btnHideMapMenu.Visible = true;
            //mapMenuControls.Visible = true;
        }

        //protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "changeMap",
        //        layerMap.Value != "GoogleRoad" ? "changeMap('GoogleRoad');" : "changeMap('GoogleSatellite');", true);
        //}

        protected void btnRefresh_Click(object sender, EventArgs e)
        {           
                if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
                {
                    mapCoinca_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
                }
                //else
                //{
                //    mapGoogle_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
                //}
            
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            short lWidth = Convert.ToInt16(width.Value);
            short lHeigth = Convert.ToInt16(height.Value);
            if (lWidth >= 0 && lHeigth > 0)
            {
                //pnlGoogle.Width = lWidth;
                //pnlGoogle.Height = lHeigth;
                pnlCoinca.Width = lWidth;
                pnlCoinca.Height = lHeigth;
                //mapGoogle.Width = lWidth;
                //mapGoogle.Height = lHeigth;
                mapCoinca.Width = lWidth;
                mapCoinca.Height = lHeigth;
            }       
        }

        /*protected int GetItemMedio(int total)
        {
            int medio = 0;

            medio = total / 2;
            if (medio > 0)
                medio--;

            return medio;
        }*/

        protected Point GetCenterPoint(List<Models.ReportLast> lstVehicleRoute)
        {
            double logdMax = -180;
            double logdMim = 180;
            double latdMax = -90;
            double latdMim = 90;
            double logdMedia = 0.0;
            double latdMedia = 0.0;
            //double temp = 0.0;

            foreach (Models.ReportLast pointTemp in lstVehicleRoute)
            {
                if (logdMax < pointTemp.Longitude)
                    logdMax = pointTemp.Longitude;

                if (logdMim > pointTemp.Longitude)
                    logdMim = pointTemp.Longitude;

                if (latdMax < pointTemp.Latitude)
                    latdMax = pointTemp.Latitude;

                if (latdMim > pointTemp.Latitude)
                    latdMim = pointTemp.Latitude;
            }
            //temp = (logdMax - logdMim)/2;
            logdMedia = logdMax - ((logdMax - logdMim) / 2);
            latdMedia = latdMax - ((latdMax - latdMim) / 2);

            return new Point(_mapsFunctions.GetX(logdMedia, layerMap.Value), _mapsFunctions.GetY(latdMedia, layerMap.Value));
        }

        protected void AddInitFinishPoints(List<Models.ReportLast> lstVehicleRoute)
        {

            mapCoinca.EnableAnimation = true;

            Point location = new Point(_mapsFunctions.GetX(lstVehicleRoute[lstVehicleRoute.Count - 1].Longitude, layerMap.Value), _mapsFunctions.GetY(lstVehicleRoute[lstVehicleRoute.Count - 1].Latitude, layerMap.Value));
            GeoEvent geoEvent = new GeoEvent
            {
                ImageUrl = "Content/Images/markerEnd.png",
                Location = location,
                ImageWidth = 40,
                ImageHeight = 40
                //Rotation = lstVehicleRoute[lstVehicleRoute.Count - 1].Heading
            };
            mapCoinca.AnimationLayer.Add(geoEvent);

            location = new Point(_mapsFunctions.GetX(lstVehicleRoute[0].Longitude, layerMap.Value), _mapsFunctions.GetY(lstVehicleRoute[0].Latitude, layerMap.Value));
            geoEvent = new GeoEvent
            {
                ImageUrl = "Content/Images/markerStart.png",
                Location = location,
                ImageWidth = 25,
                ImageHeight = 25
                //Rotation = lstVehicleRoute[0].Heading,
            };
            mapCoinca.AnimationLayer.Add(geoEvent);
        }

    }
}
