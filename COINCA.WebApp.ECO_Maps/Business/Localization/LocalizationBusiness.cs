﻿/************************************************************************************************************
*  File    : LocalizationBusiness.cs
*  Summary : Localization Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 14/09/2015
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;

namespace ECO_Maps.Business
{
    public class LocalizationBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve ReportLast
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.MobileReportLast> RetrieveLocalizationReportLast(string pVehicleType, int pVehicleId)
        {
            using (var dba = new DataBaseAccess("ConnectionStringNameCAD"))
            {
                return dba.ExecuteReader<Models.MobileReportLast>("[General].[Sp_ReportLastIncident_Retrieve]",
                    new
                    {
                        VehicleId = pVehicleId
                    });
            }
        }

        public IEnumerable<Models.MobileReportLast> RetrieveLocalizationReportLastAzure(string plateId)
        {
            using (var dba = new DataBaseAccess("ConnectionStringNameEcoSystems"))
            {
                return dba.ExecuteReader<Models.MobileReportLast>("[General].[Sp_ReportLastLineVita_Retrieve]",
                    new
                    {
                        PlateId = plateId
                    });
            }
        }

        /// <summary>
        /// Send POI to smartphone
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert operations</param>
        public void SendPoi(int pVehicleId, double pLongitude, double pLatitude, string pDescription)
        {
            using (var dba = new DataBaseAccess("ConnectionStringNameCAD"))
            {
                dba.ExecuteScalar<int?>("[General].[Sp_SendPOI_AddOrEdit]",
                    new
                    {
                        VehicleId = pVehicleId,
                        Longitude = pLongitude,
                        Latitude = pLatitude,
                        Description = pDescription
                    });
            }
        }

        ///// <summary>
        ///// Update report last of vehicle
        ///// </summary>
        ///// <param name="model">Model or class passed as parameter  which contains all properties to perform insert operations</param>
        //public void UpdateReportLast(int pVehicleId, string pVehicleType, double pLongitude, double pLatitude)
        //{
        //    using (var dba = new DataBaseAccess("ConnectionStringNameCAD"))
        //    {
        //        dba.ExecuteScalar<int?>("[General].[Sp_UpdateReportLast_AddOrEdit]",
        //            new
        //            {
        //                VehicleId = pVehicleId,
        //                VehicleType = pVehicleType,
        //                Longitude = pLongitude,
        //                Latitude = pLatitude
        //            });
        //    }
        //}

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
