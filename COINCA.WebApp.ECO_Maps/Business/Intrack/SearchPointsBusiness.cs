﻿/************************************************************************************************************
*  File    : SearchPointsBusiness.cs
*  Summary : SearchPoints Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 11/27/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;

namespace ECO_Maps.Business.Intrack
{
    public class SearchPointsBusiness
    {

        /// <summary>
        /// Retrieve Points
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.SearchResult> RetrievePoints(int? pCustomerId, string pKey)
        {
            IEnumerable<Models.SearchResult> result = null;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Models.SearchResult>("[Efficiency].[Sp_Points_Retrieve]",
                   new
                   {
                       CustomerId = pCustomerId,
                       Key = pKey
                   });
            }
            return result;
        }

        public IEnumerable<Models.SearchResult> RetrieveCommerces(int? pCustomerId, string pKey)
        {
            IEnumerable<Models.SearchResult> result = null;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Models.SearchResult>("[Efficiency].[Sp_CommercesbyCustomer_Retrieve]",
                   new
                   {
                       CustomerId = pCustomerId,
                       Key = pKey
                   });
            }
            return result;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }


    }
}
