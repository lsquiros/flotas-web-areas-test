﻿using AspMap;
/************************************************************************************************************
*  File    : GetLocationsBusiness.cs
*  Summary : Get locations based on lat and lon
*  Author  : Henry Retana
*  Date    : 15/06/2016
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using ECO_Maps.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECO_Maps.Business.Intrack
{
    public class GetLocationsBusiness
    {
        AspMap.Layer _gLayerCiudades;
        string _countryCode;

        private string RutaMapa()
        {
            
            Maps map = MapsBusiness.RetrieveMaps(_countryCode).FirstOrDefault();

            return map.Directory.Trim();
        }

        public string[] GetAddress(List<string> locationList, string cCode)
        {
            _countryCode = cCode;
            AspMap.Point vUbicacion = new AspMap.Point();
            string[] rlist = new string[locationList.Count];
            int i = 0;

            Maps map = MapsBusiness.RetrieveMaps(cCode).FirstOrDefault();

            List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList(); //businessLayerMaps.RetrieveLayerMaps(map.MapId).ToList();

            AspMap.Layer layer = new Layer();
            layer = AspMap.Layer.Open(map.Directory.Trim() + "GeoCodificacionCiudades.TAB");

            foreach (var item in locationList)
            {
                double lat, lon;
                string[] x = item.Split('|');
                lat = Convert.ToDouble(x[0]);
                lon = Convert.ToDouble(x[1]);

                if (!double.IsNaN(lat) || !(double.IsNaN(lon)))
                {
                   
                    var gPunto = new AspMap.Point(lon, lat);
                    string vPlace = string.Empty;
                    string vHeading = string.Empty;
                    double vDistance = 0;
                    string result = string.Empty;

                    AspMap.Recordset vRecords = layer.SearchNearest(gPunto);

                    if (vRecords.EOF == false)
                    {
                        vUbicacion = vRecords.Shape.Centroid;
                        vPlace = vRecords[1].ToString();
                    }
                    vDistance = Math.Round(GetDistanceKm(gPunto, vUbicacion), 1);

                    string mainAddress = vRecords[4] + ", " + vRecords[3] + ", ";

                    if (vDistance > 0)
                    {
                        if (vDistance >= 1)
                        {
                            result = mainAddress + vDistance.ToString("n1");
                            result = result + " km al ";
                        }
                        else
                        {
                            result = mainAddress + (vDistance * 1000).ToString("n0");
                            result = result + " mts al ";
                        }
                    }
                    vHeading = DeltaHeading(gPunto, vUbicacion);
                    result = result + vHeading + " de ";
                    result = result + vPlace;

                    if ((vUbicacion != null) && (result.Trim().Length > 0))
                    {
                        rlist[i] = result;
                    }
                    else
                    {
                        rlist[i] = "";
                    }
                }
                i++;
            }

            return rlist;
        }

        public AspMap.Layer GetCartography()
        {
            AspMap.Web.Map gMapa = new AspMap.Web.Map();

            string rutaMap = RutaMapa();

            var gLayer = gMapa.AddLayer(rutaMap + "GeoCodificacion.TAB");

            if(_gLayerCiudades == null && rutaMap != "")
            {
                _gLayerCiudades = new AspMap.Layer();
                _gLayerCiudades = gMapa.AddLayer(rutaMap + "GeoCodificacionCiudades.TAB");
            }

            return _gLayerCiudades;
        }

        public double GetDistanceKm(Point pPointA, Point pPointB)
        {
            try
            {
                double rlat1 = Math.PI * pPointA.Y / 180;
                double rlat2 = Math.PI * pPointB.Y / 180;
                double theta = pPointA.X - pPointB.X;
                double rtheta = Math.PI * theta / 180;
                double dist =
                    Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                    Math.Cos(rlat2) * Math.Cos(rtheta);
                dist = Math.Acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                return dist * 1.609344;
            }
            catch
            {
                return -1;
            }
        }

        public string DeltaHeading(AspMap.Point pPointA, AspMap.Point pPointB)
        {
            string result = string.Empty;
            double vHeadingX = 0;
            double vHeadingY = 0;
            double vHeading = 0;
            double vAtan = 0;
            double vSetting = 0;

            try
            {
                vHeadingX = pPointA.X - pPointB.X;
                vHeadingY = pPointA.Y - pPointB.Y;

                //no tiene rumbo
                if (vHeadingX == 0 && vHeadingY == 0)
                {
                    return string.Empty;
                }
                if (vHeadingX == 0)
                {
                    if (vHeadingY > 0) { vHeading = 90; }
                    else { vHeading = 270; }
                }
                else
                {
                    if (vHeadingY == 0)
                    {
                        if (vHeadingX > 0)
                        {
                            vHeading = 0;
                        }
                        else
                        {
                            vHeading = 180;
                        }
                    }
                    else
                    {
                        vAtan = Math.Atan(vHeadingY / vHeadingX);
                        vAtan = vAtan * 180 / Math.PI;
                        if (vHeadingX < 0 && vHeadingY > 0)
                        {
                            vSetting = 180;
                        }
                        else if (vHeadingX < 0 && vHeadingY < 0)
                        {
                            vSetting = 180;
                        }
                        else if (vHeadingX > 0 && vHeadingY < 0)
                        {
                            vSetting = 360;
                        }
                        vHeading = vAtan + vSetting;
                    }
                }
                if (vHeading >= 338 && vHeading <= 360 || vHeading >= 0 && vHeading <= 23) { result = "Norte"; }
                else if (vHeading >= 23 && vHeading <= 68) { result = "Noreste"; }
                else if (vHeading >= 68 && vHeading <= 113) { result = "Este"; }
                else if (vHeading >= 113 && vHeading <= 158) { result = "Sureste"; }
                else if (vHeading >= 158 && vHeading <= 203) { result = "Sur"; }
                else if (vHeading >= 203 && vHeading <= 248) { result = "Suroeste"; }
                else if (vHeading >= 248 && vHeading <= 293) { result = "Oeste"; }
                else if (vHeading >= 293 && vHeading <= 338) { result = "Noreste"; }
                return result;
            }
            catch
            {
                return result;
            }
        }

    }
}