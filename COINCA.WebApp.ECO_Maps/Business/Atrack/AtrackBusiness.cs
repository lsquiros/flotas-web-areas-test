﻿/************************************************************************************************************
*  File    : AtrackBusiness.cs
*  Summary : Atrack Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/30/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using ECOsystem.Management.Models;

namespace ECO_Maps.Business.Atrack
{
    public class AtrackBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve ReportLast
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.ReportLast> RetrieveReportLast(string pVehiclesId)
        {
            var businessIntrack = new Intrack.IntrackVehicleBusiness();
            string unitsId = string.Empty;
            List<Models.Numeroimei> unitIdList = (List<Models.Numeroimei>)businessIntrack.RetrieveVehicleEmei(pVehiclesId);
            foreach (var u in unitIdList)
            {
                if (unitsId == string.Empty) { unitsId = u.numeroimei; }
                else { unitsId = unitsId + "," + u.numeroimei; }
            }
            using (var dba = new DataBaseAccess())
            {
                if (unitsId == string.Empty)
                {
                    return new List<Models.ReportLast>();
                }
                else
                {
                    return dba.ExecuteReader<Models.ReportLast>("[dbo].[Sp_ReportLast_Retrieve]",
                        new
                        {
                            UnitsID = unitsId
                        });
                }
            }
        }

        /// <summary>
        /// Retrieve RetrieveReports
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.ReportLast> RetrieveReports(int pVehicleId, string pStartDate, string pEndDate)
        {
            var businessIntrack = new Intrack.IntrackVehicleBusiness();
            long unitId = 0;
            List<Models.Numeroimei> unitIdList = (List<Models.Numeroimei>)businessIntrack.RetrieveVehicleEmei(pVehicleId.ToString());
            foreach (var u in unitIdList)
            {
                unitId = Convert.ToInt64(u.numeroimei);
            }
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.ReportLast>("[dbo].[Sp_Reports_Retrieve]",
                    new
                    {
                        UnitID = unitId,
                        StartDate = pStartDate,
                        EndDate = pEndDate
                    });
            }
        }

        /// <summary>
        /// Retrieve RetrieveReports
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="pVehicleId">todo: describe pVehicleId parameter on RetrieveReportsRange</param>
        /// <param name="pStartDate">todo: describe pStartDate parameter on RetrieveReportsRange</param>
        /// <param name="pEndDate">todo: describe pEndDate parameter on RetrieveReportsRange</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public IEnumerable<Models.ReportLast> RetrieveReportsRange(int pVehicleId, string pStartDate, string pEndDate, bool pZoom)
        {
            var businessIntrack = new Intrack.IntrackVehicleBusiness();
            long unitId = 0;
            List<Models.Numeroimei> unitIdList = (List<Models.Numeroimei>)businessIntrack.RetrieveVehicleEmei(pVehicleId.ToString());
            foreach (var u in unitIdList)
            {
                unitId = Convert.ToInt64(u.numeroimei);
            }
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.ReportLast>("[dbo].[Sp_ReportsRangeZoom_Retrieve]",
                    new
                    {
                        UnitID = unitId,
                        StartDate = pStartDate,
                        EndDate = pEndDate,
                        Zoom = pZoom
                    });
            }
        }
         
        /// <summary>
        /// Retrieve ContinuousMonitoring
        /// </summary>
        /// <param name="pVehicles">list of ECOsystem vehicles separated by commas</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<COINCA.WebApp.ECO_Maps.Models.ContinuousMonitoring> RetrieveContinuousMonitoring(string pVehiclesList, string pDate, int pUtc)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<COINCA.WebApp.ECO_Maps.Models.ContinuousMonitoring>("[dbo].[Sp_ContinuousMonitoring_Retrieve]",
                    new
                    {
                        Date = pDate,
                        UTC = pUtc,
                        Vehicles = pVehiclesList
                    });
            }
        }

        //Get the commercess by Ids
        public IEnumerable<Commerce> RetrieveCommerces(string pIds)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Commerce>("[General].[Sp_Commerces_Retrieve]", new { IdList = pIds });
            }
        }

        //Get the commercess by Ids
        public IEnumerable<Agent> RetrieveAgents(string Ids)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentDetailInformation]", new { Ids });
            }
        }

        //Get the commercess by Ids
        public IEnumerable<Agent> RetrieveAgentReconstructing(int UserId, string StartDate, string EndDate)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentRouteReconstructing]", new 
                { 
                    UserId, 
                    StartDate, 
                    EndDate
                });
            }
        }

        //Get the Agent Agenda
        public IEnumerable<Agent> RetrieveAgentAgenda(int UserId, string StartDate, string EndDate)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agent>("[Management].[Sp_Retrieve_AgentAgendaMarkets]", new
                {
                    UserId,
                    StartDate,
                    EndDate
                });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
