﻿/************************************************************************************************************
*  File    : VehicleRouteBusiness.cs
*  Summary : VehicleRoute Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 11/21/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using System.Web.UI.WebControls;
using Newtonsoft.Json; 

namespace ECO_Maps.Business.VehicleRoutes
{
    public class VehicleRouteBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve VehicleRoute
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.List> RetrieveRoutesByVehicleList(int? vehicleId, int? customerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.List>("[Efficiency].[Sp_RoutesByVehicleIntrackList_Retrieve]",
                    new
                    {
                        VehicleId = vehicleId,
                        CustomerId = customerId
                    });
            }
        }

        public void SaveReportRoute(List<TableRow> vehiculeRouteResult, List<TableRow> summaryResult, int vehicleID, DateTime StartDate) 
        {

            List<ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney> Listreport1 = new List<ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney>();
            List<ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney> Listreport2 = new List<ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney>();

            ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney report1;
            ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney report2;


            for (int i = 0; i <= vehiculeRouteResult.Count-1; i++)
            {
                report1 = new ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney();
                report1.Nombre = vehiculeRouteResult[i].Cells[0].Text;
                report1.Valor1 = vehiculeRouteResult[i].Cells[1].Text;
                report1.Valor2 = vehiculeRouteResult[i].Cells[2].Text;
                report1.Valor3 = vehiculeRouteResult[i].Cells[3].Text;
                report1.Color = vehiculeRouteResult[i].Cells[4].CssClass;
                report1.Fecha = StartDate.ToShortDateString();
                Listreport1.Add(report1);
            }

            for (int y = 0; y <= summaryResult.Count-1; y++)
            {
                report2 = new ECO_Maps.Models.RoutesSegmentsBase.RouteReportsJourney();
                report2.Nombre = summaryResult[y].Cells[0].Text;
                report2.Valor1 = summaryResult[y].Cells[1].Text;
                report2.Valor2 = summaryResult[y].Cells[2].Text;
                report2.Valor3 = summaryResult[y].Cells[3].Text;
                report2.Color  = summaryResult[y].Cells[4].CssClass;
                report2.Fecha = StartDate.ToShortDateString();
                Listreport2.Add(report2);
            }

            using (var dba = new DataBaseAccess())
            {
                Models.VehicleRouteBase.RoutesSegments x = new Models.VehicleRouteBase.RoutesSegments();
                dba.ExecuteNonQuery("[Efficiency].[Sp_ReportRoute_AddOrEdit]",
                   new
                   {
                       DataInfoPercentage = JsonConvert.SerializeObject(Listreport1),
                       DataInfoRoute = JsonConvert.SerializeObject(Listreport2),
                       VehicleId = vehicleID
                   });          
            }
        }

        /// <summary>
        /// Retrieve VehicleRouteDetail
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public Models.VehicleRouteBase VehicleRouteDetail(int? routeId, string day, int intrackReference)
        {
            using (var dba = new DataBaseAccess())
            {
                Models.VehicleRouteBase result = new Models.VehicleRouteBase();
                List<Models.VehicleRouteBase.RoutesSegments> segmentsList =
                    (List<Models.VehicleRouteBase.RoutesSegments>)dba.ExecuteReader<Models.VehicleRouteBase.RoutesSegments>("[Efficiency].[Sp_VehicleRouteSegments_Retrieve]",
                    new
                    {
                        RouteId = routeId
                        //,
                        //Day = day,
                        //IntrackRefenrence = intrackReference
                    });
                List<Models.VehicleRouteBase.RoutesPoints> pointsList =
                    (List<Models.VehicleRouteBase.RoutesPoints>)dba.ExecuteReader<Models.VehicleRouteBase.RoutesPoints>("[Efficiency].[Sp_VehicleRoutePoints_Retrieve]",
                    new
                    {
                        RouteId = routeId
                    });

                result.Points = pointsList;
                result.Segments = segmentsList;
                return result;
            }
        }

        //public Models.RoutesSegmentsBase RetrieveRoutesDetail(int? routeId)
        //{
        //    using (var dba = new DataBaseAccess("ConnectionStringNameEcoSystems"))
        //    {
        //        Models.RoutesSegmentsBase result = new Models.RoutesSegmentsBase();
        //        List<Models.RoutesSegmentsBase.RoutesPoints> pointsList =
        //            (List<Models.RoutesSegmentsBase.RoutesPoints>)dba.ExecuteReader<Models.RoutesSegmentsBase.RoutesPoints>("[Efficiency].[Sp_RoutesPoints_Retrieve]",
        //            new
        //            {
        //                RouteId = routeId,
        //            });
        //        List<Models.RoutesSegmentsBase.RoutesSegments> segmentsList =
        //            (List<Models.RoutesSegmentsBase.RoutesSegments>)dba.ExecuteReader<Models.RoutesSegmentsBase.RoutesSegments>("[Efficiency].[Sp_RoutesSegments_Retrieve]",
        //            new
        //            {
        //                RouteId = routeId,
        //            });
        //        result.Points = pointsList;
        //        result.Segments = segmentsList;
        //        return result;
        //    }
        //}


        /// <summary>
        /// Retrieve VehicleRouteDetail
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.VehicleRoutePointInfo> VehicleRoutePointsInfo(string pVehiclesId, string pStartDate, string pEndDate, string pPoints, int pDistanceMin, int pTimeMin)
        {
            var businessIntrack = new Intrack.IntrackVehicleBusiness();
            long unitId = 0;
            IEnumerable<Models.Numeroimei> unitIdList = (List<Models.Numeroimei>)businessIntrack.RetrieveVehicleEmei(pVehiclesId);
            foreach (var u in unitIdList)
            {
                unitId = Convert.ToInt64(u.numeroimei);
            }
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.VehicleRoutePointInfo>("[dbo].[Sp_VehicleRoutePointsInfo_Retrieve]",
                new
                {
                    UnitID = unitId,
                    StartDate = pStartDate,
                    EndDate = pEndDate,
                    Points = pPoints,
                    DistanceMin = pDistanceMin,
                    TimeMin = pTimeMin
                });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    
    }
}
