﻿/************************************************************************************************************
*  File    : PassControlBusiness.cs
*  Summary : PassControl Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 12/11/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Providers.Entities;
using ECOsystem.DataAccess;

namespace ECO_Maps.Business.PassControl
{
    public class PassControlBusiness
    {
        /// <summary>
        /// Retrieve PassControlBusiness
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="pLevel">todo: describe pLevel parameter on PassControlVehicleInfo</param>
        /// <param name="pId">todo: describe pId parameter on PassControlVehicleInfo</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public IEnumerable<Models.PassControlVehicle> PassControlVehicleInfo(int? pLevel, int? pId)
        {
            IEnumerable<Models.PassControlVehicle> result = null;
            using (var dba = new DataBaseAccess())
                //vehiculo
                if (pLevel == 1)
                {
                    {
                        result = dba.ExecuteReader<Models.PassControlVehicle>("[Efficiency].[Sp_PassControlVehicleInfo_Retrieve]",
                            new
                            {
                                IntrackReference = pId
                            });
                    }
                }
                //Grupo
                else if (pLevel == 2)
                {
                    {
                        result = dba.ExecuteReader<Models.PassControlVehicle>("[Efficiency].[Sp_PassControlGroupInfo_Retrieve]",
                            new
                            {
                                Id = pId
                            });
                    }
                }
                //Unidad
                else if (pLevel == 3)
                {
                    {
                        result = dba.ExecuteReader<Models.PassControlVehicle>("[Efficiency].[Sp_PassControlUnitInfo_Retrieve]",
                            new
                            {
                                Id = pId
                            });
                    }
                }
                //SubUnidad
                else if (pLevel == 4)
                {
                    {
                        result = dba.ExecuteReader<Models.PassControlVehicle>("[Efficiency].[Sp_PassControlSubUnitInfo_Retrieve]",
                            new
                            {
                                Id = pId
                            });
                    }
                }

            return result;
        }

        public IEnumerable<Models.PassControlVehicle> PassControlDriverInfo(string pFilter, string pStartDate, string pEndDate)
        {
            IEnumerable<Models.PassControlVehicle> result = null;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Models.PassControlVehicle>("[Efficiency].[Sp_PassControlVehicleDriver_Retrieve]",
                    new
                    {
                        Id = pFilter,
                        StartDate = pStartDate,
                        EndDate = pEndDate
                    });
                }
            return result;
        }

        public IEnumerable<Models.PassControlVehicle> GetUnitsVehicles(string pFilter)
        {
            IEnumerable<Models.PassControlVehicle> result = null;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Models.PassControlVehicle>("[dbo].[Sp_PassControlVehicleUnitsId_Retrieve]",
                    new
                    {
                        Id = pFilter
                    });
            }
            return result;
        }

        public IEnumerable<Models.PassControlVehicle> GetUnitsVehicles(double pLongitude, double pLatitude, double pDistance, string pUnitId, string pStartDate, string pEndDate, int pEventOn, int pEventOff)

        {
            IEnumerable<Models.PassControlVehicle> result = null;
            using (var dba = new DataBaseAccess())
            {
                result = dba.ExecuteReader<Models.PassControlVehicle>("[dbo].[Sp_PassControl_Retrieve]",
                    new
                    {
                        Longitude = pLongitude,
                        Latitude = pLatitude, 
                        Distance = pDistance,
                        UnitId = pUnitId,
                        StartDate = pStartDate, 
                        EndDate = pEndDate,
                        EventOn = pEventOn,
                        EventOff = pEventOff
                    });
            }
            return result;
        }
        
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    
    }
}


