﻿/************************************************************************************************************
*  File    : RecomendationsBusiness.cs
*  Summary : Recomendations Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 12/01/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using System.Linq;

namespace ECO_Maps.Business.Recomendation
{
    public class RecomendationsBusiness
    {
        /// <summary>
        /// Retrieve Recomendations
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public List<Models.Recomendations> RetrieveRecomendations(int pTypeId, int pSubTypeId, int pVehiclesQuantity, int pTime, double pLongitude, double pLatitude, int pUtc, string countryCode, int filterType, string filterRecomendation)
        {
            List<Models.Recomendations> result = new List<Models.Recomendations>();
            using (var dba = new DataBaseAccess("ConnectionStringNameCAD"))
            {
                IEnumerable<Models.Recomendations> list = dba.ExecuteReader<Models.Recomendations>("[General].[Sp_Recomendation_Retrieve]",
                    new
                    {
                        TypeId = pTypeId,
                        SubTypeId = pSubTypeId,
                        VehiclesQuantity = pVehiclesQuantity,
                        Time = pTime,
                        Longitude = pLongitude,
                        Latitude = pLatitude,
                        UTC = pUtc,
                        CountryCode = countryCode,
                        FilterType = filterType, 
                        FilterRecomendation = filterRecomendation
                    });
                foreach (var x in list) 
                {
                    List<Models.Recomendations> filterResult = (from l in result where l.UnidId == x.UnidId select l).ToList();
                    if (filterResult.Count == 0) 
                    {
                        result.Add(x);
                        if (result.Count >= pVehiclesQuantity) 
                        {
                            break;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
