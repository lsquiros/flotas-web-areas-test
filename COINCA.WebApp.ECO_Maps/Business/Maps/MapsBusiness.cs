﻿/************************************************************************************************************
*  File    : MapsBusiness.cs
*  Summary : Maps Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/14/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System.Collections.Generic;
using ECOsystem.DataAccess;

namespace ECO_Maps.Business
{
    public class MapsBusiness
    {

        /// <summary>
        /// Retrieve Maps
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="countryCode">todo: describe CountryCode parameter on RetrieveMaps</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public static IEnumerable<Models.Maps> RetrieveMaps(string countryCode)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.Maps>("[Maps].[Sp_Maps_Retrieve]",
                    new
                    {
                        CountryCode = countryCode
                    });
            }
        }

        public static IEnumerable<Models.ImagesSymbols> RetrieveLayerMaps()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.ImagesSymbols>("[Maps].[Sp_ImagesSymbols_Retrieve]", null);
            }
        }

        /// <summary>
        /// Retrieve LayerMaps
        /// </summary>
        /// <param name="pMapId">todo: describe pMapId parameter on RetrieveLayerMaps</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public static IEnumerable<Models.LayerMaps> RetrieveLayerMaps(int pMapId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.LayerMaps>("[Maps].[Sp_LayerMaps_Retrieve]",
                    new
                    {
                        MapId = pMapId
                    });
            }
        }

        /// <summary>
        /// Retrieve Layer
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <param name="pLayerId">todo: describe pLayerId parameter on RetrieveLayerMapsByLayer</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public static IEnumerable<Models.Layers> RetrieveLayerMapsByLayer(int pLayerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.Layers>("[Maps].[Sp_Layers_Retrieve]",
                    new
                    {
                        LayerId = pLayerId
                    });
            }
        }

        /// <summary>
        /// Retrieve LayerType
        /// </summary>
        /// <param name="pLayerId">todo: describe pLayerId parameter on RetrieveLayerMapsByLayerTypes</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public static IEnumerable<Models.LayerTypes> RetrieveLayerMapsByLayerTypes(int pLayerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.LayerTypes>("[Maps].[Sp_LayerTypes_Retrieve]",
                    new
                    {
                        LayerId = pLayerId
                    });
            }
        }

        public static string GetAddressByLatLon(double Longitude, double Latitude)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<string>("[Efficiency].[Sp_GetAddress_Retrieve]",
                    new
                    {
                        Latitude,
                        Longitude
                    });
            }
        }

        public static IEnumerable<Models.List> RetrieveStates(int pCountryid)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.List>("[General].[Sp_States_Retrieve]",
                    new
                    {
                        CountryId = pCountryid,
                    });
            }
        }

        public static IEnumerable<Models.List> RetrieveCantons( int pCountryid, string ProvinceId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.List>("[General].[Sp_Counties_Retrieve]",
                    new
                    { 
                        CountryId   = pCountryid,
                        StateId  = ProvinceId
                    });
            }
        }

    }
}