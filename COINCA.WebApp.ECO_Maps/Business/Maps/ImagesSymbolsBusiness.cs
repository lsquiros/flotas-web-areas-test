﻿/************************************************************************************************************
*  File    : Images SymbolsBusiness.cs
*  Summary : Images Symbols Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/14/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECO_Maps.DataAccess;
using ECO_Maps.Models;
using System.Linq;

namespace ECO_Maps.Business
{
    public class ImagesSymbolsBusiness
    {
        /// <summary>
        /// Retrieve ImagesSymbols
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.ImagesSymbols> RetrieveLayerMaps()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.ImagesSymbols>("[Maps].[Sp_ImagesSymbols_Retrieve]", null);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}