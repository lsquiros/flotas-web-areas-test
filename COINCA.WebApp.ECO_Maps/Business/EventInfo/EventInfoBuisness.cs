﻿/************************************************************************************************************
*  File    : EventInfoBusiness.cs
*  Summary : Event Info Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/21/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/

using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;
using System.Linq;

namespace COINCA.WebApp.ECO_Maps.Business.EventInfo
{
    public class EventInfoBuisness
    {
        /// <summary>
        /// Retrieve Event Info
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public Models.EventInfo RetrieveEventInfo(int pEventId)
        {
            List<Models.EventInfo> result = new List<Models.EventInfo>();
            using (var dba = new DataBaseAccess("ConnectionStringNameCAD"))
            {
                return dba.ExecuteReader<Models.EventInfo>("[General].[Sp_UnidEventInfo_Retrieve]",
                    new
                    {
                        EventId = pEventId
                    }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    }
}
