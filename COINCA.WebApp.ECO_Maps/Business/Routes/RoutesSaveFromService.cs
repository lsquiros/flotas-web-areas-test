﻿using AspMap;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace ECO_Maps.Business.Routes
{
    public class RoutesSaveFromService
    {

        Utilities.Maps _mapsFunctions = new Utilities.Maps();

        /// <summary>
        /// Add or Edit Routes from Service
        /// </summary>
        /// <param name="pointList"></param>
        /// <param name="countryCode"></param>
        /// <param name="routeId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool AddRoutesFromService(string pointList, string countryCode, int routeId, int userId)
        {
            try 
	        {
                List<Models.RoutesSegmentsBase.RoutesPoints> points = JsonConvert.DeserializeObject<List<Models.RoutesSegmentsBase.RoutesPoints>>(pointList);
                  
                Models.RoutesSegmentsBase model = new Models.RoutesSegmentsBase();
                model.Points = points;
                model.Segments = CalculatedSegmentList(points, countryCode);

                using(var business = new RoutesBusiness())
                {
                    business.AddOrEditRoutesDetail(userId, routeId, model);
                }

                return true;
	        }
	        catch (Exception e)
	        {
                return false;
	        }
        }

        public List<Models.RoutesSegmentsBase.RoutesSegments> CalculatedSegmentList(List<Models.RoutesSegmentsBase.RoutesPoints> pPointsList, string countryCode)
        {
            List<Models.RoutesSegmentsBase.RoutesSegments> pSegmentList = new List<Models.RoutesSegmentsBase.RoutesSegments>();

            for (int i = 0; i < pPointsList.Count - 1; i++)
            {
                Models.RoutesSegmentsBase.RoutesPoints point1 = pPointsList.FirstOrDefault(x => x.Order == i + 1);
                Models.RoutesSegmentsBase.RoutesPoints point2 = pPointsList.FirstOrDefault(x => x.Order == i + 2);
                Models.RoutesSegmentsBase.RoutesSegments seg = pSegmentList.FirstOrDefault(x => x.StartReference == point1.PointReference && x.EndReference == point2.PointReference);

                if (point1 != null && point2 != null)
                {
                    AspMap.Route route = _mapsFunctions.findRoute(countryCode, new Point(Convert.ToDouble(point1.Longitude), Convert.ToDouble(point1.Latitude)), new Point(Convert.ToDouble(point2.Longitude), Convert.ToDouble(point2.Latitude)), "Shortest");
                    AspMap.Polyline polyline = new AspMap.Polyline();
                    AspMap.Points points = new Points();
                    int x = 0;
                    if (route != null)
                    {
                        for (x = 0; x < route.RoutePath.Count; x++)
                        {
                            points.Add(new Point(route.RoutePath[x].X, route.RoutePath[x].Y));
                        }
                        polyline.Add(points);
                        seg = new Models.RoutesSegmentsBase.RoutesSegments();
                        seg.Poliyline = GetStringFromPolyline(polyline);
                        seg.StartReference = pPointsList[i].PointReference;
                        seg.EndReference = pPointsList[i + 1].PointReference;
                        double time = 0;
                        double distance = 0;
                        for (int e = 0; e < route.Streets.Count; e++)
                        {
                            distance = distance + route.Streets[e].Distance;
                            time = time + Math.Round((Convert.ToDouble(route.Streets[e].Time) / 60), 1);
                        }
                        seg.Distance = distance;
                        seg.Time = Convert.ToInt32(time);
                        pSegmentList.Add(seg);
                    }
                }                
            }

            return pSegmentList;
        }

        string GetStringFromPolyline(AspMap.Polyline pPolyline)
        {
            string result = string.Empty;
            for (int x = 0; x < pPolyline.PointCount; x++)
            {
                if (result != string.Empty)
                {
                    result = result + ", ";
                }
                result = result + pPolyline.GetPoint(x).X;
                result = result + " ";
                result = result + pPolyline.GetPoint(x).Y;
            }
            result = "LINESTRING (" + result + ")";
            return result;
        }

        
    }
}