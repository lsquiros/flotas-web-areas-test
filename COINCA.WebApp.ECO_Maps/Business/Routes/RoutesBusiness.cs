﻿/************************************************************************************************************
*  File    : RoutesBusiness.cs
*  Summary : Routes Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/30/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ECOsystem.DataAccess;

namespace ECO_Maps.Business.Routes
{
    public class RoutesBusiness : IDisposable
    {
        /// <summary>
        /// Retrieve RoutesDetail
        /// </summary>
        /// <param name="routeId">todo: describe routeId parameter on RetrieveRoutesDetail</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>

        public Models.RoutesSegmentsBase RetrieveRoutesDetail(int? routeId)
        {
            using (var dba = new DataBaseAccess())
            {
                Models.RoutesSegmentsBase result = new Models.RoutesSegmentsBase();
                List<Models.RoutesSegmentsBase.RoutesPoints> pointsList =
                    (List<Models.RoutesSegmentsBase.RoutesPoints>)dba.ExecuteReader<Models.RoutesSegmentsBase.RoutesPoints>("[Efficiency].[Sp_RoutesPoints_Retrieve]",
                    new
                    {
                        RouteId = routeId,
                    });
                List<Models.RoutesSegmentsBase.RoutesSegments> segmentsList =
                    (List<Models.RoutesSegmentsBase.RoutesSegments>)dba.ExecuteReader<Models.RoutesSegmentsBase.RoutesSegments>("[Efficiency].[Sp_RoutesSegments_Retrieve]",
                    new
                    {
                        RouteId = routeId,
                    });
                result.Points = pointsList;
                result.Segments = segmentsList;
                return result;
            }
        }

        /// <summary>
        /// Retrieve Stops
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.Stop> RetrieveStops(int? vehicleId, string startDate, string endDate)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.Stop>("[Efficiency].[Sp_RoutesStopsList_Retrieve]", 
                    new 
                    {
                        VehicleId = vehicleId,
                        StartDate = startDate,
                        EndDate = endDate
                    });
            }
        }


        public int AddNewCommerce(int PointIdCommerce, int UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_AddNewCommercebyPoint]",
                    new
                    {
                       PointId = PointIdCommerce,
                       UserIdId = UserId
                    });
            }
         return 0;
        }
        
        /// <summary>
        /// Performs the maintenance Insert or Update of the entity Route
        /// </summary>
        /// <param name="model">Model or class passed as parameter  which contains all properties to perform insert or update operations</param>
        public void AddOrEditRoutesDetail(int pLoggedUserId, int pRouteId, Models.RoutesSegmentsBase model)
        {            
            XmlSerializer xsSegments = new XmlSerializer(typeof(System.Collections.Generic.List<Models.RoutesSegmentsBase.RoutesSegments>));
            StringWriter swwSegments = new StringWriter();
            XmlWriter writerSegments = XmlWriter.Create(swwSegments);
            xsSegments.Serialize(writerSegments, model.Segments);
            string xmlSegments = swwSegments.ToString();

            XmlSerializer xsPoints = new XmlSerializer(typeof(System.Collections.Generic.List<Models.RoutesSegmentsBase.RoutesPoints>));
            StringWriter swwPoints = new StringWriter();
            XmlWriter writerPoints = XmlWriter.Create(swwPoints);
            xsPoints.Serialize(writerPoints, model.Points);
            string xmlPoints = swwPoints.ToString();
            
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Efficiency].[Sp_RoutesDetail_AddOrEdit]",
                    new
                    {
                        RouteId = pRouteId,
                        LoggedUserId = pLoggedUserId,
                        XmlPoints = xmlPoints,
                        XmlSegments = xmlSegments
                    });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }



    }


}
