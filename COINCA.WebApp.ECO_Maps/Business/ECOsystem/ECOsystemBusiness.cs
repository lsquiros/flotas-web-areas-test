﻿/************************************************************************************************************
*  File    : AtrackBusiness.cs
*  Summary : Atrack Business Methods
*  Author  : Danilo Hidalgo G
*  Date    : 10/30/2014
* 
*  Copyright 2014 COINCA, All rights reserved
*  This software and documentation is the confidential and proprietary information of COINCA S.A.
*  You shall not disclose such Confidential Information and shall use it only in accordance with the
*  terms of the license agreement you entered into with this company.
* 
************************************************************************************************************/
using System;
using System.Collections.Generic;
using ECOsystem.DataAccess;

namespace ECO_Maps.Business.ECOsystem
{
    public class EcOsystemBusiness
    {
        /// <summary>
        /// Retrieve Vehicles By Level
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.VehiclesInfo> RetrieveVehiclesByLevel(string pId, int pLevel)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.VehiclesInfo>("[dbo].[Sp_VehiclesByLevel_Retrieve]",
                   new
                   {
                       Id = pId,
                       Level = pLevel
                   });
                //foreach (var x in list) { if (result == "0") { result = x.UnitId.ToString(); } else { result = result + "," + x.UnitId.ToString(); } }
            }
        }

        /// <summary>
        /// Retrieve Vehicles By Level
        /// </summary>
        /// <param name="vehicleCategoryId">The PRIMARY KEY uniquely that identifies each record of this model</param>
        /// <param name="key">Key is the search criteria to find a record using a value and it trying to lookup the value through multiple columns</param>
        /// <returns>An IEnumerable<T> T is the class or model which contains all properties and retrieves a collection of the model from database</returns>
        public IEnumerable<Models.CustomerThresholds> RetrieveCustomerThresholdsBusiness(int pCustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Models.CustomerThresholds>("[General].[Sp_CustomerThresholds_Retrieve]",
                   new
                   {
                       CustomerId = pCustomerId
                   });
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
    
    
    }
}
