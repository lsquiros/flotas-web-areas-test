﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reconstructing.aspx.cs" Inherits="ECO_Maps.Reconstructing1" EnableEventValidation="false" %>
<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>Reconstruccion</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <%: Styles.Render("~/bundles/css") %>
    <%: Styles.Render("~/bundles/timeline_css") %>
     <link href="css/materialize.css" rel="stylesheet" />
     <link href="css/datepicker3.css" rel="stylesheet" /> 
     <link href="css/jquery.timepicker.css" rel="stylesheet" />   
</head>

<body>
    <form id="ReconstructiongForm" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="Div1">
            <section class="content-wrapper main-content clear-fix">
                <section id="Content2">
                    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="loader" CssClass="loaderMap" runat="server">
                                <div style="text-align: center;">
                                    <img src="Content/Images/loader.GIF" class="image-loader" />
                                </div>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>                  

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                            <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
                            <asp:HiddenField ID="refrehing" runat="server" Value="false" />
                            <asp:HiddenField ID="width" runat="server" Value="0" />
                            <asp:HiddenField ID="height" runat="server" Value="0" />
                            <asp:HiddenField ID="refreshDate" runat="server" Value="true" />
                            <%--<div class="div100 ReconstructingTop">
                               <div class="ReconstructingPeriod">
                                    <strong>Periodo:</strong>
                                </div>
                                <p id="pickergroup" style="margin:0">

                                    <asp:TextBox ID="txtStartDate" runat="server" Text="Fecha Inicial" CssClass="ReconstructingDate txtDate form-control date start"></asp:TextBox>
                                    <asp:TextBox ID="TxtStartTime" runat="server" Text="Hora Inicial" CssClass="ReconstructingDdl txtDate form-control time start"></asp:TextBox>
                                    <asp:TextBox ID="txtEndDate" runat="server"  Text="Fecha Final" CssClass="ReconstructingDate txtDate form-control date end"></asp:TextBox>
                                    <asp:TextBox ID="TxtEndTime" runat="server" Text="Hora Final" CssClass="ReconstructingDdl txtDate form-control time end"></asp:TextBox>
                                </p>
                                <div class="ReconstructingBtnGroupTop">
                                    <div class="btn-group">
                                       <%-- <asp:Button  runat="server" Text="Cancelar"  OnClick="btnCancel_Click" OnClientClick="">
                                            <span class="mdi mdi-close"> </span>
                                        </asp:Button>--%>
<%--
                                        <button id="btnCancel" runat="server" class="btn btn-default" onserverclick="btnCancel_Click" onclick="setvalue(false); showTimeline();">
                                            <i class="mdi mdi-close" style="font-size: 14px; float: left;"></i>Cancelar
                                        </button>

                                        <button ID="btnOk" runat="server" class="btn btn-primary" onserverclick="btnOk_Click" onclick="setvalue(true); showTimeline();" >
                                            <i class="mdi mdi-vector-polyline" style="font-size: 14px; float: left;"></i>Cargar Recorrido
                                        </button>-
                                    </div>
                                </div>

                            </div>--%>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <div id="divColorListMonitor">
                                <div class="divColorListMonitor hidden">
                                    <%--<div class="itemColorList">
                                        <div class="monitorMovimiento info mdi mdi-car"></div>
                                        <div class="ColorListText"> Encendido </div>
                                    </div>
                                    <div class="itemColorList">
                                        <div class="monitorApagado info mdi mdi-car"></div>
                                        <div class="ColorListText"> Apagado </div>
                                    </div>--%>
                                    <div class="itemColorList">
                                        <div class="monitorDetenido info mdi mdi-car"></div>
                                        <div class="ColorListText"> Detenido </div>
                                    </div>
                                    <div class="itemColorList">
                                        <div class="monitorSinTransmitir info mdi mdi-car"></div>
                                        <div class="ColorListText"> Sin Transmitir </div>
                                    </div>

                                     <div class="ColorListText" style="width: 240px;">
                                           © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>  
                                        </div>
                                
                                   <%-- <div class="itemColorList">
                                        <div class="ColorListText">
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged" />
                                        </div>
                                    </div>--%>
                                </div>

                                <div class="GeoFences-Content">
                                    <div class="div100">
                                        <div class="ContentMap">
                                            <asp:Panel ID="pnlCoinca" runat="server" Visible="true">
                                                <aspmap:Map ID="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapCoinca_RefreshAnimationLayer" AnimationInterval="2000" EnableAnimation="true"></aspmap:Map>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlGoogle" runat="server" Visible="false">
                                                <aspmap:Map ID="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapGoogle_RefreshAnimationLayer" AnimationInterval="2000" EnableAnimation="true"></aspmap:Map>
                                            </asp:Panel>
                                        <%--    <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                                                <div class="LeftMenu">
                                                    <div style="text-align: center;" >
                                                        <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                                         <%--   <asp:ImageButton ID="CoincaMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/openstreetmaps.png" OnClientClick="changeMap('OSM')" ></asp:ImageButton>
                                                            <asp:ImageButton ID="GoogleMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                                       --% > </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <div class="mapMenu-controls">
                                                <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                <i class="glyphicon glyphicon-chevron-left"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                <i class="glyphicon glyphicon-chevron-right"></i>
                                                </asp:LinkButton>
                                            </div>--%>
                                            <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                                            <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                                            <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="div100">
                                    <%--     <div class="ReconstructingBtnGoupFooter">
                                      <div class="btn-group">
                        <asp:LinkButton ID="btnStart" runat="server" class="btn btn-primary" OnClick="btnStart_Click"><span class="glyphicon glyphicon-fast-backward"></span></asp:LinkButton>
                        <asp:LinkButton ID="btnBackward" runat="server" class="btn btn-primary" OnClick="btnBackward_Click"><span class="glyphicon glyphicon-backward"></span></asp:LinkButton>
                        <asp:LinkButton ID="btnForward" runat="server" class="btn btn-primary" OnClick="btnForward_Click"><span class="glyphicon glyphicon-forward"></span></asp:LinkButton>
                        <asp:LinkButton ID="btnEnd" runat="server" class="btn btn-primary" OnClick="btnEnd_Click"><span class="glyphicon glyphicon-fast-forward"></span></asp:LinkButton>
                    </div>  </div> --%>

                                     
                                </div>

                                <%--<div class="div75 ReconstructingFooterInfo">
                                    <div class="div20">
                                        <div class="ReconstructingInfoRoad"></div>
                                        <div class="Reconstructing-title-small">
                                            <strong>Trayecto
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div40">
                                        <div class="ReconstructingInfoSpeeding">
                                        </div>
                                        <div class="Reconstructing-title-large">
                                            <strong>Exceso de Velocidad
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div20">
                                        <div class="divDistance-Image">
                                            <img class="Distance-Image" src="Content/Images/Markers/Marker-Cian.png">
                                        </div>
                                        <div class="VehicleRoute-title">
                                            <strong>Inicio
                                            </strong>
                                        </div>
                                    </div>
                                     <div class="div20">
                                        <div class="divDistance-Image">
                                            <img class="Distance-Image" src="Content/Images/Markers/Marker-Green.png">
                                        </div>
                                        <div class="VehicleRoute-title">
                                            <strong>Final
                                            </strong>
                                        </div>
                                    </div>
                                </div>  --%>                           

                                <div class="hidden">
                                    <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
                                    <asp:Button ID="btnRefresh" runat="server" Text="Button" OnClick="btnRefresh_Click" />
                                    <asp:Button ID="btnPlay" runat="server" Text="Button" OnClick="btnPlay_Click" />
                                    <asp:Button ID="btnPause" runat="server" Text="Button" OnClick="btnPause_Click" />
                                    <asp:Button ID="btnOk" runat="server" Text="Button" OnClick="btnOk_Click" />
                                    <asp:TextBox ID="txtReset" runat="server" Text=""/>
                                    <asp:TextBox ID="txtStartDate" runat="server" Text=""></asp:TextBox>
                                    <asp:TextBox ID="txtEndDate" runat="server" Text=""></asp:TextBox>
                                </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </section>
            </section>
            
        </div>

       <%--<div id="panel_timeline" class="hidden">

            <div class="cd-horizontal-timeline" >   
                   <div style="display:inline-block; margin-left:50%; margin-top:20px">
                    <asp:LinkButton ID="btnPlay" runat="server" class="btn-floating btn-large teal mdi mdi-play mdi-24px" OnClick="btnPlay_Click" Visible="false"></asp:LinkButton>
                    <asp:LinkButton ID="btnPause" runat="server" class="btn-floating btn-large teal mdi mdi-pause mdi-24px" OnClick="btnPause_Click"></asp:LinkButton>
                </div>  

           <div class="timeline" style="display:inline-block; visibility:hidden">
                    <div class="events-wrapper">
                        <div class="events">
                            <ol>--%>
                               <%-- <asp:Literal ID="Literal1" runat="server"></asp:Literal>--%>
                                <%--<li><a data-date="08/24/2016T08:00" class="selected">8am</a></li>                                                         
                            </ol>

                            <span class="filling-line" aria-hidden="true" style="visibility:hidden"></span>
                        </div>
                        <!-- .events -->
                    </div>
                    <!-- .events-wrapper -->
                    <ul class="cd-timeline-navigation">
                        <li><a class="prev inactive">Prev</a></li>
                        <li><a class="next">Next</a></li>
                    </ul>
                    <!-- .cd-timeline-navigation -->
                </div>
                
                <!-- .timeline -->
            </div>

        </div>--%>

    </form>

    <%: Scripts.Render("~/bundles/jquery") %>
    <script src="Scripts/jquery.ba-resize.min.js"></script>
    <%: Scripts.Render("~/bundles/datetimepicker") %>
    <script src="Scripts/main.js"></script>
    
    <script>       
        var pausing = '<%=Session["ReconstructingPaused"]%>';
        this.show_buttons = false;
        if (pausing)
        {
            this.show_buttons = true;
            showTimeline();
        }
        

        $(window).resize(function () {
            resize();
        });

        $(function () {
            resize();
            refresh();           
            fncLoadDate();         
        });   

        function refresh() {        
            var timeout = 2000;
            var action = function () {
                
                if (document.getElementById('<%= refrehing.ClientID%>').value === 'true') {
                $('#<%=btnRefresh.ClientID %>').click();
            }
            else {
                return false;
            }
                return false;
            };
        setInterval(action, timeout);
    }

    function fncCancel() {
        location.reload();
    }

    function setvalue(value) {
        this.show_buttons = value;
    }

    function showTimeline()
    {
        if (show_buttons) {
            $("#panel_timeline").removeClass("hidden");
            $("#panel_timeline").addClass(" ");           
        }
        else {
            $("#panel_timeline").addClass("hidden");
        }
    }


    function resize() {
        updateDivColor();
        var width = $(window).width() - 105;
        var height = $(window).height();
        $('#<% =width.ClientID %>').attr('value', width);
        $('#<% =height.ClientID %>').attr('value', height);
        $("#ContentSearch").width(width);
        $('#<%=btnResize.ClientID %>').click();
    }

    function updateDivColor() {
        var height = $(window).height();
        $(".pnlSatellite").css('margin-left', "10% !important");
        $(".pnlSatellite").css('margin-top:', "-112px !important");
        $(".divColorListMonitor").css('margin-top', height - 40 + "px");
        $(".divColorListMonitor").removeClass("hidden");
        $(".divColorListMonitor").css('min-width', "0px");
        $('.ContentMap').css('margin-left', "0px");
    }

    function fncEnableButtons() {
        $("#ContentPlaceHolder1_btnStart").removeAttr("disabled");
        $("#ContentPlaceHolder1_btnBackward").removeAttr("disabled");
        $("#ContentPlaceHolder1_btnForward").removeAttr("disabled");
        $("#ContentPlaceHolder1_btnEnd").removeAttr("disabled");
    }

    function fncDisableButtons() {
        $("#ContentPlaceHolder1_btnStart").attr("disabled", "disabled");
        $("#ContentPlaceHolder1_btnBackward").attr("disabled", "disabled");
        $("#ContentPlaceHolder1_btnForward").attr("disabled", "disabled");
        $("#ContentPlaceHolder1_btnEnd").attr("disabled", "disabled");
    }

    function fncDisablePlayButtoms() {
        $("#ContentPlaceHolder1_btnPlay").attr("disabled", "disabled");
        $("#ContentPlaceHolder1_btnPause").attr("disabled", "disabled");
    }

    function fncLoadDate() {
        $('#pickergroup .date').datepicker({
            'language':'es',
            'format': 'dd/mm/yyyy', 'autoclose': true });
        $('#pickergroup .time').timepicker({
            'timeFormat': 'h:i A', 'scrollDefault': 'now', 'showDuration': true });
        $('#pickergroup').datepair();
    }

  function returnResult(serialize) {
        //para devolver llamado a la pantalla que lo llamó (en la misma pantalla)
        parent.postMessage(serialize, "*");
        //para devolver llamado a la pantalla que lo llamó (en pantalla emergente)
        //opener.postMessage(serialize, "*");
    }


  window.addEventListener("message", function (event) {      
      var instruction = event.data.split("|");
      var action = instruction[0];
      document.getElementById('<%=txtReset.ClientID %>').value = instruction[3];
      if (action == 0 || action == "Off") {
          if (instruction[1] == undefined || instruction[1] == null || instruction[1] == "null")
          {
              $('#<%=btnPlay.ClientID %>').click();
          }
          else
          {
              document.getElementById('<%=txtStartDate.ClientID %>').value = instruction[1];
              document.getElementById('<%=txtEndDate.ClientID %>').value = instruction[2];              
              $('#<%=btnOk.ClientID %>').click();
          }
      }
      else
      {
          $('#<%=btnPause.ClientID %>').click();
      }
   });
   </script>

</body>
</html>



