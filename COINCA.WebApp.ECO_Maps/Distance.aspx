﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Distance.aspx.cs" Inherits="ECO_Maps.Distance1" EnableEventValidation="false"%>
<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="css/siteRoutes.css" />

    </head>
    <body>
        <form id="DistanceForm"  method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                </Scripts>
            </asp:ScriptManager>
            <div id="body">
                <section class="content-wrapper main-content clear-fix">
                    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>           
                            <asp:Panel ID="loader" CssClass="loaderRoute" runat="server" >
                                <center>
                                    <img src="Content/Images/loader.GIF" class="image-loader" />
                                </center>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <asp:UpdateProgress ID="updProgress2" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
                        <ProgressTemplate>           
                            <asp:Panel ID="loader1" CssClass="loaderRoute" runat="server" >
                                <center>
                                    <img src="Content/Images/loader.GIF" class="image-loader" />
                                </center>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                        <asp:HiddenField ID="customerId" runat="server" Value="false" />
                        <asp:HiddenField ID="width" runat="server" Value="0" />
                        <asp:HiddenField ID="height" runat="server" Value="0" />

                        <%--<div class="GeoFences-Content">--%>
                            <div id="divSearchResultBtn">
                                <asp:Panel id="pnlResultButtons" runat="server" Visible="false">
                                    <asp:LinkButton ID="btnDirectionsSearch" runat="server" CssClass="btnSearchResult" Width="163" Height="30" OnClick="btnDirectionsSearch_Click" >Direcciones</asp:LinkButton>
                                    <asp:LinkButton ID="btnPointsSearch" runat="server" CssClass="btnSearchResult" Width="163" Height="30" OnClick="btnPointsSearch_Click">Puntos</asp:LinkButton>
                                </asp:Panel>
                            </div>

                            <div id="divSearchResult">
                                <asp:Panel id="Panel10" runat="server">
                                    <asp:Table Visible="false" id="directionsTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                    <asp:Table Visible="false" id="pointsSearchTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                </asp:Panel>
                            </div>

                            <div class="routes-map">
                                <div class="div100">
                                    <div id="ContentSearchId" class="ContentSearch" runat="server">                            
                                        <div class="col-lg-4 text-right">
                                            <div class="LabelSearch">
                                                <strong>
                                                    <asp:Label ID="lblLevel1" class="control-label" runat="server" Text=""></asp:Label>
                                                </strong>
                                            </div>
                                            <div class="ControlSearch">
                                                <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="provinciaList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="provinciaList_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <div class="LabelSearch">
                                                <strong>
                                                    <asp:Label ID="lblLevel2" class="control-label" runat="server" Text=""></asp:Label>
                                                </strong>
                                            </div>
                                            <div class="ControlSearch">
                                                <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="cantonesList" runat="server" AutoPostBack="false" ></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 pull-right" style="padding-right:-100px;">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtSearch" runat="server" name="key" type="text" class="form-control" placeholder="Buscar..."></asp:TextBox>
                                                <span class="input-group-btn">
                                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancelar" class="btn btn-default" onclick="btnCancel_Click" title="Cancelar">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnSearch" runat="server" Text="Buscar" class="btn btn-primary" onclick="btnSearch_Click" title="Buscar">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ContentMap" >
                                        <asp:Panel ID="pnlCoinca" runat="server" CssClass="mapMenu-controls" Visible="true">
                                            <aspmap:Map id="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnPointTool="mapCoinca_PointTool"></aspmap:Map>
                            
                                          <div class="ColorListText" style=" margin-left:40px; width: 240px;">
                                                       © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>  
                                                    </div>

                                        </asp:Panel>
                                        <asp:Panel ID="pnlGoogle" runat="server" CssClass="mapMenu-controls" Visible="false">
                                            <aspmap:Map id="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnPointTool="mapGoogle_PointTool"></aspmap:Map>
                                        </asp:Panel>
                                        <style> /*Estilo para los botones de zoom automatico*/
                                            .imageZoom{
                                                margin-left: -7px;
                                                margin-top: -2px;
                                                height: 53px;
                                                width: 53px;
                                                border: 1px outset #ccc !important;
                                            }
                                        </style>
                                        <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                                            <div class="LeftMenu" >
                                                <center>
                                                    <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                                       <%-- <asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('GoogleRoad')" ></asp:ImageButton>
                                                        <aspmap:MapToolButton id="ZoomInTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapCoinca" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="zoomOutTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapCoinca" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                        <aspmap:MapToolButton id="panTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapCoinca" MapTool="Pan" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="pointTool" Visible="true" class="mapMenu" runat="server" ToolTip="Point Tool" ImageUrl="~/TOOLS/Custom/Pin.png" Map="mapCoinca" MapTool="Point" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="centerTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapCoinca" MapTool="Center" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton ID="distanceTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapCoinca" MapTool="Distance" ToolTip="Measure Distance" />
                                                        <button onclick="zoomIn()" type="button" class="mapMenu" style="margin-top: 4px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomIn.png" /></button>
                                                        <button onclick="zoomOut()" type="button" class="mapMenu" style="margin-top: 5px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomOut.png" /></button>

                                                      <%--  <asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                                        <aspmap:MapToolButton id="ZoomInToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapGoogle" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="zoomOutToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapGoogle" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                        <aspmap:MapToolButton id="panToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapGoogle" MapTool="Pan" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="pointToolGoogle" Visible="false" class="mapMenu" runat="server" ToolTip="Point Tool" ImageUrl="~/TOOLS/Custom/Pin.png" Map="mapGoogle" MapTool="Point" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="centerToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapGoogle" MapTool="Center" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton ID="distanceToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapGoogle" MapTool="Distance" ToolTip="Measure Distance" />
                                                    </div>
                                                </center>
                                            </div>
                                        </asp:Panel>
                                        <div class="mapMenu-controls">
                                            <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                                <i class="glyphicon glyphicon-chevron-left"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <%--</div>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
    
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="routes-table">
                            <div class="div100">
                                <div class="panel-group" id="accordion">
                                     <div class="panel panel-default">
                                        <div class="panel-heading pointer">
                                            <h4 class="panel-title">
                                               <strong>Inicio</strong>
                                               <asp:Image ID="imgStartPoint" ImageUrl="Content/Images/Markers/Marker-Cian.png" runat="server" CssClass="Distance-Image"/>
                                            </h4>
                                        </div>                            
                                        <asp:Panel ID="editPoints" runat="server" Visible="true">
                                            <div class="ContentTableRoute">
                                                <asp:TextBox ID="txtStartLongitude" name="txtStartLongitude" runat="server" class="form-control" placeholder="Longitud" disabled="disabled"></asp:TextBox>
                                            </div>
                                            <div class="ContentTableRoute">
                                                <asp:TextBox ID="txtStartLatitude" name="txtStartLatitude" runat="server" class="form-control" placeholder="Latitud" disabled="disabled"></asp:TextBox>
                                            </div>
                                            <div class="ContentTableRoute">                                    
                                                <div class="btn-group" style="margin-left:170px;">
                                                    <asp:LinkButton ID="btnEditStartPoint" runat="server" class="btn btn-primary" ToolTip="Editar" OnClick="btnEditStartPoint_Click">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCenterStartPoint" runat="server" class="btn btn-primary" ToolTip="Centrar" OnClick="btnCenterStartPoint_Click">
                                                        <i class="glyphicon glyphicon-map-marker"></i>
                                                    </asp:LinkButton>
                                                </div>                                    
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <br />
                                    <div class="panel panel-default">
                                        <div class="panel-heading pointer">
                                            <h4 class="panel-title">
                                               <strong>Fin</strong>
                                               <asp:Image ID="Image1" ImageUrl="Content/Images/Markers/Marker-Green.png" runat="server" CssClass="Distance-Image"/>
                                            </h4>
                                        </div>                            
                                        <asp:Panel ID="Panel1" runat="server" Visible="true">
                                            <div class="ContentTableRoute">
                                                <asp:TextBox ID="txtEndLongitude" name="txtEndLongitude" runat="server" class="form-control" placeholder="Longitud" disabled="disabled"></asp:TextBox>
                                            </div>
                                            <div class="ContentTableRoute">
                                                <asp:TextBox ID="txtEndLatitude" name="txtEndLatitude" runat="server" class="form-control" placeholder="Latitud" disabled="disabled"></asp:TextBox>
                                            </div>
                                            <div class="ContentTableRoute">
                                                <div class="btn-group" style="margin-left:170px;">
                                                    <asp:LinkButton ID="btnEditEndPoint" runat="server" class="btn btn-primary" ToolTip="Editar" OnClick="btnEditEndPoint_Click">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCenterEndPoint" runat="server" class="btn btn-primary" ToolTip="Editar" OnClick="btnCenterEndPoint_Click">
                                                        <i class="glyphicon glyphicon-map-marker"></i>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>

                                    <div class="panel panel-default">                                                     
                                        <asp:Panel ID="Panel2" runat="server" Visible="true">
                                            <div class="ContentTableRoute">
                                                <div class="row">
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="ddlAlgorithm" runat="server" AutoPostBack="true" CssClass="select2-container form-control select2-container-active select2-dropdown-open" OnSelectedIndexChanged="ddlAlgorithm_SelectedIndexChanged" Width="140px">
                                                            <asp:ListItem Text="Ruta Corta" Value="Shortest" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Ruta Rápida" Value="Quickest"></asp:ListItem>
                                                        </asp:DropDownList>                               
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="btn-group">
                                                            <asp:LinkButton ID="btnCancelEvent" runat="server" class="btn btn-default" ToolTip="Cancelar" OnClick="btnCancelEvent_Click">
                                                                <i class="glyphicon glyphicon-remove"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnInvest" runat="server" class="btn btn-primary" ToolTip="Invertir Ruta" OnClick="btnInvest_Click">
                                                                <i class="glyphicon glyphicon-random"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ContentTableRoute">
                                                <strong>Distancia:</strong>
                                                <asp:Label ID="lblDistance" runat="server" Text="0.00 km"></asp:Label>
                                            </div>
                                            <div class="ContentTableRoute">
                                                <strong>T. Estimado:</strong>
                                                <asp:Label ID="lblTime" runat="server" Text="00:00 min."></asp:Label>
                                            </div>                               
                                            <div class="ContentTableRoute">
                                                 <asp:Panel ID="pnlSatellite" runat="server">  <%--CssClass="pnlSatellite f-right">--%>
                                                    <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged"/>
                                                </asp:Panel>
                                            </div>                                
                                        </asp:Panel>
                                    </div> 
                        
                                    <div style="visibility:hidden; height:1px">
                                        <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />              
                                    </div>
                               </div>             
                            </div>
                        </div>            
                    </ContentTemplate>
                </asp:UpdatePanel>
        
                   <%: Scripts.Render("~/bundles/jquery") %>   
                    <script src="Scripts/jquery.ba-resize.min.js"></script>     
                    <script>
                    $(function () {
                        resize();
                        //refresh();
                    });

                    $(window).resize(function () {
                        resize();
                    });

                    function resize() {
                        //updateDivColor();
                        var width = $(window).width() - 300;// -40;
                        var height = $(window).height() - 100;//- 40; //-310;//-290

                        $('#<% =width.ClientID %>').attr('value', width);
                        $('#<% =height.ClientID %>').attr('value', height);
                        $("#ContentSearch").width(width);
                        $('#<%=btnResize.ClientID %>').click();
                    }

                    function LoadGeoFenceValue(value) {
                        parent.postMessage(value, "*");
                    }
                    var map = AspMap.getMap('<%=mapCoinca.ClientID%>');

                    function zoomIn() {
                        map.zoomIn();
                    }

                    function zoomOut() {
                        map.zoomOut();
                    }
                </script>
                </section>
            </div>
        </form>
    </body>
</html>