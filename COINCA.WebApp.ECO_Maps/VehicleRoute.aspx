﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="VehicleRoute.aspx.cs" Inherits="ECO_Maps.VehicleRoute1" EnableEventValidation="false"%>
<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>VehicleRoute</title>
        <%: Styles.Render("~/bundles/css") %>
        <link rel="stylesheet" href="css/datepicker3.css" />
    </head>
    <body>
        <form id="VehicleRouteForm"  method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
            </Scripts>
        </asp:ScriptManager>
            <div id="body">
                <section class="content-wrapper main-content clear-fix">
                    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>           
                            <asp:Panel ID="loader" CssClass="loaderGeoFences" runat="server" >
                                <div style="text-align: center;" >
                                    <img src="Content/Images/loader.GIF" class="image-loader" alt=""/>
                                </div>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="VehicleId" runat="server" Value="false" />
                            <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
                            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                            <asp:HiddenField ID="DistanceMargin" runat="server" Value="false" />
                            <asp:HiddenField ID="StopTime" runat="server" Value="false" />
                            <asp:HiddenField ID="CustomerId" runat="server" Value="0" />
                            <asp:HiddenField ID="width" runat="server" Value="0" />
                            <asp:HiddenField ID="height" runat="server" Value="0" />
                            <div class="GeoFences-Content">   
                                <div class="SearchBox">
                                    <div class="div60">
                                        <div class="div20 VehicleRouteControl">
                                            <strong>
                                                <asp:Label ID="lblRoute" class="control-label" runat="server" Text="Ruta: "></asp:Label>
                                            </strong>
                                        </div>
                                        <div class="div80">
                                            <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="routeList" runat="server" AutoPostBack="true" ></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="div30">
                                        <div class="div50 VehicleRouteControl">
                                            <strong>
                                                <asp:Label ID="lblDate" class="control-label" runat="server" Text="Fecha: "></asp:Label>
                                            </strong>
                                        </div>
                                        <div class="div50">
                                            <asp:TextBox ID="txtDate" runat="server" name="txtDate" type="text" class="form-control datepicker" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="div5">
                                        <div class="BtnSearch">
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnOk" runat="server" class="btn btn-primary" title="Buscar" OnClick="btnOk_Click">
                                                    <i class="glyphicon glyphicon-ok"></i>
                                                </asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>             
                                <div class="ContentSearch">            
                                    <div class="ContentMap" >
                                        <asp:Panel ID="pnlCoinca" runat="server" CssClass="mapMenu-controls" Visible="true">
                                            <aspmap:Map id="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" ></aspmap:Map>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlGoogle" runat="server" CssClass="mapMenu-controls" Visible="false">
                                            <aspmap:Map id="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" ></aspmap:Map>
                                            <asp:Panel ID="pnlSatellite" runat="server" CssClass="pnlSatellite">
                                                <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged"/>
                                            </asp:Panel>
                                        </asp:Panel>
                                        <style> /*Estilo para los botones de zoom automatico*/
                                            .imageZoom{
                                                margin-left: -7px;
                                                margin-top: -2px;
                                                height: 53px;
                                                width: 53px;
                                                border: 1px outset #ccc !important;
                                            }
                                        </style>
                                        <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                                            <div class="LeftMenu" >
                                                <div style="text-align: center;" >
                                                    <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                                      <%--  <asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('GoogleRoad')" ></asp:ImageButton>
                                                        <aspmap:MapToolButton id="ZoomInTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapCoinca" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="zoomOutTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapCoinca" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                        <aspmap:MapToolButton id="panTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapCoinca" MapTool="Pan" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="centerTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapCoinca" MapTool="Center" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton ID="distanceTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapCoinca" MapTool="Distance" ToolTip="Measure Distance" />
                                                        <button onclick="zoomIn()" type="button" class="mapMenu" style="margin-top: 4px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomIn.png" /></button>
                                                        <button onclick="zoomOut()" type="button" class="mapMenu" style="margin-top: 5px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomOut.png" /></button>

                                                        <%--<asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                                        <aspmap:MapToolButton id="ZoomInToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapGoogle" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="zoomOutToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapGoogle" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                        <aspmap:MapToolButton id="panToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapGoogle" MapTool="Pan" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="centerToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapGoogle" MapTool="Center" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton ID="distanceToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapGoogle" MapTool="Distance" ToolTip="Measure Distance" />
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="mapMenu-controls">
                                            <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                                <i class="glyphicon glyphicon-chevron-left"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </asp:LinkButton>
                                        </div>
                                        <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                                        <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                                        <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                                    </div>
                                </div>            
                                <div class="VehicleRouteFooterInfo">
                                    <div class="div16">
                                        <div class="VehicleRouteInfoVehicle">
                                        </div>
                                        <div class="VehicleRoute-title-small">
                                            <strong>
                                                Trayecto
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div16">
                                        <div class="VehicleRouteInfoRoute">
                                        </div>
                                        <div class="VehicleRoute-title-small">
                                            <strong>
                                                Ruta
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div16">
                                        <div class="divDistance-Image">
                                            <img class="Distance-Image" src="Content/Images/Markers/Marker-Green.png">
                                        </div>
                                        <div class="VehicleRoute-title">
                                            <strong>
                                                A tiempo
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div16">
                                        <div class="divDistance-Image">
                                            <img class="Distance-Image" src="Content/Images/Markers/Marker-Yellow.png">
                                        </div>
                                        <div class="VehicleRoute-title">
                                            <strong>
                                                Tarde
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div16">
                                        <div class="divDistance-Image">
                                            <img id="Img1" class="Distance-Image" src="Content/Images/Markers/Marker-Red.png">
                                        </div>
                                        <div class="VehicleRoute-title">
                                            <strong>
                                                No Visitado
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="div16">
                                        <div class="divDistance-Image">
                                            <img id="Img2" class="Distance-Image" src="Content/Images/Markers/Marker-Gray.png">
                                        </div>
                                        <div class="VehicleRoute-title">
                                            <strong>
                                                Pendiente
                                            </strong>
                                        </div>
                                    </div>

                                    <div class="div16">
                                       <div class="ColorListText" style=" margin-left:40px; width: 240px;">
                                            © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>  
                                        </div>
                                    </div>

                   
                     
                                </div>

                                <asp:Panel ID="pnlResult" runat="server" Visible="false">
                                    <div class="VehicleRoute-result">
                                      
                            
                                        <div class="col-lg-12 result-title">
                                            <h4 class="modal-title">Indices de Cumplimiento de Ruta</h4>
                                        </div>

                                        <div class="table-white">
                                            <asp:Table Visible="true" id="summaryTable" runat="server" GridLines="Horizontal" CellSpacing="10" CellPadding="20" CssClass="div100 table-VehicleRoute">
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute"></asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Programado</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Real</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Indice</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute infoColor"></asp:TableHeaderCell>
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                        </div>

                                        <div class="table-clear">
                                            <asp:Table Visible="true" id="colorTable" runat="server" GridLines="Horizontal" CellSpacing="10" CellPadding="20" CssClass="div100">
                                                <asp:TableRow>
                                                    <asp:TableCell CssClass="Threshold vehicleRouteGreen"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblVehicleRouteGreen" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>

                                                    <asp:TableCell CssClass="Threshold vehicleRouteYellow"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblVehicleRouteYellow" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>

                                                    <asp:TableCell CssClass="Threshold vehicleRouteRed"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblVehicleRouteRed" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>

                                                </asp:TableRow>
                                            </asp:Table>
                                        </div>

                                        <div class="col-lg-12 result-title-detail">
                                            <h4 class="modal-title">Detalle de Cumplimiento de Ruta</h4>
                                        </div>

                                        <div class="table-white">
                                            <asp:Table Visible="true" id="resultTable" runat="server" GridLines="Horizontal" CellSpacing="10" CellPadding="20" CssClass="div100 table-VehicleRoute">
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Punto</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Hora Programada</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Hora Real</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute">Diferencia Hora</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="table-header-vehicleRoute infoColor"></asp:TableHeaderCell>
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                        </div>

                                        <div class="table-clear">
                                            <asp:Table Visible="true" id="colorTable2" runat="server" GridLines="Horizontal" CellSpacing="10" CellPadding="20" CssClass="div100">
                                                <asp:TableRow>
                                                    <asp:TableCell CssClass="Threshold vehicleRouteGreen"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label1" runat="server" Text="A Tiempo"></asp:Label>
                                                    </asp:TableCell>

                                                    <asp:TableCell CssClass="Threshold vehicleRouteYellow"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label2" runat="server" Text="Tarde"></asp:Label>
                                                    </asp:TableCell>

                                                    <asp:TableCell CssClass="Threshold vehicleRouteRed"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label3" runat="server" Text="No Visitado"></asp:Label>
                                                    </asp:TableCell>

                                                    <asp:TableCell CssClass="Threshold vehicleRouteGray"></asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label4" runat="server" Text="Pendiente"></asp:Label>
                                                    </asp:TableCell>

                                                </asp:TableRow>
                                            </asp:Table>
                                        </div>

                                        <div class="ContentFooter-back">
                                            <br />
                                            <div class="BtnFooter t-right">   
                                                <asp:LinkButton ID="btnBack" runat="server"  class="btn btn-primary" title="Atrás" OnClick="btnBack_Click" >
                                                    <i class="glyphicon glyphicon-arrow-left"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                
                                <%--<div class="PassControl-result">
                                    <div class="table-white">
                                        <asp:Table Visible="true" id="resultTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="div100">
                                            <asp:TableHeaderRow>
                                                <asp:TableHeaderCell CssClass="table-header">Punto</asp:TableHeaderCell>
                                                <asp:TableHeaderCell CssClass="table-header">Hora Programada</asp:TableHeaderCell>
                                                <asp:TableHeaderCell CssClass="table-header">Hora Real</asp:TableHeaderCell>
                                                <asp:TableHeaderCell CssClass="table-header">Cumplimiento</asp:TableHeaderCell>
                                            </asp:TableHeaderRow>

                                        </asp:Table>

                                    </div>
                                </div>--%>
                            </div>
                            <div style="visibility:hidden">
                                <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click"/>              
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%: Scripts.Render("~/bundles/jquery") %>
                    <script src="Scripts/jquery.ba-resize.min.js"></script>
                    <script src="Scripts/bootstrap-datepicker.js"></script>
                    <script>
                            function fncLoadDate() {
                                $('.datepicker').datepicker({
                                    language: 'es',
                                    minDate: new Date(),
                                    format: 'dd/mm/yyyy',
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            }

                            $(function () {
                                resize();
                                //refresh();
                            });

                            $(window).resize(function () {
                                resize();
                            });

                            function resize() {
                                //updateDivColor();
                                var width = $(window).width();// -40;
                                var height = $(window).height() - 100; //-310;//-290
                                //VehicleRouteFooterInfo
                                $('#<% =width.ClientID %>').attr('value', width);
                                $('#<% =height.ClientID %>').attr('value', height);
                                $("#ContentSearch").width(width);
                                $('#<%=btnResize.ClientID %>').click();
                            }

                            var map = AspMap.getMap('<%=mapCoinca.ClientID%>');

                            function zoomIn() {
                                map.zoomIn();
                            }

                            function zoomOut() {
                                map.zoomOut();
                            }
                        </script>
                </section>
            </div>
        </form>
    </body>
</html>