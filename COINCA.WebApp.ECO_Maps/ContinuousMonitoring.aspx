﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ContinuousMonitoring.aspx.cs" Inherits="ECO_Maps.ContinuousMonitoring" EnableEventValidation="false" %>

<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <%: Styles.Render("~/bundles/css") %>
    <style>
        #UpdatePanel1{
            line-height:0px;
        }
    </style>
</head>
<body>
    <form id="RealTimeForm" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts></Scripts>
        </asp:ScriptManager>
        <div id="body">
            <section class="content-wrapper main-content clear-fix">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
                        <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                        <asp:HiddenField ID="Vehicles" runat="server" Value="0" />
                        <asp:HiddenField ID="InactiveTime" runat="server" Value="0" />
                        <asp:HiddenField ID="ZoneTime" runat="server" Value="0" />
                        <asp:HiddenField ID="TemplateType" runat="server" Value="0" />
                        <asp:HiddenField ID="Center" runat="server" Value="0" />
                        <asp:HiddenField ID="Loaded" runat="server" Value="false" />
                        <asp:HiddenField ID="VehiclesInfoLoaded" runat="server" Value="false" />
                        <asp:HiddenField ID="width" runat="server" Value="0" />
                        <asp:HiddenField ID="height" runat="server" Value="0" />
                        <div class="zoomGroup">
                            <button onclick="zoomIn()" type="button" class="brnZoomPlus"><img src="Content/Images/plus.png" /></button>
                            <button onclick="zoomOut()" type="button" class="brnZoomMinus"><img src="Content/Images/minus.png" /></button>
                        </div>
                        <div id="divColorListMonitor">
                            <div class="divColorListMonitor hidden">
                                <div class="itemColorList">
                                    <div class="monitorMovimiento info mdi mdi-car"></div>
                                    <div class="ColorListText">Encendido </div>
                                </div>
                                <div class="itemColorList">
                                    <div class="monitorApagado info mdi mdi-car"></div>
                                    <div class="ColorListText">Apagado </div>
                                </div>
                                <div class="itemColorList">
                                    <div class="monitorDetenido info mdi mdi-car"></div>
                                    <div class="ColorListText">Detenido </div>
                                </div>
                                <div class="itemColorList">
                                    <div class="monitorSinTransmitir info mdi mdi-car"></div>
                                    <div class="ColorListText">Sin Transmitir </div>
                                </div>
                                <div class="itemColorList">
                                    <div class="monitorGpsEnModoRepodo info mdi mdi-car"></div>
                                    <div class="ColorListText">GPS en Reposo </div>
                                </div>
                                <%--<div class="itemColorList">
                                        <div class="ColorListText">
                                            <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged" />
                                        </div>
                                    </div>--%>


                                <div class="ColorListText">
                                    <asp:Image ID="imgBack" runat="server" ImageUrl="~/Images/icons/logoEco1.png" ToolTip="Volver" onclick="fncReturn()" CssClass="ContinuosMonitoringLinkImg" />
                                </div>
                                <div class="ColorListText" style="margin-left: 40px; width: 240px;">
                                    © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>
                                </div>


                            </div>

                        </div>
                        <div class="GeoFences-Content">
                            <div class="div100">
                                <div class="ContentMap">
                                    <asp:Panel ID="pnlCoinca" runat="server" Visible="true">
                                        <aspmap:Map ID="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapCoinca_RefreshAnimationLayer"></aspmap:Map>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlGoogle" runat="server" Visible="false">
                                        <aspmap:Map ID="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapGoogle_RefreshAnimationLayer"></aspmap:Map>
                                    </asp:Panel>
                                    <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls-up" Visible="false">
                                        <div class="LeftMenu">
                                            <center>
                                    <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                        <asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('GoogleRoad')" ></asp:ImageButton>
                                        <asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                    </div>
                                </center>
                                        </div>
                                    </asp:Panel>
                                    <div class="mapMenu-controls-up">
                                        <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" OnClick="btnHideMapMenu_Click" Visible="false">
                                <i class="glyphicon glyphicon-chevron-left"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" OnClick="btnShowMapMenu_Click" Visible="false">
                                <i class="glyphicon glyphicon-chevron-right"></i>
                                        </asp:LinkButton>
                                    </div>
                                    <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                                    <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                                    <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div style="visibility: hidden;">
                            <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Button" OnClick="btnRefresh_Click" />
                        </div>


                    </ContentTemplate>
                </asp:UpdatePanel>
                <%: Scripts.Render("~/bundles/jquery") %>
                <script src="Scripts/jquery.ba-resize.min.js"></script>
                <script>

                    $(function () {
                        resize();
                        refresh();
                    });

                    $(window).resize(function () {
                        resize();
                    });

                    function refresh() {
                        var timeout = 15000;
                        var action = function () {
                            $('#<%=btnRefresh.ClientID %>').click();
            };
            setInterval(action, timeout);
        }

        function resize() {
            updateDivColor();
            var width = $(window).width();
            var height = $(window).height() - 27;
            $('#<% =width.ClientID %>').attr('value', width);
            $('#<% =height.ClientID %>').attr('value', height);
            $("#ContentSearch").width(width);
            $('#<%=btnResize.ClientID %>').click();
        }

        function updateDivColor() {
            var height = $(window).height();
            $(".pnlSatellite").css('margin-left', "5px !important");
            $(".pnlSatellite").css('margin-top:', "-112px !important");
            $(".divColorListMonitor").css('margin-top', height - 68 + "px");
            $(".divColorListMonitor").removeClass("hidden");
        }

        function returnResult(serialize) {
            //para devolver llamado a la pantalla que lo llamó (en la misma pantalla)
            parent.postMessage(serialize, "*");
            //para devolver llamado a la pantalla que lo llamó (en pantalla emergente)
            //opener.postMessage(serialize, "*");
        }

        function returnPlate(plate) {
            plate = "***" + plate;
            //para devolver llamado a la pantalla que lo llamó (en la misma pantalla)
            parent.postMessage(plate, "*");
            //para devolver llamado a la pantalla que lo llamó (en pantalla emergente)
            //opener.postMessage(plate, "*");
        }


        window.addEventListener("message", function (event) {
            if (event.origin != window.location.origin) {
                var direction = event.data.split(" ");
                var latitude = direction[0];
                var longitude = direction[1];
                fncSeleccionaItem(latitude, longitude);
            }
        });

        var map = AspMap.getMap('<%=mapCoinca.ClientID%>');

        function zoomIn() {
            map.zoomIn();
        }

        function zoomOut() {
            map.zoomOut();
        }

        function fncReturn() {
            var result = "*Re";
            //para devolver llamado a la pantalla que lo llamó (en la misma pantalla)
            parent.postMessage(result, "*");
            //para devolver llamado a la pantalla que lo llamó (en pantalla emergente)
            //opener.postMessage(result, "*");
        }

                </script>


            </section>
        </div>
    </form>
</body>
</html>

