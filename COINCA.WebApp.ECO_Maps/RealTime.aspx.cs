﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using ECO_Maps.Business;
using AspMap.Web;

namespace ECO_Maps
{
    public partial class RealTime : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();

        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            string countryCode = string.Empty;
            string Params = Request.QueryString["Params"];
            string layer = Request.QueryString["Map"];
            if (countryCode == null) { countryCode = string.Empty; }
            if (layer == null) { layer = string.Empty; }
            if (layer == string.Empty) { layerMap.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"]; } else { layerMap.Value = layer; }
            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                CoincaMap.Visible = false;
                GoogleMap.Visible = true;
                mapGoogle.EnableAnimation = false;
                mapCoinca.EnableAnimation = true;
                mapCoinca.AnimationInterval = 1000;
                //pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                CoincaMap.Visible = false;
                GoogleMap.Visible = true;
                mapGoogle.EnableAnimation = false;
                mapCoinca.EnableAnimation = true;
                mapCoinca.AnimationInterval = 1000;                
            }
            else
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                GoogleMap.Visible = false;
                CoincaMap.Visible = true;
                mapCoinca.EnableAnimation = false;
                mapGoogle.EnableAnimation = true;
                mapGoogle.AnimationInterval = 1000;
                //pnlSatellite.Visible = true;
            }

            if (Params != string.Empty)
            {
                string[] p = Params.Split('|');
                countryCode = p[0];
                Session["CountryCode"] = countryCode;
                valueId.Value = p[1];
                Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[2]));
                level.Value = p[3];
                InactiveTime.Value = p[4];
            }
            if (countryCode != string.Empty)
            {
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);
                if (Page.IsPostBack == false)
                {
                    if (layerMap.Value == "Coinca")
                    {
                        TrackVehiclesCoinca(true);
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        TrackVehiclesCoinca(true);
                    }
                    else
                    {
                        TrackVehiclesGoogle(true);
                    }
                }
                MapsBusiness businessMap = new MapsBusiness();
                if (pageLoaded.Value == "false") 
                {
                    pageLoaded.Value = "true";
                    mapCoinca.RemoveAllLayers();
                    mapGoogle.RemoveAllLayers();

                    if (level.Value != "1")
                    {
                        ECO_Maps.Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                        if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null)
                        {
                            AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                            if (layerMap.Value == "Coinca")
                            {
                                mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                            }
                            else if (layerMap.Value == "OSM")
                            {
                                mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                            }
                            else
                            {
                                if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                                mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                            }
                        }
                    }
                }
            }
            if (Page.IsPostBack == false && layerMap.Value != "Coinca" && layerMap.Value != "OSM") 
            {
                if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
            }

        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void mapCoinca_RefreshAnimationLayer(object sender, AspMap.Web.RefreshAnimationLayerArgs e)
        {
            e.NeedRefreshMap = TrackVehiclesCoinca(false);
        }

        protected void mapGoogle_RefreshAnimationLayer(object sender, AspMap.Web.RefreshAnimationLayerArgs e)
        {
            e.NeedRefreshMap = TrackVehiclesGoogle(false);
        }

        bool TrackVehiclesGoogle(bool center)
        {
            int animationLayers = mapGoogle.AnimationLayer.Count;
            mapGoogle.AnimationLayer.Clear();
            List<Models.ReportLast> report = new List<Models.ReportLast>();
            List<Models.VehiclesInfo> list = new List<Models.VehiclesInfo>();
            if (level.Value == "1") 
            {
                report = _mapsFunctions.GetVehicleReportLast(valueId.Value);
            }
            else 
            {
                string vehiclesId = "0";
                if (VehiclesInfoLoaded.Value == "false") 
                {
                    list = (List<Models.VehiclesInfo>)_mapsFunctions.GetVehiclesByLevel(valueId.Value, Convert.ToInt16(level.Value));
                    List<Models.VehiclesInfo> listInTrack = new List<Models.VehiclesInfo>();
                    foreach (var x in list) { if (vehiclesId == "0") { vehiclesId = x.IntrackReference.ToString(); } else { vehiclesId = vehiclesId + "," + x.IntrackReference; } }
                    listInTrack = (List<Models.VehiclesInfo>)_mapsFunctions.RetrieveTntrackReferenceDetail(vehiclesId);

                    list = (from l1 in list 
                            join l2 in listInTrack on l1.IntrackReference equals l2.IntrackReference
                            select new Models.VehiclesInfo {
                            IntrackReference = l1.IntrackReference,
                            Name = l1.Name,
                            PlateId = l1.PlateId,
                            UnitId = l2.UnitId,
                            VehicleId = l1.VehicleId
                        }).ToList();

                    Session["listVehiclesInfo"] = list;
                    VehiclesInfoLoaded.Value = "true";
                } 
                else 
                {
                    list = (List<Models.VehiclesInfo>)Session["listVehiclesInfo"];
                }
                foreach (var x in list) { if (vehiclesId == "0") { vehiclesId = x.IntrackReference.ToString(); } else { vehiclesId = vehiclesId + "," + x.IntrackReference; } }
                report = _mapsFunctions.GetVehicleReportLast(vehiclesId);
            }
            if (report != null)
            {
                foreach (var x in report)
                {
                    AspMap.Point location = new AspMap.Point(_mapsFunctions.GetX(x.Longitude, layerMap.Value), _mapsFunctions.GetY(x.Latitude, layerMap.Value));
                    GeoEvent geoEvent = new GeoEvent();
                    geoEvent.Location = location;
                    geoEvent.ImageUrl = _mapsFunctions.getCarImage(x.InputStatus, x.GPSDateTime, System.DateTime.UtcNow, Convert.ToInt32(InactiveTime.Value), x.VSSSpeed, true); ;
                    geoEvent.ImageWidth = 50;
                    geoEvent.ImageHeight = 50;
                    geoEvent.Rotation = x.Heading;
                    if (level.Value == "1" || mapGoogle.ZoomLevel >= 15)
                    {
                        string info = string.Empty;
                        Models.VehiclesInfo vehicleInfo = (from l in list where l.UnitId == x.UnitID select l).FirstOrDefault();
                        if (vehicleInfo !=null)
                        {
                            info = "<strong>Placa: </strong>" + vehicleInfo.PlateId + "<br/><strong>Vehículo: </strong>" + vehicleInfo.Name + "<br/>";
                        }
                        geoEvent.Label = info + "<strong>Velocidad: </strong>" + x.VSSSpeed + " km/h <br/><strong>Fecha: </strong>" + x.GPSDateTimeClient.ToString("dd/MM/yyyy") + "<br/><strong>Hora: </strong>" + x.GPSDateTimeClient.AddHours(Convert.ToDouble(HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"])).ToString("HH:mm:ss");
                        geoEvent.LabelStyle.CssClass = "mapInfo";
                    }
                    mapGoogle.AnimationLayer.Add(geoEvent);
                    if (level.Value == "1")
                    {                            
                        if (center) { if (center) { mapGoogle.CenterAndZoom(location, mapGoogle.ZoomLevels.Count - 2); } }
                        else { mapGoogle.CenterAt(location); }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        bool TrackVehiclesCoinca(bool center)
        {

            int animationLayers = mapCoinca.AnimationLayer.Count;
            mapCoinca.AnimationLayer.Clear();
            //string vehicleImage = "\\Images\\pointCar.png";
            List<Models.ReportLast> report = new List<Models.ReportLast>();
            List<Models.VehiclesInfo> list = new List<Models.VehiclesInfo>();
            if (level.Value == "1")
            {
                report = _mapsFunctions.GetVehicleReportLast(valueId.Value);
            }
            else
            {
                string vehiclesId = "0";
                if (VehiclesInfoLoaded.Value == "false")
                {
                    list = (List<Models.VehiclesInfo>)_mapsFunctions.GetVehiclesByLevel(valueId.Value, Convert.ToInt16(level.Value));
                    List<Models.VehiclesInfo> listInTrack = new List<Models.VehiclesInfo>();
                    foreach (var x in list) { if (vehiclesId == "0") { vehiclesId = x.IntrackReference.ToString(); } else { vehiclesId = vehiclesId + "," + x.IntrackReference; } }
                    listInTrack = (List<Models.VehiclesInfo>)_mapsFunctions.RetrieveTntrackReferenceDetail(vehiclesId);
                    list = (from l1 in list
                            join l2 in listInTrack on l1.IntrackReference equals l2.IntrackReference
                            select new Models.VehiclesInfo
                            {
                                IntrackReference = l1.IntrackReference,
                                Name = l1.Name,
                                PlateId = l1.PlateId,
                                UnitId = l2.UnitId,
                                VehicleId = l1.VehicleId
                            }).ToList();
                    Session["listVehiclesInfo"] = list;
                    VehiclesInfoLoaded.Value = "true";
                }
                else
                {
                    list = (List<Models.VehiclesInfo>)Session["listVehiclesInfo"];
                }
                foreach (var x in list) { if (vehiclesId == "0") { vehiclesId = x.IntrackReference.ToString(); } else { vehiclesId = vehiclesId + "," + x.IntrackReference; } }
                report = _mapsFunctions.GetVehicleReportLast(vehiclesId);
            }
            if (report != null)
            {
                foreach (var x in report)
                {
                    AspMap.Point location = new AspMap.Point(x.Longitude, x.Latitude);
                    var geoEvent = new GeoEvent
                    {
                        Location = location,
                        ImageUrl = _mapsFunctions.getCarImage(x.InputStatus, System.DateTime.UtcNow, x.GPSDateTime, Convert.ToInt32(InactiveTime.Value), x.VSSSpeed, true),
                        ImageWidth = 50,
                        ImageHeight = 50,
                        Rotation = x.Heading
                    };
                    if (level.Value == "1" || mapCoinca.ZoomLevel >= 9)
                    {
                        string info = string.Empty;
                        Models.VehiclesInfo vehicleInfo = (from l in list where l.UnitId == x.UnitID select l).FirstOrDefault();
                        if (vehicleInfo != null)
                        {
                            info = "<strong>Placa: </strong>" + vehicleInfo.PlateId + "<br/><strong>Vehículo: </strong>" + vehicleInfo.Name + "<br/>";
                        }
                        geoEvent.Label = info + "<strong>Velocidad: </strong>" + x.VSSSpeed + " km/h <br/><strong>Fecha: </strong>" + x.GPSDateTimeClient.ToString("dd/MM/yyyy") + "<br/><strong>Hora: </strong>" + x.GPSDateTimeClient.AddHours(Convert.ToDouble(HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"])).ToString("HH:mm:ss");
                        geoEvent.LabelStyle.CssClass = "mapInfo";
                    }
                    mapCoinca.AnimationLayer.Add(geoEvent);
                    if (level.Value == "1")
                    {
                        if (center) { if (center) { mapCoinca.CenterAndZoom(location, mapCoinca.ZoomLevels.Count - 2); } }
                        else { mapCoinca.CenterAt(location); }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "updateDivColor", "updateDivColor();", true);
            if (layerMap.Value == "Coinca")
            {
                TrackVehiclesCoinca(false);
            }
            else
            {
                TrackVehiclesGoogle(false);
            }
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            if (layerMap.Value != "GoogleRoad")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleRoad');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleSatellite');", true);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            if (layerMap.Value == "Coinca")
            {
                mapCoinca_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
            }
            else
            {
                mapGoogle_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
            }
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            var lWidth = Convert.ToInt16(width.Value);
            var lHeigth = Convert.ToInt16(height.Value);

            if (lWidth >= 0 && lHeigth > 0)
            {
                pnlGoogle.Width = lWidth;
                pnlGoogle.Height = lHeigth;

                pnlCoinca.Width = lWidth;
                pnlCoinca.Height = lHeigth;

                mapGoogle.Width = lWidth;
                mapGoogle.Height = lHeigth;

                mapCoinca.Width = lWidth;
                mapCoinca.Height = lHeigth;
            }
        }

    }
}