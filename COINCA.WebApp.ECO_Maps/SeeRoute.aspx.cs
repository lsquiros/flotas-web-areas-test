﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using AspMap;
using AspMap.Web;
using ECO_Maps.Utilities;

namespace ECO_Maps
{
    public partial class SeeRoute1 : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();
        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            string countryCode = string.Empty;
            int vehicleId = 0;
            string Params = Request.QueryString["Params"];
            string layer = Request.QueryString["Map"];
            if (countryCode == null) { countryCode = string.Empty; }
            if (layer == null) { layer = string.Empty; }
            if (layer == string.Empty) { layerMap.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"]; } else { layerMap.Value = layer; }
            if (loaded.Value == "false") { loaded.Value = "true"; Session["route"] = null; }
            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                CoincaMap.Visible = false;
                GoogleMap.Visible = true;
                chkSatellite.Visible = false;
            }
            else
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                GoogleMap.Visible = false;
                CoincaMap.Visible = true;
                chkSatellite.Visible = true;
            }
            if (Params != string.Empty)
            {
                string[] p = Params.Split('|');
                countryCode = p[0];
                vehicleId = Convert.ToInt32(p[1]);
                Session["CountryCode"] = countryCode;
                Session["VehicleId"] = vehicleId;
                Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[2]));
            }
            if (Page.IsPostBack == false && layerMap.Value != "Coinca")
            {
                if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
            }
            if (countryCode != null)
            {
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);
                TrackVehicles(true);
                string[] p = Params.Split('|');
                if (Convert.ToInt32(p[3]) == 0)                
                    LoadRoute(true, Convert.ToInt32(p[3]));
                else
                    LoadRoute(false, Convert.ToInt32(p[3]));               
                
            }
        }

        public void LoadRoute(bool loadFromSession, int searchType)
        {
            if (loadFromSession)
            {
                if (Session["route"] != null) 
                {
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.MapShapes.Clear();
                        AspMap.Polyline route = (AspMap.Polyline)Session["route"];
                        AspMap.MapShape mapShape = mapCoinca.MapShapes.Add(route);
                        mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                        mapShape.Symbol.Size = 3;
                        mapShape.Symbol.FillStyle = FillStyle.Invisible;

                    }
                    else
                    {
                        mapGoogle.MapShapes.Clear();
                        AspMap.Polyline route = (AspMap.Polyline)Session["route"];
                        AspMap.MapShape mapShape = mapGoogle.MapShapes.Add(route);
                        mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                        mapShape.Symbol.Size = 3;
                        mapShape.Symbol.FillStyle = FillStyle.Invisible;
                    }
                }
                else
                {
                    mapGoogle.MapShapes.Clear();
                    mapCoinca.MapShapes.Clear();
                }
            }
            else 
            {
                if (searchType == 0)
                {
                    mapGoogle.MapShapes.Clear();
                    mapCoinca.MapShapes.Clear();
                }
                DateTime date1 = System.DateTime.Now.ToServerTime();
                DateTime date2 = System.DateTime.Now.ToServerTime();
                if (searchType == 1)
                {
                    date1 = date1.AddHours(-1);
                }
                else if (searchType == 2)
                {
                    date1 = date1.AddDays(-1);
                }
                else if (searchType == 3)
                {
                    date1 = date1.AddDays(-7);
                }
                else if (searchType == 4)
                {
                    date1 = date1.AddDays(-14);
                }
                else if (searchType == 5)
                {
                    date1 = date1.AddMonths(-1);
                }
                string startDate = date1.ToString("MM/dd/yyyy HH:mm:ss");
                string endDate = date2.ToString("MM/dd/yyyy HH:mm:ss");
                if (layerMap.Value == "Coinca")
                {
                    mapCoinca.MapShapes.Clear();
                    AspMap.Polyline route = _mapsFunctions.GetVehicleReportsPolyline(_mapsFunctions.GetVehicleReports(Convert.ToInt16(Session["VehicleId"]), startDate, endDate), layerMap.Value);
                    Session["route"] = route;
                    AspMap.MapShape mapShape = mapCoinca.MapShapes.Add(route);
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;

                }
                else
                {
                    mapGoogle.MapShapes.Clear();
                    AspMap.Polyline route = _mapsFunctions.GetVehicleReportsPolyline(_mapsFunctions.GetVehicleReports(Convert.ToInt16(Session["VehicleId"]), startDate, endDate), layerMap.Value);
                    AspMap.MapShape mapShape = mapGoogle.MapShapes.Add(route);
                    Session["route"] =route;
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
            }
        }

        bool TrackVehicles(bool center)
        {
            mapCoinca.AnimationLayer.Clear();
            mapGoogle.AnimationLayer.Clear();
            //string vehicleImage = "\\Images\\pointCar.png";
            var report = _mapsFunctions.GetVehicleReportLast(Convert.ToString(Session["VehicleId"]));
            GeoEvent geoEvent = new GeoEvent();
            if (center == false) 
            {
                geoEvent = (GeoEvent)Session["geoEvent"];
                return true;
            }
            else if (report != null)
            {
                if(report.Count == 0 )
                {
                    MapsBusiness businessMap = new MapsBusiness();
                    ECO_Maps.Models.Maps maps = MapsBusiness.RetrieveMaps(Session["CountryCode"].ToString()).FirstOrDefault();
                    AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                    }                   
                    else 
                    {
                        if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                        mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                    }
                }
                else
                { 
                    foreach (var x in report)
                    {
                        if (layerMap.Value == "Coinca")
                        {
                            AspMap.Point location = new AspMap.Point(x.Longitude, x.Latitude);
                            geoEvent.Location = location;
                            geoEvent.ImageUrl = _mapsFunctions.getCarImage(x.InputStatus, System.DateTime.UtcNow, x.GPSDateTime, 4, x.VSSSpeed, true);
                            geoEvent.ImageWidth = 50;
                            geoEvent.ImageHeight = 50;
                            geoEvent.Rotation = x.Heading;
                            geoEvent.Label = "<strong>Velocidad: </strong>" + x.VSSSpeed + " km/h <br/><strong>Fecha: </strong>" + x.GPSDateTimeClient.ToString("dd/MM/yyyy") + "<br/><strong>Hora: </strong>" + x.GPSDateTimeClient.AddHours(Convert.ToDouble(HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"])).ToString("HH:mm:ss");
                            geoEvent.LabelStyle.CssClass = "mapInfo";
                            mapCoinca.AnimationLayer.Add(geoEvent);
                            if (center)
                            {
                                mapCoinca.CenterAndZoom(location, mapCoinca.ZoomLevels.Count - 2);
                            }
                            mapCoinca.CenterAt(location);

                        }
                        else
                        {
                            AspMap.Point location = new AspMap.Point(_mapsFunctions.GetX(x.Longitude, layerMap.Value), _mapsFunctions.GetY(x.Latitude, layerMap.Value));
                            geoEvent.Location = location;
                            geoEvent.ImageUrl = _mapsFunctions.getCarImage(x.InputStatus, System.DateTime.UtcNow, x.GPSDateTime, 4, x.VSSSpeed, true);
                            geoEvent.ImageWidth = 50;
                            geoEvent.ImageHeight = 50;
                            geoEvent.Rotation = x.Heading;
                            geoEvent.Label = "<strong>Velocidad: </strong>" + x.VSSSpeed + " km/h <br/><strong>Fecha: </strong>" + x.GPSDateTimeClient.ToString("dd/MM/yyyy") + "<br/><strong>Hora: </strong>" + x.GPSDateTimeClient.AddHours(Convert.ToDouble(HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"])).ToString("HH:mm:ss");
                            geoEvent.LabelStyle.CssClass = "mapInfo";
                            mapGoogle.AnimationLayer.Add(geoEvent);
                            if (center)
                            {
                                mapGoogle.CenterAndZoom(location, mapGoogle.ZoomLevels.Count - 2);
                            }
                            mapGoogle.CenterAt(location);
                        }
                        Session["geoEvent"] = geoEvent;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            GeoEvent geoEvent = (GeoEvent)Session["geoEvent"];
            if (geoEvent != null) 
            {
                if (layerMap.Value == "Coinca")
                {
                    mapCoinca.AnimationLayer.Add(geoEvent);
                }
                else //if (layerMap.Value == "GoogleRoad")
                {
                    mapGoogle.AnimationLayer.Add(geoEvent);
                }
            }
        }

        protected void mapCoinca_RefreshAnimationLayer(object sender, AspMap.Web.RefreshAnimationLayerArgs e)
        {
            e.NeedRefreshMap = TrackVehicles(false);
        }

        protected void mapGoogle_RefreshAnimationLayer(object sender, AspMap.Web.RefreshAnimationLayerArgs e)
        {
            e.NeedRefreshMap = TrackVehicles(false);
        }

        protected void ddlHour_SelectedIndexChanged(object sender, EventArgs e)
        {
           // LoadRoute(false);
        }


        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "changeMap",
                layerMap.Value != "GoogleRoad" ? "changeMap('GoogleRoad');" : "changeMap('GoogleSatellite');", true);
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            var lWidth = Convert.ToInt16(width.Value);
            var lHeigth = Convert.ToInt16(height.Value);

            if (lWidth >= 0 && lHeigth > 0)
            {
                pnlGoogle.Width = lWidth;
                pnlGoogle.Height = lHeigth;

                pnlCoinca.Width = lWidth;
                pnlCoinca.Height = lHeigth;

                mapGoogle.Width = lWidth;
                mapGoogle.Height = lHeigth;

                mapCoinca.Width = lWidth;
                mapCoinca.Height = lHeigth;
            }
        }

    }
}

