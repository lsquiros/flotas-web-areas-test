﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Management.aspx.cs" Inherits="ECO_Maps.Management" EnableEventValidation="false" %>

<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>Gestión</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <%: Styles.Render("~/bundles/css") %>
    <%: Styles.Render("~/bundles/timeline_css") %>
</head>

<body>
    <form id="ReconstructiongForm" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="Div1">
            <section class="content-wrapper main-content clear-fix">
                <section id="Content2">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                            <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
                            <asp:HiddenField ID="refrehing" runat="server" Value="false" />
                            <asp:HiddenField ID="width" runat="server" Value="0" />
                            <asp:HiddenField ID="height" runat="server" Value="0" />
                            <asp:HiddenField ID="refreshDate" runat="server" Value="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="divManageOSM">
                                <div class="ColorListText" style="width: 240px;">
                                    © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>
                                </div>
                            </div>

                            <div class="GeoFences-Content">
                                <div class="div100">
                                    <div class="ContentMap">
                                        <asp:Panel ID="pnlOSM" runat="server" Visible="true">
                                            <aspmap:Map ID="mapOSM" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" AnimationInterval="2000" EnableAnimation="true"></aspmap:Map>
                                        </asp:Panel>
                                        <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                                        <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                                        <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="hidden">
                                <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
                                <asp:Button ID="btnRefresh" runat="server" Text="Button" OnClick="btnRefresh_Click" />
                                <asp:Button ID="btnPlay" runat="server" Text="Button" OnClick="btnPlay_Click" />
                                <asp:Button ID="btnPause" runat="server" Text="Button" OnClick="btnPause_Click" />
                                <asp:Button ID="btnOk" runat="server" Text="Button" OnClick="btnOk_Click" />
                                <asp:Button ID="btnShowAgenda" runat="server" Text="Button" OnClick="btnShowAgenda_Click" />
                                <asp:TextBox ID="txtReset" runat="server" Text="" />
                                <asp:TextBox ID="txtStartDate" runat="server" Text=""></asp:TextBox>
                                <asp:TextBox ID="txtEndDate" runat="server" Text=""></asp:TextBox>
                                <asp:TextBox ID="txtShowAgenda" runat="server" Text=""></asp:TextBox>
                                <asp:TextBox ID="txtResetPart" runat="server" Text=""></asp:TextBox>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </section>
            </section>
        </div>
    </form>

    <%: Scripts.Render("~/bundles/jquery") %>
    <script src="Scripts/jquery.ba-resize.min.js"></script>
    <script src="Scripts/main.js"></script>
    <script type='text/javascript'>
        $(function () {
            resize();
            refresh();
        });

        $(window).resize(function () {
            resize();
        });

        function resize() {
            var width = $(window).width() - 105;
            var height = $(window).height();
            $('#<% =width.ClientID %>').attr('value', width);
            $('#<% =height.ClientID %>').attr('value', height);
            $("#ContentSearch").width(width);
            $('#<%=btnResize.ClientID %>').click();
        }

        function returnResult(serialize) {
            parent.postMessage(serialize, "*");
        }

        function refresh() {
            var type = '<%=Session["PointsType"]%>'
            var timeout = 0;

            if (type == 'R' || type == 'RP') {
                timeout = 3500;
            } else {
                timeout = 150000;
            }

            var action = function () {
                $('#<%=btnRefresh.ClientID %>').click();
            };

            setInterval(action, timeout);
        }

        window.addEventListener("message", function (event) {
            
            var instruction = event.data.split("|");
            var action = instruction[0];
            var showAgenda = instruction[4];
            var finishTrack = instruction[5];

            document.getElementById('<%=txtReset.ClientID %>').value = instruction[3];

            if (finishTrack == 'True') {
                document.getElementById('<%=txtResetPart.ClientID %>').value = "";
            }
            else {
                if (showAgenda != "" && action == "null") {
                    document.getElementById('<%=txtStartDate.ClientID %>').value = instruction[1];
                    document.getElementById('<%=txtEndDate.ClientID %>').value = instruction[2];
                    document.getElementById('<%=txtShowAgenda.ClientID %>').value = showAgenda;

                    $('#<%=btnShowAgenda.ClientID %>').click();
                }
                else {
                    if (action == 0 || action == "Off") {
                        if (instruction[1] == undefined || instruction[1] == null || instruction[1] == "null") {
                            $('#<%=btnPlay.ClientID %>').click();
                    }
                    else {
                        document.getElementById('<%=txtStartDate.ClientID %>').value = instruction[1];
                        document.getElementById('<%=txtEndDate.ClientID %>').value = instruction[2];
                        document.getElementById('<%=txtShowAgenda.ClientID %>').value = showAgenda;

                        $('#<%=btnOk.ClientID %>').click();
                    }
                }
                else {
                    $('#<%=btnPause.ClientID %>').click();
                    }
                }
            }
        });
    </script>

</body>
</html>
