﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using ECO_Maps.Business;
using AspMap;
using AspMap.Web;
using System.Globalization;

namespace ECO_Maps
{
    public partial class ContinuousMonitoring : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();        
        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            string countryCode = string.Empty;
            string Params = Request.QueryString["Params"];
            string layer = Request.QueryString["Map"];
            if (layer == null) { layer = string.Empty; }
            if (layer == string.Empty) { layerMap.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"]; } else { layerMap.Value = layer; }
            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                CoincaMap.Visible = false;
                GoogleMap.Visible = true;
                mapGoogle.EnableAnimation = false;
                mapCoinca.EnableAnimation = true;
                mapCoinca.AnimationInterval = 1000;
                //pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                CoincaMap.Visible = false;
                GoogleMap.Visible = true;
                mapGoogle.EnableAnimation = false;
                mapCoinca.EnableAnimation = true;
                mapCoinca.AnimationInterval = 1000;
            }
            else
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                GoogleMap.Visible = false;
                CoincaMap.Visible = true;
                mapCoinca.EnableAnimation = false;
                mapGoogle.EnableAnimation = true;
                mapGoogle.AnimationInterval = 1000;
                //pnlSatellite.Visible = true;
            }

            if (Params != string.Empty)
            {
                string[] p = Params.Split('|');
                countryCode = p[0];
                Session["CountryCode"] = countryCode;
                Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[1]));
                ZoneTime.Value = p[1];
                InactiveTime.Value = p[2];
                TemplateType.Value = p[3];
                Center.Value = p[4];
                Vehicles.Value = p[5];
                imgBack.ImageUrl = "Content/Images/icons/logoEco" + TemplateType.Value.Trim() +".png";
            }
            if (countryCode != string.Empty)
            {
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);
                if (!Page.IsPostBack)
                {
                    TrackVehicles();
                }             
                if (pageLoaded.Value == "false") 
                {
                    pageLoaded.Value = "true";
                    mapCoinca.RemoveAllLayers();
                    mapGoogle.RemoveAllLayers();
                    Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                    if (Loaded.Value == "false") 
                    {
                        if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null)
                        {
                            AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                            if (layerMap.Value == "Coinca")
                            {
                                mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                            }
                            else if (layerMap.Value == "OSM")
                            {
                                mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                            }
                            else
                            {
                                //if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                                mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                            }
                        }                    
                    }
                }
            }
            if (Page.IsPostBack == false && layerMap.Value != "Coinca" && layerMap.Value != "OSM") 
            {
                //if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
            }
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void mapCoinca_RefreshAnimationLayer(object sender, AspMap.Web.RefreshAnimationLayerArgs e)
        {
            e.NeedRefreshMap = TrackVehicles();
        }

        protected void mapGoogle_RefreshAnimationLayer(object sender, AspMap.Web.RefreshAnimationLayerArgs e)
        {
            e.NeedRefreshMap = TrackVehicles();
        }

        public bool TrackVehicles()
        {
            bool returnList = true;
            string vehicles = Vehicles.Value.Trim();
            int inactive = 0;
            if (!string.IsNullOrEmpty(InactiveTime.Value)) { inactive = Convert.ToInt32(InactiveTime.Value); }
            if (string.IsNullOrEmpty(vehicles)) { vehicles = "0"; }

            if (layerMap.Value == "Coinca")
            {
                mapCoinca.AnimationLayer.Clear();
            }
            else if (layerMap.Value == "OSM")
            {
                mapCoinca.AnimationLayer.Clear();
            }
            else 
            {
                mapGoogle.AnimationLayer.Clear();
            }
            List<COINCA.WebApp.ECO_Maps.Models.ContinuousMonitoring> reports = _mapsFunctions.RetrieveContinuousMonitoring(vehicles, System.DateTime.Now.ToString(new CultureInfo("en-US")), Convert.ToInt32(HttpContext.Current.Session["LOGGED_USER_TIMEZONE_OFF_SET"]) * 1);

            if (Center.Value != "0" && Loaded.Value == "false") {
                int vehicleId = Convert.ToInt32(Center.Value);
                COINCA.WebApp.ECO_Maps.Models.ContinuousMonitoring vehicle = (from r in reports where r.VehicleId == vehicleId select r).FirstOrDefault();
                if (vehicle != null) 
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "returnPlate", "returnPlate('" + vehicle.PlateId + "|" + vehicle.VehicleId + "');", true);
                    returnList = false;
                    //geoEvent.NavigateUrl = "javascript:returnPlate('" + vehicle.PlateId + "|" + vehicle.VehicleId + "')";
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(vehicle.Longitude, layerMap.Value), _mapsFunctions.GetY(vehicle.Latitude, layerMap.Value)), Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(vehicle.Longitude, layerMap.Value), _mapsFunctions.GetY(vehicle.Latitude, layerMap.Value)), Convert.ToInt32(mapCoinca.ZoomLevels.Count - 5));
                    }
                    else
                    {
                        //if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                        mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(vehicle.Longitude, layerMap.Value), _mapsFunctions.GetY(vehicle.Latitude, layerMap.Value)), Convert.ToInt32(mapGoogle.ZoomLevels.Count - 5));
                    }
                }
                Loaded.Value = "true";
            }

            Business.Intrack.GetLocationsBusiness bus = new Business.Intrack.GetLocationsBusiness();
            List<string> list = new List<string>();

            foreach (var x in reports)
            {
                Point location = new Point();
                location = new AspMap.Point(_mapsFunctions.GetX(x.Longitude, layerMap.Value), _mapsFunctions.GetY(x.Latitude, layerMap.Value));
                GeoEvent geoEvent = new GeoEvent();
                geoEvent.Location = location;
                x.VSSSpeed = Math.Round(x.VSSSpeed, 0);
                //DateTime timeZone = x.GPSDateTime.AddMinutes(Convert.ToInt32(ZoneTime.Value));
                DateTime timeZone = x.GPSDateTime;
                x.Date = timeZone.ToString("dd/MM/yyyy");
                x.Time = timeZone.ToString("HH:mm:ss");
                //geoEvent.ImageUrl = _mapsFunctions.getCarImage(x.Status, x.ActualDate, x.GPSDateTime, inactive, x.VSSSpeed, true);
                geoEvent.ImageUrl = x.ImageURL;
                x.Status = _mapsFunctions.getCarStatusId(x.Status, x.ActualDate, x.GPSDateTime, inactive, x.VSSSpeed);                
                list.Add(x.Latitude + "|" + x.Longitude);                
                if (returnList)
                {
                    geoEvent.NavigateUrl = "javascript:returnPlate('" + x.PlateId + "|" + x.VehicleId + "')";
                }

                geoEvent.ImageWidth = 45;
                geoEvent.ImageHeight = 45;
                geoEvent.Rotation = x.Heading;
                if (layerMap.Value == "Coinca")
                {
                    mapCoinca.AnimationLayer.Add(geoEvent);
                }
                else if (layerMap.Value == "OSM")
                {
                    mapCoinca.AnimationLayer.Add(geoEvent);
                }
                else 
                {
                    mapGoogle.AnimationLayer.Add(geoEvent);
                }
                x.SensorsXML = string.IsNullOrEmpty(x.SensorsXML) ? x.SensorsXML : x.SensorsXML.Replace("\"", "'");
            }            
            var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + jSearializer.Serialize(reports) + "');", true);
            return true;
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            TrackVehicles();
            ScriptManager.RegisterStartupScript(this, GetType(), "updateDivColor", "updateDivColor();", true);
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            if (layerMap.Value != "GoogleRoad")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleRoad');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleSatellite');", true);
            }
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            var lWidth = Convert.ToInt16(width.Value);
            var lHeigth = Convert.ToInt16(height.Value);

            if (lWidth >= 0 && lHeigth > 0)
            {
                pnlGoogle.Width  = lWidth;
                pnlGoogle.Height = lHeigth;

                pnlCoinca.Width  = lWidth;
                pnlCoinca.Height = lHeigth;

                mapGoogle.Width  = lWidth;
                mapGoogle.Height = lHeigth;

                mapCoinca.Width  = lWidth;
                mapCoinca.Height = lHeigth;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            if (layerMap.Value == "Coinca")
            {
                mapCoinca_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
            }
            else
            {
                mapGoogle_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
            }
        }

        protected void btnZoomIn_Click(object sender, EventArgs e)
        {
            mapGoogle.ZoomLevel = mapGoogle.ZoomLevel - 1;
        }

        protected void btnZoomOut_Click(object sender, EventArgs e)
        {
            mapGoogle.ZoomLevel = mapGoogle.ZoomLevel + 1;
        }

    }
}