﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using AspMap;
using AspMap.Web;
using ECO_Maps.Business.Intrack;
using System.Globalization;

namespace ECO_Maps
{
    public partial class Distance1 : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();
        Utilities.Maps _utilities = new Utilities.Maps();
        int _pSpeed = 40 ;// velocidad promedio

        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            customerId.Value = Request.QueryString["CustomerId"];
            string countryCode = Request.QueryString["CountryCode"];
            string latDirection = Request.QueryString["latDirection"];
            string lonDirection = Request.QueryString["lonDirection"];
            string layer = Request.QueryString["Map"];
            Session["CountryCode"] = countryCode;
            if (countryCode == null) { countryCode = string.Empty; }
            if (latDirection == null) { latDirection = string.Empty; }
            if (lonDirection == null) { lonDirection = string.Empty; }
            if (layer == null) { layer = string.Empty; }
            if (layer == string.Empty) { layerMap.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"]; } else { layerMap.Value = layer; }
            LoadLabels();
            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                pointToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                panTool.Visible = true;
                centerTool.Visible = true;
                pointTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                //GoogleMap.Visible = false;
                //CoincaMap.Visible = true;
                //GoogleMap.Visible = false;
                //ZoomInTool.Visible = false;
                //zoomOutTool.Visible = false;
                panTool.Visible = false;
                centerTool.Visible = false;
                pointTool.Visible = false;
                distanceTool.Visible = false;
                //CoincaMap.Visible = true;
                //ZoomInToolGoogle.Visible = true;
                //zoomOutToolGoogle.Visible = true;
                panToolGoogle.Visible = true;
                centerToolGoogle.Visible = true;
                pointToolGoogle.Visible = true;
                distanceToolGoogle.Visible = true;
                pnlSatellite.Visible = true;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                pointToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                panTool.Visible = true;
                centerTool.Visible = true;
                pointTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }            

            if (countryCode != string.Empty)
            {
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);
                if (Page.IsPostBack == false)
                {

                    LoadProvincias();
                    LoadCantones();
                    RefreshPoints();
                    MapsBusiness businessMap = new MapsBusiness();
                    ECO_Maps.Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                    if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null)
                    {
                        AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                        }
                        //satelite modificado
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            //satelite modificado
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                    }
                    if (latDirection != string.Empty && lonDirection != string.Empty)
                    {
                        AspMap.Point punto = new AspMap.Point(Convert.ToDouble(lonDirection), Convert.ToDouble(latDirection));
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                        }
                        //satelite modificado
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            //satelite modificado
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapGoogle.ZoomLevels.Count - 2);
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            //satelite modificado
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }

                         
                            MarkerSymbol symbol = new MarkerSymbol("../Content/Images/Marker_1.png", 32, 32);
                            Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), symbol, "Búsqueda");
                            mapCoinca.Markers.Add(marker);

                            //AspMap.Web.GeoEvent geoEvent = new AspMap.Web.GeoEvent();
                            //geoEvent.Location = punto;
                            //geoEvent.ImageWidth = 35;
                            //geoEvent.ImageHeight = 35;
                            //geoEvent.ImageUrl = "../Content/Images/Marker_1.png";
                            //mapCoinca.AnimationLayer.Add(geoEvent);
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                        }
                    }
                }
            }
        }

        protected void LoadLabels()
        {
            Models.Maps info = _mapsFunctions.GetMapInfo(Convert.ToString(Session["CountryCode"]).Trim());
            if (info != null)
            {
                lblLevel1.Text = info.Level1 + ":";
                lblLevel2.Text = info.Level2 + ":";
            }
        }

        protected void mapCoinca_PointTool(object sender, AspMap.Web.PointToolEventArgs e)
        {
            SetPoint(e.Point);
        }

        protected void RefreshPoints()
        {
            if (layerMap.Value == "Coinca")
            {
                mapCoinca.Markers.Clear();
                if (Session["startPoint"] != null)
                {
                    AspMap.Point startPoint = (AspMap.Point)Session["startPoint"];
                    MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Cian.png", 32, 32);
                    Marker marker = new Marker(startPoint, symbol, "Inicio");
                    mapCoinca.Markers.Add(marker);
                    txtStartLatitude.Text = startPoint.Y.ToString();
                    txtStartLongitude.Text = startPoint.X.ToString();
                }
                if (Session["endPoint"] != null)
                {
                    AspMap.Point endPoint = (AspMap.Point)Session["endPoint"];
                    MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Green.png", 32, 32);
                    Marker marker = new Marker(endPoint, symbol, "Inicio");
                    mapCoinca.Markers.Add(marker);
                    txtEndLatitude.Text = endPoint.Y.ToString();
                    txtEndLongitude.Text = endPoint.X.ToString();
                }
                if (Session["startPoint"] != null && Session["endPoint"] != null)
                {
                    RefreshRoute();
                }
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                mapGoogle.Markers.Clear();
                if (Session["startPoint"] != null)
                {
                    AspMap.Point startPoint = (AspMap.Point)Session["startPoint"];
                    MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Cian.png", 32, 32);
                    Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(startPoint.X, layerMap.Value), _mapsFunctions.GetY(startPoint.Y, layerMap.Value)), symbol, "Inicio");
                    mapGoogle.Markers.Add(marker);
                    txtStartLatitude.Text = startPoint.Y.ToString();
                    txtStartLongitude.Text = startPoint.X.ToString();
                }
                if (Session["endPoint"] != null)
                {
                    AspMap.Point endPoint = (AspMap.Point)Session["endPoint"];
                    MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Green.png", 32, 32);
                    Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(endPoint.X, layerMap.Value), _mapsFunctions.GetY(endPoint.Y, layerMap.Value)), symbol, "Inicio");
                    mapGoogle.Markers.Add(marker);
                    txtEndLatitude.Text = endPoint.Y.ToString();
                    txtEndLongitude.Text = endPoint.X.ToString();
                }
                if (Session["startPoint"] != null && Session["endPoint"] != null)
                {
                    RefreshRoute();
                }
            }
            else if (layerMap.Value == "OSM")
            {
                mapCoinca.Markers.Clear();
                if (Session["startPoint"] != null)
                {
                    AspMap.Point startPoint = (AspMap.Point)Session["startPoint"];
                    MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Cian.png", 32, 32);
                    Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(startPoint.X, layerMap.Value), _mapsFunctions.GetY(startPoint.Y, layerMap.Value)), symbol, "Inicio");
                    mapCoinca.Markers.Add(marker);
                    txtStartLatitude.Text = startPoint.Y.ToString();
                    txtStartLongitude.Text = startPoint.X.ToString();
                }
                if (Session["endPoint"] != null)
                {
                    AspMap.Point endPoint = (AspMap.Point)Session["endPoint"];
                    MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Green.png", 32, 32);
                    Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(endPoint.X, layerMap.Value), _mapsFunctions.GetY(endPoint.Y, layerMap.Value)), symbol, "Inicio");
                    mapCoinca.Markers.Add(marker);
                    txtEndLatitude.Text = endPoint.Y.ToString();
                    txtEndLongitude.Text = endPoint.X.ToString();
                }
                if (Session["startPoint"] != null && Session["endPoint"] != null)
                {
                    RefreshRoute();
                }
            }
        }
        
        protected void RefreshRoute() 
        {
            mapCoinca.MapShapes.Clear();
            mapGoogle.MapShapes.Clear();
            lblDistance.Text = "0.00";
            lblTime.Text = "00:00";
            if (Session["startPoint"] != null && Session["endPoint"] != null)
            {
                AspMap.Route route = _mapsFunctions.findRoute(Convert.ToString(Session["CountryCode"]), (AspMap.Point)Session["startPoint"], (AspMap.Point)Session["endPoint"], ddlAlgorithm.SelectedValue);
                AspMap.Polyline polyline = new AspMap.Polyline();
                AspMap.Points points = new Points();
                int x = 0;
                for (x = 0; x < route.RoutePath.Count; x++)
                {
                    if (layerMap.Value == "Coinca") 
                    {
                        points.Add(new Point(route.RoutePath[x].X, route.RoutePath[x].Y));
                    }
                    else
                    {
                        points.Add(new Point(_mapsFunctions.GetX(route.RoutePath[x].X, layerMap.Value), _mapsFunctions.GetY(route.RoutePath[x].Y, layerMap.Value)));
                    }
                }
                polyline.Add(points);
                if (layerMap.Value == "Coinca")
                {
                    MapShape mapShape = mapCoinca.MapShapes.Add(polyline);
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
                else if (layerMap.Value == "GoogleRoad")
                {
                    MapShape mapShape = mapGoogle.MapShapes.Add(polyline);
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
                else if (layerMap.Value == "OSM")
                {
                    MapShape mapShape = mapCoinca.MapShapes.Add(polyline);
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
                double distance = 0;
                double time = 0;
                for (int i = 0; i < route.Streets.Count; i++)
                {
                    distance = distance + route.Streets[i].Distance;
                    time = time + Math.Round((Convert.ToDouble(route.Streets[i].Time) / 60 ), 1);
                }

                var PhysicalSpeedFormula = distance / _pSpeed;

                lblDistance.Text = distance.ToString("###,###.00");
                TimeSpan output = new TimeSpan(0, Convert.ToInt16(time), 0);
                TimeSpan output2 = new TimeSpan(Convert.ToInt16(PhysicalSpeedFormula), 0, 0);
                lblTime.Text = ((output.Days * 24) + output.Hours).ToString().PadLeft(2, '0') + "h " + output.Minutes.ToString().PadLeft(2, '0') + "m / " + ((output2.Days * 24) + output2.Hours).ToString().PadLeft(2, '0') + "h " + output2.Minutes.ToString().PadLeft(2, '0') + "m ";
            }
        }

        protected void mapGoogle_PointTool(object sender, AspMap.Web.PointToolEventArgs e)
        {
            SetPoint(e.Point);
        }

        protected void SetPoint(AspMap.Point pPoint) 
        {
            bool valid = false;
            string editing = string.Empty;
            if (Session["editMarker"] != null) { editing = Convert.ToString(Session["editMarker"]).Trim(); }
            if (layerMap.Value == "Coinca") 
            { 
                if (pointTool.Visible) 
                { 
                    valid = true;
                    if (mapCoinca.Markers.Count == 0) { editing = "start"; } else if (mapCoinca.Markers.Count == 1) { editing = "end"; }
                }
            } 
            else if (layerMap.Value == "GoogleRoad")
            {
                if (pointToolGoogle.Visible) 
                { 
                    valid = true;
                    if (mapGoogle.Markers.Count == 0) { editing = "start"; } else if (mapGoogle.Markers.Count == 1) { editing = "end"; }
                }
            }
            else if (layerMap.Value == "OSM")
            {
                if (pointTool.Visible)
                {
                    valid = true;
                    if (mapCoinca.Markers.Count == 0) { editing = "start"; } else if (mapCoinca.Markers.Count == 1) { editing = "end"; }
                }
            }

            if (valid)
            {
                if (layerMap.Value == "Coinca")
                {
                    if (editing == "start")
                    {
                        Session["startPoint"] = pPoint;
                    }
                    else if (editing == "end")
                    {
                        Session["endPoint"] = pPoint;
                    }
                }
                else if (layerMap.Value == "GoogleRoad")
                {
                    if (editing == "start")
                    {
                        Session["startPoint"] = new AspMap.Point(_mapsFunctions.GetAspMapX(pPoint.X), _mapsFunctions.GetAspMapY(pPoint.Y));
                    }
                    else if (editing == "end")
                    {
                        Session["endPoint"] = new AspMap.Point(_mapsFunctions.GetAspMapX(pPoint.X), _mapsFunctions.GetAspMapY(pPoint.Y));
                    }
                }
                else if (layerMap.Value == "OSM")
                {
                    if (editing == "start")
                    {
                        Session["startPoint"] = new AspMap.Point(_mapsFunctions.GetAspMapX(pPoint.X), _mapsFunctions.GetAspMapY(pPoint.Y));
                    }
                    else if (editing == "end")
                    {
                        Session["endPoint"] = new AspMap.Point(_mapsFunctions.GetAspMapX(pPoint.X), _mapsFunctions.GetAspMapY(pPoint.Y));
                    }
                }

                if (editing != string.Empty)
                {
                    RefreshPoints();
                }
                Session["editMarker"] = null;
            }
        }


        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            if (directionsTable.Rows.Count == 0 && pointsSearchTable.Rows.Count == 0) { directionsTable.Visible = false; pnlResultButtons.Visible = false; }
            ScriptManager.RegisterStartupScript(this, GetType(), "fncMenuRefresh", "fncMenuRefresh();", true);
        }

        protected void provinciaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCantones();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string countryCode = Session["CountryCode"].ToString();
            List<Models.SearchResult> listPlaces = new List<Models.SearchResult>();
            if (layerMap.Value == "Coinca")
            {
                listPlaces = _mapsFunctions.Search(mapCoinca, countryCode, provinciaList.SelectedItem.Text.Trim(), cantonesList.SelectedItem.Text.Trim(), string.Empty, txtSearch.Text.Trim());
            }
            //satelite modificado
            else if (layerMap.Value == "GoogleRoad")
            {
                listPlaces = _mapsFunctions.Search(mapGoogle, countryCode, provinciaList.SelectedItem.Text.Trim(), cantonesList.SelectedItem.Text.Trim(), string.Empty, txtSearch.Text.Trim());
            }
            else if (layerMap.Value == "OSM")
            {
                listPlaces = _mapsFunctions.Search(mapCoinca, countryCode, provinciaList.SelectedItem.Text.Trim(), cantonesList.SelectedItem.Text.Trim(), string.Empty, txtSearch.Text.Trim());
            }

            SearchPointsBusiness business = new SearchPointsBusiness();
            List<Models.SearchResult> listPoints = business.RetrievePoints(Convert.ToInt16(customerId.Value), txtSearch.Text.Trim()).ToList();
            Session["listPoints"] = listPoints;
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }

            Session["listPlaces"] = listPlaces;
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            if (listPlaces.Count > 0)
            {
                directionsTable.Visible = true;
                pnlResultButtons.Visible = true;
                btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            }
            else
            {
                directionsTable.Visible = false;
                pnlResultButtons.Visible = false;
            }
        }

        TableCell Td(string text)
        {
            TableCell cell = new TableCell();
            cell.Text = text;
            if (text != string.Empty)
                cell.BorderWidth = Unit.Pixel(1);
            return cell;
        }

        void Tr(string text, string latitud, string longitud, string type)
        {
            TableRow row = new TableRow();
            row.Attributes.Add("onclick", "fncSeleccionaItem(" + latitud + "," + longitud + ")");
            row.Cells.Add(Td(text));
            if (type == "direction")
            {
                directionsTable.Rows.Add(row);
            }
            else
            {
                pointsSearchTable.Rows.Add(row);
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            directionsTable.Visible = false;
            txtSearch.Text = string.Empty;
            cantonesList.Text = string.Empty;
            provinciaList.Text = string.Empty;
        }

        protected void LoadProvincias()
        {
            string countryCode = Session["CountryCode"].ToString();
            provinciaList.DataSource = _mapsFunctions.GetProvinciasList(countryCode);
            provinciaList.DataBind();
        }

        protected void LoadCantones()
        {
            string countryCode = Session["CountryCode"].ToString();
            cantonesList.DataSource = _mapsFunctions.GetCantonList(countryCode, provinciaList.SelectedValue);
            cantonesList.DataBind();
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void btnEditStartPoint_Click(object sender, EventArgs e)
        {
            Session["editMarker"] = "start";
        }

        protected void btnCenterStartPoint_Click(object sender, EventArgs e)
        {
            if (Session["startPoint"] != null)
            {
                AspMap.Point point = (AspMap.Point)Session["startPoint"];
                if (layerMap.Value == "Coinca")
                {
                    mapCoinca.CenterAt(point);
                }
                else
                {
                    mapGoogle.CenterAt(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)));
                }
            }
        }

        protected void btnEditEndPoint_Click(object sender, EventArgs e)
        {
            Session["editMarker"] = "end";
        }

        protected void btnCenterEndPoint_Click(object sender, EventArgs e)
        {
            if (Session["endPoint"] != null)
            {
                AspMap.Point point = (AspMap.Point)Session["endPoint"];
                if (layerMap.Value == "Coinca")
                {
                    mapCoinca.CenterAt(point);
                }
                else
                {
                    mapGoogle.CenterAt(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)));
                }
            }
        }

        protected void btnCancelEvent_Click(object sender, EventArgs e)
        {
            mapCoinca.Markers.Clear();
            mapCoinca.MapShapes.Clear();
            mapGoogle.Markers.Clear();
            mapGoogle.MapShapes.Clear();
            Session.Clear();
        }

        protected void btnInvest_Click(object sender, EventArgs e)
        {
            if (Session["startPoint"] != null && Session["endPoint"] != null)
            {
                AspMap.Point startPoint = (AspMap.Point)Session["startPoint"];
                AspMap.Point endPoint = (AspMap.Point)Session["endPoint"];
                Session["startPoint"] = endPoint;
                Session["endPoint"] = startPoint;
                RefreshPoints();
            }
        }

        protected void ddlAlgorithm_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshRoute();
        }
        protected void btnDirectionsSearch_Click(object sender, EventArgs e)
        {
            List<Models.SearchResult> listPlaces = (List<Models.SearchResult>)Session["listPlaces"];
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            List<Models.SearchResult> listPoints = (List<Models.SearchResult>)Session["listPoints"];
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }
            pointsSearchTable.Visible = false;
            directionsTable.Visible = true;
            btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            btnPointsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("428BCA", NumberStyles.HexNumber)); //"#132C59";
        }

        protected void btnPointsSearch_Click(object sender, EventArgs e)
        {
            List<Models.SearchResult> listPlaces = (List<Models.SearchResult>)Session["listPlaces"];
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            List<Models.SearchResult> listPoints = (List<Models.SearchResult>)Session["listPoints"];
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }
            directionsTable.Visible = false;
            pointsSearchTable.Visible = true;
            btnPointsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(int.Parse("428BCA", NumberStyles.HexNumber)); //"#132C59";
        }

        //satelite modificado
        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            if (layerMap.Value != "GoogleRoad")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleRoad');", true);
            }
            else 
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleSatellite');", true);
            }
        }

        protected void txtStartLongitude_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt16(width.Value) >= 0 && Convert.ToInt16(height.Value) > 0)
            {
                pnlGoogle.Width = Convert.ToInt16(width.Value);
                pnlGoogle.Height = Convert.ToInt16(height.Value);
                pnlCoinca.Width = Convert.ToInt16(width.Value);
                pnlCoinca.Height = Convert.ToInt16(height.Value);
                mapGoogle.Width = Convert.ToInt16(width.Value);
                mapGoogle.Height = Convert.ToInt16(height.Value);
                mapCoinca.Width = Convert.ToInt16(width.Value);
                mapCoinca.Height = Convert.ToInt16(height.Value);
                ContentSearchId.Style.Add("width", width.Value + "px");    
            }
        }

    
    }
}

