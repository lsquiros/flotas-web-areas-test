﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using AspMap;
using AspMap.Web;
using System.Data;
using ECO_Maps.Business.Routes;
using ECO_Maps.Business.Intrack;
using ECO_Maps.Models;

namespace ECO_Maps
{
    public partial class Routes : System.Web.UI.Page
    {
        private readonly Utilities.Maps MapsFunctions = new Utilities.Maps();
        private readonly MarkerSymbol symbol = new MarkerSymbol("Content/Images/Markers/Marker-Cian.png", 32, 32);
        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            string latDirection = Request.QueryString["latDirection"];
            string lonDirection = Request.QueryString["lonDirection"];
            string PageLoaded = Request.QueryString["PageLoaded"];

            string PageCenterPoint = Request.QueryString["PageCenterPoint"];
            string CenterPointImage = "0";
            CenterPointImage = Request.QueryString["CenterPointImage"];

            if (PageCenterPoint == null) { PageCenterPoint = "0"; }
            if (CenterPointImage == null) { CenterPointImage = "0"; }

            string layer = Request.QueryString["Map"];
            string Params = Request.QueryString["Params"];
            string IsRouteLoad = Request.QueryString["IsRouteLoad"];
            if (latDirection == null) { latDirection = string.Empty; }
            if (lonDirection == null) { lonDirection = string.Empty; }
            if (PageLoaded == null) { PageLoaded = string.Empty; }
            if (PageLoaded != string.Empty) { pageLoaded.Value = PageLoaded; }
            if (layer == null) { layer = string.Empty; }
            layerMap.Value = layer == string.Empty ? System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"] : layer;
            string countryCode = string.Empty;  
            string UserId = string.Empty;
            string RouteId = string.Empty;
            
            //if (Params != string.Empty)
            if (!string.IsNullOrEmpty(Params))
            {
                string[] p = Params.Split('|');
                countryCode = p[0];
                RouteId = p[1];
                UserId = p[2];
                customerId.Value = p[3];

                if (pageLoaded.Value == "false")
                {
                    pageLoaded.Value = "true";
                    mapCoinca.RemoveAllLayers();
                    mapGoogle.RemoveAllLayers();

                    if (IsRouteLoad == "1")
                        Session.Clear();

                    Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[4]));
                    loadSegments(Convert.ToInt32(RouteId));
                }
                Session["RouteId"] = Convert.ToInt32(RouteId);
                Session["UserId"] = Convert.ToInt32(UserId);
                Session["CountryCode"] = countryCode;
                loadLabels();
            }

            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                pointToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                panTool.Visible = true;
                centerTool.Visible = true;
                pointTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                panTool.Visible = false;
                centerTool.Visible = false;
                pointTool.Visible = false;
                distanceTool.Visible = false;
                panToolGoogle.Visible = true;
                centerToolGoogle.Visible = true;
                pointToolGoogle.Visible = true;
                distanceToolGoogle.Visible = true;
                pnlSatellite.Visible = true;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                pointToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                panTool.Visible = true;
                centerTool.Visible = true;
                pointTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }


            if (countryCode != string.Empty)
            {
                MapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);

                if (Page.IsPostBack == false)
                {                                     
                    LoadProvincias(countryCode);
                    LoadCantones(countryCode);
                    ECO_Maps.Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();

                    if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null)
                    {
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat)), Convert.ToInt32(maps.CenterZoom));
                        }
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            chkSatellite.Checked = layerMap.Value != "GoogleRoad";
                            mapGoogle.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(maps.CenterLon), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(maps.CenterLat), layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            chkSatellite.Checked = layerMap.Value != "GoogleRoad";
                            // mapGoogle.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(maps.CenterLon), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(maps.CenterLat), layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                            mapCoinca.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(maps.CenterLon), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(maps.CenterLat), layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                    }
                    if (latDirection != string.Empty && lonDirection != string.Empty)
                    {
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(Convert.ToDouble(lonDirection), Convert.ToDouble(latDirection)), mapCoinca.ZoomLevels.Count - 2);
                        }
                        else if (layerMap.Value == "GoogleRoad")
                        {
                            chkSatellite.Checked = layerMap.Value != "GoogleRoad";
                            mapGoogle.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(lonDirection), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(latDirection), layerMap.Value)), mapGoogle.ZoomLevels.Count - 2);
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            chkSatellite.Checked = layerMap.Value != "GoogleRoad";
                            // mapGoogle.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(maps.CenterLon), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(maps.CenterLat), layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                            
                            if (PageCenterPoint == "1")
                            {
                                mapCoinca.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(lonDirection), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(latDirection), layerMap.Value)), 17);
                            }
                            else
                            {
                                if (CenterPointImage=="1")
                                {
                                    AspMap.Point punto = new AspMap.Point(Convert.ToDouble(lonDirection), Convert.ToDouble(latDirection));
                                    MarkerSymbol symbol = new MarkerSymbol("../Content/Images/Marker_1.png", 32, 32);
                                    Marker marker = new Marker(new AspMap.Point(MapsFunctions.GetX(punto.X, layerMap.Value), MapsFunctions.GetY(punto.Y, layerMap.Value)), symbol, "Búsqueda");
                                    mapCoinca.Markers.Add(marker);
                                    mapCoinca.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(lonDirection), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(latDirection), layerMap.Value)), 18);
                                }
                                else
                                {
                                    mapCoinca.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(Convert.ToDouble(lonDirection), layerMap.Value), MapsFunctions.GetY(Convert.ToDouble(latDirection), layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                                }


                               
                            }
                            
                        }
                    }
                   
                }
                try
                {
                    if (PageCenterPoint != "1" && CenterPointImage.Contains("1") == false)
                    {
                        validateSegmentList();
                    }
                }
                catch (Exception ex)
                { }
               
                    
            }
        }

        protected void loadLabels()
        {
            Models.Maps info = MapsFunctions.GetMapInfo(Convert.ToString(Session["CountryCode"]).Trim());
            if (info != null)
            {
                lblLevel1.Text = info.Level1 + ":";
                lblLevel2.Text = info.Level2 + ":";
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "fncMenuRefresh", "fncMenuRefresh();", true);
        }

        protected void mapCoinca_PointTool(object sender, AspMap.Web.PointToolEventArgs e)
        {
            if (pointTool.Visible)
            {
                var serialize = string.Empty;
                List<Models.PointsList> pointList = GetPointsList();
                if (layerMap.Value == "OSM")
                {
                    //List<Models.PointsList> pointList = GetPointsList();
                    int countPoint = 1;
                    int pointReference = 1;
                    if (pointList.Count > 0)
                    {
                        //countPoint = pointList.Max(p => p.Order) + 1;
                        countPoint = getCountPoint(pointList);
                        pointReference = pointList.Max(p => p.PointReference) + 1;
                    }
                    Marker marker = new Marker(new AspMap.Point(e.Point.X, e.Point.Y), symbol, "Punto " + countPoint, "Punto " + countPoint);
                    mapGoogle.Markers.Add(marker);
                    AddPointToList(pointList, countPoint, MapsFunctions.GetAspMapX(e.Point.X), MapsFunctions.GetAspMapY(e.Point.Y), pointReference);
                }
                else
                {
                    //List<Models.PointsList> pointList = GetPointsList();
                    int countPoint = 1;
                    int pointReference = 1;
                    if (pointList.Count > 0)
                    {
                        //countPoint = pointList.Max(p => p.Order) + 1;
                        countPoint = getCountPoint(pointList);
                        pointReference = pointList.Max(p => p.PointReference) + 1;
                    }

                    Marker marker = new Marker(e.Point, symbol, "Punto " + countPoint, "Punto " + countPoint);
                    mapCoinca.Markers.Add(marker);
                    AddPointToList(pointList, countPoint, e.Point.X, e.Point.Y, pointReference);
                }
                int newOrder = 1;
                pointList.OrderBy(x => x.Order).ToList().ForEach(x => { x.Order = newOrder; newOrder++; });
                validateSegmentList();
            }
        }

        private int getCountPoint(List<PointsList> pointList)
        {
            return pointList.Count == 0 ? 0 : pointList.Count + 1;
        }

        protected void mapGoogle_PointTool(object sender, AspMap.Web.PointToolEventArgs e)
        {
            if (pointToolGoogle.Visible)
            {
                List<Models.PointsList> pointList = GetPointsList();
                int countPoint = 1;
                int pointReference = 1; 
                if (pointList.Count > 0)
                {
                    //countPoint = pointList.Max(p => p.Order) + 1;
                    countPoint = getCountPoint(pointList);
                    pointReference = pointList.Max(p => p.PointReference) + 1;
                }
                Marker marker = new Marker(new AspMap.Point(e.Point.X, e.Point.Y), symbol, "Punto " + countPoint, "Punto " + countPoint);
                mapGoogle.Markers.Add(marker);
                AddPointToList(pointList, countPoint, MapsFunctions.GetAspMapX(e.Point.X), MapsFunctions.GetAspMapY(e.Point.Y), pointReference);
                validateSegmentList();
            }
        }

        public void AddPointToList(List<Models.PointsList> pPointsList, int pId, double pX, double pY, int pointReference)
        {
            Models.PointsList point = new Models.PointsList
            {
                Id = pId,
                Order = pId,
                Name = "Punto " + pId,
                Hour = 0,
                Minute = 0,
                x = pX,
                y = pY,
                Address = MapsBusiness.GetAddressByLatLon(pX, pY),
                NewPoint = true,
                PointReference = pointReference,
				RouteId = Convert.ToInt32(Session["RouteId"])
			};
            pPointsList.Add(point);
            Session["pointsList"] = pPointsList;

            ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult(" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(point) +");", true);
        }

        protected AspMap.Point GetPointFromMarker(string pMarker)
        {
            Point result = new Point();
            if (layerMap.Value == "Coinca")
            {
                for (int i = 0; i < mapCoinca.Markers.Count; i++)
                {
                    if (pMarker.Trim() == mapCoinca.Markers[i].Tooltip)
                    {
                        result.X = mapCoinca.Markers[i].Position.X;
                        result.Y = mapCoinca.Markers[i].Position.Y;
                    }
                }
            }
            else
            {
                for (int i = 0; i < mapGoogle.Markers.Count; i++)
                {
                    if (pMarker.Trim() == mapGoogle.Markers[i].Tooltip)
                    {
                        result.X = MapsFunctions.GetAspMapX(mapGoogle.Markers[i].Position.X);
                        result.Y = MapsFunctions.GetAspMapY(mapGoogle.Markers[i].Position.Y);
                    }
                }
            }

            return result;
        }

        public static List<Models.PointsList> GetPointsList()
        {
            List<Models.PointsList> pointsList = (List<Models.PointsList>)(HttpContext.Current.Session["pointsList"]);
            pointsList = pointsList == null ? new List<Models.PointsList>() : (from p in pointsList orderby p.Order select p).ToList();
            return pointsList;
        }

        public static List<Models.SegmentList> GetSegmentList()
        {
            List<Models.SegmentList> segmentList = (List<Models.SegmentList>)(HttpContext.Current.Session["segmentList"]);
            if (segmentList == null)
            {
                segmentList = new List<Models.SegmentList>();
            }
            return segmentList;
        }

        public void SetSegmentList(List<Models.SegmentList> pSegmentList)
        {
            int id = 0;
            foreach (var x in pSegmentList) { id = id + 1; x.id = id; }
            Session["segmentList"] = pSegmentList;
        }

        public void validateSegmentList()
        {
            List<Models.PointsList> pointsList = GetPointsList();
            Session["pointsList"] = pointsList;

            List<Models.SegmentList> segmentList = GetSegmentList();
            for (int i = 0; i < pointsList.Count - 1; i++)
            {
                Models.PointsList point1 = pointsList.FirstOrDefault(x => x.Order == i + 1);
                Models.PointsList point2 = pointsList.FirstOrDefault(x => x.Order == i + 2);

                if (point1 != null && point2 != null)
                {
                    Models.SegmentList seg = segmentList.FirstOrDefault(x => x.startPoint == point1.PointReference);
                    if (seg != null)
                    {
                        if (seg.endPoint != point2.PointReference)
                        {
                            segmentList.RemoveAll(x => x.startPoint == point1.PointReference);
                        }
                    }
                }
            }

            List<int> intList = (from l in pointsList select l.PointReference).ToList();
            segmentList.RemoveAll(x => !intList.Contains(x.startPoint) || !intList.Contains(x.endPoint));

            CalculatedSegmentList(pointsList, segmentList);

            double routeDistance = 0.0;
			int routeTime = 0;
            segmentList.ForEach(s => { routeDistance += s.distance; routeTime += s.time; });
            pointsList.ForEach(s => { routeTime += s.StopTime; });

            ScriptManager.RegisterStartupScript(this, GetType(), "updateDistance" + Guid.NewGuid(), "updateDistance(" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(new { distance = routeDistance.ToString("#,##0.##"), time = routeTime.ToString("#,##0") } ) + ");", true);
        }

        public void CalculatedSegmentList(List<Models.PointsList> pPointsList, List<Models.SegmentList> pSegmentList)
        {
            for (int i = 0; i < pPointsList.Count - 1; i++)
            {
                //Models.PointsList point1 = pPointsList.FirstOrDefault(x => x.Order == i + 1);
                //Models.PointsList point2 = pPointsList.FirstOrDefault(x => x.Order == i + 2);
                Models.PointsList point1 = pPointsList[i];
                Models.PointsList point2 = pPointsList[i+1];

                if (point1 != null && point2 != null)
                {
                    Models.SegmentList seg = pSegmentList.FirstOrDefault(x => x.startPoint == point1.PointReference && x.endPoint == point2.PointReference);
                    if (seg == null)
                    {
                        AspMap.Route route = MapsFunctions.findRoute(Convert.ToString(Session["CountryCode"]), new Point(pPointsList[i].x, pPointsList[i].y), new Point(pPointsList[i + 1].x, pPointsList[i + 1].y), "Shortest");
                        AspMap.Polyline polyline = new AspMap.Polyline();
                        AspMap.Points Points = new Points();
                        if (route != null)
                        {
                            int x;
                            for (x = 0; x < route.RoutePath.Count; x++)
                            {
                                Points.Add(new Point(route.RoutePath[x].X, route.RoutePath[x].Y));
                            }
                            polyline.Add(Points);
                            seg = new Models.SegmentList
                            {
                                route = polyline,
                                startPoint = pPointsList[i].PointReference,
                                endPoint = pPointsList[i + 1].PointReference
                            };
                            double time = 0;
                            double distance = 0;
                            for (int e = 0; e < route.Streets.Count; e++)
                            {
                                distance = distance + route.Streets[e].Distance;
                                time = time + Math.Round((Convert.ToDouble(route.Streets[e].Time) / 60), 1);
                            }
                            seg.distance = distance;
                            seg.time = Convert.ToInt32(time);
                            pSegmentList.Add(seg);
                        }
                    }
                }
            }
            SetSegmentList(pSegmentList);
            printSegmentList(pSegmentList);
            refreshPoints(pPointsList);
            if (pPointsList.Any())
            {
                mapCoinca.CenterAndZoom(new AspMap.Point(MapsFunctions.GetX(pPointsList.FirstOrDefault().x, layerMap.Value), MapsFunctions.GetY(pPointsList.FirstOrDefault().y, layerMap.Value)), mapCoinca.ZoomLevels.Count + 3);
            }
        }

        public void printSegmentList(List<Models.SegmentList> pSegmentList)
        {
            if (layerMap.Value == "Coinca")
            {
                mapCoinca.MapShapes.Clear();
                foreach (var segment in pSegmentList)
                {

                    MapShape mapShape = mapCoinca.MapShapes.Add(segment.route);
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
                if (Session["segmentTemp"] != null)
                {
                    Models.SegmentList segment = (Models.SegmentList)Session["segmentTemp"];
                    MapShape mapShape = mapCoinca.MapShapes.Add(segment.route);
                    mapShape.Symbol.LineColor = System.Drawing.Color.Cyan;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                mapGoogle.MapShapes.Clear();
                foreach (var segment in pSegmentList)
                {
                    MapShape mapShape = mapGoogle.MapShapes.Add(MapsFunctions.convertSegmentToGoogle(segment.route, layerMap.Value));
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
                if (Session["segmentTemp"] != null)
                {
                    Models.SegmentList segment = (Models.SegmentList)Session["segmentTemp"];
                    MapShape mapShape = mapGoogle.MapShapes.Add(MapsFunctions.convertSegmentToGoogle(segment.route, layerMap.Value));
                    mapShape.Symbol.LineColor = System.Drawing.Color.Cyan;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
            }
            else if (layerMap.Value == "OSM")
            {
                mapCoinca.MapShapes.Clear();
                foreach (var segment in pSegmentList)
                {
                    MapShape mapShape = mapCoinca.MapShapes.Add(MapsFunctions.convertSegmentToGoogle(segment.route, layerMap.Value));
                    mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
                if (Session["segmentTemp"] != null)
                {
                    Models.SegmentList segment = (Models.SegmentList)Session["segmentTemp"];
                    MapShape mapShape = mapCoinca.MapShapes.Add(MapsFunctions.convertSegmentToGoogle(segment.route, layerMap.Value));
                    mapShape.Symbol.LineColor = System.Drawing.Color.Cyan;
                    mapShape.Symbol.Size = 3;
                    mapShape.Symbol.FillStyle = FillStyle.Invisible;
                }
            }
        }

        public void refreshPoints(List<Models.PointsList> pPointsList)
        {
            if (layerMap.Value == "Coinca")
            {
                mapCoinca.Markers.Clear();
                foreach (var poin in pPointsList)
                {
                    string tooltip = GetToolTipInformation(poin);
                    Marker marker = new Marker(new AspMap.Point(poin.x, poin.y), symbol, tooltip, tooltip);                    
                    mapCoinca.Markers.Add(marker);
                }
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                mapGoogle.Markers.Clear();
                foreach (var poin in pPointsList)
                {
                    string tooltip = GetToolTipInformation(poin);
                    Marker marker = new Marker(new AspMap.Point(MapsFunctions.GetX(poin.x, layerMap.Value), MapsFunctions.GetY(poin.y, layerMap.Value)), symbol, tooltip, tooltip);
                    mapGoogle.Markers.Add(marker);
                }
            }
            else
            {
                mapCoinca.Markers.Clear();
                foreach (var poin in pPointsList)
                {
                    string tooltip = GetToolTipInformation(poin);
                    string tooltipQuarter = tooltip;
                   
                    if (poin.Iscommerce == 0)
                    {
                          tooltipQuarter += " <button type='button' class='btn btn-default' onclick='AddCommerceJs(" + poin.Id.ToString() + ")' id ='btnCancelPoint'><i class='mdi mdi-home-variant'></i>Agregar como nuevo comercio</button>";
                    }
                   
                    Marker marker = new Marker(new AspMap.Point(MapsFunctions.GetX(poin.x, layerMap.Value), MapsFunctions.GetY(poin.y, layerMap.Value)), symbol, tooltip, tooltipQuarter);
                    mapCoinca.Markers.Add(marker);
                }
            }
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void loadSegments(int pRouteId)
        {
            List<Models.PointsList> points = (List<Models.PointsList>)Session["pointsList"];
            if (points == null)
                points = new List<PointsList>();

            using (var business = new RoutesBusiness())
            {
                RoutesSegmentsBase routes = business.RetrieveRoutesDetail(pRouteId);
                if (points.Exists(p => p.NewPoint == true))
                {
                    Session["pointsList"] = points;
                    SetSegmentList(MapsFunctions.GetRouteSegments(routes));
                }
                else
                {
                    Session["pointsList"] = MapsFunctions.GetRoutePoints(routes);
                    SetSegmentList(MapsFunctions.GetRouteSegments(routes));
                }
            }
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "changeMap",
                layerMap.Value != "GoogleRoad" ? "changeMap('GoogleRoad');" : "changeMap('GoogleSatellite');", true);
        }

        protected void btnCancelPage_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            short lWidth = Convert.ToInt16(width.Value);
            short lHeigth = Convert.ToInt16(height.Value);
            if (lWidth >= 0 && lHeigth > 0)
            {
                pnlGoogle.Width = lWidth;
                pnlGoogle.Height = lHeigth;
                pnlCoinca.Width = lWidth;
                pnlCoinca.Height = lHeigth;
                mapGoogle.Width = lWidth;
                mapGoogle.Height = lHeigth;
                mapCoinca.Width = lWidth;
                mapCoinca.Height = lHeigth;
                ContentSearchId.Style.Add("width", width.Value + "px");
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            var data = txtData.Text.Split('|')[1];
            var flag = txtData.Text.Split('|')[0];
            int routeId;

            switch (flag)
            {
                case "r":
                    PointsList pointR = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PointsList>(data);
                    List<Models.PointsList> pointListR = GetPointsList();
                    pointListR.Where(x => x.Id == pointR.Id).ToList().ForEach(s =>
                    {
                        s.Name = pointR.Name;
                        s.Hour = pointR.Hour;
                        s.Minute = pointR.Minute;
                        s.StopTime = pointR.StopTime;
                    });
                    Session["pointsList"] = pointListR;

                    //routeId = Convert.ToInt32(data);
                    //loadSegments(routeId);
                    validateSegmentList();
                    break;
                case "s":
                    SaveRouteInformation();
                    //routeId = Convert.ToInt32(data);
                    //loadSegments(routeId);
                    break;
                case "a":
                    PointsList point = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<PointsList>(data);
                    List<Models.PointsList> pointList = GetPointsList();
                    pointList.Where(x => x.Id == point.Id).ToList().ForEach(s =>
                    {
                        s.Name = point.Name;
                        s.Hour = point.Hour;
                        s.Minute = point.Minute;
                        s.StopTime = point.StopTime;
                    });
                    Session["pointsList"] = pointList;

                    //Refresh view
                    validateSegmentList();
                    //Guardo los puntos nuevos inmediatamente
                    //SaveRouteInformation();
                    break;
                case "d":
                    DeletePoint(Convert.ToInt32(data));
                    //SaveRouteInformation();
                    break;                
            }
        }      

        void DeletePoint(int pointReference)
        {
            List<Models.PointsList> pointList = GetPointsList();
            pointList.RemoveAll(x => x.PointReference == pointReference);
            Session["pointsList"] = pointList;
            validateNewSegments(pointReference);            
            validateSegmentList();
            List<Models.SegmentList> segmentList = GetSegmentList();
            printSegmentList(segmentList);
            refreshPoints(pointList);
        }


        protected string GetToolTipInformation(PointsList point)
        {
            string text = "<div style=\"width:300px\"><b>Nombre: </b> " + point.Name + "<br />" +
                        "<b>Descripción: </b> " + point.Description + "<br />" +
                        "<b>Dirección: </b> " + point.Address + "<br />" +
                        "<b>Hora de Llegada: </b> " + point.Hour + ":" + point.Minute + "<br />" +
                        "<b>Tiempo de Parada: </b> " + point.StopTime + " minutos<br />";
            if (point.Iscommerce > 0)
            {
                text += "<b>Comercio ligado a este punto: </b> " + point.CommerceName + "<br />";
            }

            text += "</div>";

            return text;
 
                   
        }

        protected void SaveRouteInformation()
        {
            List<Models.PointsList> pointsList = GetPointsList().Where(p => p.Deleted == false).ToList();
            List<Models.SegmentList> segmentList = GetSegmentList();

            int order = 1;
            pointsList.ForEach(s => s.Order = order++);

            Utilities.Maps function = new Utilities.Maps();
            using (var business = new RoutesBusiness())
            {
                business.AddOrEditRoutesDetail(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["RouteId"]), function.GetSegments(pointsList, segmentList));
            }

            pointsList.ForEach(point => point.NewPoint = false);

            ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult(" + Convert.ToInt32(Session["RouteId"]) + ");", true);

            validateSegmentList();//Refresh view
        }

        void validateNewSegments(int pointReference)
        {
            List<Models.SegmentList> segmentList = GetSegmentList();
            segmentList.RemoveAll(x => x.startPoint == pointReference);
            segmentList.RemoveAll(x => x.endPoint == pointReference);
            SetSegmentList(segmentList);
        }

        #region LocationTools
        protected void provinciaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCantones(Session["CountryCode"].ToString());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            directionsTable.Visible = false;
            txtSearch.Text = string.Empty;
            cantonesList.Text = string.Empty;
            provinciaList.Text = string.Empty;
        }

        protected void btnAddCommerce_Click(object sender, EventArgs e)
        {
            using (var business = new RoutesBusiness())
            {
                business.AddNewCommerce(Convert.ToInt32(PointIDNewCommerce.Text), Convert.ToInt32(Session["UserId"]));
                Session["pointsList"] = new List<Models.PointsList>();
                loadSegments(Convert.ToInt32(Session["RouteId"]));

                List<Models.PointsList> points = (List<Models.PointsList>)Session["pointsList"];

                refreshPoints(points);
                btnRefresh_Click(sender, e);
            }
        }

            protected void btnSearch_Click(object sender, EventArgs e)
        {
            string CountryCode = Session["CountryCode"].ToString();
            List<Models.SearchResult> listPlaces = new List<Models.SearchResult>();
            listPlaces = MapsFunctions.Search(layerMap.Value == "Coinca" ? mapCoinca : mapGoogle, CountryCode, provinciaList.SelectedItem.Text.Trim(), cantonesList.SelectedItem.Text.Trim(), string.Empty, txtSearch.Text.Trim());

            SearchPointsBusiness business = new SearchPointsBusiness();
            List<Models.SearchResult> listPoints = business.RetrievePoints(Convert.ToInt16(customerId.Value), txtSearch.Text.Trim()).ToList();

            List<Models.SearchResult> ListCommerces =  business.RetrieveCommerces(Convert.ToInt16(customerId.Value), txtSearch.Text.Trim()).ToList();

 
            Session["listPoints"] = listPoints;
            foreach (var x in listPoints)
            {
                TR(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }

 

            TableRow rowOnePoint = new TableRow();
            TableCell cell = new TableCell { Text = "Puntos" };
            cell.BackColor = System.Drawing.Color.Orange;
            rowOnePoint.Cells.Add(cell);
            directionsTable.Rows.Add(rowOnePoint);
            Session["listPlaces"] = listPlaces;
            foreach (var x in listPlaces)
            {
                TR(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            directionsTable.Visible = listPlaces.Count > 0;

            ////////////////////////////

            TableRow rowOneCommerce = new TableRow();
            TableCell cell1 = new TableCell { Text = "Comercios" };
            cell1.BackColor = System.Drawing.Color.Orange;
            rowOneCommerce.Cells.Add(cell1);
            CommercesTable.Rows.Add(rowOneCommerce);
            foreach (var x in ListCommerces)
            {
                TR(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "Commerces");
            }
            CommercesTable.Visible = ListCommerces.Count > 0;

        }

        protected void LoadProvincias(string CountryCode)
        {
            provinciaList.DataSource = MapsFunctions.GetProvinciasList(CountryCode);
            provinciaList.DataBind();
        }

        protected void LoadCantones(string CountryCode)
        {
            cantonesList.DataSource = MapsFunctions.GetCantonList(CountryCode, provinciaList.SelectedItem.Text.Trim());
            cantonesList.DataBind();
        }

        TableCell TD(string text)
        {
            TableCell cell = new TableCell { Text = text };
            if (text != string.Empty)
                cell.BorderWidth = Unit.Pixel(1);
            return cell;
        }

        void TR(string text, string latitud, string longitud, string type)
        {
            TableRow row = new TableRow();
            row.Attributes.Add("onclick", "fncSeleccionaItem(" + latitud + "," + longitud + ",1)");
            row.Cells.Add(TD(text));
            if (type == "direction")
            {
                directionsTable.Rows.Add(row);
            }
            else if (type == "Commerces")
            {
                CommercesTable.Rows.Add(row);
            }
            else
            {
                pointsSearchTable.Rows.Add(row);
            }
        }
        #endregion

        protected void mapCoinca_MarkerClick(object sender, MarkerClickEventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "markerclick", "SelectRow(" + e.MarkerIndex + ");", true);
            
        }
    }
}