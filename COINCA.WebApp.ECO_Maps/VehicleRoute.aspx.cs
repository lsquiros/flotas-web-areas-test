﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using AspMap;
using AspMap.Web.Extensions;
using ECO_Maps.Business.VehicleRoutes;
using System.Globalization;
using AspMap.Web;
using ECO_Maps.Utilities;

namespace ECO_Maps
{
    public partial class VehicleRoute1 : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();
        Utilities.Maps _utilities = new Utilities.Maps();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            string countryCode = string.Empty;
            string Params = Request.QueryString["Params"];
            string layer = Request.QueryString["Map"] ?? string.Empty;
            layerMap.Value = layer == string.Empty ? System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"] : layer;
          
            if (Params != string.Empty)
            {
                string[] p = Params.Split('|');
                countryCode = p[0];
                VehicleId.Value = p[1];
                Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[2]));
                CustomerId.Value = p[3];
                if (pageLoaded.Value == "false")
                {
                    pageLoaded.Value = "true";
                    mapCoinca.RemoveAllLayers();
                    mapGoogle.RemoveAllLayers();
                    Session.Clear();
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.MapShapes.Clear();
                        mapCoinca.Markers.Clear();
                    }
                    else if (layerMap.Value == "GoogleRoad")
                    {
                        mapGoogle.MapShapes.Clear();
                        mapGoogle.Markers.Clear();
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        mapCoinca.MapShapes.Clear();
                        mapCoinca.Markers.Clear();
                    }
                }
                Session["CountryCode"] = countryCode;
            }

            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                panTool.Visible = true;
                centerTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                //GoogleMap.Visible = false;
                //CoincaMap.Visible = true;
                //GoogleMap.Visible = false;
                //ZoomInTool.Visible = false;
                //zoomOutTool.Visible = false;
                panTool.Visible = false;
                centerTool.Visible = false;
                distanceTool.Visible = false;
                //CoincaMap.Visible = true;
                //ZoomInToolGoogle.Visible = true;
                //zoomOutToolGoogle.Visible = true;
                panToolGoogle.Visible = true;
                centerToolGoogle.Visible = true;
                distanceToolGoogle.Visible = true;
                pnlSatellite.Visible = true;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                panTool.Visible = true;
                centerTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }

            if (countryCode != string.Empty)
            {
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);

                if (Page.IsPostBack == false)
                {
                    routeList.Items.Clear();
                    VehicleRouteBusiness businessVehicleRoute = new VehicleRouteBusiness();
                    List<Models.List> routes = businessVehicleRoute.RetrieveRoutesByVehicleList(Convert.ToInt32(VehicleId.Value), Convert.ToInt16(CustomerId.Value)).ToList();
                    routeList.DataValueField = "Id";
                    routeList.DataTextField = "Name";
                    routeList.DataSource = routes;
                    routeList.DataBind();

                    DistanceMargin.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DistanceMargin"];
                    StopTime.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["StopTime"];

                    MapsBusiness businessMap = new MapsBusiness();
                    Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                    if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null)
                    {
                        AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                        if (layerMap.Value == "Coinca")
                        {
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                        }
                        else if (layerMap.Value == "GopogleRoad")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                        else if (layerMap.Value == "OSM")
                        {
                            if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
                            mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                        }
                    }
                }
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "fncLoadDate", "fncLoadDate();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "fncMenuRefresh", "fncMenuRefresh();", true);
            List<TableRow> vehicleRouteResult = (List<TableRow>)Session["vehicleRouteResult"];
            List<TableRow> summaryResult = (List<TableRow>)Session["summaryResult"];
            //if (vehicleRouteResult == null || vehicleRouteResult.Count == 0) { pnlResult.Visible = false; } else { pnlResult.Visible = true; }
            if (vehicleRouteResult != null)
            {
                foreach (var x in vehicleRouteResult)
                {
                    resultTable.Rows.Add(x);
                }
                foreach (var x in summaryResult)
                {
                    summaryTable.Rows.Add(x);
                }
            }
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Session["vehicleRouteResult"] = null;
            Session["summaryResult"] = null;
            List<TableRow> vehiculeRouteResult = new List<TableRow>();
            List<TableRow> summaryResult = new List<TableRow>();
            int countVisitedClients = 0;
            int countAtTimeClients = 0;
            if (layerMap.Value == "Coinca")
            {
                mapCoinca.Markers.Clear();
                mapCoinca.MapShapes.Clear();
            }
            else if (layerMap.Value == "GoogleRoad")
            {
                mapGoogle.Markers.Clear();
                mapGoogle.MapShapes.Clear();
            }
            else if (layerMap.Value == "OSM")
            {
                mapCoinca.Markers.Clear();
                mapCoinca.MapShapes.Clear();
            }

            if (routeList.SelectedValue != "0" && txtDate.Text.Trim() != string.Empty)
            {
                DateTime startDate = DateTime.ParseExact(txtDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToServerTime();
                DateTime endDate = startDate.AddDays(1).ToServerTime();
                VehicleRouteBusiness businessVehicleRoute = new VehicleRouteBusiness();
                List<Models.ReportLast> reports = new List<Models.ReportLast>();
                string day = string.Empty;
                if (startDate.DayOfWeek == DayOfWeek.Sunday) { day = "D"; }
                else if (startDate.DayOfWeek == DayOfWeek.Monday) { day = "L"; }
                else if (startDate.DayOfWeek == DayOfWeek.Thursday) { day = "K"; }
                else if (startDate.DayOfWeek == DayOfWeek.Wednesday) { day = "M"; }
                else if (startDate.DayOfWeek == DayOfWeek.Thursday) { day = "J"; }
                else if (startDate.DayOfWeek == DayOfWeek.Friday) { day = "V"; }
                else if (startDate.DayOfWeek == DayOfWeek.Saturday) { day = "S"; }
                List<Polyline> polylineList = new List<Polyline>();
                Models.VehicleRouteBase route = businessVehicleRoute.VehicleRouteDetail(Convert.ToInt16(routeList.SelectedValue), day, Convert.ToInt32(VehicleId.Value));
                string listPoints = string.Empty;
                foreach (var point in route.Points)
                {
                    if (listPoints.Trim() != string.Empty)
                    {
                        listPoints = listPoints + ",";
                    }
                    listPoints = listPoints + point.PointReference + "*" + point.Longitude + " " + point.Latitude;
                }
                List<Models.VehicleRoutePointInfo> pointsInfo = businessVehicleRoute.VehicleRoutePointsInfo(VehicleId.Value, startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"), listPoints, Convert.ToInt16(DistanceMargin.Value), Convert.ToInt16(StopTime.Value)).ToList();
                if (route.Segments.Count > 0)
                {
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.MapShapes.Clear();
                        reports = _mapsFunctions.GetVehicleReports(Convert.ToInt32(VehicleId.Value), startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"));
                        AspMap.MapShape mapShape = mapCoinca.MapShapes.Add(_mapsFunctions.GetVehicleReportsPolyline(reports, layerMap.Value));
                        mapShape.Symbol.LineColor = System.Drawing.Color.DarkGreen;
                        mapShape.Symbol.Size = 3;
                        mapShape.Symbol.FillStyle = FillStyle.Invisible;
                    }
                    else if (layerMap.Value == "GoogleRoad")
                    {
                        mapGoogle.MapShapes.Clear();
                        reports = _mapsFunctions.GetVehicleReports(Convert.ToInt32(VehicleId.Value), startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"));
                        AspMap.MapShape mapShape = mapGoogle.MapShapes.Add(_mapsFunctions.GetVehicleReportsPolyline(reports, layerMap.Value));
                        mapShape.Symbol.LineColor = System.Drawing.Color.DarkGreen;
                        mapShape.Symbol.Size = 3;
                        mapShape.Symbol.FillStyle = FillStyle.Invisible;
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        mapCoinca.MapShapes.Clear();
                        reports = _mapsFunctions.GetVehicleReports(Convert.ToInt32(VehicleId.Value), startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"));
                        AspMap.MapShape mapShape = mapCoinca.MapShapes.Add(_mapsFunctions.GetVehicleReportsPolyline(reports, layerMap.Value));
                        mapShape.Symbol.LineColor = System.Drawing.Color.DarkGreen;
                        mapShape.Symbol.Size = 3;
                        mapShape.Symbol.FillStyle = FillStyle.Invisible;
                    }

                    foreach (var x in route.Segments)
                    {
                        polylineList.Add(_mapsFunctions.GetPolylineFromString(x.Poliyline));
                    }
                    if (layerMap.Value == "Coinca")
                    {
                        foreach (var segment in polylineList)
                        {
                            MapShape mapShape = mapCoinca.MapShapes.Add(segment);
                            mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                            mapShape.Symbol.Size = 3;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                        }
                    }
                    else if (layerMap.Value == "GoogleRoad")
                    {
                        foreach (var segment in polylineList)
                        {
                            MapShape mapShape = mapGoogle.MapShapes.Add(_mapsFunctions.convertSegmentToGoogle(segment, layerMap.Value));
                            mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                            mapShape.Symbol.Size = 3;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                        }
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        foreach (var segment in polylineList)
                        {
                            MapShape mapShape = mapCoinca.MapShapes.Add(_mapsFunctions.convertSegmentToGoogle(segment, layerMap.Value));
                            mapShape.Symbol.LineColor = System.Drawing.Color.DarkMagenta;
                            mapShape.Symbol.Size = 3;
                            mapShape.Symbol.FillStyle = FillStyle.Invisible;
                        }
                    }
                    foreach (var p in route.Points)
                    {
                        string pointName = string.Empty;
                        TimeSpan pointHour = DateTime.Now.ToServerTime().TimeOfDay;
                        int pointMinutes = 0;
                        bool infoExist = false;
                        TimeSpan infoStart = DateTime.Now.ToServerTime().TimeOfDay;
                        TimeSpan infoEnd = DateTime.Now.ToServerTime().TimeOfDay;
                        int infoSeconds = 0;
                        string color = string.Empty;

                        string markerIcon = "Content/Images/Markers/";
                        Models.VehicleRoutePointInfo pInfo = (from i in pointsInfo where p.PointReference == i.PointId select i).FirstOrDefault();

                        pointName = p.Name;
                        pointHour = p.Time;
                        pointMinutes = p.StopTime;

                        TimeSpan aditionalMinutes = TimeSpan.FromMinutes(15);
                        if (pInfo == null)
                        {
                            if (DateTime.Now.ToServerTime().Date > startDate.Date)
                            {
                                markerIcon = markerIcon + "Marker-Red.png";
                                color = "vehicleRouteRed";
                            }
                            else if (DateTime.Now.ToServerTime().Date < startDate.Date)
                            {
                                markerIcon = markerIcon + "Marker-Gray.png";
                                color = "vehicleRouteGray";
                            }
                            else 
                            {
                                if (pointHour > System.DateTime.Now.ToServerTime().TimeOfDay.Add(aditionalMinutes))
                                {
                                    markerIcon = markerIcon + "Marker-Red.png";
                                    color = "vehicleRouteRed";
                                }
                                else
                                {
                                    markerIcon = markerIcon + "Marker-Gray.png";
                                    color = "vehicleRouteGray";
                                }
                            }
                        }
                        else
                        {
                            countVisitedClients = countVisitedClients + 1;
                            infoStart = pInfo.Date.TimeOfDay;
                            infoSeconds = pInfo.Time;
                            infoEnd = infoStart.Add(TimeSpan.FromSeconds(pInfo.Time));
                            if (pInfo.Date.TimeOfDay > p.Time.Add(aditionalMinutes))
                            {
                                infoExist = true;
                                markerIcon = markerIcon + "Marker-Yellow.png";
                                color = "vehicleRouteYellow";
                            }
                            else
                            {
                                infoExist = true;
                                markerIcon = markerIcon + "Marker-Green.png";
                                color = "vehicleRouteGreen";
                                countAtTimeClients = countAtTimeClients + 1;
                            }
                        }
                        MarkerSymbol symbol = new MarkerSymbol(markerIcon, 32, 32);
						//TimeSpan pHour = pointHour.ToClientTime();
						//TimeSpan infoHour = infoStart.ToClientTime();
						TimeSpan pHour = pointHour;
						TimeSpan infoHour = infoStart;

						if (pHour.Days < 0 || pHour.Hours < 0 || pHour.Minutes < 0) { pHour = TimeSpan.FromHours(24).Add(pHour); } else if (pHour.Days > 1) { pHour = pHour.Add(TimeSpan.FromDays(-1)); }
                        if (infoHour.Days < 0 || infoHour.Hours < 0 || infoHour.Minutes < 0) { infoHour = TimeSpan.FromHours(24).Add(infoHour); } else if (infoHour.Days > 1) { infoHour = infoHour.Add(TimeSpan.FromDays(-1)); }
                        string quote = "\"";
                        string text = string.Empty;
                        text = text + "<div class=" + quote + "map-toolTip" + quote + ">";
                        text = text + "<strong style=" + quote + "color: #132C59;" + quote + ">Información de la Ruta</strong>";
                        text = text + "<br />";
                        text = text + "<strong>Nombre: </strong>";
                        text = text + pointName;
                        text = text + "<br />";
                        text = text + "<strong>Hora Proyectada: </strong>";
                        text = text + pHour.ToString(@"hh\:mm");
                        text = text + "<br />";
                        text = text + "<strong>Tiempo Proyectado: </strong>";
                        text = text + TimeSpan.FromMinutes(pointMinutes).ToString(@"hh\:mm");
                        text = text + "<br />";
                        text = text + "<strong style=" + quote + "color: #132C59;" + quote + ">Información del Vehículo</strong>";
                        text = text + "<br />";
                        if (infoExist)
                        {
                            text = text + "<strong>Llegada Real: </strong>";
                            text = text + infoHour.ToString(@"hh\:mm");
                            text = text + "<br />";
                            text = text + "<strong>Salida Real: </strong>";
                            text = text + infoEnd.ToClientTime().ToString(@"hh\:mm");
                            text = text + "<br />";
                            text = text + "<strong>Tiempo Real: </strong>";
                            if (infoSeconds < 60 && infoSeconds > 0) { infoSeconds = 60; }
                            text = text + TimeSpan.FromSeconds(infoSeconds).ToString(@"hh\:mm");
                            vehiculeRouteResult.Add(Tr(pointName, pHour.ToString(@"hh\:mm"), infoHour.ToString(@"hh\:mm"), color, pHour,infoHour));
                        }
                        else
                        {
                            text = text + "<strong>Parada no Efectuada </strong>";
                            vehiculeRouteResult.Add(Tr(pointName, pHour.ToString(@"hh\:mm"), string.Empty, color, pHour,infoHour));
                        }
                        text = text + "</div>";
                        if (layerMap.Value == "Coinca")
                        {
                            Marker marker = new Marker(new AspMap.Point(p.Longitude, p.Latitude), symbol, null, text);
                            mapCoinca.Markers.Add(marker);
                        }
                        else
                        {
                            Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX(p.Longitude, layerMap.Value), _mapsFunctions.GetY(p.Latitude, layerMap.Value)), symbol, null, text);
                            mapGoogle.Markers.Add(marker);
                        }
                    }

                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.CenterAt(new Point(route.Points[0].Longitude, route.Points[0].Latitude));
                        mapCoinca.ZoomLevel = mapCoinca.ZoomLevels.Count - 3;
                    }
                    else if (layerMap.Value == "GoogleRoad")
                    {
                        if (mapGoogle.Markers.Count > 0) 
                        {
                            mapGoogle.CenterAt(new Point(_mapsFunctions.GetX(route.Points[0].Longitude, layerMap.Value), _mapsFunctions.GetY(route.Points[0].Latitude, layerMap.Value)));
                            mapGoogle.ZoomLevel = mapGoogle.ZoomLevels.Count - 5;
                        }
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        if (mapCoinca.Markers.Count > 0)
                        {
                            mapCoinca.CenterAt(new Point(_mapsFunctions.GetX(route.Points[0].Longitude, layerMap.Value), _mapsFunctions.GetY(route.Points[0].Latitude, layerMap.Value)));
                            mapCoinca.ZoomLevel = mapCoinca.ZoomLevels.Count - 5;
                        }
                    }
                }
                double summary = 0;
                double estimatedTime = 0;
                double estimatedDistance = 0;
                double realDistance = 0;
                double realTime = 0;
                if (route.Points.Count > 0) {
                    estimatedTime = (route.Points[route.Points.Count - 1].Time - route.Points[0].Time).TotalMinutes; 
                }
                if (pointsInfo.Count > 0) 
                {
                    realTime = (pointsInfo[pointsInfo.Count - 1].Date - pointsInfo[0].Date).TotalMinutes;
                }
                if (route.Segments.Count > 0) 
                {
                    estimatedDistance = Convert.ToDouble((from p in route.Segments select p.Distance).Sum());
                }
                if (reports.Count > 0) { realDistance = Convert.ToDouble(reports[0].TotalDistance) / 1000; }
                Models.CustomerThresholds thresholds = _mapsFunctions.GetThresholds(Convert.ToInt16(CustomerId.Value));
                Session["vehicleRouteResult"] = vehiculeRouteResult;
                summaryResult.Add(TrSummary("Clientes Visitados vrs Programados", route.Points.Count.ToString("###,###.00"), countVisitedClients.ToString("###,###.00"), (countVisitedClients == 0 ? 0 : Convert.ToDouble(countVisitedClients) / Convert.ToDouble(route.Points.Count) * 100).ToString("###,###.00") + "%", _mapsFunctions.FillColors(thresholds, (countVisitedClients == 0 ? 0 : Convert.ToDouble(countVisitedClients) / Convert.ToDouble(route.Points.Count) * 100)), string.Empty));
                summary = summary + (countVisitedClients == 0 ? 0 : Math.Round(Convert.ToDouble(countVisitedClients) / Convert.ToDouble(route.Points.Count) * 100, 2));
                summaryResult.Add(TrSummary("Visitas Realizadas a Tiempo", route.Points.Count.ToString("###,###.00"), countAtTimeClients.ToString("###,###.00"), (countAtTimeClients == 0 ? 0 : Convert.ToDouble(countAtTimeClients) / Convert.ToDouble(route.Points.Count) * 100).ToString("###,###.00") + "%", _mapsFunctions.FillColors(thresholds, (countAtTimeClients == 0 ? 0 : Convert.ToDouble(countAtTimeClients) / Convert.ToDouble(route.Points.Count) * 100)), string.Empty));
                summary = summary + (countAtTimeClients == 0 ? 0 : Math.Round(Convert.ToDouble(countAtTimeClients) / Convert.ToDouble(route.Points.Count) * 100, 2));
                summaryResult.Add(TrSummary("Tiempo Total Programado (minutos)", estimatedTime.ToString("###,###.00"), realTime.ToString("###,###.00"), (realTime == 0 ? 0 : (Convert.ToDouble(estimatedTime) / Convert.ToDouble(realTime) * 100)).ToString("###,###.00") + "%", _mapsFunctions.FillColors(thresholds, (realTime == 0 ? 0 : (Convert.ToDouble(estimatedTime) / Convert.ToDouble(realTime) * 100))), string.Empty));
                //summaryResult.Add(TRSummary("Tiempo Total Programado (minutos)", estimatedTime.ToString("###,###.00"), realTime.ToString("###,###.00"), (Convert.ToDouble(estimatedTime) / Convert.ToDouble(realTime) * 100).ToString("###,###.00") + "%", MapsFunctions.FillColors(Thresholds, (Convert.ToDouble(estimatedTime) / Convert.ToDouble(realTime) * 100)), string.Empty));
                summary = summary + (realTime == 0 ? 0 : Math.Round(Convert.ToDouble(estimatedTime) / Convert.ToDouble(realTime) * 100, 2));
                summaryResult.Add(TrSummary("Recorrido Total Programado (kilómetros)", estimatedDistance.ToString("###,###.00"), realDistance.ToString("###,###.00"), (Convert.ToDouble(estimatedDistance) / Convert.ToDouble(realDistance) * 100).ToString("###,###.00") + "%", _mapsFunctions.FillColors(thresholds, (Convert.ToDouble(estimatedDistance) / Convert.ToDouble(realDistance) * 100)), string.Empty));
                summary = summary + (realDistance == 0 ? 0 : Math.Round(Convert.ToDouble(estimatedDistance) / Convert.ToDouble(realDistance) * 100, 2));
                summaryResult.Add(TrSummary("Indice General de Cumplimiento", string.Empty, string.Empty, (summary / 4).ToString("###,###.00") + "%", "Threshold " + _mapsFunctions.FillColors(thresholds, (summary / 4)), "table-footer"));
                
                Session["summaryResult"] = summaryResult;
                lblVehicleRouteGreen.Text = "Indice >= " + thresholds.GreenLower + "%";
                lblVehicleRouteYellow.Text = "Indice >= " + thresholds.YellowLower + " y < " + thresholds.YellowHigher + "%";
                lblVehicleRouteRed.Text = "Indice >= " + thresholds.RedLower + " y < " + thresholds.RedHigher + "%";
                pnlResult.Visible = true;

                businessVehicleRoute.SaveReportRoute(vehiculeRouteResult, summaryResult, Convert.ToInt32(VehicleId.Value), startDate);
            }
        }

        TableCell Td(string text, bool color)
        {
            TableCell cell = new TableCell();
            if (color)
            {
                cell.CssClass = text;
                cell.Text = string.Empty;
            }
            else
            {
                cell.Text = text;
            }
            //cell.BorderWidth = Unit.Pixel(1);
            return cell;
        }

        TableRow Tr(string pPoint, string pScheduled, string pRealTime, string pCellColor, TimeSpan pRealTimeOri, TimeSpan pScheduleOri)
        {
            string diff = "No Visitado";
            if (!string.IsNullOrEmpty(pRealTime))
            {
                diff = (pScheduleOri - pRealTimeOri).ToString();
            } 

            TableRow row = new TableRow();
            row.Cells.Add(Td(pPoint, false));
            row.Cells.Add(Td(pScheduled, false));
            row.Cells.Add(Td(pRealTime, false));
            row.Cells.Add(Td(diff, false));
            row.Cells.Add(Td(pCellColor, true));
            resultTable.Rows.Add(row);
            return row;
        }

        TableRow TrSummary(string pName, string pScheduled, string pReal, string pPercentage, string cssClass, string rowCssClass)
        { 
            TableRow row = new TableRow();
            row.Cells.Add(Td(pName, false));
            row.Cells.Add(Td(pScheduled, false));
            row.Cells.Add(Td(pReal, false));
            row.Cells.Add(Td(pPercentage, false));
            row.Cells.Add(Td(cssClass, true));
            row.CssClass = rowCssClass;
            summaryTable.Rows.Add(row);
            return row;
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "changeMap",
                layerMap.Value != "GoogleRoad" ? "changeMap('GoogleRoad');" : "changeMap('GoogleSatellite');", true);
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt16(width.Value) >= 0 && Convert.ToInt16(height.Value) > 0)
            {
             
                pnlGoogle.Width = Convert.ToInt16(width.Value);
                pnlGoogle.Height = Convert.ToInt16(height.Value);
                pnlCoinca.Width = Convert.ToInt16(width.Value);
                pnlCoinca.Height = Convert.ToInt16(height.Value);
                mapGoogle.Width = Convert.ToInt16(width.Value);
                mapGoogle.Height = Convert.ToInt16(height.Value);
                mapCoinca.Width = Convert.ToInt16(width.Value);
                mapCoinca.Height = Convert.ToInt16(height.Value);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnlResult.Visible = false;

            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                panToolGoogle.Visible = false;
                centerToolGoogle.Visible = false;
                distanceToolGoogle.Visible = false;
                panTool.Visible = true;
                centerTool.Visible = true;
                distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }
            else
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                panTool.Visible = false;
                centerTool.Visible = false;
                distanceTool.Visible = false;
                panToolGoogle.Visible = true;
                centerToolGoogle.Visible = true;
                distanceToolGoogle.Visible = true;
                pnlSatellite.Visible = true;
            }
        }
    }
}
