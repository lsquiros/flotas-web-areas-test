﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="GeoFences.aspx.cs" Inherits="ECO_Maps.GeoFences" EnableEventValidation="false"%>
<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <%: Styles.Render("~/bundles/css") %>
    <script type="text/javascript" src="Scripts/jquery-3.1.0.slim.min.js"></script>
</head>
<body>
    <form id="MapForm"  method="post" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
            </Scripts>
        </asp:ScriptManager>
        <div id="body">     
            <section class="content-wrapper main-content clear-fix">
                <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                    <ProgressTemplate>           
                        <asp:Panel ID="loader" CssClass="loaderGeoFences" runat="server" >
                            <center>
                                <img src="Content/Images/loader.GIF" class="image-loader" />
                            </center>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                        <asp:HiddenField ID="customerId" runat="server" Value="false" />
                        <asp:HiddenField ID="width" runat="server" Value="0" />
                        <asp:HiddenField ID="height" runat="server" Value="0" />
                        <div class="GeoFences-Content">

                            <div id="divSearchResultBtn">
                                <asp:Panel id="pnlResultButtons" runat="server" Visible="false">
                                    <asp:LinkButton ID="btnDirectionsSearch" runat="server" CssClass="btnSearchResult" Width="163" Height="30" OnClick="btnDirectionsSearch_Click" >Direcciones</asp:LinkButton>
                                    <asp:LinkButton ID="btnPointsSearch" runat="server" CssClass="btnSearchResult" Width="163" Height="30" OnClick="btnPointsSearch_Click">Puntos</asp:LinkButton>
                                </asp:Panel>
                            </div>

                            <div id="divSearchResult">
                                <asp:Panel id="Panel10" runat="server">
                                    <asp:Table Visible="false" id="directionsTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                    <asp:Table Visible="false" id="pointsSearchTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                </asp:Panel>
                            </div>
                            <div id="ContentSearchId" class="ContentSearch" runat="server">
                                <div class="row">
                                    <div class="col-sm-1 col-sm-offset-1"> 
                                                <strong>
                                                    <asp:Label ID="lblLevel1" class="control-label" runat="server" Text=""></asp:Label>
                                                </strong>                            
                                    </div>
                                    <div class="col-sm-2">  
                                                <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="provinciaList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="provinciaList_SelectedIndexChanged"></asp:DropDownList>
                                                                        
                                   </div>
                                   <div class="col-sm-1">                            
                                                <strong>
                                                    <asp:Label ID="lblLevel2" class="control-label" runat="server" Text=""></asp:Label>
                                                </strong> 
                                   </div>
                                    <div class="col-sm-2">
                                                <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="cantonesList" runat="server" AutoPostBack="false"></asp:DropDownList>
                                   </div>
                                    <div class="col-sm-4">                            
                                            <div class="input-group">
                                                <asp:TextBox ID="txtSearch" runat="server" name="key" type="text" class="form-control" placeholder="Buscar..."></asp:TextBox>
                                                <span class="input-group-btn">
                                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancelar" class="btn btn-default" OnClick="btnCancel_Click" title="Cancelar">
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnSearch" runat="server" Text="Buscar" class="btn btn-primary" OnClick="btnSearch_Click" title="Buscar">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                            
                                    </div>
                                </div>
                            </div>
                                <div class="ContentMap" >
                                    <asp:Panel ID="pnlCoinca" runat="server" CssClass="mapMenu-controls" Visible="true">
                                        <aspmap:Map id="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnPolygonTool="map_PolygonToolCoinca" ></aspmap:Map>
                                    <div class="ColorListText" style="margin-left:5px ; width: 240px;">
                                           © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>  
                                        </div>
                                         </asp:Panel>
                                    <asp:Panel ID="pnlGoogle" runat="server" CssClass="mapMenu-controls" Visible="false">
                                        <aspmap:Map id="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnPolygonTool="map_PolygonToolGoogle" ></aspmap:Map>
                                        <asp:Panel ID="pnlSatellite" runat="server" CssClass="pnlSatellite">
                                            <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged"/>
                                        </asp:Panel>
                                    </asp:Panel>
                                     <style> /*Estilo para los botones de zoom automatico*/
                                        .imageZoom{
                                            margin-left: -7px;
                                            margin-top: -2px;
                                            height: 53px;
                                            width: 53px;
                                            border: 1px outset #ccc !important;
                                        }
                                    </style>
                                    <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                                        <div class="LeftMenu" >
                                            <center>
                                                <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                                    <%--<asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('OSM')" ></asp:ImageButton>--%>
                                                    <%--<aspmap:MapToolButton id="ZoomInTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapCoinca" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                    <aspmap:MapToolButton id="zoomOutTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapCoinca" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                    <aspmap:MapToolButton id="panTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapCoinca" MapTool="Pan" ></aspmap:MapToolButton>
                                                    <aspmap:MapToolButton id="centerTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapCoinca" MapTool="Center" ></aspmap:MapToolButton>
                                	                <aspmap:MapToolButton id="polygonTool" Visible="true" class="mapMenu" runat="server" ToolTip="Polygon Tool" ImageUrl="~/TOOLS/Custom/Polygon.png" Map="mapCoinca" MapTool="Polygon" ></aspmap:MapToolButton>
                                                    <button onclick="zoomIn()" type="button" class="mapMenu" style="margin-top: 4px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomIn.png" /></button>
                                                    <button onclick="zoomOut()" type="button" class="mapMenu" style="margin-top: 5px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomOut.png" /></button>

                                                    <%--<asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                                    <aspmap:MapToolButton id="ZoomInToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapGoogle" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                    <aspmap:MapToolButton id="zoomOutToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapGoogle" MapTool="ZoomOut" ></aspmap:MapToolButton>--%>
                                                    <aspmap:MapToolButton id="panToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapGoogle" MapTool="Pan" ></aspmap:MapToolButton>
                                                    <aspmap:MapToolButton id="centerToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapGoogle" MapTool="Center" ></aspmap:MapToolButton>
                                	                <aspmap:MapToolButton id="polygonToolGoogle" Visible="false" class="mapMenu" runat="server" ToolTip="Polygon Tool" ImageUrl="~/TOOLS/Custom/Polygon.png" Map="mapGoogle" MapTool="Polygon" ></aspmap:MapToolButton>
                                                </div>
                                            </center>
                                        </div>
                                    </asp:Panel>
                                    <div class="mapMenu-controls">
                                        <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                            <i class="glyphicon glyphicon-chevron-left"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                        </asp:LinkButton>
                                    </div>
                                    <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                                    <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                                    <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                                     
                                </div>
                            </div>
                        </div>
                         <div style="visibility:hidden">
                            <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click"/>              
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <%: Scripts.Render("~/bundles/jquery") %>
                <script src="Scripts/jquery.ba-resize.min.js"></script>
                <script>
                    function LoadGeoFenceValue(value) {
                        parent.postMessage(value, "*");
                    }

                    $(function () {
                        if ($('#<% =width.ClientID %>').val() == '0' && $('#<% =height.ClientID %>').val() == '0') {
                            resize();
                        }
                    });

                    $(window).resize(function () {
                        resize();
                    });

                    function resize() {
                        //updateDivColor();
                        var width = $(window).width() - 10;// -40;
                        
                        //alert(width);
                        var height = $(window).height() - 100; //-310;//-290
                        //VehicleRouteFooterInfo
                        $('#<% =width.ClientID %>').attr('value', width);
                        $('#<% =height.ClientID %>').attr('value', height);
                        //$("#ContentSearchId").width(width + "!important");

                        $('#<%=btnResize.ClientID %>').click();
                    }

                    var map = AspMap.getMap('<%=mapCoinca.ClientID%>');

                    function zoomIn() {
                        map.zoomIn();
                    }

                    function zoomOut() {
                        map.zoomOut();
                    }
                </script>
            </section>
        </div>
    </form>
</body>
</html>
