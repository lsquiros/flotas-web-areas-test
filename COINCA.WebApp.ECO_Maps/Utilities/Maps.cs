﻿using System.Collections.Generic;
using ECO_Maps.Business;
using ECO_Maps.Models;
using System;
using System.Linq;
using AspMap;
using System.Web;
using System.Globalization;
using ECO_Maps.Business.Atrack;
using ECO_Maps.Business.Routes;
using AspMap.Web.Extensions;
using ECO_Maps.Business.PassControl;
using ECO_Maps.Business.ECOsystem;
using ECO_Maps.Business.Intrack;
using System.Data;
using Newtonsoft.Json;
using ECOsystem.Management.Models;

namespace ECO_Maps.Utilities
{
    public class Maps
    {
        public AspMap.Web.TileLayer GetTileLayer(string CountryCode)
        {
            List<ECO_Maps.Models.LayerTypes> vDataTypes = new List<ECO_Maps.Models.LayerTypes>();
            AspMap.Web.TileLayer vTileLayer = null;
            string vRutaCache = string.Empty;
            int layersLoaded = 0;
            bool errorLoadingLayer = true;
            AspMap.Layer vLayer = new Layer();
            ECO_Maps.Models.Layers layer = null;

            List<ECO_Maps.Models.LayerTypes> listLayerTypes = null;
            List<ECO_Maps.Models.ImagesSymbols> listImagesSimbols = MapsBusiness.RetrieveLayerMaps().ToList();

            ECO_Maps.Models.LayerTypes layerType = new ECO_Maps.Models.LayerTypes();
            AspMap.FeatureRenderer vRenderer;
            AspMap.Feature vFeature;
            ECO_Maps.Models.Maps map = null;
            map = MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();
            if (map != null)
            {
                vRutaCache = map.Code;
            }
            if (vRutaCache != string.Empty)
            {
                vTileLayer = AspMap.Web.TileLayer.Create(vRutaCache);
                if (vTileLayer == null)
                {
                    vTileLayer = AspMap.Web.TileLayer.Find(vRutaCache);
                    return vTileLayer;
                }
                List<LayerMaps> listLayerMaps = null;

                listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();
                foreach (LayerMaps layerMap in listLayerMaps)
                {
                    errorLoadingLayer = true;
                    vLayer = null;
                    layerType = null;
                    try
                    {
                        layer = MapsBusiness.RetrieveLayerMapsByLayer(layerMap.LayerId).FirstOrDefault();
                        listLayerTypes = MapsBusiness.RetrieveLayerMapsByLayerTypes(layerMap.LayerId).ToList();

                        if (layer.LayerTypeId == 1)
                        {
                            vLayer = vTileLayer.AddLayer(map.Directory + layer.FileName);
                            vLayer.Name = layer.Description;
                            vLayer.Tag = layer.LayerId;
                            errorLoadingLayer = false;
                        }
                    }
                    catch (Exception)
                    {
                        errorLoadingLayer = true;
                    }
                    if (errorLoadingLayer == false)
                    {
                        if (vLayer != null && layer.LayerTypeId == 1)
                        {
                            layersLoaded = layersLoaded + 1;
                            vLayer.Symbol.Size = 0;
                            vLayer.Symbol.PointStyle = AspMap.PointStyle.Circle;
                            vLayer.Symbol.InnerColor = System.Drawing.Color.Transparent;
                            vLayer.Symbol.LineColor = System.Drawing.Color.Transparent;
                            vLayer.Symbol.FillColor = System.Drawing.Color.Transparent;
                            vLayer.DuplicateLabels = false;
                            vLayer.UseDefaultSymbol = false;
                            vLayer.ShowLabels = false;
                            if (layer.LabelShow)
                            {
                                if (layer.Label.Length > 0)
                                {
                                    vLayer.ShowLabels = true;
                                    vLayer.LabelField = layer.Label;
                                }
                            }
                            if (listLayerTypes != null && listLayerTypes.Count > 0)
                            {
                                vRenderer = vLayer.Renderer;
                                vRenderer.Field = "TYPE";
                                foreach (var type in listLayerTypes)
                                {
                                    vFeature = new AspMap.Feature();
                                    vFeature.Value = type.LayerType;
                                    if (type.LabelShow)
                                    {
                                        vFeature.ShowLabels = true;
                                        vFeature.LabelFont.Outline = true;
                                        vFeature.LabelFont.Size = 14;
                                    }
                                    else
                                    {
                                        vFeature.ShowLabels = false;
                                    }
                                    if (type.BorderShow)
                                    {
                                        vFeature.Symbol.LineColor = System.Drawing.Color.FromArgb(type.BorderColor);
                                        vFeature.Symbol.Size = type.BorderWidth;
                                    }
                                    if (type.FillUse)
                                    {
                                        vFeature.Symbol.FillColor = System.Drawing.Color.FromArgb(type.FillColor);
                                    }
                                    if (type.ZoomUse)
                                    {
                                        vFeature.MinScale = type.ZoomMin;
                                        vFeature.MaxScale = type.ZoomMax;
                                    }
                                    if (type.StyleAspMap > 0)
                                    {
                                        if (layer.LayerSubtypeId == 2)
                                        {
                                            vFeature.Symbol.LineStyle = (LineStyle)type.StyleAspMap;
                                            vFeature.Symbol.FillStyle = AspMap.FillStyle.Vertical;
                                        }
                                        else if (layer.LayerSubtypeId == 3)
                                        {
                                            vFeature.Symbol.FillStyle = (FillStyle)type.StyleAspMap;
                                        }
                                    }
                                    if (type.ImageSymbol != null)
                                    {
                                        if (type.ImageSymbol > 0)
                                        {
                                            try
                                            {
                                                ImagesSymbols vImageSimbol = (from i in listImagesSimbols
                                                                              where i.ImageSymbolId == type.ImageSymbol
                                                                              select i).FirstOrDefault();
                                                if (layer.LayerTypeId == 1 && layer.LayerSubtypeId == 1)
                                                {
                                                    vFeature.Symbol.PointStyle = AspMap.PointStyle.Bitmap;
                                                }
                                                if (vImageSimbol != null)
                                                {
                                                    vFeature.Symbol.Bitmap = HttpContext.Current.Server.MapPath(vImageSimbol.Image.Replace("/", "\\"));
                                                    vFeature.Symbol.Size = vImageSimbol.Height;
                                                }
                                                vFeature.Symbol.TransparentColor = System.Drawing.Color.Black;
                                            }
                                            catch (Exception)
                                            {
                                                vFeature.Symbol.PointStyle = AspMap.PointStyle.Circle;
                                            }
                                        }
                                    }
                                    vRenderer.Add(vFeature);
                                }
                            }
                            else
                            {
                                if (layer.ZoomUse)
                                {
                                    vLayer.MinScale = layer.ZoomMin;
                                    vLayer.MaxScale = layer.ZoomMax;
                                    if (vLayer.ShowLabels)
                                    {
                                        vLayer.LabelMinScale = layer.ZoomMin;
                                        vLayer.LabelMaxScale = layer.ZoomMax;
                                    }
                                }
                                vLayer.Visible = layer.Visible;
                                if (layer.LayerTypeId == 1)
                                {
                                    if (layer.LayerSubtypeId == 1)
                                    {
                                        vLayer.Symbol.PointStyle = AspMap.PointStyle.CircleWithSmallCenter;
                                        vLayer.Symbol.Size = 10;
                                        if (layer.LabelShow)
                                        {
                                            vLayer.LabelField = layer.Label;
                                            vLayer.ShowLabels = true;
                                            vLayer.LabelFont.Outline = true;
                                            vLayer.LabelFont.Size = 14;
                                        }
                                        if (layer.FillUse)
                                        {
                                            vLayer.Symbol.FillColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                    }
                                    else if (layer.LayerSubtypeId == 2)
                                    {
                                        vLayer.Symbol.LineStyle = (LineStyle)layer.AspMapStyle;
                                        vLayer.Symbol.LineColor = System.Drawing.Color.FromArgb(layer.BorderColor);
                                        if (layer.LabelShow)
                                        {
                                            vLayer.LabelField = layer.Label;
                                            vLayer.ShowLabels = true;
                                            vLayer.LabelFont.Outline = true;
                                            vLayer.LabelFont.Size = 14;
                                        }
                                        if (layer.FillUse)
                                        {
                                            vLayer.Symbol.FillColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                        vLayer.Symbol.Size = layer.BorderWidth;
                                    }
                                    else if (layer.LayerSubtypeId == 3)
                                    {
                                        if (layer.LabelShow)
                                        {
                                            vLayer.LabelField = layer.Label;
                                            vLayer.ShowLabels = true;
                                            vLayer.LabelFont.Outline = true;
                                            vLayer.LabelFont.Size = 14;
                                        }
                                        if (layer.BorderUse)
                                        {
                                            vLayer.Symbol.LineColor = System.Drawing.Color.FromArgb(layer.BorderColor);
                                        }
                                        else
                                        {
                                            vLayer.Symbol.LineColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                        vLayer.Symbol.FillStyle = (FillStyle)layer.AspMapStyle;
                                        if (layer.FillUse)
                                        {
                                            vLayer.Symbol.FillColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                    }
                                }
                            }
                            vLayer = null;
                        }
                    }
                }
            }
            if (vRutaCache != string.Empty)
            {
                vTileLayer.Initialize();
            }
            return vTileLayer;
        }

        public static AspMap.Web.TileLayer GetGoogleTileLayer(string CountryCode)
        {
            List<ECO_Maps.Models.LayerTypes> vDataTypes = new List<ECO_Maps.Models.LayerTypes>();
            AspMap.Web.TileLayer vTileLayer = null;
            string vRutaCache = string.Empty;
            int layersLoaded = 0;
            bool errorLoadingLayer = true;
            AspMap.Layer vLayer = new AspMap.Layer();
            ECO_Maps.Models.Layers layer = null;
            List<ECO_Maps.Models.LayerTypes> listLayerTypes = null;

            List<ECO_Maps.Models.ImagesSymbols> listImagesSimbols = MapsBusiness.RetrieveLayerMaps().ToList();

            ECO_Maps.Models.LayerTypes layerType = new ECO_Maps.Models.LayerTypes();
            AspMap.FeatureRenderer vRenderer;
            AspMap.Feature vFeature;
            ECO_Maps.Models.Maps map = null;
            var businessMaps = new MapsBusiness();
            map = MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();
            if (map != null)
            {
                vRutaCache = map.Code;
            }
            if (vRutaCache != string.Empty)
            {
                vTileLayer = AspMap.Web.TileLayer.Create(vRutaCache);
                if (vTileLayer == null)
                {
                    vTileLayer = AspMap.Web.TileLayer.Find(vRutaCache);
                    return vTileLayer;
                }
                List<LayerMaps> listLayerMaps = null;
                listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();
                foreach (LayerMaps layerMap in listLayerMaps)
                {
                    errorLoadingLayer = true;
                    vLayer = null;
                    layerType = null;
                    try
                    {
                        layer = MapsBusiness.RetrieveLayerMapsByLayer(layerMap.LayerId).FirstOrDefault();
                        listLayerTypes = MapsBusiness.RetrieveLayerMapsByLayerTypes(layerMap.LayerId).ToList();

                        if (layer.LayerTypeId == 1)
                        {
                            vLayer = vTileLayer.AddLayer(map.Directory + layer.FileName);
                            vLayer.Name = layer.Description;
                            vLayer.Tag = layer.LayerId;
                            errorLoadingLayer = false;
                        }
                    }
                    catch (Exception)
                    {
                        errorLoadingLayer = true;
                    }
                    if (!errorLoadingLayer)
                    {
                        if (vLayer != null && layer.LayerTypeId == 1)
                        {
                            layersLoaded = layersLoaded + 1;
                            vLayer.Symbol.Size = 0;
                            vLayer.Symbol.PointStyle = AspMap.PointStyle.Circle;
                            vLayer.Symbol.InnerColor = System.Drawing.Color.Transparent;
                            vLayer.Symbol.LineColor = System.Drawing.Color.Transparent;
                            vLayer.Symbol.FillColor = System.Drawing.Color.Transparent;
                            vLayer.DuplicateLabels = false;
                            vLayer.UseDefaultSymbol = false;
                            vLayer.ShowLabels = false;
                            if (layer.LabelShow)
                            {
                                if (layer.Label.Length > 0)
                                {
                                    vLayer.ShowLabels = true;
                                    vLayer.LabelField = layer.Label;
                                }
                            }
                            if (listLayerTypes != null && listLayerTypes.Count > 0)
                            {
                                vRenderer = vLayer.Renderer;
                                vRenderer.Field = "TYPE";
                                foreach (var type in listLayerTypes)
                                {
                                    vFeature = new AspMap.Feature();
                                    vFeature.Value = type.LayerType;
                                    if (type.LabelShow)
                                    {
                                        vFeature.ShowLabels = true;
                                        vFeature.LabelFont.Outline = true;
                                        vFeature.LabelFont.Size = 14;
                                    }
                                    else
                                    {
                                        vFeature.ShowLabels = false;
                                    }
                                    if (type.BorderShow)
                                    {
                                        vFeature.Symbol.LineColor = System.Drawing.Color.FromArgb(type.BorderColor);
                                        vFeature.Symbol.Size = type.BorderWidth;
                                    }
                                    if (type.FillUse)
                                    {
                                        vFeature.Symbol.FillColor = System.Drawing.Color.FromArgb(type.FillColor);
                                    }
                                    if (type.ZoomUse)
                                    {
                                        vFeature.MinScale = type.ZoomMin;
                                        vFeature.MaxScale = type.ZoomMax;
                                    }
                                    if (type.StyleAspMap > 0)
                                    {
                                        if (layer.LayerSubtypeId == 2)
                                        {
                                            vFeature.Symbol.LineStyle = (LineStyle)type.StyleAspMap;
                                            vFeature.Symbol.FillStyle = AspMap.FillStyle.Vertical;
                                        }
                                        else if (layer.LayerSubtypeId == 3)
                                        {
                                            vFeature.Symbol.FillStyle = (FillStyle)type.StyleAspMap;
                                        }
                                    }
                                    if (type.ImageSymbol != null)
                                    {
                                        if (type.ImageSymbol > 0)
                                        {
                                            try
                                            {
                                                ImagesSymbols vImageSimbol = (from i in listImagesSimbols
                                                                              where i.ImageSymbolId == type.ImageSymbol
                                                                              select i).FirstOrDefault();
                                                if (layer.LayerTypeId == 1 && layer.LayerSubtypeId == 1)
                                                {
                                                    vFeature.Symbol.PointStyle = AspMap.PointStyle.Bitmap;
                                                }
                                                if (vImageSimbol != null)
                                                {
                                                    vFeature.Symbol.Bitmap = HttpContext.Current.Server.MapPath(vImageSimbol.Image.Replace("/", "\\"));
                                                    vFeature.Symbol.Size = vImageSimbol.Height;
                                                }
                                                vFeature.Symbol.TransparentColor = System.Drawing.Color.Black;
                                            }
                                            catch (Exception)
                                            {
                                                vFeature.Symbol.PointStyle = AspMap.PointStyle.Circle;
                                            }
                                        }
                                    }
                                    vRenderer.Add(vFeature);
                                }
                            }
                            else
                            {
                                if (layer.ZoomUse)
                                {
                                    vLayer.MinScale = layer.ZoomMin;
                                    vLayer.MaxScale = layer.ZoomMax;
                                    if (vLayer.ShowLabels)
                                    {
                                        vLayer.LabelMinScale = layer.ZoomMin;
                                        vLayer.LabelMaxScale = layer.ZoomMax;
                                    }
                                }
                                vLayer.Visible = layer.Visible;
                                if (layer.LayerTypeId == 1)
                                {
                                    if (layer.LayerSubtypeId == 1)
                                    {
                                        vLayer.Symbol.PointStyle = AspMap.PointStyle.CircleWithSmallCenter;
                                        vLayer.Symbol.Size = 10;
                                        if (layer.LabelShow)
                                        {
                                            vLayer.LabelField = layer.Label;
                                            vLayer.ShowLabels = true;
                                            vLayer.LabelFont.Outline = true;
                                            vLayer.LabelFont.Size = 14;
                                        }
                                        if (layer.FillUse)
                                        {
                                            vLayer.Symbol.FillColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                    }
                                    else if (layer.LayerSubtypeId == 2)
                                    {
                                        vLayer.Symbol.LineStyle = (LineStyle)layer.AspMapStyle;
                                        vLayer.Symbol.LineColor = System.Drawing.Color.FromArgb(layer.BorderColor);
                                        if (layer.LabelShow)
                                        {
                                            vLayer.LabelField = layer.Label;
                                            vLayer.ShowLabels = true;
                                            vLayer.LabelFont.Outline = true;
                                            vLayer.LabelFont.Size = 14;
                                        }
                                        if (layer.FillUse)
                                        {
                                            vLayer.Symbol.FillColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                        vLayer.Symbol.Size = layer.BorderWidth;
                                    }
                                    else if (layer.LayerSubtypeId == 3)
                                    {
                                        if (layer.LabelShow)
                                        {
                                            vLayer.LabelField = layer.Label;
                                            vLayer.ShowLabels = true;
                                            vLayer.LabelFont.Outline = true;
                                            vLayer.LabelFont.Size = 14;
                                        }
                                        if (layer.BorderUse)
                                        {
                                            vLayer.Symbol.LineColor = System.Drawing.Color.FromArgb(layer.BorderColor);
                                        }
                                        else
                                        {
                                            vLayer.Symbol.LineColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                        vLayer.Symbol.FillStyle = (FillStyle)layer.AspMapStyle;
                                        if (layer.FillUse)
                                        {
                                            vLayer.Symbol.FillColor = System.Drawing.Color.FromArgb(layer.FillColor);
                                        }
                                    }
                                }
                            }
                            vLayer = null;
                        }
                    }
                }
            }
            if (vRutaCache != string.Empty)
            {
                vTileLayer.Initialize();
            }
            return vTileLayer;
        }

        public void SetUpMap(AspMap.Web.Map pMap)
        {
            int i;
            pMap.EnableAnimation = true;
            pMap.ScaleBar.Visible = true;
            pMap.ScaleBar.BarUnit = AspMap.UnitSystem.Metric;
            pMap.ScaleBar.MaxWidth = Convert.ToInt16(pMap.Width.Value / 4);
            if (pMap.ZoomLevels.Count == 0)
            {
                if (pMap.LayerCount >= 0)
                {
                    pMap.RemoveAllLayers();
                }
                for (i = pMap.ZoomLevels.Count; i == 1; i--)
                {
                    pMap.ZoomLevels.RemoveAt(i - 1);
                }
                pMap.ZoomLevels.Add(2280000, "Total");
                pMap.ZoomLevels.Add(1824000, "País");
                pMap.ZoomLevels.Add(1368000, "Región");
                pMap.ZoomLevels.Add(912000, "Subregión");
                pMap.ZoomLevels.Add(684000, "Provincia");
                pMap.ZoomLevels.Add(456000);
                pMap.ZoomLevels.Add(228000);
                pMap.ZoomLevels.Add(114000);
                pMap.ZoomLevels.Add(57000);
                pMap.ZoomLevels.Add(28500);
                pMap.ZoomLevels.Add(14250);
                pMap.ZoomLevels.Add(7125);
                pMap.ZoomLevels.Add(3562);
                pMap.ZoomLevels.Add(1781);
                pMap.ZoomLevels.Add(891, "Máximo");
                pMap.ImageOpacity = 0;
            }
        }

        public Polygon GetPolygonFromString(string pPolygonStrring, string pMap)
        {
            Polygon result = new Polygon();
            Points Points = new Points();
            pPolygonStrring = pPolygonStrring.Replace("POLYGON ((", string.Empty).Replace("))", string.Empty).Trim();
            string[] points = pPolygonStrring.Split(',');
            foreach (string point in points)
            {
                string[] ps = point.Trim().Split(' ');
                int count = 0;
                AspMap.Point Point = new AspMap.Point();
                foreach (string p in ps)
                {
                    if (count == 0)
                    {
                        Point.X = GetX(Convert.ToDouble(p), pMap);
                    }
                    else
                    {
                        Point.Y = GetY(Convert.ToDouble(p), pMap);
                    }
                    count = count + 1;
                }
                Points.Add(Point);
            }
            result.Add(Points);
            return result;
        }

        public string GetStringFromPolygon(AspMap.Polygon pPolygon, string pMap)
        {
            string result = string.Empty;
            for (int x = 0; x < pPolygon.PointCount; x++)
            {
                if (result != string.Empty)
                {
                    result = result + ", ";
                }
                result = result + GetAspX(pPolygon.GetPoint(x).X, pMap);
                result = result + " ";
                result = result + GetAspY(pPolygon.GetPoint(x).Y, pMap);
            }
            result = "POLYGON ((" + result + "))";
            return result;
        }

        public Polyline GetPolylineFromString(string pPolylineStrring)
        {
            Polyline result = new Polyline();
            Points Points = new Points();
            pPolylineStrring = pPolylineStrring.Replace("LINESTRING (", string.Empty).Replace(")", string.Empty).Trim();
            string[] points = pPolylineStrring.Split(',');
            foreach (string point in points)
            {
                string[] ps = point.Trim().Split(' ');
                int count = 0;
                AspMap.Point Point = new AspMap.Point();
                foreach (string p in ps)
                {
                    if (count == 0)
                    {
                        Point.X = Convert.ToDouble(p);
                    }
                    else
                    {
                        Point.Y = Convert.ToDouble(p);
                    }
                    count = count + 1;
                }
                Points.Add(Point);
            }
            result.Add(Points);
            return result;
        }

        public string GetStringFromPolyline(AspMap.Polyline pPolyline)
        {
            string result = string.Empty;
            for (int x = 0; x < pPolyline.PointCount; x++)
            {
                if (result != string.Empty)
                {
                    result = result + ", ";
                }
                result = result + pPolyline.GetPoint(x).X;
                result = result + " ";
                result = result + pPolyline.GetPoint(x).Y;
            }
            result = "LINESTRING (" + result + ")";
            return result;
        }

        public List<string> GetProvinciasList(string CountryCode)
        {
            List<string> result = new List<string>();
            var businessMaps = new MapsBusiness();
            ECO_Maps.Models.Maps map = MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();

            List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();

            AspMap.Layer layer = new Layer();
            layer = AspMap.Layer.Open(map.Directory.Trim() + "GeoCodificacion.TAB");

            AspMap.Recordset recordSet = new AspMap.Recordset();
            recordSet = layer.SearchExpression("PAIS = " + '\u0022' + map.Description + '\u0022');
            result.Add(string.Empty);
            while (!recordSet.EOF)
            {
                result.Add(recordSet[2].ToString().Trim());
                recordSet.MoveNext();
            }
            result = (from r in result orderby r select r).Distinct().ToList();
            return result;
        }

        public string GetDirection(string pCountryCode, AspMap.Point pPoint)
        {
            AspMap.Point vUbicacion = null;
            string result = string.Empty;
            string vPlace = string.Empty;
            string vHeading = string.Empty;
            double vDistance = 0;

            var businessMaps = new MapsBusiness();
            ECO_Maps.Models.Maps map = MapsBusiness.RetrieveMaps(pCountryCode).FirstOrDefault();

            List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();

            AspMap.Layer layer = new Layer();
            layer = AspMap.Layer.Open(map.Directory.Trim() + "GeoCodificacionCiudades.TAB");
            AspMap.Recordset vRecords = layer.SearchNearest(pPoint);

            if (vRecords.EOF == false)
            {
                vUbicacion = vRecords.Shape.Centroid;
                vPlace = vRecords[1].ToString();
            }
            vDistance = Math.Round(GetDistanceKm(pPoint, vUbicacion), 1);
            if (vDistance > 0)
            {
                if (vDistance >= 1)
                {
                    result = vDistance.ToString("n1");
                    result = result + " km al ";
                }
                else
                {
                    result = (vDistance * 1000).ToString("n0");
                    result = result + " mts al ";
                }
            }
            vHeading = DeltaHeading(pPoint, vUbicacion);
            result = result + vHeading + " de ";
            result = result + vPlace;
            return result.ToUpper();
        }

        public string DeltaHeading(AspMap.Point pPointA, AspMap.Point pPointB)
        {
            string result = string.Empty;
            double vHeadingX = 0;
            double vHeadingY = 0;
            double vHeading = 0;
            double vAtan = 0;
            double vSetting = 0;

            try
            {
                vHeadingX = pPointA.X - pPointB.X;
                vHeadingY = pPointA.Y - pPointB.Y;

                //no tiene rumbo
                if (vHeadingX == 0 && vHeadingY == 0)
                {
                    return string.Empty;
                }
                if (vHeadingX == 0)
                {
                    if (vHeadingY > 0) { vHeading = 90; }
                    else { vHeading = 270; }
                }
                else
                {
                    if (vHeadingY == 0)
                    {
                        if (vHeadingX > 0)
                        {
                            vHeading = 0;
                        }
                        else
                        {
                            vHeading = 180;
                        }
                    }
                    else
                    {
                        vAtan = Math.Atan(vHeadingY / vHeadingX);
                        vAtan = vAtan * 180 / Math.PI;
                        if (vHeadingX < 0 && vHeadingY > 0)
                        {
                            vSetting = 180;
                        }
                        else if (vHeadingX < 0 && vHeadingY < 0)
                        {
                            vSetting = 180;
                        }
                        else if (vHeadingX > 0 && vHeadingY < 0)
                        {
                            vSetting = 360;
                        }
                        vHeading = vAtan + vSetting;
                    }
                }
                if (vHeading >= 338 && vHeading <= 360 || vHeading >= 0 && vHeading <= 23) { result = "Norte"; }
                else if (vHeading >= 23 && vHeading <= 68) { result = "Noreste"; }
                else if (vHeading >= 68 && vHeading <= 113) { result = "Este"; }
                else if (vHeading >= 113 && vHeading <= 158) { result = "Sureste"; }
                else if (vHeading >= 158 && vHeading <= 203) { result = "Sur"; }
                else if (vHeading >= 203 && vHeading <= 248) { result = "Suroeste"; }
                else if (vHeading >= 248 && vHeading <= 293) { result = "Oeste"; }
                else if (vHeading >= 293 && vHeading <= 338) { result = "Noreste"; }
                return result;
            }
            catch
            {
                return result;
            }
        }

        public double GetDistanceKm(Point pPointA, Point pPointB)
        {
            try
            {
                double rlat1 = Math.PI * pPointA.Y / 180;
                double rlat2 = Math.PI * pPointB.Y / 180;
                double theta = pPointA.X - pPointB.X;
                double rtheta = Math.PI * theta / 180;
                double dist =
                    Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                    Math.Cos(rlat2) * Math.Cos(rtheta);
                dist = Math.Acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                return dist * 1.609344;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public List<string> GetCantonList(string CountryCode, string provincia)
        {
            List<string> result = new List<string>();
            var businessMaps = new MapsBusiness();
            ECO_Maps.Models.Maps map = MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();

            List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();

            AspMap.Layer layer = new Layer();
            layer = AspMap.Layer.Open(map.Directory.Trim() + "GeoCodificacion.TAB");
            AspMap.Recordset recordSet = new AspMap.Recordset();
            recordSet = layer.SearchExpression("PAIS = " + '\u0022' + map.Description + '\u0022' + " AND PROVINCIA = " + '\u0022' + provincia + '\u0022');
            result.Add(string.Empty);
            while (!recordSet.EOF)
            {
                result.Add(recordSet[3].ToString().Trim());
                recordSet.MoveNext();
            }
            result = (from r in result orderby r select r).Distinct().ToList();
            return result;
        }

        public List<string> GetDistritoList(string CountryCode, string provincia, string canton)
        {
            List<string> result = new List<string>();
            var businessMaps = new MapsBusiness();
            ECO_Maps.Models.Maps map = MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();

            List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();

            AspMap.Layer layer = new Layer();
            layer = AspMap.Layer.Open(map.Directory.Trim() + "GeoCodificacion.TAB");
            AspMap.Recordset recordSet = new AspMap.Recordset();
            recordSet = layer.SearchExpression("PAIS = " + '\u0022' + map.Description + '\u0022' + " AND PROVINCIA = " + '\u0022' + provincia + '\u0022' + " AND CANTON = " + '\u0022' + canton + '\u0022');
            result.Add(string.Empty);
            while (!recordSet.EOF)
            {
                result.Add(recordSet[4].ToString().Trim());
                recordSet.MoveNext();
            }
            result = (from r in result orderby r select r).Distinct().ToList();
            return result;
        }

        public List<Models.SearchResult> Search(AspMap.Web.Map pMap, string pCountryCode, string pProvincia, string pCanton, string pDistrito, string pSearch)
        {
            List<SearchResult> result = new List<SearchResult>();
            var businessMaps = new MapsBusiness();
            string fileName = string.Empty;
            ECO_Maps.Models.Maps map = MapsBusiness.RetrieveMaps(pCountryCode).FirstOrDefault();
            AspMap.Layer vLayer = new Layer();
            AspMap.Shape vCentro = new AspMap.Shape(AspMap.ShapeType.Line);
            AspMap.Points vPuntos = new AspMap.Points(AspMap.ShapeType.Line);
            vPuntos.Add(new AspMap.Point(map.CenterLon.Value + 0.000001, map.CenterLat.Value));
            vPuntos.Add(new AspMap.Point(map.CenterLon.Value - 0.000001, map.CenterLat.Value));
            vCentro.Add(vPuntos);
            AspMap.Recordset rs;
            string filter = string.Empty;

            if (pDistrito == string.Empty)
            {
                if (pProvincia != string.Empty)
                {
                    if (filter != string.Empty)
                    {
                        filter = filter + " AND ";
                    }
                    filter = filter + " Region = " + '\u0022' + pProvincia + '\u0022';
                }
                if (pCanton != string.Empty)
                {
                    if (pCanton != string.Empty)
                    {
                        filter = filter + " AND ";
                    }
                    filter = filter + " City = " + '\u0022' + pCanton + '\u0022';
                }

                //If the search is empty and the Canton has info then use the Canton info
                pSearch = (string.IsNullOrEmpty(pSearch)) ? ((!string.IsNullOrEmpty(pCanton)) ? pCanton : string.Empty) : pSearch;

                if (pSearch != string.Empty)
                {
                    if (filter != string.Empty)
                    {
                        filter = filter + " AND ";
                    }
                    filter = filter + "LIKE(Label," + '\u0022' + "*" + pSearch + "*" + '\u0022' + ")";
                }

                List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();

                foreach (var x in listLayerMaps)
                {
                    if (x.Description == "Puntos")
                    {
                        ECO_Maps.Models.Layers layer = MapsBusiness.RetrieveLayerMapsByLayer(x.LayerId).FirstOrDefault(); //businessLayers.RetrieveLayerMaps(x.LayerId).FirstOrDefault();

                        fileName = layer.FileName;
                    }
                }
                vLayer = AspMap.Layer.Open(map.Directory + fileName);
                rs = vLayer.SearchBuffer(vCentro, pMap.ConvertDistance(1000000, AspMap.MeasureUnit.Kilometer, AspMap.MeasureUnit.Degree), AspMap.SearchMethod.Inside, filter);
                while (!rs.EOF)
                {
                    Models.SearchResult item = new Models.SearchResult();
                    item.description = rs.Fields[1].Value.ToString().Trim() + ", " + rs.Fields[3].Value.ToString().Trim() + ", " + rs.Fields[4].Value;
                    item.lat = rs.Shape.GetPoint(0).Y;
                    item.lon = rs.Shape.GetPoint(0).X;
                    result.Add(item);
                    rs.MoveNext();
                }
                return result;
            }
            else
            {
                var layerDistrito = new AspMap.DynamicLayer(LayerType.Polygon);
                AspMap.Layer layerMap = AspMap.Layer.Open(map.Directory + "GeoCodificacion.TAB");
                filter = "PAIS = " + '\u0022' + map.Description + '\u0022' + " AND PROVINCIA = " + '\u0022' + pProvincia + '\u0022' + " AND CANTON = " + '\u0022' + pCanton + '\u0022' + " AND DISTRITO = " + '\u0022' + pDistrito + '\u0022';
                rs = layerMap.SearchExpression(filter);
                while (!rs.EOF)
                {
                    layerDistrito.Add(rs.Shape, pCanton, pCanton);
                    rs.MoveNext();
                }
                filter = "LIKE(Label," + '\u0022' + "*" + pSearch + "*" + '\u0022' + ")";

                List<LayerMaps> listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();

                foreach (var x in listLayerMaps)
                {
                    if (x.Description == "Puntos")
                    {
                        ECO_Maps.Models.Layers layer = MapsBusiness.RetrieveLayerMapsByLayer(x.LayerId).FirstOrDefault();
                        fileName = layer.FileName;
                    }
                }
                vLayer = AspMap.Layer.Open(map.Directory + fileName);
                for (int i = 0; i <= layerDistrito.Count - 1; i++)
                {
                    rs = vLayer.SearchShape(layerDistrito[0].Shape, SearchMethod.Intersect, filter);
                    while (!rs.EOF)
                    {
                        Models.SearchResult item = new Models.SearchResult();
                        item.description = rs.Fields[1].Value.ToString().Trim() + ", " + rs.Fields[3].Value.ToString().Trim() + ", " + rs.Fields[4].Value;
                        item.lat = rs.Shape.GetPoint(0).Y;
                        item.lon = rs.Shape.GetPoint(0).X;
                        result.Add(item);
                        rs.MoveNext();
                    }
                }
                return result;
            }

        }

        public AspMap.Route findRoute(string countryCode, Point punto1, AspMap.Point punto2, string routeType)
        {
            if (countryCode == string.Empty) { return null; }
            AspMap.Network roadNetwork;
            using (roadNetwork = new AspMap.Network())
            {
                string capaEdita = string.Empty;
                string capaArchivo = string.Empty;
                ECO_Maps.Models.Maps map = null;
                var businessMaps = new MapsBusiness();
                map = MapsBusiness.RetrieveMaps(countryCode.Trim()).FirstOrDefault();
                if (map != null)
                {
                    List<LayerMaps> listLayerMaps = null;

                    listLayerMaps = MapsBusiness.RetrieveLayerMaps(map.MapId).ToList();
                    foreach (var x in listLayerMaps)
                    {
                        if (x.Description == "Calles" || x.Description == "Ruteo")
                        {
                            Models.Layers layer = MapsBusiness.RetrieveLayerMapsByLayer(x.LayerId).FirstOrDefault();
                            if (x.Description == "Calles")
                            {
                                capaEdita = map.Directory + layer.FileName;
                            }
                            else if (x.Description == "Ruteo")
                            {
                                capaArchivo = map.Directory + layer.FileName;
                            }
                        }
                    }
                }
                if (!roadNetwork.Open(capaEdita, capaArchivo))
                {
                    throw new Exception("Cannot load road network: MAPS/CR/CR_Calles.rtn");
                }
                AspMap.Route route = roadNetwork.CreateRoute();
                route.DistanceUnit = MeasureUnit.Kilometer;
                if (routeType == "Shortest")
                {
                    route.RouteType = RouteType.Shortest;
                }
                else
                {
                    route.RouteType = RouteType.Quickest;
                }
                route.MaxDistance = 10000;
                if (route.FindRoute(punto1.X, punto1.Y, punto2.X, punto2.Y))
                {
                    route.MergeSegments("LABEL");
                }
                else
                {
                    route.Dispose();
                }
                return route;
            }
        }

        public Models.RoutesSegmentsBase GetSegments(List<Models.PointsList> pPointsList, List<Models.SegmentList> pSegmentsList)
        {

            Models.RoutesSegmentsBase result = new Models.RoutesSegmentsBase();
            foreach (var x in pSegmentsList)
            {
                var segment = new RoutesSegmentsBase.RoutesSegments
                {
                    StartReference = x.startPoint,
                    EndReference = x.endPoint,
                    Poliyline = GetStringFromPolyline(x.route),
                    Time = x.time,
                    Distance = x.distance
                };
                result.Segments.Add(segment);
            }
            foreach (var x in pPointsList)
            {
                var point = new RoutesSegmentsBase.RoutesPoints
                {
                    PointReference = x.PointReference,
                    Order = x.Order,
                    Name = x.Name
                };
                string hour = "0" + x.Hour;
                string minute = "0" + x.Minute;

                //Checks if the information comes from the file
                if (x.flag != 1)
                {
                    TimeSpan time = DateTime.ParseExact(hour.Substring(hour.Length - 2, 2) + ":" + minute.Substring(minute.Length - 2, 2), "HH:mm", CultureInfo.CurrentCulture).TimeOfDay.ToServerTime();
                    if (time.Hours < 0)
                    {
                        time = TimeSpan.FromHours(24).Add(time);
                    }
                    else if (time.Days > 0)
                    {
                        time = TimeSpan.FromHours(-24).Add(time);
                    }
                    point.Time = time.ToString();
                }
                else
                {
                    DateTime Ntime = Convert.ToDateTime(hour + ":" + minute, new CultureInfo("en-US"));
                    point.Time = Ntime.ToString();
                }

                point.Longitude = x.x.ToString();
                point.Latitude = x.y.ToString();
                point.StopTime = x.StopTime;
                result.Points.Add(point);
            }
            return result;
        }

        public List<Models.PointsList> GetRoutePoints(Models.RoutesSegmentsBase pRoutesPointsList)
        {
            var result = new List<PointsList>();
            foreach (var x in pRoutesPointsList.Points)
            {
                PointsList point = new PointsList
                {
                    Id = x.PointId,
                    Order = x.Order,
                    Name = x.Name,
                    Address = x.Address,
                    Description = x.Description,
                    PointReference = x.PointReference,
                    Iscommerce =  x.IsCommerce,
                    CommerceName = x.CommerceName
                };
                TimeSpan time = Convert.ToDateTime(x.Time).TimeOfDay.ToClientTime();
                if (time.Days < 0 || time.Hours < 0 || time.Minutes < 0) { time = TimeSpan.FromHours(24).Add(time); } else if (time.Days > 1) { time = time.Add(TimeSpan.FromDays(-1)); }
                point.Hour = time.Hours;
                point.Minute = time.Minutes;
                point.x = Convert.ToDouble(x.Longitude);
                point.y = Convert.ToDouble(x.Latitude);
                point.StopTime = x.StopTime;
                result.Add(point);
            }
            return result;
        }

        public List<Models.SegmentList> GetRouteSegments(Models.RoutesSegmentsBase pRoutesSegmentsList)
        {
            List<SegmentList> result = new List<SegmentList>();
            foreach (var x in pRoutesSegmentsList.Segments)
            {
                SegmentList segment = new SegmentList();
                segment.id = x.SegmentId;
                segment.startPoint = x.StartReference;
                segment.endPoint = x.EndReference;
                segment.route = GetPolylineFromString(x.Poliyline);
                segment.time = x.Time;
                segment.distance = x.Distance;
                result.Add(segment);
            }
            return result;
        }

        public IEnumerable<Models.VehiclesInfo> RetrieveTntrackReferenceDetail(string pIds)
        {
            IntrackVehicleBusiness business = new IntrackVehicleBusiness();
            return business.RetrieveTntrackReferenceDetail(pIds);
        }

        public IEnumerable<Models.VehiclesInfo> GetVehiclesByLevel(string pId, int pLevel)
        {
            var business = new EcOsystemBusiness();
            return business.RetrieveVehiclesByLevel(pId, pLevel);
        }

        public List<Models.ReportLast> GetVehicleReportLast(string pVehiclesId)
        {
            var atrackBusiness = new AtrackBusiness();
            return atrackBusiness.RetrieveReportLast(pVehiclesId).ToList();
        }

        public List<Models.ReportLast> GetVehicleReports(int pVehicleId, string pStartDate, string pEndDate)
        {
            var atrackBusiness = new AtrackBusiness();
            return atrackBusiness.RetrieveReports(pVehicleId, pStartDate, pEndDate).ToList();
        }

        public List<Models.Stop> GetVehicleStops(int pVehicleId, string pStartDate, string pEndDate)
        {
            var business = new RoutesBusiness();
            return business.RetrieveStops(pVehicleId, pStartDate, pEndDate).ToList();
        }

        public List<Models.ReportLast> GetVehicleReportsRange(int pVehicleId, string pStartDate, string pEndDate, bool pZoom)
        {
            var atrackBusiness = new AtrackBusiness();
            return atrackBusiness.RetrieveReportsRange(pVehicleId, pStartDate, pEndDate, pZoom).ToList();
        }

        public AspMap.Polyline GetVehicleReportsPolyline(List<Models.ReportLast> pListReports, string pMap)
        {
            Polyline result = new Polyline();
            AspMap.Points Points = new AspMap.Points();
            AspMap.Point lastPoint = null;
            foreach (var x in pListReports)
            {
                if (lastPoint != null)
                {
                    if (Math.Abs(Math.Abs(x.Longitude) - Math.Abs(lastPoint.X)) < 1 && Math.Abs(Math.Abs(x.Latitude) - Math.Abs(lastPoint.Y)) < 1)
                    {
                        if (pMap.Trim() == "Coinca")
                        {
                            Points.Add(new AspMap.Point(x.Longitude, x.Latitude));
                        }
                        else
                        {
                            Points.Add(new AspMap.Point(GetX(x.Longitude, pMap), GetY(x.Latitude, pMap)));
                        }
                    }
                }
                lastPoint = new AspMap.Point(x.Longitude, x.Latitude);
            }
            result.Add(Points);
            return result;
        }

        public List<Models.MapShapeSpeeding> GetVehicleReportsSpeedingPolyline(List<Models.ReportLast> pListReports, string pMap)
        {
            List<MapShapeSpeeding> result = new List<MapShapeSpeeding>();
            AspMap.Points Points = null;
            AspMap.Point lastPoint = null;
            int? lastSpeeding = 2;
            int? speeding = 2;
            foreach (var x in pListReports)
            {
                if (lastSpeeding != x.Speeding)
                {
                    speeding = lastSpeeding;
                    lastSpeeding = x.Speeding;
                    if (Points != null && speeding != 0)
                    {
                        Polyline polilyne = new Polyline();
                        Points.Add(lastPoint);
                        polilyne.Add(Points);
                        MapShapeSpeeding item = new MapShapeSpeeding();
                        item.Polyline = polilyne;
                        item.Speeding = Convert.ToInt16(speeding);
                        result.Add(item);
                    }
                    Points = new Points();
                }

                if (lastPoint != null)
                {
                    if (pMap.Trim() == "Coinca")
                    {
                        Points.Add(new AspMap.Point(x.Longitude, x.Latitude));
                    }
                    else
                    {
                        Points.Add(new AspMap.Point(GetX(x.Longitude, pMap), GetY(x.Latitude, pMap)));
                    }
                }
                lastSpeeding = x.Speeding;
                if (pMap.Trim() == "Coinca")
                {
                    lastPoint = new AspMap.Point(x.Longitude, x.Latitude);
                }
                else
                {
                    lastPoint = new AspMap.Point(GetX(x.Longitude, pMap), GetY(x.Latitude, pMap));
                }
            }
            if (Points != null && lastSpeeding != 0)
            {
                Polyline polilyne = new Polyline();
                Points.Add(lastPoint);
                polilyne.Add(Points);
                MapShapeSpeeding item = new MapShapeSpeeding();
                item.Polyline = polilyne;
                item.Speeding = Convert.ToInt16(lastSpeeding);
                result.Add(item);
            }
            return result;
        }

        public double GetX(double x, string layer)
        {
            if (layer.Trim() != "Coinca")
            {
                x = GetGoogleX(x);
            }
            return x;
        }

        public double GetY(double y, string layer)
        {
            if (layer.Trim() != "Coinca")
            {
                y = GetGoogleY(y);
            }
            return y;
        }

        public double GetAspX(double x, string layer)
        {
            if (layer.Trim() != "Coinca")
            {
                x = GetAspMapX(x);
            }
            return x;
        }

        public double GetAspY(double y, string layer)
        {
            if (layer.Trim() != "Coinca")
            {
                y = GetAspMapY(y);
            }
            return y;
        }

        public double GetGoogleX(double x)
        {
            return x * 20037508.34 / 180;
        }

        public double GetGoogleY(double y)
        {
            return (Math.Log(Math.Tan((90 + y) * Math.PI / 360)) / (Math.PI / 180)) * 20037508.34 / 180;
        }

        public double GetAspMapX(double x)
        {
            return (x / 20037508.34) * 180;
        }

        public double GetAspMapY(double y)
        {
            return 180 / Math.PI * (2 * Math.Atan(Math.Exp(((y / 20037508.34) * 180) * Math.PI / 180)) - Math.PI / 2);
        }

        public void addMapLayer(AspMap.Web.Map pMapCoinca, AspMap.Web.Map pMapGoogle, string layer, string pCountryCode)
        {
            if (!string.IsNullOrEmpty(pCountryCode))
            {
                if (layer == "Coinca")
                {
                    pMapGoogle.Visible = false;
                    pMapCoinca.Visible = true;

                    if (pMapCoinca.LayerCount <= 0)
                    {
                        AspMap.Web.TileLayer tileLayer = GetTileLayer(pCountryCode);
                        if (tileLayer != null)
                        {
                            pMapCoinca.AddLayer(tileLayer); // Carga las capas la primera vez
                            SetUpMap(pMapCoinca); // Carga la configuración la primera vez
                        }
                    }
                }
                else if (layer == "GoogleRoad")
                {
                    pMapCoinca.Visible = false;
                    pMapGoogle.Visible = true;

                    if (pMapGoogle.BackgroundLayer == null)
                    {
                        GoogleMapsLayer gml = new GoogleMapsLayer();
                        gml.MapType = GoogleMapType.Normal;
                        gml.EnableSSL = true;

                        pMapGoogle.AddLayer(GetGoogleTileLayer(pCountryCode));

                        pMapGoogle.BackgroundLayer = gml;
                    }
                    else
                    {
                        GoogleMapsLayer gml = (GoogleMapsLayer)pMapGoogle.BackgroundLayer;
                        gml.MapType = GoogleMapType.Normal;
                        gml.EnableSSL = true;

                        pMapGoogle.AddLayer(GetGoogleTileLayer(pCountryCode));

                        pMapGoogle.BackgroundLayer = gml;
                    }

                    pMapGoogle.ImageFormat = ImageFormat.Png;
                    pMapGoogle.ImageOpacity = 0.6;
                    pMapGoogle.MapUnit = MeasureUnit.Degree;
                    pMapGoogle.CoordinateSystem = new AspMap.CoordSystem(CoordSystemCode.GCS_WGS84);
                }
                else if (layer == "GoogleSatellite")
                {
                    pMapCoinca.Visible = false;
                    pMapGoogle.Visible = true;

                    if (pMapGoogle.BackgroundLayer == null)
                    {
                        GoogleMapsLayer gml = new GoogleMapsLayer();
                        gml.MapType = GoogleMapType.Satellite;
                        gml.EnableSSL = true;
                        pMapGoogle.BackgroundLayer = gml;
                    }
                    else
                    {
                        GoogleMapsLayer gml = (GoogleMapsLayer)pMapGoogle.BackgroundLayer;
                        gml.MapType = GoogleMapType.Satellite;
                        gml.EnableSSL = true;
                        pMapGoogle.BackgroundLayer = gml;
                    }

                    pMapGoogle.ImageFormat = ImageFormat.Png;
                    pMapGoogle.ImageOpacity = 0.6;
                    pMapGoogle.MapUnit = MeasureUnit.Degree;
                    pMapGoogle.CoordinateSystem = new AspMap.CoordSystem(CoordSystemCode.GCS_WGS84);
                }
                else if (layer == "OSM")
                {
                    OSMLayer osmLayer = null;

                    if (pMapCoinca.BackgroundLayer == null)
                    {
                        osmLayer = new OSMLayer("http://tile.openstreetmap.org/{z}/{x}/{y}.png");
                        osmLayer.EnableSSL = true;
                    }
                    else
                    {
                        osmLayer = (OSMLayer)pMapCoinca.BackgroundLayer;
                    }
                    pMapCoinca.BackgroundLayer = osmLayer;
                    pMapCoinca.ImageFormat = ImageFormat.Png;
                    pMapCoinca.ImageOpacity = 0.6;
                    pMapCoinca.MapUnit = MeasureUnit.Degree;
                    pMapCoinca.CoordinateSystem = new AspMap.CoordSystem(CoordSystemCode.GCS_WGS84);
                }

            }
        }

        public void GetCountryCoordinates(ref string lat, ref string lon, string CountryCode)
        {
            ECO_Maps.Models.Maps map = null;
            var businessMaps = new MapsBusiness();
            map = MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();
            lat = map.CenterLat.ToString();
            lon = map.CenterLon.ToString();
        }

        public AspMap.Polyline convertSegmentToGoogle(AspMap.Polyline route, string layerMap)
        {
            AspMap.Polyline result = new AspMap.Polyline();
            Points Points = new Points();
            for (int i = 0; i < route.PointCount; i++)
            {
                Points.Add(new AspMap.Point(GetX(route[0][i].X, layerMap), GetY(route[0][i].Y, layerMap)));
            }
            result.Add(Points);
            return result;
        }

        public List<Models.PassControlVehicle> GetPassControlVehicles(int pLevel, int pId, double pDistance, DateTime pStartDate, DateTime pEndDate, double pLongitude, double pLatitude, int eventOn, int eventOff)
        {
            string VehicleFilter = string.Empty;
            string IntrackReferenceFilter = string.Empty;
            string UnitsFilter = string.Empty;
            var passControlBusiness = new PassControlBusiness();
            List<Models.PassControlVehicle> result = new List<Models.PassControlVehicle>();
            List<Models.PassControlVehicle> VehicleInfo = passControlBusiness.PassControlVehicleInfo(pLevel, pId).ToList();
            foreach (var x in VehicleInfo)
            {
                if (VehicleFilter == string.Empty)
                {
                    VehicleFilter = x.VehicleId.ToString();
                }
                else
                {
                    VehicleFilter = VehicleFilter + "," + x.VehicleId;
                }

                if (IntrackReferenceFilter == string.Empty)
                {
                    IntrackReferenceFilter = x.IntrackReference.ToString();
                }
                else
                {
                    IntrackReferenceFilter = IntrackReferenceFilter + "," + x.IntrackReference;
                }
            }
            /*foreach (var x in VehicleInfo)
            {
                if (IntrackReferenceFilter == string.Empty)
                {
                    IntrackReferenceFilter = x.IntrackReference.ToString();
                }
                else
                {
                    IntrackReferenceFilter = IntrackReferenceFilter + "," + x.IntrackReference;
                }
            }*/
            List<Models.PassControlVehicle> UnitsVehicle = passControlBusiness.GetUnitsVehicles(IntrackReferenceFilter).ToList();
            foreach (var x in UnitsVehicle)
            {
                if (UnitsFilter == string.Empty)
                {
                    UnitsFilter = x.UnitID;
                }
                else
                {
                    UnitsFilter = UnitsFilter + "," + x.UnitID;
                }
            }

            List<Models.PassControlVehicle> VehicleInOut = passControlBusiness.GetUnitsVehicles(pLongitude, pLatitude, pDistance, UnitsFilter, pStartDate.ToString("MM/dd/yyyy"), pEndDate.ToString("MM/dd/yyyy"), eventOn, eventOff).ToList();

            result = (from v in VehicleInfo
                      join u in UnitsVehicle on v.IntrackReference equals u.IntrackReference
                      join vio in VehicleInOut on u.UnitID equals vio.UnitID
                      select new Models.PassControlVehicle
                      {
                          Id = vio.Id,
                          VehicleId = v.VehicleId,
                          IntrackReference = v.IntrackReference,
                          UnitID = u.UnitID,
                          InDateTime = vio.InDateTime,
                          OutDateTime = vio.OutDateTime,
                          PlateId = v.PlateId,
                          VehicleName = v.VehicleName,
                          CostCenterId = v.CostCenterId,
                          CostCenterName = v.CostCenterName,
                          Latitude = vio.Latitude,
                          Longitude = vio.Longitude,
                          Dir = vio.Dir,
                          EventOnOff = vio.EventOnOff,
                          ParentID = vio.ParentID
                      }).ToList();

            List<Models.PassControlVehicle> DriverInfo = passControlBusiness.PassControlDriverInfo(VehicleFilter, pStartDate.ToString("MM/dd/yyyy"), pEndDate.ToString("MM/dd/yyyy")).ToList();
            foreach (var x in result)
            {
                List<Models.PassControlVehicle> tempDriver = (from d in DriverInfo
                                                              where d.VehicleId == x.VehicleId &&
                                                              ((d.InDateTime >= x.InDateTime && d.InDateTime < x.OutDateTime) ||
                                                              (d.OutDateTime >= x.InDateTime && d.OutDateTime < x.OutDateTime) ||
                                                              (d.InDateTime < x.InDateTime && d.OutDateTime >= x.InDateTime))
                                                              select d).ToList();
                foreach (var t in tempDriver)
                {
                    if (x.DriverName == null) { x.DriverName = Utilities.TripleDesEncryption.Decrypt(t.EncryptedDriverName); } else { x.DriverName = x.DriverName + ", " + Utilities.TripleDesEncryption.Decrypt(t.EncryptedDriverName); }
                    if (x.Dallas == null) { x.Dallas = Utilities.TripleDesEncryption.Decrypt(t.EncryptedDallas); } else { x.Dallas = x.Dallas + ", " + Utilities.TripleDesEncryption.Decrypt(t.EncryptedDallas); }
                }
            }
            return result;
        }

        public Models.CustomerThresholds GetThresholds(int pCustomerId)
        {
            var business = new EcOsystemBusiness();
            return business.RetrieveCustomerThresholdsBusiness(pCustomerId).FirstOrDefault();
        }

        public string FillColors(Models.CustomerThresholds pThresholds, double pValue)
        {
            string result = string.Empty;
            if (pValue <= pThresholds.RedHigher && pValue >= pThresholds.RedLower)
            {
                result = "vehicleRouteRed";
            }
            else if (pValue <= pThresholds.YellowHigher && pValue >= pThresholds.YellowLower)
            {
                result = "vehicleRouteYellow";
            }
            else if (pValue >= pThresholds.GreenLower)
            {
                result = "vehicleRouteGreen";
            }
            return result;
        }

        public double GetDistance(string CountryCode, AspMap.Point pPoint1, AspMap.Point pPoint2)
        {
            double distance = 0;
            string routeType = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultRouteType"];
            AspMap.Route route = findRoute(CountryCode, pPoint1, pPoint2, routeType);

            string res = string.Empty;

            for (int i = 1; i <= route.Segments.Count - 1; i++)
            {
                res = res + route.Segments[i].Start.Y + " " + route.Segments[i].Start.X + ", ";
                distance = distance + route.Segments[i].Distance;
            }
            return distance;
        }

        public Models.Maps GetMapInfo(string CountryCode)
        {
            var businessMaps = new MapsBusiness();
            return MapsBusiness.RetrieveMaps(CountryCode).FirstOrDefault();
        }

        public void sendPOI(int vehicleId, double longitude, double latitude, string description)
        {
            LocalizationBusiness business = new LocalizationBusiness();
            business.SendPoi(vehicleId, longitude, latitude, description);
        }

        public List<COINCA.WebApp.ECO_Maps.Models.ContinuousMonitoring> RetrieveContinuousMonitoring(string pVehiclesList, string pDate, int pUTC)
        {
            using (var business = new AtrackBusiness())
            {
                return business.RetrieveContinuousMonitoring(pVehiclesList, pDate, pUTC).ToList();
            }
        }

        public string getCarImage(int status, DateTime gpsDate, DateTime actualDate, int inactiveHours, double speed, bool validateActive)
        {
            if (Convert.ToInt32(Math.Round((actualDate - gpsDate).TotalHours)) > inactiveHours)
            {
                return "Content/Images/carGray.png"; //SIN TRANSMITIR
            }
            else if ((status % 2) == 0)
            {
                return "Content/Images/carRed.png"; //APAGADO
            }
            else
            {
                if (speed > 0)
                {
                    return "Content/Images/carGreen.png"; //ENCENDIDO
                }
                else
                {
                    return "Content/Images/carYellow.png"; //DETENIDO
                }
            }
        }

        public int getCarStatusId(int status, DateTime actualDate, DateTime gpsDate, int inactiveHours, double speed)
        {
            if (Convert.ToInt32(Math.Round((actualDate - gpsDate).TotalHours)) > inactiveHours)
            {
                return 2; //SIN TRANSMITIR
            }
            else if ((status % 2) == 0)
            {
                return 1; //APAGADO
            }
            else
            {
                if (speed > 0)
                {
                    return 3; //ENCENDIDO
                }
                else
                {
                    return 4; //DETENIDO
                }
            }
        }

        #region Management

        public List<Commerce> RetrieveCommerces(string pCommercesList)
        {
            using (var bus = new AtrackBusiness())
            {
                return bus.RetrieveCommerces(pCommercesList).ToList();
            }
        }

        public List<Agent> RetrieveAgents(string pAgentsList)
        {
            using (var bus = new AtrackBusiness())
            {
                return bus.RetrieveAgents(pAgentsList).ToList();
            }
        }

        public List<Agent> RetrieveAgentReconstructing(int userid, string startdate, string enddate)
        {
            using (var bus = new AtrackBusiness())
            {
                return bus.RetrieveAgentReconstructing(userid, startdate, enddate).ToList();
            }
        }

        public List<Agent> RetrieveAgentAgenda(int userid, string startdate, string enddate)
        {
            using (var bus = new AtrackBusiness())
            {
                return bus.RetrieveAgentAgenda(userid, startdate, enddate).ToList();
            }
        }

        public AspMap.Polyline GetAgentReportsPolyline(List<Agent> pListReports, string pMap)
        {
            Polyline result = new Polyline();
            AspMap.Points Points = new AspMap.Points();
            AspMap.Point lastPoint = null;
            foreach (var x in pListReports)
            {
                if (lastPoint != null)
                {
                    if (Math.Abs(Math.Abs((double)x.Longitude) - Math.Abs(lastPoint.X)) < 1 && Math.Abs(Math.Abs((double)x.Latitude) - Math.Abs(lastPoint.Y)) < 1)
                    {
                        Points.Add(new AspMap.Point(GetX((double)x.Longitude, pMap), GetY((double)x.Latitude, pMap)));
                    }
                }
                //lastPoint = new AspMap.Point((double)x.Longitude, (double)x.Latitude);
            }
            result.Add(Points);
            return result;
        }                            
        #endregion


        #region Helper Methods

        /// <summary>
        /// Permit convert DataTable To JSON
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public string DataTableToJSON(DataTable table)
        {
            string JSONString = string.Empty;
            if (table.Rows.Count > 0)
            {
                JSONString = JsonConvert.SerializeObject(table);
            }
            return JSONString;
        }

        #endregion

    }
}
