﻿using System.Web.Optimization;

namespace COINCA.WebApp.ECO_Maps
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254726
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                 "~/css/bootstrap.css",
                   "~/css/bootstrap.min.css",
                 "~/css/bootstrap-theme.min.css",
                 "~/css/siteMaps.css",
                 "~/Content/Styles/materialdesignicons.css"
                ));
				
		    bundles.Add(new StyleBundle("~/bundles/cssRoutes").Include(
                 "~/css/bootstrap.css",
                  "~/css/bootstrap.min.css",
                 "~/css/bootstrap-theme.min.css",
                 "~/css/siteRoutes.css",
                 "~/Content/Styles/materialdesignicons.css"              
                ));

            bundles.Add(new StyleBundle("~/bundles/timeline_css").Include(
               "~/Content/Styles/reset.css",
               "~/Content/Styles/timeline.css"
              )); 

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.min.js",
                "~/Scripts/bootstrap.min.js",           
                "~/Scripts/map.js"            
                ));

            bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
                  "~/Scripts/WebForms/WebForms.js",
                  "~/Scripts/WebForms/WebUIValidation.js",
                  "~/Scripts/WebForms/MenuStandards.js",
                  "~/Scripts/WebForms/Focus.js",
                  "~/Scripts/WebForms/GridView.js",
                  "~/Scripts/WebForms/DetailsView.js",
                  "~/Scripts/WebForms/TreeView.js",
                  "~/Scripts/WebForms/WebParts.js"));

            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/bootstrap-datepicker.es.js",
                "~/Scripts/jquery.timepicker.js",
                "~/Scripts/datepair.js",
                "~/Scripts/jquery.datepair.js"
                ));

            BundleTable.EnableOptimizations = true;
           
        }
    }
}