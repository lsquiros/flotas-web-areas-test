﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="RealTime.aspx.cs" Inherits="ECO_Maps.RealTime" EnableEventValidation="false"%>
<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
          <%: Styles.Render("~/bundles/css") %>
        <%--<link rel="stylesheet" href="css/site.css"/>--%>
    </head>
    <body>
        <form id="RealTimeForm" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts></Scripts>
            </asp:ScriptManager>
            <div id="body">
                <section class="content-wrapper main-content clear-fix">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
            <asp:HiddenField ID="level" runat="server" Value="0" />
            <asp:HiddenField ID="valueId" runat="server" Value="0" />
            <asp:HiddenField ID="VehiclesInfoLoaded" runat="server" Value="false" />
            <asp:HiddenField ID="InactiveTime" runat="server" Value="0" />
            <asp:HiddenField ID="width" runat="server" Value="0" />
            <asp:HiddenField ID="height" runat="server" Value="0" />
            <div id="divColorListMonitor">
                <div class="divColorListMonitor hidden">
                    <div class="itemColorList">
                        <div class="monitorMovimiento info"></div>
                        <div class="ColorListText">
                            <span class="ColorListMarker">
                                <i class="fa fa-map-marker fa-1"></i>
                            </span>
                            Encendido
                        </div>
                    </div>
                    <div class="itemColorList">
                        <div class="monitorApagado info"></div>
                        <div class="ColorListText">
                            <span class="ColorListMarker">
                                <i class="fa fa-map-marker fa-1"></i>
                            </span>
                            Apagado
                        </div>
                    </div>
                    <div class="itemColorList">
                        <div class="monitorDetenido info"></div>
                        <div class="ColorListText">
                            <span class="ColorListMarker">
                                <i class="fa fa-map-marker fa-1"></i>
                            </span>
                            Detenido
                        </div>
                    </div>
                    <div class="itemColorList">
                        <div class="monitorSinTransmitir info"></div>
                        <div class="ColorListText">
                            <span class="ColorListMarker">
                                <i class="fa fa-map-marker fa-1"></i>
                            </span>
                            Sin Transmitir
                        </div>
                    </div>
                    <div class="itemColorList">
                        <div class="ColorListText">
                            <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="GeoFences-Content">
                <div class="div100">
                    <div class="ContentMap" >
                        <asp:Panel ID="pnlCoinca" runat="server" Visible="true">
                            <aspmap:Map id="mapCoinca" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapCoinca_RefreshAnimationLayer" ></aspmap:Map>
                        </asp:Panel>
                        <asp:Panel ID="pnlGoogle" runat="server" Visible="false">
                            <aspmap:Map id="mapGoogle" Visible="false" EnableSession="true" runat="server" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnRefreshAnimationLayer="mapGoogle_RefreshAnimationLayer"  ></aspmap:Map>
                        </asp:Panel>
                        <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls-up" Visible="true">
                            <div class="LeftMenu" >
                                <center>
                                    <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                        <asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('GoogleRoad')" ></asp:ImageButton>
                                        <asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                    </div>
                                </center>
                            </div>
                        </asp:Panel>
                        <div class="mapMenu-controls-up">
                            <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                <i class="glyphicon glyphicon-chevron-left"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                <i class="glyphicon glyphicon-chevron-right"></i>
                            </asp:LinkButton>
                        </div>
                        <asp:Label Visible="false" ID="lblLatitudPunto" runat="server"></asp:Label>
                        <asp:Label Visible="false" ID="lblLongitudPunto" runat="server"></asp:Label>
                        <asp:Label Visible="false" ID="lblTotalDistancia" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div style="visibility:hidden;">
                <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
                <asp:Button ID="btnRefresh" runat="server" Text="Button" OnClick="btnRefresh_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     <%: Scripts.Render("~/bundles/jquery") %>
    <script>
        $(function () {
            resize();
            refresh();
        });

        $(window).resize(function () {
            resize();
        });

        function refresh() {
            var timeout = 15000;
            var action = function () {
                $('#<%=btnRefresh.ClientID %>').click();
            };
            setInterval(action, timeout);
        }

        function resize() {
            updateDivColor();
            var width = $(window).width() - 5;
            var height = $(window).height() - 30;
            $('#<% =width.ClientID %>').attr('value', width);
            $('#<% =height.ClientID %>').attr('value', height);
            $("#ContentSearch").width(width);
            $('#<%=btnResize.ClientID %>').click();
        }

        function updateDivColor() {
            var height = $(window).height();
            $(".pnlSatellite").css('margin-left', "5px !important");
            $(".pnlSatellite").css('margin-top:', "-112px !important");
            $(".divColorListMonitor").css('margin-top', height - 68 + "px");
            $(".divColorListMonitor").removeClass("hidden");
        }
        

    </script>

              </section>
            </div>
        </form>
    </body>
</html>