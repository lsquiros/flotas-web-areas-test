﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using ECO_Maps.Utilities;
using AspMap;
using AspMap.Web;
using System.Globalization;
using ECOsystem.Management.Models;

namespace ECO_Maps
{
    public partial class Management : System.Web.UI.Page
    {
        private static string _lLicense;
        private static int _refreshInterval;
        private readonly Maps _mapsFunctions = new Maps();
        private static Maps _mapsFunctionBus = new ECO_Maps.Utilities.Maps();
        private readonly Utilities.Maps MapsFunctions = new Utilities.Maps();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_lLicense == null)
                _lLicense = System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"];

            if (_refreshInterval == 0)
                _refreshInterval = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["RefreshInterval"]);

            try
            {
                License.SetLicenseKey(_lLicense);
                string Params = Request.QueryString["Params"];
                string layer = Request.QueryString["Map"];
                var countryCode = string.Empty;
                var pointstype = string.Empty;
                layerMap.Value = layer ?? System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"];

                if (Params != string.Empty)
                {
                    var p = Params.Split('|');
                    Session["CountryCode"] = countryCode = p[0];
                    Session["ObjectsIds"] = p[2];
                    Session["PointsType"] = pointstype = p[5];
                    Session["CardPlaying"] = p[6];
                    Session["seeName"] = 0;
                    Session["IsUser"] = 0;

                    try
                    {
                        if (pointstype == "R" || pointstype == "RP")
                        {
                            Session["IsUser"] = string.IsNullOrEmpty(p[7]) ? true : Convert.ToBoolean(p[7]);
                            Session["ShowAgentAgenda"] = string.IsNullOrEmpty(p[8]) ? "false" : p[8];
                        }
                        else
                        {
                            Session["seeName"] = Convert.ToInt16(p[7]);
                        }
                    }
                    catch (Exception g)
                    {
                        if (pointstype == "R" || pointstype == "RP") Session["IsUser"] = true;
                    }

                    if (Session["Playing"] != Session["CardPlaying"] && p[3] != "0" && p[4] != "0")
                    {
                        Session["StartDate"] = p[3];
                        Session["EndDate"] = p[4];
                    }
                    if (p[3] == "0" && p[4] == "0")
                    {
                        Session["StartDate"] = null;
                        Session["EndDate"] = null;
                    }
                    Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt16(p[1]));
                }
                if (countryCode != string.Empty)
                {
                    _mapsFunctions.addMapLayer(mapOSM, null, layerMap.Value, countryCode);

                    if (pageLoaded.Value == "false")
                    {
                        pageLoaded.Value = "true";
                    }

                    Models.Maps map = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                    if (layerMap.Value == "Coinca" || layerMap.Value == "OSM")
                    {
                        pnlOSM.Visible = true;
                        mapOSM.EnableAnimation = true;
                        mapOSM.AnimationInterval = 1000;

                        if (Page.IsPostBack == false)
                        {
                            ClearRoute();
                            Session["VehicleDetail"] = null;
                        }

                        if (map != null && Session["VehicleDetail"] == null)
                        {
                            mapOSM.CenterAndZoom(new Point(_mapsFunctions.GetX(map.CenterLon.Value, layerMap.Value), _mapsFunctions.GetY(map.CenterLat.Value, layerMap.Value)), map.CenterZoom.Value + 2);

                            if (Session["ShowAgentAgenda"] != null && Session["ShowAgentAgenda"].ToString() == "true")
                            {
                                SetAgendaOnMap();
                            }
                        }
                    }
                }
                if (pointstype == "C")
                {
                    Session["Playing"] = null;
                    TrackObjects();
                }
                else if (pointstype == "A")
                {
                    Session["Playing"] = null;
                    TrackAgentsLocation();
                }
                else
                {
                    var playing = Session["Playing"] == null ? string.Empty : Session["Playing"].ToString();
                    var reset = txtResetPart.Text;

                    if (playing != Session["CardPlaying"].ToString())
                    {
                        txtResetPart.Text = string.Empty;
                        Session["Playing"] = Session["CardPlaying"];
                        LoadRoute();
                    }
                    else if (pointstype == "RP" && string.IsNullOrEmpty(reset))
                    {
                        txtResetPart.Text = "RP";
                        LoadRoute();
                    }         
                }
            }
            catch (Exception)
            {
            }
        }

        //Get commerce location
        void TrackObjects()
        {
            string objects = string.IsNullOrEmpty(Session["ObjectsIds"].ToString()) ? "0" : Session["ObjectsIds"].ToString();
            int zoomin = 0;

            mapOSM.MapShapes.Clear();
            mapOSM.Markers.Clear();
            mapOSM.AnimationLayer.Clear();

            List<Commerce> reports = _mapsFunctions.RetrieveCommerces(objects);

            var center = reports.FirstOrDefault();
            if (center != null)
            {
                zoomin = reports.Count > 1 ? zoomin : 5;
                mapOSM.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX((double)center.Longitude, layerMap.Value), _mapsFunctions.GetY((double)center.Latitude, layerMap.Value)), Convert.ToInt32(mapOSM.ZoomLevels.Count - zoomin));
            }

            foreach (var x in reports)
            {
                //Point location = new Point();
                //location = new AspMap.Point(_mapsFunctions.GetX((double)x.Longitude, layerMap.Value), _mapsFunctions.GetY((double)x.Latitude, layerMap.Value));   
                //GeoEvent geoEvent = new GeoEvent();
                //geoEvent.Location = location;
                //geoEvent.ImageUrl = "Content/Images/building.png";
                //geoEvent.ImageWidth = 25;
                //geoEvent.ImageHeight = 35;  
                //if (Convert.ToInt16(Session["seeName"]) == 0)
                //{
                //    geoEvent.Tooltip = "<div style=\"width:300px\"><b>Comercio: </b> " + x.Name + "</div>";
                //}
                //else
                //{
                //    //////////////////////////labelInformation////////////////
                //    geoEvent.Label = x.Name;
                //    geoEvent.LabelStyle.Width = System.Web.UI.WebControls.Unit.Point(180);
                //    geoEvent.LabelStyle.BorderColor = System.Drawing.Color.Black;
                //    geoEvent.LabelStyle.BorderWidth = System.Web.UI.WebControls.Unit.Point(1);
                //    geoEvent.LabelStyle.BackColor = System.Drawing.Color.LightYellow;
                //    geoEvent.LabelStyle.Font.Size = FontUnit.Small;
                //    //////////////////////////endlabelInformation////////////////
                //}
                //mapOSM.AnimationLayer.Add(geoEvent);

                string tooltip = string.Empty;
                if (Convert.ToInt16(Session["seeName"]) == 1)
                {
                    tooltip = "<div style=\"width:300px\"><b>Comercio: </b> " + x.Name + "</div>";
                }
                Marker marker = new Marker(new AspMap.Point(_mapsFunctions.GetX((double)x.Longitude, layerMap.Value), _mapsFunctions.GetY((double)x.Latitude, layerMap.Value)),
                                           new MarkerSymbol("Content/Images/building.png", 32, 42), string.Empty, tooltip);
                mapOSM.Markers.Add(marker);
            }
        }

        //Get agents location
        void TrackAgentsLocation()
        {
            string objects = string.IsNullOrEmpty(Session["ObjectsIds"].ToString()) ? "0" : Session["ObjectsIds"].ToString();
            int zoomin = 0;

            mapOSM.MapShapes.Clear();
            mapOSM.Markers.Clear();
            mapOSM.AnimationLayer.Clear();

            List<Agent> reports = _mapsFunctions.RetrieveAgents(objects);

            var center = reports.FirstOrDefault();
            if (center != null)
            {
                zoomin = reports.Count > 1 ? zoomin : 5;
                mapOSM.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX((double)center.Longitude, layerMap.Value), _mapsFunctions.GetY((double)center.Latitude, layerMap.Value)), Convert.ToInt32(mapOSM.ZoomLevels.Count - zoomin));
            }

            foreach (var x in reports)
            {
                Point location = new Point();
                location = new AspMap.Point(_mapsFunctions.GetX((double)x.Longitude, layerMap.Value), _mapsFunctions.GetY((double)x.Latitude, layerMap.Value));

                DateTime timeZone = x.EventDate;
                x.Date = timeZone.ToString("dd/MM/yyyy");
                x.Time = timeZone.ToString("HH:mm:ss");

                GeoEvent geoEvent = new GeoEvent();
                geoEvent.Location = location;
                geoEvent.ImageUrl = "Content/Images/User.png";
                geoEvent.ImageWidth = 32;
                geoEvent.ImageHeight = 42;

                string battery = "";

                if (x.BatteryLevel <= 10) { battery = "mdi mdi-battery-alert"; }
                else if (x.BatteryLevel > 10 && x.BatteryLevel < 40) { battery = "mdi mdi-battery-30"; }
                else if (x.BatteryLevel > 80) { battery = "mdi mdi-battery-90"; }
                else { battery = "mdi mdi-battery-60"; }
                  
                if (Convert.ToInt16(Session["seeName"]) == 0)
                {
                    geoEvent.Tooltip = x.DecryptName + " - " + " <i class='" + battery + "'></i>" + "%" + x.BatteryLevel;
                }
                else
                {
                    //////////////////////////LabelInformation////////////////

                    geoEvent.Label = x.DecryptName + " - " + " <i class='" + battery + "'></i>" + "%" + x.BatteryLevel;
                    geoEvent.LabelStyle.Width = System.Web.UI.WebControls.Unit.Point(180);
                    geoEvent.LabelStyle.BorderColor = System.Drawing.Color.Black;
                    geoEvent.LabelStyle.BorderWidth = System.Web.UI.WebControls.Unit.Point(1);
                    geoEvent.LabelStyle.BackColor = System.Drawing.Color.LightYellow;
                    geoEvent.LabelStyle.Font.Size = FontUnit.Small;
                    //////////////////////EndLabelInformation////////////////
                }

                mapOSM.AnimationLayer.Add(geoEvent);
            }
            //The list is instantiated so that it is not null
            reports.ToList().ForEach(x => x.GeoNames = new List<string>());
            ReturnDetail(0, new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(reports));
        }

        #region Reconstructing Functions
        private bool TrackAgentsReconstructing()
        {
            try
            {
                int count = GetCountSession();
                int zoomin = 0;
                var isuser = Session["IsUser"] == null ? true : (bool)Session["IsUser"];
                bool result = true;

                var data = new Agent();
                List<Agent> reports = GetRouteSession();

                if (count >= 0 && count < reports.Count && reports.Count > 0)
                {
                    data = reports[count];
                }
                else
                {
                    data = (Agent)Session["AgentStaticDetail"];
                    result = false;
                }

                mapOSM.AnimationLayer.Clear();

                Point location = new Point(_mapsFunctions.GetX((double)data.Longitude, layerMap.Value), _mapsFunctions.GetY((double)data.Latitude, layerMap.Value));

                GeoEvent geoEvent = new GeoEvent();
                geoEvent.Location = location;
                geoEvent.ImageUrl = "Content/Images/UserRecons.png";
                geoEvent.ImageWidth = 32;
                geoEvent.ImageHeight = 72;
                geoEvent.Tooltip = isuser ? string.Format("{0} - {1}", data.DecryptName, data.EventName) : data.CommerceName;

                DateTime timeZone = data.EventDate;
                data.Date = timeZone.ToString("dd/MM/yyyy");
                data.Time = timeZone.ToString("HH:mm:ss");

                data.ReconstructionId = Session["CardPlaying"].ToString();
                data.IsFinish = reports.Count - 1 == count;
                                 
                var jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                mapOSM.AnimationLayer.Add(geoEvent);

                if (Session["ReconstructingPaused"] != null && (bool)Session["ReconstructingPaused"] == false)
                {
                    zoomin = reports.Count > 1 ? zoomin : 5;
                    mapOSM.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX((double)data.Longitude, layerMap.Value), _mapsFunctions.GetY((double)data.Latitude, layerMap.Value)), Convert.ToInt32(mapOSM.ZoomLevels.Count - zoomin));
                    Session["ReconstructingPaused"] = isuser ? false : (bool?)null;
                }
                //The list is instantiated so that it is not null
                data.GeoNames = new List<string>();
                Session["VehicleDetail"] = jSearializer.Serialize(data);
                    
                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        protected void LoadRoute()
        {
            try
            {
                ClearRoute();
                var isuser = Session["IsUser"] == null ? true : (bool)Session["IsUser"];

                DateTime startDate = DateTime.Parse(Session["StartDate"].ToString(), CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.Parse(Session["EndDate"].ToString(), CultureInfo.InvariantCulture);

                List<Agent> AgentRoute = _mapsFunctions.RetrieveAgentReconstructing(Convert.ToInt16(Session["ObjectsIds"]), startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"));

                //Set markets for the route
                SetRouteMarkets(AgentRoute);

                if (Session["ShowAgentAgenda"] != null && Session["ShowAgentAgenda"].ToString() == "true")
                {
                    SetAgendaOnMap();
                }

                SetCountSession(0);
                SetRouteSession(AgentRoute);
                if (AgentRoute.Count > 0)
                {
                    mapOSM.CenterAndZoom(new Point(_mapsFunctions.GetX((double)AgentRoute[0].Longitude, layerMap.Value), _mapsFunctions.GetY((double)AgentRoute[0].Latitude + 0.001, layerMap.Value)), Convert.ToInt32(mapOSM.ZoomLevels.Count - 5));
                }
                else
                {
                    var AgentDetail = _mapsFunctions.RetrieveAgents(Session["ObjectsIds"].ToString()).FirstOrDefault();
                    Session["AgentStaticDetail"] = AgentDetail;
                    mapOSM.CenterAndZoom(new Point(_mapsFunctions.GetX((double)AgentDetail.Longitude, layerMap.Value), _mapsFunctions.GetY((double)AgentDetail.Latitude + 0.001, layerMap.Value)), Convert.ToInt32(mapOSM.ZoomLevels.Count - 5));
                }
                refrehing.Value = isuser ? "true" : "false";
                Session["ReconstructingPaused"] = false;

                ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "setTimeout(function(){ refresh(); }, 250);", true);
            }
            catch (Exception ex) { }
        }

        protected void SetRouteMarkets(List<Agent> route)
        {
            //if (route != null && route.Any() && route.FirstOrDefault().ShowLines)
            //{
            //    MapShape mapShapeGreen = mapOSM.MapShapes.Add(_mapsFunctions.GetAgentReportsPolyline(route, layerMap.Value), "route");
            //    mapShapeGreen.Symbol.LineColor = System.Drawing.Color.DarkGreen;
            //    mapShapeGreen.Symbol.Size = 4;
            //    mapShapeGreen.Symbol.FillStyle = FillStyle.Invisible;
            //}
            //else
            //{
                foreach (var item in route.Where(x => x.Latitude != null))
                {
                    Point location = new Point(_mapsFunctions.GetX((double)item.Longitude, layerMap.Value), _mapsFunctions.GetY((double)item.Latitude, layerMap.Value));
                    var symbol = new MarkerSymbol("Content/Images/RouteMarket.png", 10, 10);
                    Marker geoEvent = new Marker(location, symbol, "", "");

                    mapOSM.Markers.Add(geoEvent);
                }
            //}
        }

        protected void SetAgendaOnMap()
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            if (Session["StartDate"] != null)
            {
                startDate = DateTime.Parse(Session["StartDate"].ToString(), CultureInfo.InvariantCulture);
                endDate = DateTime.Parse(Session["EndDate"].ToString(), CultureInfo.InvariantCulture);
            }
            else
            {
                startDate = DateTime.Parse(txtStartDate.Text, CultureInfo.InvariantCulture);
                endDate = DateTime.Parse(txtEndDate.Text, CultureInfo.InvariantCulture);
            }
            endDate.AddDays(1);
            List<Agent> list = _mapsFunctions.RetrieveAgentAgenda(Convert.ToInt16(Session["ObjectsIds"]), startDate.ToString("MM/dd/yyyy"), endDate.ToString("MM/dd/yyyy"));

            foreach (var item in list.Where(x => x.Latitude != null))
            {
                Point location = new Point(_mapsFunctions.GetX((double)item.Longitude, layerMap.Value), _mapsFunctions.GetY((double)item.Latitude, layerMap.Value));
                var symbol = new MarkerSymbol("Content/Images/building.png", 25, 35);
                Marker geoEvent = new Marker(location, symbol, item.CommerceName, string.Empty);
                mapOSM.Markers.Add(geoEvent);
            }
        }

        protected void SetRouteSession(List<Agent> route)
        {
            Session["ReconstructingRoute"] = route;
        }

        protected List<Agent> GetRouteSession()
        {
            var result = new List<Agent>();
            if (Session["ReconstructingRoute"] != null)
            {
                result = (List<Agent>)Session["ReconstructingRoute"];
            }
            return result;
        }

        protected int GetCountSession()
        {
            var result = 0;
            if (Session["ReconstructingCount"] != null)
            {
                result = Convert.ToInt16(Session["ReconstructingCount"]);
            }
            return result;
        }

        protected void SetCountSession(int count)
        {
            Session["ReconstructingCount"] = count;
        }

        protected void ClearRoute()
        {
            Session["ReconstructingRoute"] = null;
            Session["ReconstructingCount"] = 0;
            Session["ReconstructingRefresh"] = false;
            Session["ReconstructingPaused"] = true;
            Session["timevents"] = null;
            mapOSM.RemoveAllLayers();
            mapOSM.AnimationLayer.Clear();
            mapOSM.MapShapes.Clear();
            mapOSM.Markers.Clear();
        }

        protected void btnPause_Click(object sender, EventArgs e)
        {
            Session["ReconstructingPaused"] = true;
            RefreshAnimationLayerArgs x = new RefreshAnimationLayerArgs();
            mapOSM.EnableAnimation = false;
            mapOSM.AnimationInterval = 0;
            mapOSM_RefreshAnimationLayer(sender, x);
        }

        protected void btnPlay_Click(object sender, EventArgs e)
        {
            if (txtReset.Text == "RS") SetCountSession(0);

            Session["ReconstructingPaused"] = false;
            RefreshAnimationLayerArgs x = new RefreshAnimationLayerArgs();
            mapOSM.EnableAnimation = true;
            mapOSM.AnimationInterval = 1000;
            mapOSM_RefreshAnimationLayer(sender, x);
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Session["StartDate"] = txtStartDate.Text;
            Session["EndDate"] = txtEndDate.Text;
            Session["ShowAgentAgenda"] = txtShowAgenda.Text;

            refrehing.Value = "true";
            LoadRoute();
            RefreshAnimationLayerArgs x = new RefreshAnimationLayerArgs();
            mapOSM.EnableAnimation = true;
            mapOSM.AnimationInterval = 1000;
            mapOSM_RefreshAnimationLayer(sender, x);

            //ClientScript.RegisterStartupScript(GetType(), "Javascript", "setTimeout(function(){ refresh(); }, 250);", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "setTimeout(function(){ refresh(); }, 250);", true); //"refresh();", true);
        }

        public void MoveCount(int steps)
        {
            int count = GetCountSession();
            if (count + steps >= 0)
            {
                if (count + steps <= GetRouteSession().Count - 1)
                {
                    Session["ReconstructingCount"] = count + steps;
                }
                else
                {
                    Session["ReconstructingCount"] = GetRouteSession().Count - 1;
                }
            }
        }

        protected void mapOSM_RefreshAnimationLayer(object sender, RefreshAnimationLayerArgs e)
        {
            if (Session["ReconstructingPaused"] == null)
            {
                TrackAgentsReconstructing();
                return;
            }
            if ((bool)Session["ReconstructingPaused"] == false)
            {
                MoveCount(_refreshInterval);
            }
            e.NeedRefreshMap = TrackAgentsReconstructing();
            ReturnDetail(GetCountSession(), null);
        }

        protected void btnShowAgenda_Click(object sender, EventArgs e)
        {
            var showAgenda = txtShowAgenda.Text;
            Session["ShowAgentAgenda"] = showAgenda;
            if (showAgenda == "true")
            {
                SetAgendaOnMap();
            }
            else
            {
                ClearRoute();
                TrackAgentsLocation();
            }
        }
        #endregion

        //Register return data
        void ReturnDetail(int count, string data)
        {
            if (count == 0 && Session["VehicleDetail"] == null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "setTimeout(function(){ returnResult('" + data + "'); }, 250);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + Session["VehicleDetail"] + "');", true);
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "returnResult('" + Session["VehicleDetail"] + "');", true);
            }
        }

        //Refresh actual information
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            if (Session["PointsType"] != null && Session["PointsType"].ToString() == "A")
            {
                TrackAgentsLocation();
            }
            else if (Session["PointsType"] != null && (Session["PointsType"].ToString() == "R" || Session["PointsType"].ToString() == "RP"))
            {
                mapOSM_RefreshAnimationLayer(new object(), new RefreshAnimationLayerArgs());
            }
        }

        //Change the size of the map in the window
        protected void btnResize_Click(object sender, EventArgs e)
        {
            short lWidth = Convert.ToInt16(width.Value);
            short lHeigth = Convert.ToInt16(height.Value);
            if (lWidth >= 0 && lHeigth > 0)
            {
                pnlOSM.Width = lWidth;
                pnlOSM.Height = lHeigth;
                mapOSM.Width = lWidth;
                mapOSM.Height = lHeigth;
            }
        }
    }
}
