﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ECO_Maps.Business;
using AspMap;
using System.Globalization;
using System.Data;
using ECO_Maps.Business.Intrack;
using ECO_Maps.Utilities;

namespace ECO_Maps
{
    public partial class PassControl1 : System.Web.UI.Page
    {
        Utilities.Maps _mapsFunctions = new Utilities.Maps();
        Utilities.Maps _utilities = new Utilities.Maps();

        protected void Page_Load(object sender, EventArgs e)
        {
            AspMap.License.SetLicenseKey(System.Web.Configuration.WebConfigurationManager.AppSettings["AspMapLicense"]);
            int id = 0;
            int level = 0;
            string countryCode = string.Empty;
            string Params = Request.QueryString["Params"];
            string latDirection = Request.QueryString["latDirection"];
            string lonDirection = Request.QueryString["lonDirection"];
            string layer = Request.QueryString["Map"];
            string PageLoaded = Request.QueryString["PageLoaded"];

            if (countryCode == null) { countryCode = string.Empty; }
            if (latDirection == null) { latDirection = string.Empty; }
            if (lonDirection == null) { lonDirection = string.Empty; }
            if (layer == null) { layer = string.Empty; }
            if (PageLoaded == null) { PageLoaded = string.Empty; }
            if (PageLoaded != string.Empty) { pageLoaded.Value = PageLoaded; }
            
            if (pageLoaded.Value == "false")
            {
                pageLoaded.Value = "true";
                mapCoinca.RemoveAllLayers();
                mapGoogle.RemoveAllLayers();
                Session.Clear();
                mapCoinca.MapShapes.Clear();
                mapGoogle.MapShapes.Clear();
            }
            if (Params != string.Empty)
            {
                string[] p = Params.Split('|');
                if (p[0].Trim() != string.Empty) { countryCode = p[0]; }
                if (p[1].Trim() != string.Empty) { level = Convert.ToInt32(p[1]); }
                if (p[2].Trim() != string.Empty) { id = Convert.ToInt32(p[2]); }
                customerId.Value = p[3];
                Session["CountryCode"] = countryCode;
                Session["Level"] = level;
                Session["Id"] = id;
                Session.Add("LOGGED_USER_TIMEZONE_OFF_SET", Convert.ToInt32(p[4]));
            }
            if (layer == string.Empty) { layerMap.Value = System.Web.Configuration.WebConfigurationManager.AppSettings["DefaultMap"]; } else { layerMap.Value = layer; }
            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                //panToolGoogle.Visible = false;
                //centerToolGoogle.Visible = false;
                //polygonToolGoogle.Visible = false;
                //distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                //panTool.Visible = true;
                //centerTool.Visible = true;
                //polygonTool.Visible = true;
                //distanceTool.Visible = true;
                pnlSatellite.Visible = false;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                //panToolGoogle.Visible = false;
                //centerToolGoogle.Visible = false;
                //polygonToolGoogle.Visible = false;
                //distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                //panTool.Visible = true;
                //centerTool.Visible = true;
                polygonTool.Visible = true;
                distanceTool.Visible = true;
                //pnlSatellite.Visible = false;
            }
            else //if (layerMap.Value == "GoogleRoad")
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                //GoogleMap.Visible = false;
                //CoincaMap.Visible = true;
                //GoogleMap.Visible = false;
                //ZoomInTool.Visible = false;
                //zoomOutTool.Visible = false;
                //panTool.Visible = false;
                //centerTool.Visible = false;
                polygonTool.Visible = false;
                distanceTool.Visible = false;
                //CoincaMap.Visible = true;
                //ZoomInToolGoogle.Visible = true;
                //zoomOutToolGoogle.Visible = true;
                //panToolGoogle.Visible = true;
                //centerToolGoogle.Visible = true;
                //polygonToolGoogle.Visible = true;
                //distanceToolGoogle.Visible = true;
                //pnlSatellite.Visible = true;
            }
            if (countryCode != string.Empty)
            {
                LoadLabels(countryCode);
                _mapsFunctions.addMapLayer(mapCoinca, mapGoogle, layerMap.Value, countryCode);

                if (Page.IsPostBack == false)
                {
                    LoadProvincias();
                    LoadCantones();
                }

                MapsBusiness businessMap = new MapsBusiness();
                ECO_Maps.Models.Maps maps = MapsBusiness.RetrieveMaps(countryCode).FirstOrDefault();
                if (maps.CenterZoom != null && maps.CenterLat != null && maps.CenterLon != null)
                {
                    AspMap.Point point = new AspMap.Point(Convert.ToDouble(maps.CenterLon), Convert.ToDouble(maps.CenterLat));
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom));
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                    }
                    else 
                    {
                        mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(point.X, layerMap.Value), _mapsFunctions.GetY(point.Y, layerMap.Value)), Convert.ToInt32(maps.CenterZoom + 3));
                    }
                }
                if (latDirection != string.Empty && lonDirection != string.Empty)
                {
                    AspMap.Point punto = new AspMap.Point(Convert.ToDouble(lonDirection), Convert.ToDouble(latDirection));
                    if (layerMap.Value == "Coinca")
                    {
                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                    }
                    else if (layerMap.Value == "OSM")
                    {
                        AspMap.Web.GeoEvent geoEvent = new AspMap.Web.GeoEvent();
                        geoEvent.Location = punto;
                        geoEvent.ImageWidth = 35;
                        geoEvent.ImageHeight = 35;
                        geoEvent.ImageUrl = "../Content/Images/Marker_1.png";
                        mapCoinca.AnimationLayer.Add(geoEvent);

                        mapCoinca.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                    }
                    else 
                    {
                        mapGoogle.CenterAndZoom(new AspMap.Point(_mapsFunctions.GetX(punto.X, layerMap.Value), _mapsFunctions.GetY(punto.Y, layerMap.Value)), mapCoinca.ZoomLevels.Count - 2);
                    }
                }

            }

            if (Page.IsPostBack == false && layerMap.Value != "Coinca" && layerMap.Value != "OSM")
            {
                if (layerMap.Value == "GoogleRoad") { chkSatellite.Checked = false; } else { chkSatellite.Checked = true; }
            }

            //Validate filter Start Date and End Date from external input
            if (Request.QueryString["ExternalReport"] != null && Request.QueryString["ExternalReport"] == "true")
            {
                distance.Value = (Request.QueryString["Distance"] != null)? Request.QueryString["Distance"] : string.Empty;
                Longitude.Value = (Request.QueryString["Longitude"] != null) ? Request.QueryString["Longitude"] : string.Empty;
                Latitude.Value = (Request.QueryString["Latitude"] != null) ? Request.QueryString["Latitude"] : string.Empty;

                GetReportByRange(Request.QueryString["StartDate"].Trim(), Request.QueryString["EndDate"].Trim(), int.Parse(Request.QueryString["EventOn"].Trim()), int.Parse(Request.QueryString["EventOff"].Trim()));
            }

            //Validate if the search is external from other source - example: ECOsystem
            if (Request.QueryString["ExternalSearch"] != null && Request.QueryString["ExternalSearch"] == "true")
            {
                SearchFromExternalSource();
            }
        }
        protected void LoadLabels(string countryCode)
        {
            Models.Maps info = _mapsFunctions.GetMapInfo(countryCode.Trim());
            if (info != null)
            {
                lblLevel1.Text = info.Level1 + ":";
                lblLevel2.Text = info.Level2 + ":";
            }
        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            if (directionsTable.Rows.Count == 0 && pointsSearchTable.Rows.Count == 0) { directionsTable.Visible = false; }
            //if (directionsTable.Rows.Count == 0 && pointsSearchTable.Rows.Count == 0) { directionsTable.Visible = false; pnlResultButtons.Visible = false; }
            ScriptManager.RegisterStartupScript(this, GetType(), "fncMenuRefresh", "fncMenuRefresh();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "fncLoadDate", "fncLoadDate();", true);
            ScriptManager.GetCurrent(Page).AsyncPostBackTimeout = 180000;
        }

        protected void map_CircleToolCoinca(object sender, AspMap.Web.CircleToolEventArgs e)
        {
            mapCoinca.MapShapes.Clear();
            MapShape mapShape = mapCoinca.MapShapes.Add(e.Circle);
            mapShape.Symbol.Size = 2;
            mapShape.Symbol.FillStyle = FillStyle.Invisible;
            mapShape.Symbol.LineColor = System.Drawing.Color.Red;

            if (layerMap.Value == "OSM")
            {
                distance.Value = ((2 * Math.PI * 6370) / 360 * _mapsFunctions.GetAspY(e.Radius, "OSM") * 1000).ToString();
                Longitude.Value = _mapsFunctions.GetAspMapX(e.Circle.Centroid.X).ToString();
                Latitude.Value = _mapsFunctions.GetAspMapY(e.Circle.Centroid.Y).ToString();
            }
            else
            {
                distance.Value = ((2 * Math.PI * 6370) / 360 * e.Radius * 1000).ToString();
                Longitude.Value = e.Circle.Centroid.X.ToString();
                Latitude.Value = e.Circle.Centroid.Y.ToString();
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "returnPointsResult", "returnPointsResult();", true);
        }

        protected void map_CircleToolGoogle(object sender, AspMap.Web.CircleToolEventArgs e)
        {
            mapGoogle.MapShapes.Clear();
            MapShape mapShape = mapGoogle.MapShapes.Add(e.Circle);
            mapShape.Symbol.Size = 2;
            mapShape.Symbol.FillStyle = FillStyle.Invisible;
            mapShape.Symbol.LineColor = System.Drawing.Color.Red;
            distance.Value = ((2 * Math.PI * 6370) / 360 * _mapsFunctions.GetAspY(e.Radius, "Google") * 1000).ToString();
            Longitude.Value = _mapsFunctions.GetAspMapX(e.Circle.Centroid.X).ToString();
            Latitude.Value = _mapsFunctions.GetAspMapY(e.Circle.Centroid.Y).ToString();

            ScriptManager.RegisterStartupScript(this, GetType(), "returnPointsResult", "returnPointsResult();", true);
        }

        protected void provinciaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCantones();
        }

        protected void cantonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplySearch(Session["CountryCode"].ToString(),
                        provinciaList.SelectedItem.Text.Trim(),
                        cantonesList.SelectedItem.Text.Trim(),
                        string.Empty);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ApplySearch(Session["CountryCode"].ToString(),
                        provinciaList.SelectedItem.Text.Trim(),
                        cantonesList.SelectedItem.Text.Trim(),
                        txtSearch.Text.Trim());
        }

        protected void SearchFromExternalSource()
        {
            string countryCode = Request.QueryString["CountryCode"];
            string province = Request.QueryString["Province"];
            string canton = Request.QueryString["Canton"];
            string search = Request.QueryString["Search"];

            ApplySearch(countryCode.Trim(),
                        province.Trim(),
                        canton.Trim(),
                        search.Trim());
        }

        TableCell Td(string text)
        {
            TableCell cell = new TableCell();
            cell.Text = text;
            if (text != string.Empty)
                cell.BorderWidth = Unit.Pixel(1);
            return cell;
        }


        void Tr(string text, string latitud, string longitud, string type)
        {
            TableRow row = new TableRow();
            row.Attributes.Add("onclick", "fncSeleccionaItem(" + latitud + "," + longitud + ")");
            row.Cells.Add(Td(text));
            if (type == "direction")
            {
                directionsTable.Rows.Add(row);
            }
            else
            {
                pointsSearchTable.Rows.Add(row);
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            directionsTable.Visible = false;
            txtSearch.Text = string.Empty;
            cantonesList.Text = string.Empty;
            provinciaList.Text = string.Empty;
        }

        protected void LoadProvincias()
        {
            string countryCode = Session["CountryCode"].ToString();
            provinciaList.DataSource = _mapsFunctions.GetProvinciasList(countryCode);
            provinciaList.DataBind();
        }

        protected void LoadCantones()
        {
            string countryCode = Session["CountryCode"].ToString();
            cantonesList.DataSource = _mapsFunctions.GetCantonList(countryCode, provinciaList.SelectedItem.Text.Trim());
            cantonesList.DataBind();
        }

        protected void btnHideMapMenu_Click(object sender, EventArgs e)
        {
            btnHideMapMenu.Visible = false;
            btnShowMapMenu.Visible = true;
            mapMenuControls.Visible = false;
        }

        protected void btnShowMapMenu_Click(object sender, EventArgs e)
        {
            btnShowMapMenu.Visible = false;
            btnHideMapMenu.Visible = true;
            mapMenuControls.Visible = true;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            GetReportByRange(txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), 0, 0);
        }

        protected void ApplySearch(string countryCode, string provincia, string canton, string searchText)
        {

            List<Models.SearchResult> listPlaces = new List<Models.SearchResult>();
            if (layerMap.Value == "Coinca")
            {
                listPlaces = _mapsFunctions.Search(mapCoinca, countryCode, provincia, canton, string.Empty, searchText);
            }
            else if (layerMap.Value == "OSM")
            {
                listPlaces = _mapsFunctions.Search(mapCoinca, countryCode, provincia, canton, string.Empty, searchText);
            }
            else //if (layerMap.Value == "GoogleRoad")
            {
                listPlaces = _mapsFunctions.Search(mapGoogle, countryCode, provincia, canton, string.Empty, searchText);
            }

            SearchPointsBusiness business = new SearchPointsBusiness();
            List<Models.SearchResult> listPoints = business.RetrievePoints(Convert.ToInt16(customerId.Value), searchText).ToList();
            Session["listPoints"] = listPoints;
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }

            Session["listPlaces"] = listPlaces;
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            if (listPlaces.Count > 0)
            {
                directionsTable.Visible = true;
                //pnlResultButtons.Visible = true;
                //btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(Int32.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            }
            else
            {
                directionsTable.Visible = false;
                //pnlResultButtons.Visible = false;
            }
        }

        protected void GetReportByRange(string strStartDate, string strEndDate, int eventOn, int eventOff)
        {         
            if (distance.Value != string.Empty && Longitude.Value != string.Empty && Latitude.Value != string.Empty && strStartDate != string.Empty && strEndDate != string.Empty)
            {
                DateTime startDate = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);

                var lon = Convert.ToDouble(Longitude.Value);
                var lat = Convert.ToDouble(Latitude.Value);
                
                List<Models.PassControlVehicle> listInfo = _mapsFunctions.GetPassControlVehicles(Convert.ToInt16(Session["Level"]), Convert.ToInt16(Session["Id"]), Convert.ToDouble(distance.Value), startDate.ToServerTime(), endDate.ToServerTime(), lon, lat, eventOn, eventOff);
                if (listInfo != null)
                {
                    if (listInfo.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[14] {
                            new DataColumn("Id"),
                            new DataColumn("InDateTime"),
                            new DataColumn("OutDateTime"),
                            new DataColumn("PlateId"),
                            new DataColumn("VehicleName"),
                            new DataColumn("SubUnitName"),
                            new DataColumn("GroupName"),
                            new DataColumn("DriverName"),
                            new DataColumn("Dallas"),
                            new DataColumn("Latitude"),
                            new DataColumn("Longitude"),
                            new DataColumn("Dir"),
                            new DataColumn("EventOnOff"),
                            new DataColumn("ParentID"),
                        });
                        foreach (Models.PassControlVehicle x in listInfo)
                        {
                            string inDateTime = string.Empty;
                            string outDateTime = string.Empty;
                            if (x.InDateTime.Year == 1) { inDateTime = string.Empty; } else { inDateTime = x.InDateTimeClient.ToString("dd/MM/yyyy HH:mm:ss"); }
                            if (x.OutDateTime.Year == 1) { outDateTime = string.Empty; } else { outDateTime = x.OutDateTimeClient.ToString("dd/MM/yyyy HH:mm:ss"); }
                            dt.Rows.Add(
                                x.Id,
                                inDateTime,
                                outDateTime,
                                x.PlateId,
                                x.VehicleName,
                                x.CostCenterName,
                                x.CostCenterName,
                                x.DriverName,
                                ((!string.IsNullOrEmpty(x.Dallas))? x.Dallas : "-----"),
                                x.Latitude,
                                x.Longitude,
                                x.Dir,
                                x.EventOnOff,
                                x.ParentID);
                        }

                        // This is for external data --> Extract from iframe
                        PCReportData.Value = _mapsFunctions.DataTableToJSON(dt);
                    }
                }                
                ScriptManager.RegisterStartupScript(this, GetType(), "returnResult", "returnResult('" + PCReportData.Value + "');", true);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnlResult.Visible = false;
            pnlMapPassControl.Visible = true;

            if (layerMap.Value == "Coinca")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                //panToolGoogle.Visible = false;
                //centerToolGoogle.Visible = false;
                //polygonToolGoogle.Visible = false;
                //distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                //panTool.Visible = true;
                //centerTool.Visible = true;
                polygonTool.Visible = true;
                distanceTool.Visible = true;
            }
            else if (layerMap.Value == "OSM")
            {
                pnlGoogle.Visible = false;
                pnlCoinca.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //CoincaMap.Visible = false;
                //GoogleMap.Visible = true;
                //ZoomInToolGoogle.Visible = false;
                //zoomOutToolGoogle.Visible = false;
                //panToolGoogle.Visible = false;
                //centerToolGoogle.Visible = false;
                //polygonToolGoogle.Visible = false;
                //distanceToolGoogle.Visible = false;
                //ZoomInTool.Visible = true;
                //zoomOutTool.Visible = true;
                //panTool.Visible = true;
                //centerTool.Visible = true;
                polygonTool.Visible = true;
                distanceTool.Visible = true;
            }
            else //if (layerMap.Value == "GoogleRoad")
            {
                pnlGoogle.Visible = true;
                pnlCoinca.Visible = false;
                //GoogleMap.Visible = false;
                //CoincaMap.Visible = true;
                //GoogleMap.Visible = false;
                //ZoomInTool.Visible = false;
                //zoomOutTool.Visible = false;
                //panTool.Visible = false;
                //centerTool.Visible = false;
                polygonTool.Visible = false;
                distanceTool.Visible = false;
                //CoincaMap.Visible = true;
                //ZoomInToolGoogle.Visible = true;
                //zoomOutToolGoogle.Visible = true;
                //panToolGoogle.Visible = true;
                //centerToolGoogle.Visible = true;
                //polygonToolGoogle.Visible = true;
                //distanceToolGoogle.Visible = true;
            }
        }

        protected void btnDirectionsSearch_Click(object sender, EventArgs e)
        {
            List<Models.SearchResult> listPlaces = (List<Models.SearchResult>)Session["listPlaces"];
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            List<Models.SearchResult> listPoints = (List<Models.SearchResult>)Session["listPoints"];
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }
            pointsSearchTable.Visible = false;
            directionsTable.Visible = true;
            //btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(Int32.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            //btnPointsSearch.BackColor = System.Drawing.Color.FromArgb(Int32.Parse("428BCA", NumberStyles.HexNumber)); //"#132C59";
        }

        protected void btnPointsSearch_Click(object sender, EventArgs e)
        {
            List<Models.SearchResult> listPlaces = (List<Models.SearchResult>)Session["listPlaces"];
            foreach (var x in listPlaces)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "direction");
            }
            List<Models.SearchResult> listPoints = (List<Models.SearchResult>)Session["listPoints"];
            foreach (var x in listPoints)
            {
                Tr(x.description, "'" + x.lat + "'", "'" + x.lon + "'", "point");
            }
            directionsTable.Visible = false;
            pointsSearchTable.Visible = true;
            //btnPointsSearch.BackColor = System.Drawing.Color.FromArgb(Int32.Parse("132C59", NumberStyles.HexNumber)); //"#132C59";
            //btnDirectionsSearch.BackColor = System.Drawing.Color.FromArgb(Int32.Parse("428BCA", NumberStyles.HexNumber)); //"#132C59";
        }

        protected void chkSatellite_CheckedChanged(object sender, EventArgs e)
        {
            if (layerMap.Value != "GoogleRoad")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleRoad');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "changeMap", "changeMap('GoogleSatellite');", true);
            }
        }

        protected void btnResize_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt16(width.Value) >= 0 && Convert.ToInt16(height.Value) > 0)
            {
                pnlGoogle.Width = Convert.ToInt16(width.Value);
                pnlGoogle.Height = Convert.ToInt16(height.Value);
                pnlCoinca.Width = Convert.ToInt16(width.Value);
                pnlCoinca.Height = Convert.ToInt16(height.Value);
                mapGoogle.Width = Convert.ToInt16(width.Value);
                mapGoogle.Height = Convert.ToInt16(height.Value);
                mapCoinca.Width = Convert.ToInt16(width.Value);
                mapCoinca.Height = Convert.ToInt16(height.Value);
                ContentSearchId.Style.Add("width", width.Value + "px");     
            }
        }

    }
}


