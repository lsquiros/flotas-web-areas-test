﻿<%@ Page ValidateRequest="false" Title="" Language="C#" AutoEventWireup="true" CodeBehind="Routes.aspx.cs" Inherits="ECO_Maps.Routes" EnableEventValidation="false" %>

<%@ Register TagPrefix="aspmap" Namespace="AspMap.Web" Assembly="AspMapNET" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>Rutas</title>
    <%: Styles.Render("~/bundles/cssRoutes") %>

<style>
    .buttonBox {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

</style>
</head>

<body>
    <form id="RouteForm" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts></Scripts>
        </asp:ScriptManager>
        <div id="body">
            <section class="content-wrapper main-content clear-fix">
                <div id="MainContent">
                    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="loader" CssClass="loaderRoute" runat="server">
                                <center>
                                    <img src="Content/Images/loader.GIF" class="image-loader" />
                                </center>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="layerMap" runat="server" Value="false" />
                            <asp:HiddenField ID="pageLoaded" runat="server" Value="false" />
                            <asp:HiddenField ID="width" runat="server" Value="0" />
                            <asp:HiddenField ID="height" runat="server" Value="0" />
                            <asp:HiddenField ID="customerId" runat="server" Value="false" />

                            <div id="divSearchResult"> 
                                    <asp:Table Visible="false" ID="directionsTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                    <asp:Table Visible="false" ID="CommercesTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                                    <asp:Table Visible="false" ID="pointsSearchTable" runat="server" GridLines="Both" CellSpacing="10" CellPadding="20" CssClass="directionsTable"></asp:Table>
                            </div> 
                            <div class="routes-map">
                                <div class="div100">
                                    <div id="ContentSearchId" class="ContentSearch col-sm-2" runat="server">
                                        <div class="FilterSearch">
                                            <div class="LabelSearch">
                                                <strong>
                                                    <asp:Label ID="lblLevel1" class="control-label" runat="server" Text=""></asp:Label>
                                                </strong>
                                            </div>
                                            <div class="ControlSearch">
                                                <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="provinciaList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="provinciaList_SelectedIndexChanged"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="FilterSearch">
                                            <div class="LabelSearch">
                                                <strong>
                                                    <asp:Label ID="lblLevel2" class="control-label" runat="server" Text=""></asp:Label>
                                                </strong>
                                            </div>
                                            <div class="ControlSearch text-right">
                                                <asp:DropDownList class="select2-container form-control select2-container-active select2-dropdown-open" ID="cantonesList" runat="server" AutoPostBack="false"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="BtnSearch">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtSearch" runat="server" name="key" type="text" class="form-control" placeholder="Buscar..."></asp:TextBox>
                                                <span class="input-group-btn">
                                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancelar" class="btn btn-default" OnClick="btnCancel_Click" title="Cancelar">
                                                         <i class="glyphicon glyphicon-remove"></i>
                                                    </asp:LinkButton>
                                                     <asp:LinkButton ID="btnSearch" runat="server" Text="Buscar" class="btn btn-primary" OnClick="btnSearch_Click" title="Buscar">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ContentMap">
                                        <asp:Panel ID="pnlCoinca" runat="server" Visible="true">
                                            <aspmap:Map ID="mapCoinca" EnableSession="true" runat="server" OnMarkerClick="mapCoinca_MarkerClick" FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnPointTool="mapCoinca_PointTool"></aspmap:Map>

                                            <div class="ColorListText" style="margin-left: 5px; width: 240px;">
                                                © Colaboradores de <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>
                                            </div>

                                        </asp:Panel>
                                        <asp:Panel ID="pnlGoogle" runat="server" Visible="false">
                                            <aspmap:Map ID="mapGoogle" Visible="false" EnableSession="true" runat="server"  FontQuality="ClearType" SmoothingMode="AntiAlias" ImageFormat="Gif" BackColor="#E6E6FA" Width="725px" Height="556px" OnPointTool="mapGoogle_PointTool"></aspmap:Map>
                                            <asp:Panel ID="pnlSatellite" runat="server" CssClass="pnlSatellite">
                                                <asp:CheckBox ID="chkSatellite" runat="server" Text="Vista Satelital" AutoPostBack="true" OnCheckedChanged="chkSatellite_CheckedChanged" />
                                            </asp:Panel>
                                        </asp:Panel>
                                        <style>
                                            /*Estilo para los botones de zoom automatico*/
                                            .imageZoom {
                                                margin-left: -7px;
                                                margin-top: -2px;
                                                height: 53px;
                                                width: 53px;
                                                border: 1px outset #ccc !important;
                                            }
                                        </style>
                                        <asp:Panel ID="mapMenuControls" runat="server" CssClass="mapMenu-controls" Visible="true">
                                            <div class="LeftMenu">
                                                <center>
                                                    <div id="mapMenu" class="btn-group-vertical" onclick="fncMenuRefresh();">
                                                        <%--<asp:ImageButton ID="GoogleMap" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Google.png" OnClientClick="changeMap('OSM')" ></asp:ImageButton>
                                                        <aspmap:MapToolButton id="zoomInTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapCoinca" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="zoomOutTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapCoinca" MapTool="ZoomOut"  ></aspmap:MapToolButton>--%>
                                                        <aspmap:MapToolButton id="panTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapCoinca" MapTool="Pan" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="centerTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapCoinca" MapTool="Center" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="pointTool" Visible="true" class="mapMenu" runat="server" ToolTip="Point Tool" ImageUrl="~/TOOLS/Custom/Pin.png" Map="mapCoinca" MapTool="Point" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton ID="distanceTool" Visible="true" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapCoinca" MapTool="Distance" ToolTip="Measure Distance" />
                                                        <button onclick="zoomIn()" type="button" class="mapMenu" style="margin-top: 4px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomIn.png" /></button>
                                                        <button onclick="zoomOut()" type="button" class="mapMenu" style="margin-top: 5px; height: 53px; border: 1px outset #ccc;"> <img class="imageZoom" src="TOOLS/Custom/ZoomOut.png" /></button>
                                                        
                                                        <%--<asp:ImageButton ID="CoincaMap" Visible="false" runat="server" class="mapMenu" ImageUrl="~/TOOLS/Custom/Coinca.png" OnClientClick="changeMap('Coinca')" ></asp:ImageButton>
                                                        <aspmap:MapToolButton id="zoomInToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomIn.png" Map="mapGoogle" ToolTip="Zoom In" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="zoomOutToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/ZoomOut.png" ToolTip="Zoom Out" Map="mapGoogle" MapTool="ZoomOut"  ></aspmap:MapToolButton>--%>
                                                        <aspmap:MapToolButton id="panToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Pan.png" ToolTip="Pan" Map="mapGoogle" MapTool="Pan" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="centerToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Center.png" ToolTip="Center" Map="mapGoogle" MapTool="Center" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton id="pointToolGoogle" Visible="false" class="mapMenu" runat="server" ToolTip="Point Tool" ImageUrl="~/TOOLS/Custom/Pin.png" Map="mapGoogle" MapTool="Point" ></aspmap:MapToolButton>
                                                        <aspmap:MapToolButton ID="distanceToolGoogle" Visible="false" class="mapMenu" runat="server" ImageUrl="~/TOOLS/Custom/Measuring.png" Map="mapGoogle" MapTool="Distance" ToolTip="Measure Distance" />
                                                    </div>                                                    
                                                </center>
                                            </div>
                                        </asp:Panel>
                                        <div class="mapMenu-controls">
                                            <asp:LinkButton ID="btnHideMapMenu" runat="server" Text="Ocultar Menu" class="btn btn-primary btnHide-mapMenu-controls" title="Ocultar Menu" OnClick="btnHideMapMenu_Click" Visible="true">
                                                 <i class="glyphicon glyphicon-chevron-left"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnShowMapMenu" runat="server" Text="Mostrar Menu" class="btn btn-primary btnShow-mapMenu-controls" title="Mostrar Menu" OnClick="btnShowMapMenu_Click" Visible="false">
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                            <div style="visibility: hidden;">
                                <asp:Button ID="btnResize" runat="server" Text="Button" OnClick="btnResize_Click" />
                                <asp:Button ID="btnRefresh" runat="server" Text="Button" OnClick="btnRefresh_Click" />
                                <asp:TextBox ID="txtData" runat="server" Text="" />
                                <asp:TextBox ID="PointIDNewCommerce" runat="server"  Text="" /> 
                                <asp:Button ID="btnAddCommerce" runat="server" Text="Button" OnClick="btnAddCommerce_Click" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="modal fade" id="confirmModal" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-md">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Confirmar Creación Comercio</h4>
                                </div>
                                <div class="modal-body">
                                    <p>¿Esta seguro que desea crear un comercio con el nombre: ?</p>
                                </div>

                                <div class="modal-footer">
                                    <div class="col-sm-2">
                                        <img id="processing" src="~/Content/Images/processing.gif" class="hide" />
                                    </div>
                                    <div>
                                        <button type="button" data-btnmodal="true" class="btn btn-default" data-dismiss="modal"><i class="mdi mdi-close"></i>&nbsp;Cancelar</button>
                                        <button id="btnaplicar" type="button" data-btnmodal="true" class="btn btn-primary" onclick="AddCommerceConfirm()"><i class="mdi mdi-check"></i>&nbsp;Aceptar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <%: Scripts.Render("~/bundles/jquery") %>
                    <script>
                        $(function () { resize(); });
                        $(window).resize(function () { resize(); });
                        var map = AspMap.getMap('<%=mapCoinca.ClientID%>');
                        function zoomIn() { map.zoomIn(); }
                        function zoomOut() { map.zoomOut(); }

                        function resize() {
                            var width = $(window).width() - 20;
                            var height = $(window).height() - 105;
                            $('#<% =width.ClientID %>').attr('value', width);
                            $('#<% =height.ClientID %>').attr('value', height);
                            $("#ContentSearch").width(width);
                            $('#<%=btnResize.ClientID %>').click();
                        }

                        function selectRow(index) {
                            
                            $('.grid-row').removeClass('active');
                            $("#pointsGrid").find("[order= " + index + "]").addClass('active');
                        }

                        function AddCommerceConfirm()
                        {  
                            $('#<%=btnAddCommerce.ClientID %>').click();

                            $('#confirmModal').modal('hide');
                            $('#<%=btnResize.ClientID %>').click();
                        }
                    
                        function AddCommerceJs(pointId)
                        {
                            $('#<%=PointIDNewCommerce.ClientID %>').val(pointId);
                            
                            $('#confirmModal').modal('show');
                        }

                        function returnResult(serialize) {
                            parent.postMessage(serialize, "*");
                        }

                        function updateDistance(distance) {
                            parent.postMessage(distance, "*");
                        }

                        window.addEventListener("message", function (event) {
                            var data = event.data;

                            document.getElementById('<%=txtData.ClientID %>').value = data;
                            $('#<%=btnRefresh.ClientID %>').click();
                        });

                   
 
                    </script>
                </div>
            </section>
        </div>
    </form>
</body>
</html>
