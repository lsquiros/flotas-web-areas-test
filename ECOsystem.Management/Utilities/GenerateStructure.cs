﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ECOsystem.Management.Business;

namespace ECOsystem.Management.Utilities
{
    public class GenerateStructure : IDisposable
    {
        public object GenerateLogin(int? userId, string decryptedNameUser, string decryptedEmailUser, string decryptedNameCustumer, int transmissionInterval, int locationInterval)
        {
                return new
                {
                    id = userId,
                    name = decryptedNameUser == null ? "" : decryptedNameUser,
                    email = decryptedEmailUser == null ? "" : decryptedEmailUser,
                    customerName = decryptedNameCustumer == null ? "" : decryptedNameCustumer,
                    config = new
                    {
                        transmissionInterval = transmissionInterval,
                        locationInterval = locationInterval
                    }
                };
        }

        public object GenerateEvent(int customerId)
        {
            using (var bus = new EventBusiness())
            {
                var events = bus.EventRetrieve(customerId).Select(x => new
                {
                    id = x.Id,
                    tracking = x.Tracking,
                    commerceRequired = x.CommerceRequired,
                    lapse = x.Lapse,
                    parent = x.Parent,
                    init = new
                    {
                        surveyId = x.Init.SurveyId,
                        eventDetailId = x.Init.Id,
                        name = x.Init.Name == null ? "" : x.Init.Name,
                        color = x.Init.Color == null ? "" : string.Format("#{0}", x.Init.Color),
                        icon = x.Init.Icon
                    },
                    finish = new
                    {
                        surveyId = x.Finish.SurveyId,
                        eventDetailId = x.Finish.Id,
                        name = x.Finish.Name == null ? "" : x.Finish.Name,
                        color = x.Finish.Color == null ? "" : string.Format("#{0}", x.Finish.Color),
                        icon = x.Finish.Icon
                    }
                }).ToList();

                return ECOsystem.Miscellaneous.Miscellaneous.JsonSerializeIgnoreNulls(events);
            }
        }

        public object GenerateSurvey(int? id, int customerId, int? userId)
        {
            using (var bus = new SurveyBusiness())
            {
                var survey = (dynamic)null;
                if (id == null)
                {
                    survey = bus.SurveyRetrieve(id, customerId, userId).Select(x => new
                    {
                        id = x.Id,
                        name = x.Name == null ? "" : x.Name,
                        description = x.Description == null ? "" : x.Description,
                        questions = x.Questions.Select(q => new
                        {
                            id = q.Id,
                            title = q.Text == null ? "" : q.Text,
                            help = q.Description == null ? "" : q.Description,
                            type = q.TypeId,
                            answers = q.Options.Select(o => new
                            {
                                id = o.Id,
                                text = o.Text == null ? "" : o.Text,
                                jump = o.NextQuestionId
                            })
                        })
                    });
                }
                else
                {
                    survey = bus.SurveyRetrieve(id, customerId, userId).Select(x => new
                    {
                        id = x.Id,
                        name = x.Name == null ? "" : x.Name,
                        description = x.Description == null ? "" : x.Description,
                        questions = x.Questions.Select(q => new
                        {
                            id = q.Id,
                            title = q.Text == null ? "" : q.Text,
                            help = q.Description == null ? "" : q.Description,
                            type = q.TypeId,
                            answers = q.Options.Select(o => new
                            {
                                id = o.Id,
                                text = o.Text == null ? "" : o.Text,
                                jump = o.NextQuestionId
                            })
                        })
                    }).FirstOrDefault();
                }

                return ECOsystem.Miscellaneous.Miscellaneous.JsonSerializeIgnoreNulls(survey);
            }
        }

        public object GenerateCommerce(double lat, double lon, int customerId)
        {
            using (var bus = new CommerceBusiness())
            {
                var commerces = bus.CommerceRetrieve(lat, lon, customerId).Select(x => new
                {
                    id = x.Id,
                    name = x.Name == null ? "" : x.Name,
                    description = x.Description == null ? "" : x.Description,
                    latitude = x.Latitude,
                    longitude = x.Longitude
                }).ToList();

                return ECOsystem.Miscellaneous.Miscellaneous.JsonSerializeIgnoreNulls(commerces);
            }
        }

        public object GenerateCommerceType()
        {

            using (var bus = new CommerceBusiness())
            {
                var commerceTypes = bus.CommerceTypeRetrive().Select(x => new
                {
                    id = x.Id,
                    name = x.Name == null ? "" : x.Name,
                    description = x.Description == null ? "" : x.Description,
                    icon = x.Icon,
                    requiredData = x.RequiredData
                });

                return ECOsystem.Miscellaneous.Miscellaneous.JsonSerializeIgnoreNulls(commerceTypes);
            }
        }

        public object GenerateAgenda(DateTime? date, int? UserId)
        {
            using (var bus = new AgendaBusiness())
            {
                if (date == null )
                {
                    date = DateTime.Today;
                }
                var agenda = bus.AgendaRetrieve(date, UserId).Select(x => new
                {
                    id = x.CommerceId,
                    name = x.CommerceName == null ? "" : x.CommerceName,
                    address = x.Address == null ? "" : x.Address,
                    additional = x.DynamicColumns.Where(a => !string.IsNullOrEmpty(a.Value)).Select(c => new
                    {
                        title = c.Title,
                        value = c.Value
                    })
                });

                return ECOsystem.Miscellaneous.Miscellaneous.JsonSerializeIgnoreNulls(agenda);
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
