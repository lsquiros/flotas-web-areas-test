﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Question
    {
        public int? Id { get; set; }

        public int SurveyId { get; set; }

        public int QuestionTypeId { get; set; }

        public string Text { get; set; }

        public string Description { get; set; }

        public int? Order { get; set; }

        public string TypeId { get; set; }

        public string OptionsStr { get; set; }

        public List<Option> Options
        {
            get
            {                                                                                                                                                                               
                return string.IsNullOrEmpty(OptionsStr) ? new List<Option>() : Miscellaneous.Miscellaneous.GetXMLDeserialize<List<Option>>(OptionsStr, typeof(Option).Name);
            }
        }

        public bool Delete { get; set; }
    }
}
