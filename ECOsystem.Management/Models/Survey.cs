﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Survey
    {
        public int? Id { get; set; }

        public int CustomerId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
                
        public string QuestionsStr { get; set; }

        public List<Question> Questions
        {
            get { return string.IsNullOrEmpty(QuestionsStr) ? new List<Question>() : Miscellaneous.Miscellaneous.GetXMLDeserialize<List<Question>>(QuestionsStr, typeof(Question).Name); }
        }
    }
}
