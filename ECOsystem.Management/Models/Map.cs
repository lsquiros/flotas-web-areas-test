﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class ManagementMapsBase
    {
        public ManagementMapsBase()
        {
            Data = new ManagementMaps();
        }
        public ManagementMaps Data { get; set; }
    }

    public class ManagementMaps
    {
        public IEnumerable<Commerce> Commerces { get; set; }

        public IEnumerable<Agent> Agents { get; set; }
    }
}
