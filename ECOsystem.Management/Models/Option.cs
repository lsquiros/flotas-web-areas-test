﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Option
    {
        public int? Id { get; set; }

        public int QuestionId { get; set; }

        public string Text { get; set; }

        public int Order { get; set; }

        public int? NextQuestionId { get; set; }

        public bool Delete { get; set; }
    }
}
