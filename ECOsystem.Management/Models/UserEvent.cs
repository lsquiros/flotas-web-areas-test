﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class UserEvent
    {
        public int? Id { get; set; }

        public int EventDetailId { get; set; }

        public int EventTypeId { get; set; }

        public int UserId { get; set; }

        public int? CommerceId { get; set; }

        public Int64? TempCommerceId { get; set; }

        public int CustomerId { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public int BatteryLevel { get; set; }

        public EventIndicator Indicators { get; set; }

        public int Lapse { get; set; }

        public DateTime EventDateTime { get; set; }

        public UserAnswer Survey { get; set; }
    }
}
