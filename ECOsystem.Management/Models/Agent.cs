﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Agent
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? UserEventId { get; set; }

        public int EventTypeId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptName); } }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        #region GeopoliticalInformation        
        public string Address { get; set; }

        public string GeopoliticalInformation { get; set; }

        public string State { get { return string.IsNullOrEmpty(GeopoliticalInformation) ? string.Empty : GeopoliticalInformation.Split(',')[0]; } }

        public string County { get { return string.IsNullOrEmpty(GeopoliticalInformation) ? string.Empty : GeopoliticalInformation.Split(',')[1]; } }

        public string City { get { return string.IsNullOrEmpty(GeopoliticalInformation) ? string.Empty : GeopoliticalInformation.Split(',')[2]; } }

        public List<string> GeoNames { get; set; }

        public string GeoStateName { get { return  GeoNames != null && GeoNames.Count > 0 ? GeoNames[0] : "División geopolítica nivel 1"; } }

        public string GeoCountyNames { get { return GeoNames != null && GeoNames.Count > 1 ? GeoNames[1] : "División geopolítica nivel 2"; } }

        public string GeoCityNames { get { return GeoNames != null && GeoNames.Count > 2 ? GeoNames[2] : "División geopolítica nivel 3"; } }
        #endregion

        public string CommerceName { get; set; }

        public int Parent { get; set; }

        public string EventName { get; set; }

        public string EventKey { get; set; }

        public string EventDetailName { get; set; }

        public string Lapse { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime EventDate { get; set; }

        public decimal Distance { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string ReconstructionId { get; set; }

        public int Speed { get; set; }

        public int BatteryLevel { get; set; }

        public bool IsFinish { get; set; }

        public bool ShowLines { get; set; }

        public string RowColor { get; set; }

        public string AgentName { get; set; }

        public string CustomerName { get; set; }
    }

    public class AgentReconstructing
    {
        public int? UserId { get; set; }

        public string EncryptName { get; set; }

        public string DecryptName { get { return TripleDesEncryption.Decrypt(EncryptName); } }

        public IEnumerable<Agent> FragmentList { get; set; }
    }
}
