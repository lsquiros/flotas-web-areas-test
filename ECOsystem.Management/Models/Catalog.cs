﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Catalog
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class OrderItems
    {
        public int Order { get; set; }

        public int Item { get; set; }
    }
}
