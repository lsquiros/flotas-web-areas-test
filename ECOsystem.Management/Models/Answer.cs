﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Answer
    {
        public int? Id { get; set; }

        public int UserEventId { get; set; }

        public int SurveyId { get; set; }

        public int QuestionId { get; set; }

        public int OptionId { get; set; }

        public int[] Answers { get; set; }

        public string Data { get; set; }

        public bool Delete { get; set; }
    }

    public class UserAnswer
    {
        public int SurveyId { get; set; }

        public int EventDetailId { get; set; }

        public int CommerceId { get; set; }

        public Int64? TempCommerceId { get; set; }

        public List<Answer> Questions { get; set; }

        public string QuestionsXML
        {
            get { return Miscellaneous.Miscellaneous.GetXML(Questions, string.Empty); }
        }
    }
}
