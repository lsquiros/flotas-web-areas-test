﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class EventIndicator
    {
        public double? Accuracy { get; set; }

        public double? Heading { get; set; }

        public double? Speed { get; set; }

        public double? Altitude { get; set; }
    }
}
