﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Agenda
    {
        public int? Id { get; set; }

        public int UserId { get; set; }

        public int? UserEventId { get; set; }

        public int CommerceId { get; set; }

        public string CommerceName { get; set; }
        
        public DateTime? DateOfAgenda { get; set; }

        public int? Status { get; set; }

        public int? Order { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string Code { get; set; }

        public int? LoggedUserId { get; set; }

        public string UserName { get; set; }

        public string DecryptedName
        {
            get { return ECOsystem.TripleDesEncryption.Decrypt(UserName); }
        }

        public string DynamicColumnsStr { get; set; }

        public List<AditionalColumns> DynamicColumns
        {
            get
            {
                return string.IsNullOrEmpty(DynamicColumnsStr) ? new List<AditionalColumns>() : Miscellaneous.Miscellaneous.GetXMLDeserialize<List<AditionalColumns>>(DynamicColumnsStr, typeof(AditionalColumns).Name);
            }
        }
    }

    public class AditionalColumns
    {
        public int Id { get; set; }
        public int AgendaId { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }

    }
}
