﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class Event
    {
        public int? Id { get; set; }

        public int EventDetailInitId  { get; set; }

        public int EventDetailFinishId { get; set; }

        public bool Tracking { get; set; }

        public int Lapse { get; set; }

        public bool CommerceRequired { get; set; }

        public int? Parent { get; set; }

        public bool Active { get; set; }

        public bool Delete { get; set; }

        public string InitStr { get; set; }

        [Required(ErrorMessage = "El Tipo de Evento es requerido")]
        public int? EventTypeId { get; set; }

        public EventDetail _Init;
        public EventDetail Init
        {
            get { return string.IsNullOrEmpty(InitStr) ? new EventDetail() : Miscellaneous.Miscellaneous.GetXMLDeserialize<List<EventDetail>>(InitStr, typeof(EventDetail).Name).FirstOrDefault(); }
            set { _Init = value; }
        }

        public string FinishStr { get; set; }

        public EventDetail _Finish;
        public EventDetail Finish
        {
            get { return string.IsNullOrEmpty(FinishStr) ? new EventDetail() : Miscellaneous.Miscellaneous.GetXMLDeserialize<List<EventDetail>>(FinishStr, typeof(EventDetail).Name).FirstOrDefault(); }
            set { _Finish = value; }
        }
    }
}
