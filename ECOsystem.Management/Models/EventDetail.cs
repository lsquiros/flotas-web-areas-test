﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Models
{
    public class EventDetail
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "El Formulario es requerido")]
        public int? SurveyId { get; set; }
                
        public string SurveyName { get; set; }

        public int? EventTypeId { get; set; }

        public string EventTypeName { get; set; }

        public int? IconId { get; set; }

        public string Icon { get; set; }
                                                               
        [Required(ErrorMessage = "El Nombre del Evento es requerido")]
        public string Name { get; set; }

        public string Color { get; set; }

        public bool Active { get; set; }

        public bool Delete { get; set; }
    }
}
