﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECOsystem.Management.Models
{ 

    public class Parameters
    {
        public int? LostMinutes { get; set; }

        public int? MovingMinutes { get; set; }

        public int? ClientMinutes { get; set; }

        public int? TopMargin { get; set; }

        public int? LowMargin { get; set; }

        public int? TopPercent { get; set; }

        public int? LowPercent { get; set; }

        public int? MiddlePercent { get; set; }

        public int? GeofenceDistance { get; set; }
        
        public int? DataSync { get; set; }

        public string MovementName { get; set; }

        public int? ReconstructionType { get; set; }

        public int? TimeBeginJourney { get; set; }

        public int? TimeFinishJourney { get; set; }

        public int? StopMinimumTime { get; set; }

        public int? StopMinimumDistance { get; set; }

        public int? StopMinimumSpeed { get; set; }

        public int? ManagementMinDistance { get; set; }
    }

    public class DynamicColumnsBase
    {
        public int? MaxId { get; set; }

        public string DynamicColumns { get; set; }

        public List<AdittionalAgendaColumn> ListColums { get; set; }
    }

    public class AdittionalAgendaColumn
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
