﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Business
{
    public class AgendaBusiness : IDisposable
    {
        public List<Agenda> AgendaRetrieve(DateTime? DateOfAgenda, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Agenda>("[Management].[Sp_Agenda_Retrieve]", new
                {
                    DateOfAgenda,
                    UserId
                }).ToList();
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
