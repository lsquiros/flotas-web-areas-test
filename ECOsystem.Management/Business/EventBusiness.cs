﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Business
{
    public class EventBusiness : IDisposable
    {
        public List<Event> EventRetrieve(int? CustomerId, int? Id = null)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Event>("[Management].[Sp_Event_Retrieve]",
                    new
                    {
                        Id,
                        CustomerId
                    }).ToList();
            }
        }

        public void EventAddOrEdit(Event model, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_Event_AddOrEdit]",
                    new
                    {
                        model.Id,
                        model.EventTypeId,
                        model.Lapse,
                        model.Tracking,
                        model.CommerceRequired,
                        model.Parent,
                        Init = Miscellaneous.Miscellaneous.GetXML(new List<EventDetail> { model._Init }, string.Empty),
                        Finish = Miscellaneous.Miscellaneous.GetXML(string.IsNullOrEmpty(model._Finish.Name) ? new List<EventDetail>() : new List<EventDetail> { model._Finish }, string.Empty),
                        CustomerId,
                        UserId
                    });
            }
        }

        public void EventDelete (int Id, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_Event_Delete]",
                    new
                    {
                        Id,
                        UserId
                    });
            }
        }

        #region UserEvent
        public void UserEventAddOrEdit(UserEvent model, string EventType, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                double? accuracy = new double();
                double? heading = new double();
                double? speed = new double();
                double? altitude = new double();
                int? surveyId = new int();
                string questionsXML = null;

                if (model.Indicators != null)
                {
                    accuracy = model.Indicators.Accuracy != null ? model.Indicators.Accuracy : 0;
                    heading = model.Indicators.Heading != null ? model.Indicators.Heading : 0;
                    speed = model.Indicators.Speed != null ? model.Indicators.Speed : 0;
                    altitude = model.Indicators.Altitude != null ? model.Indicators.Altitude : 0;
                }

                if(model.Survey != null)
                {
                    surveyId = model.Survey.SurveyId;
                    questionsXML = model.Survey.QuestionsXML;
                }

                dba.ExecuteNonQuery("[Management].[Sp_UserEvent_AddOrEdit]", new
                {
                    model.EventDetailId,
                    model.CommerceId,
                    model.TempCommerceId,
                    model.Latitude,
                    model.Longitude,
                    model.BatteryLevel,
                    accuracy,
                    heading,
                    speed,
                    altitude,
                    model.Lapse,
                    model.EventDateTime,
                    EventType,
                    CustomerId,
                    UserId,
                    surveyId,
                    questionsXML
                });
            }
        }

        public void UserEventDelete(int EventDetailId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_UserEvent_Delete]", new
                {
                    EventDetailId,
                    UserId
                });
            }
        }
        #endregion

        #region Reports
        public DataTable GetReportDownloadData(List<Event> list, string CustomerName)
        {
            using (var dt = new DataTable())
            {
                dt.Columns.Add("InitName");
                dt.Columns.Add("SurveyInit");
                dt.Columns.Add("FinishName");
                dt.Columns.Add("SurveyFinish");
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("EventType");

                foreach (var item in list)
                {
                    DataRow row = dt.NewRow();

                    row["InitName"] = item.Init.Name;
                    row["SurveyInit"] = item.Init.SurveyName;
                    row["FinishName"] = item.Finish.Name;
                    row["SurveyFinish"] = item.Finish.SurveyName;
                    row["CustomerName"] = CustomerName;
                    row["EventType"] = !string.IsNullOrEmpty(item.Init.EventTypeName) ? item.Init.EventTypeName.Split(' ')[1] : string.Empty;
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }
        #endregion
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
