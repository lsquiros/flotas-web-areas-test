﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Business
{
    public class CatalogBusiness : IDisposable
    {
        public List<Catalog> RetrieveCatalog(int Id, string Name)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Catalog>("[Management].[Sp_CatalogDetail_Retrieve]",
                    new
                    {
                        Id, 
                        Name
                    }).ToList(); 
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
