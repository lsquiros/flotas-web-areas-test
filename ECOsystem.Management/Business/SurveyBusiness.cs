﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Business
{
    public class SurveyBusiness : IDisposable
    {
        public List<Survey> SurveyRetrieve(int? Id, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Survey>("[Management].[Sp_Survey_Retrieve]", new
                {
                    Id,
                    CustomerId,
                    UserId
                }).ToList();
            }
        }

        public void SurveyAddOrEdit(Survey model, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_Survey_AddOrEdit]", new
                {
                    model.Id,
                    model.Name,
                    model.Description,
                    model.QuestionsStr,
                    CustomerId,
                    UserId
                });
            } 
        }

        public void SurveyDelete(int Id, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_Survey_Delete]", new
                {
                    Id,
                    UserId
                });
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
