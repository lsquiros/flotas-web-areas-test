﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Business
{
    public class CommerceBusiness : IDisposable
    {
        public List<CommerceType> CommerceTypeRetrive()
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<CommerceType>("[General].[Sp_CommerceType_Retrieve]", new { }).ToList();
            }
        }

        public List<Commerce> CommerceRetrieve(double Latitude, double Longitude, int CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteReader<Commerce>("[Management].[Sp_Commerce_Retrieve]", new
                {
                    Latitude, 
                    Longitude,
                    CustomerId
                }).ToList();
            }
        }

        public int CommerceAddOrEdit(Commerce model, int CustomerId, string UserName, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                return dba.ExecuteScalar<int>("[General].[Sp_Commerces_AddOrEdit]", new
                {
                    model.Id,
                    model.TempCommerceId,
                    model.Name,
                    model.Address, 
                    model.Latitude,
                    model.Longitude,
                    model.CommerceType, 
                    model.Code,
                    CustomerId,
                    UserId,
                    UserName
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
