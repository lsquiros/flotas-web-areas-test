﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Linq;


namespace ECOsystem.Management.Business
{
    
    public class ParametersBusiness : IDisposable
    {
        public Parameters ManagementParametersRetrieve(int? CustomerId)
        {
            using (var dba = new DataBaseAccess())
            {
                var ManagementParameters = dba.ExecuteReader<Parameters>("[Management].[Sp_ManagementParameters_Retrieve]", new
                {
                    CustomerId,
                }).ToList().FirstOrDefault();

                return ManagementParameters;
            }
        }

        public void ManagementParametersAddOrEdit(Parameters model, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_ManagementParameters_AddOrEdit]", new
                {
                    CustomerId,
                    model.DataSync,
                    model.MovementName,
                    model.ReconstructionType,
                    model.TopMargin,
                    model.LowMargin,
                    model.TopPercent,
                    model.LowPercent,
                    model.MiddlePercent,
                    model.ClientMinutes,
                    model.LostMinutes,
                    model.MovingMinutes,
                    model.TimeBeginJourney,
                    model.TimeFinishJourney,
                    model.StopMinimumDistance,
                    model.StopMinimumSpeed,
                    model.StopMinimumTime,
                    model.ManagementMinDistance,
                    UserId
                });
            }
        }        

        public void ManagementParametersGeofencesAddOrEdit(Parameters model, int? CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                dba.ExecuteNonQuery("[Management].[Sp_ManagementParametersGeofences_AddOrEdit]", new
                {
                    CustomerId,
                    model.GeofenceDistance,
                    UserId
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}