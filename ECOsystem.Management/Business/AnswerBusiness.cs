﻿using ECOsystem.DataAccess;
using ECOsystem.Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOsystem.Management.Business
{
    public class AnswerBusiness : IDisposable
    {
        public void AnswerAddOrEdit(UserAnswer model, int CustomerId, int? UserId)
        {
            using (var dba = new DataBaseAccess())
            {
                int? UserEventId = null;
                dba.ExecuteNonQuery("[Management].[Sp_Answer_AddOrEdit]", new
                {
                    UserEventId,
                    model.SurveyId, 
                    model.EventDetailId, 
                    model.CommerceId, 
                    model.TempCommerceId,
                    model.QuestionsXML,
                    CustomerId, 
                    UserId
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
